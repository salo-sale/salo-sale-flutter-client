# flutter_basic_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

#FireBase
```
Android: com.hashlink.salo.sale.client.flutter.android

OLD IOS: com.hashlink.flutter.basic.app.flutterBasicApp
NEW IOS: com.hashlink.salo.sale.client.flutter.ios

WEB: salo-sale-web

```

If need change DB (firebase project), just do this steps:
Android:
1. Go to android/key.properties and change code (file have comments for change).
2. Go to android/app/ and rename (discountik/salo-sale).google-services.json.
It`s all.

If do you wont regenerate new icons for app need write this in console

```
flutter packages pub run flutter_launcher_icons:main
```

### Custom flushbar modification
To have flushbar working correctly, the show function should be modified to:
```$xslt
Future<T> show(BuildContext context, {GlobalKey<NavigatorState> navigatorKey}) async {
    _flushbarRoute = route.showFlushbar<T>(
      context: context,
      flushbar: this,
    );
    if(navigatorKey != null){
      return await navigatorKey.currentState.push(_flushbarRoute);
    }
    return await Navigator.of(context, rootNavigator: false).push(_flushbarRoute);
  }
```