import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

const String domain = 'salo-sale.com';

const String protocol = "https:";
// const String host = "dev.salo-sale.com";
const String host = "salo-sale.com";

const String clientId = "3";
const String clientSecret = '257afb536932c2c53c4d4d379006c19876598a53';

const remoteConfig = 'https://config.salo-sale.com/remote-config.json';
const pong = 'https://config.salo-sale.com/pong.json';

const defaultPaths = {
  "login": "/auth/login",
  "login-by-social-network": "/auth/login-by-social-network",
  "registration": "/auth/registration",
  "confirm-email": "/auth/confirm-email",
  "logout": "/auth/logout",
  "refresh-token": "/auth/refresh-token",
  "agreements": "/auth/agreements",

  "forgot-password": "/auth/forgot-password",
  "check-password-reset-token": "/auth/check-password-reset-token?password_reset_token=",
  "reset-password": "/auth/reset-password",

  "app-data": "/client/api/v1/datas/app-data",
  "localities": "/client/api/v1/datas/localities",
  "languages": "/client/api/v1/datas/languages",
  "remote-config": "/client/api/v1/datas/remote-config",
  "listsOfTags": "/client/api/v1/datas/tags",
  "feedback": "/client/api/v1/datas/feedback",

  "loyalty-cards": "/client/api/v1/loyalty-cards",
  "loyalty-card": "/client/api/v1/loyalty-cards/",

  "recommended-offers": "/client/api/v1/recommended-offers",
  "clicked-recommended": "/client/api/v1/recommended-offers/", // {ID}

  "comments": "/client/api/v1/comments",
  "comment": "/client/api/v1/comments/",
  "comment-replies": "/client/api/v1/comment-replies",
  "comment-reply": "/client/api/v1/comment-replies/",

  "notifications": "/client/api/v1/notifications",
  "notification": "/client/api/v1/notifications/", // {ID}
  "read-notification": "/client/api/v1/notifications/", // {ID}

  "companies": "/client/api/v1/companies",
  "company": "/client/api/v1/companies/", // {ID}
  "company-like": "/client/api/v1/companies/", // {ID}
  "company-follower": "/client/api/v1/companies/", // {ID}

  "check-invitation-code": "/client/api/v1/users/check-invitation-code/", // {ID}
  "invitations": "/client/api/v1/users/", // {ID}
  "user": "/client/api/v1/users/", // {ID}
  "change-password": "/client/api/v1/users/change-password",
  "invitation": "/client/api/v1/users/invitation",
  "send-verification": "/client/api/v1/users/send-verification",

  "events": "/client/api/v1/events",
  "event": "/client/api/v1/events/", // {ID}

  "offers": "/client/api/v1/offers",
  "offer": "/client/api/v1/offers/", // {ID}
  "increment-view": "/client/api/v1/offers/", // {ID}
  "buy-offer": "/client/api/v1/offers/", // {ID}
  "take-ikoint": "/client/api/v1/offers/", // {ID}
  "like": "/client/api/v1/offers/", // {ID}

  "cashier": "/client/api/v1/cashiers",
  "cashier/points-of-sale": "/client/api/v1/cashiers/point-of-sale",
  "cashier/scanned-offer": "/client/api/v1/cashiers/scanned-offer",
  "": ""
};

class Data {

  static const String supportEmail = 'support@' + domain;
  static const String supportPhoneNumber = '+380505006065';

  static const int ikointCountViewOffer = 5;
  static const int startRegistrationIkoint = 50;
  static const int stepRegistrationIkoint = 10;
  static const int invitationCodeRegistrationIkoint = 50;
  static const int startTimerForTakeIkointForView = 5;

  static const double borderRadiusAdvertisingBanner = 5.0;
  static const double borderRadiusOfferCard = 5.0;

  static const String prefixRecommendedOffer = 'recommended_offer_';

  static const String nameOfApp = 'Salo-Sale';

  static IconData getIconByName(String name) {

    if (name.isEmpty) {

      return null;

    }

    switch(name) {

      case 'All':
        return FontAwesomeIcons.atom;
      case 'Dice':
        return FontAwesomeIcons.dice;
      case 'Tech':
        return FontAwesomeIcons.robot;
      case 'Food':
        return FontAwesomeIcons.pizzaSlice;
      case 'Child':
        return FontAwesomeIcons.babyCarriage;
      case 'Sport':
        return FontAwesomeIcons.futbol;
      case 'Clothing':
        return FontAwesomeIcons.tshirt;
      case 'Beauty':
        return FontAwesomeIcons.cut;
      case 'Game':
        return FontAwesomeIcons.gamepad;
      case 'Medicine':
        return FontAwesomeIcons.clinicMedical;
      case 'Products':
        return FontAwesomeIcons.dolly;
      case 'Tourism':
        return FontAwesomeIcons.route;
      case 'Services':
        return FontAwesomeIcons.tools;
      case 'Foodstuff':
        return FontAwesomeIcons.appleAlt;
      case 'Education':
        return FontAwesomeIcons.graduationCap;
      case 'Building-materials':
        return FontAwesomeIcons.wrench;
      case 'Hotel':
        return FontAwesomeIcons.hotel;
      case 'Culture':
        return FontAwesomeIcons.theaterMasks;
      case 'Conference':
        return FontAwesomeIcons.users;
      case 'Concert':
        return FontAwesomeIcons.guitar;
      case 'Festival':
        return FontAwesomeIcons.campground;
      case 'Excursion':
        return FontAwesomeIcons.route;
      case 'Cinema':
        return FontAwesomeIcons.film;
      case 'Club':
        return FontAwesomeIcons.compactDisc;
      case 'Culture':
        return FontAwesomeIcons.ellipsisH;
      case 'Children`s':
        return FontAwesomeIcons.child;
      case 'New-year':
        return FontAwesomeIcons.glassCheers;
      case 'Trainings':
        return FontAwesomeIcons.running;
      case 'Dancing':
        return FontAwesomeIcons.laughBeam;
      case 'Humor':
        return FontAwesomeIcons.grinBeam;
      case 'Exhibition':
        return FontAwesomeIcons.images;
      case 'Master-class':
        return FontAwesomeIcons.userGraduate;
      case 'Other':
        return FontAwesomeIcons.ellipsisH;
      case 'Insurance':
        return FontAwesomeIcons.carCrash;
      case 'Online':
        return FontAwesomeIcons.globe;
      case 'Laugh':
        return FontAwesomeIcons.laugh;
      default:
        return FontAwesomeIcons.home;

    }

  }

  static const String startDefaultSelectedTag = 'All';

  static const int startDefaultIndexPage = 0;

}