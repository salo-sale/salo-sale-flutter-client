
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/api/event.api.dart';
import 'package:SaloSale/shared/api/notification.api.dart';
import 'package:SaloSale/shared/api/offer.api.dart';
import 'package:SaloSale/shared/api/recommendation.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/services/firebase/messaging.service.dart';
import 'package:SaloSale/theme.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:fluro_fork/fluro_fork.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n_delegate.dart';
import 'package:flutter_localizations/flutter_localizations.dart';


Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations(

      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]

  );

  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);

  runApp(MyApp());

}


class MyApp extends StatefulWidget {

  @override
  MyAppState createState() => MyAppState();

}

class MyAppState extends State<MyApp> {

  @override
  void initState() {

    super.initState();

    defineRoutes();

    initApp();

    AppService.systemSteps.listen((value) {

      if (value == SystemStepsTypeEnum.REFRESH_APP) {

        // REFRESH APP
        print('NEED DO REFRESH APP');

        setState(() {

          FirebaseMessagingService.initialized = false;
          AppService.initialized = false;
          AuthService.initialized = false;
          initApp();

        });

      }

    });

  }

  @override
  void dispose() {

    AppService.dispose();
    AuthService.dispose();

    OfferApi.dispose();
    CompanyApi.dispose();
    EventApi.dispose();
    NotificationApi.dispose();
    RecommendationApi.dispose();

    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: lightCustomTheme,
//        darkTheme: darkCustomTheme,
      localizationsDelegates: [
        FlutterI18nDelegate(
          useCountryCode: false,
          fallbackFile: 'uk',
        ),
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [
        const Locale("uk"),
        const Locale("en"),
        const Locale("ru")
      ],
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: AppService.analytics),
      ],
      home: WillPopScope(
        onWillPop: () async {
          return !await pageNavigatorKey.currentState.maybePop();
        },
        child: Navigator(
          key: flushbarNavigatorKey,
          onGenerateRoute: (_) => MaterialPageRoute(
            builder: (_) => Navigator(
              key: pageNavigatorKey,
              initialRoute: "home",
              onGenerateRoute: Router.appRouter.generator,
            ),),
        ),
      ),
    );


  }

  void initApp() {

    FirebaseMessagingService.init();

    AppService.initUniLinks(); // init universal links

    AppService.initAppBox().then((_) {

      AppService.init().then((__) {

        AuthService.init().then((___) {

          AppService.systemSteps.add(SystemStepsTypeEnum.INITIALIZED);

        });

      });

    });

  }

}
