import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/pages/auth/login.auth.page.dart';
import 'package:SaloSale/ui/pages/auth/main.auth.page.dart';
import 'package:SaloSale/ui/pages/auth/registration.auth.page.dart';
import 'package:SaloSale/ui/pages/auth/reset-password.auth.page.dart';
import 'package:SaloSale/ui/pages/comment/detail.comment.page.dart';
import 'package:SaloSale/ui/pages/comment/list.comment.page.dart';
import 'package:SaloSale/ui/pages/company/detail.company.page.dart';
import 'package:SaloSale/ui/pages/company/map.page.dart';
import 'package:SaloSale/ui/pages/ikoint-history/list.page.dart';
import 'package:SaloSale/ui/pages/likes/list.page.dart';
import 'package:SaloSale/ui/pages/menu/about-us.page.dart';
import 'package:SaloSale/ui/pages/menu/cashier.page.dart';
import 'package:SaloSale/ui/pages/menu/change-language.page.dart';
import 'package:SaloSale/ui/pages/menu/change-locality.page.dart';
import 'package:SaloSale/ui/pages/menu/change-password.page.dart';
import 'package:SaloSale/ui/pages/menu/edit.profile.page.dart';
import 'package:SaloSale/ui/pages/menu/feedback.page.dart';
import 'package:SaloSale/ui/pages/menu/follows.page.dart';
import 'package:SaloSale/ui/pages/menu/friends.page.dart';
import 'package:SaloSale/ui/pages/menu/help.page.dart';
import 'package:SaloSale/ui/pages/menu/loyalty-card/list.loyalty-card.page.dart';
import 'package:SaloSale/ui/pages/notification/detail.notification.page.dart';
import 'package:SaloSale/ui/pages/notification/list.notification.page.dart';
import 'package:SaloSale/ui/pages/offer/detail.offer.page.dart';
import 'package:SaloSale/ui/pages/offer/invoice.offer.page.dart';
import 'package:SaloSale/ui/screens/home.screen.dart';
import 'package:fluro_fork/fluro_fork.dart';
import 'package:flutter/widgets.dart';

final GlobalKey<NavigatorState> pageNavigatorKey = GlobalKey<NavigatorState>(debugLabel: 'Page Navigator');
final GlobalKey<NavigatorState> flushbarNavigatorKey = GlobalKey<NavigatorState>(debugLabel: 'Flushbar Navigator');


void defineRoutes() {
  Router.appRouter.define("home",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              HomeScreen()),
      transitionType: TransitionType.native);

  Router.appRouter.define("offer/invoice",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              InvoiceOfferPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("notifications",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              NotificationListPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("profile/edit",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              EditProfileMenuHomePage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("about",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              AboutMenuHomePage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("feedback",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              FeedbackMenuHomePage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("help",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              HelpMenuHomePage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("likes",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              LikesPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("loyalty-cards",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              LoyaltyCardListPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("friends",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              FriendsMenuHomePage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("follows",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              FollowsMenuHomePage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("cashier",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              CashierMenuHomePage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("map",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              MapPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("ikoint-history",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              IkointHistoryPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("change-locality",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              ChangeLocalityMenuPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("change-language",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              ChangeLanguageMenuPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("auth/main",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              MainAuthPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("auth/login",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              LoginAuthPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("auth/registration",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              RegistrationAuthPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("auth/reset-password",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              ResetPasswordAuthPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("change-password",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              ChangePasswordPage()),
      transitionType: TransitionType.native);

  Router.appRouter.define("change-password/:code",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              ChangePasswordPage(code: params["code"][0])),
      transitionType: TransitionType.native);

  Router.appRouter.define("confirm-email/:verificationToken",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              HomeScreen(verificationToken: params["verificationToken"][0])),
      transitionType: TransitionType.native);

  Router.appRouter.define("offers/:id",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              DetailOfferPage(offerId: Tools.universalParse<int>(params["id"][0]))),
      transitionType: TransitionType.native);

  Router.appRouter.define("companies/:id",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              DetailCompanyPage(companyId: Tools.universalParse<int>(params["id"][0]))),
      transitionType: TransitionType.native);

  Router.appRouter.define("companies/:companyId/comments",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              ListCommentPage(companyId: Tools.universalParse<int>(params["companyId"][0]))),
      transitionType: TransitionType.native);

  Router.appRouter.define("comments/:id",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              DetailCommentPage(commentId: Tools.universalParse<int>(params["id"][0]))),
      transitionType: TransitionType.native);

  Router.appRouter.define("notifications/:id",
      handler: Handler(
          handlerFunc: (BuildContext context, Map<String, dynamic> params) =>
              DetailNotificationPage(notificationId: params["id"][0].toString())),
      transitionType: TransitionType.native);
}