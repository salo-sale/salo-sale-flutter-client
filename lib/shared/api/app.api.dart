
import 'package:SaloSale/data.dart';
import 'package:SaloSale/shared/api/request.api.dart';
import 'package:http/http.dart';

class AppApi {

  static Future<Response> getPong() async {

    var resp = await RequestApi.get(pong, headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false,
        withDevicePlatform: true
    ));

    return resp;

  }

  static Future<Response> getAppData() async {

    var resp = await RequestApi.get(Uri.https(host, defaultPaths['app-data']), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false,
        withDevicePlatform: true
    ));

    return resp;

  }

  static Future<Response> getLocalities() async {

    var resp = await RequestApi.get(Uri.https(host, defaultPaths['localities']), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ));

    return resp;

  }

  static Future<Response> getLanguages() async {

    var resp = await RequestApi.get(Uri.https(host, defaultPaths['languages']), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ));

    return resp;

  }

  static Future<Response> getRemoteConfig() async {

    var resp = await RequestApi.get(remoteConfig, headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false,
        withLanguage: false,
        withDevicePlatform: false
    ));

    return resp;

  }

  static Future<Response> getListOfTags() async {

    var resp = await RequestApi.get(Uri.https(host, defaultPaths['listsOfTags']), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ));

    return resp;

  }

  static Future<Response> postFeedback(data) async {

    var resp = await RequestApi.post(Uri.https(host, defaultPaths['feedback']), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ), body: data);

    return resp;

  }

}
