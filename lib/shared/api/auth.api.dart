
import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:http/http.dart';

class AuthApi {

  static Future<Response> postLoginData(data, {bool socialNetWork: false}) async {

    return await RequestApi.post(AppService.remoteConfig.buildUrl(socialNetWork ? 'login-by-social-network' : 'login'), headers: RequestApi.defaultHeaders(
      withToken: false,
      withDeviceInfo: true
    ), body: data);

  }

  static Future<Response> postLoginGuestData(data) async {

    return await RequestApi.post(AppService.remoteConfig.buildUrl('login-guest'), headers: RequestApi.defaultHeaders(
      withToken: false,
      withDeviceInfo: true
    ), body: data);

  }

  static Future<Response> deleteLogoutData() async {

    return await RequestApi.delete(AppService.remoteConfig.buildUrl('logout'), headers: RequestApi.defaultHeaders(
      withToken: true,
      withDeviceInfo: true
    ));

  }

  static Future<Response> deleteLogoutGuestData() async {

    return await RequestApi.delete(AppService.remoteConfig.buildUrl('logout-guest'), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: true
    ));

  }

  static Future<Response> postRegistrationData(data) async {

    return await RequestApi.post(AppService.remoteConfig.buildUrl('registration'), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: true
    ), body: data);

  }

  static Future<Response> getAgreementListData() async {

    return await RequestApi.get(AppService.remoteConfig.buildUrl('agreements'), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ));

  }

  /// RESET PASSWORD

  static Future<Response> postForgotPassword(data) async {

    return await RequestApi.post(AppService.remoteConfig.buildUrl('forgot-password'), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ), body: data);

  }

  static Future<Response> getCheckPasswordResetCode(String code) async {

    return await RequestApi.get(AppService.remoteConfig.buildUrl('check-password-reset-token') + code, headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ));

  }

  static Future<Response> postResetPassword(data) async {

    return await RequestApi.post(AppService.remoteConfig.buildUrl('reset-password'), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ), body: data);

  }

  static Future<Response> postChangePassword(data) async {

    return await RequestApi.post(AppService.remoteConfig.buildUrl('change-password'), headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ), body: data);

  }

  /// User

  static Future<Response> getUser(String id) async {

    return await RequestApi.get(AppService.remoteConfig.buildUrl('user') + id, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ));

  }

}
