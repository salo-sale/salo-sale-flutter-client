import 'dart:async';
import 'dart:convert';

import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/enums/cache-name.enum.dart';
import 'package:SaloSale/shared/models/api/company.model.api.dart';
import 'package:SaloSale/shared/models/api/scanned.model.api.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';

class CashierApi {

  static String pathOfPointsOfSale = 'cashier/points-of-sale';
  static String pathOfDocument = 'cashier/scanned-offer';
  static String pathOfDocumentList = 'cashier';

  static FilterModel<ScannedModelApi, ScannedModelApi> filter = FilterModel<ScannedModelApi, ScannedModelApi>(
    hivePath: CacheNameEnum.SCANNEDS.index.toString()
  );

  static dispose() {
    filter.dispose();
  }

  static Future refreshDocumentList({int pointOfSaleId, int companyId, String search, List<int> tagIdList}) async {

    filter.reset();

    filter.tagIdList = tagIdList;
    filter.pointOfSaleId = pointOfSaleId;
    filter.search = search;
    filter.companyId = companyId;

    await getDocumentList(refresh: true);

  }

  static Future getDocumentList({bool refresh: false}) async {

    if ((!filter.loader && !filter.totalIsLessCountPage) || refresh) {

      filter.beforeRequest();

      var resp;

      if (AppService.internetIsConnected && AppService.remoteConfig != null) {

          resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocumentList, json: filter.toJson()), headers: RequestApi.defaultHeaders(
              withToken: AuthService.authModel.isUser,
              withDeviceInfo: false,
              withLocalityIds: true
          ));

      }

      if (resp != null) {

        var data = jsonDecode(resp.body);

        filter.processingRequest(List.from(data['models']).map((doc) => ScannedModelApi.fromMap(doc)).toList(), data['total']);

      } else {

        List data = await filter.dataFromCache();

        filter.processingRequest(data.map((doc) => ScannedModelApi.fromMap(doc)).toList(), data.length, count: data.length);

      }

    }

    filter.afterRequest();

    return filter.documentList;

  }

  static Future<ScannedModelApi> getDocument(id) async {

    filter.documentChanel.sink.add(null);

    if (AppService.internetIsConnected && AppService.remoteConfig != null) {

      var url = AppService.remoteConfig.buildUri(pathOfDocument, subPath: id.toString());

      var resp = await RequestApi.get(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false,
      ));


      if (resp != null) {

        filter
          ..document = ScannedModelApi.fromMap(jsonDecode(resp.body))
          ..documentChanel.sink.add(filter.document);

      }

    } else {

      filter
        ..document = filter.documentList.firstWhere((element) => element.id == id)
        ..documentChanel.sink.add(filter.document);

    }

    return filter.document;

  }

  static Future<List<CompanyModelApi>> getPointsOfSale({bool isInitialized: false}) async {

    if (!isInitialized) {

      return await RequestApi.get(
          AppService.remoteConfig.buildUri(pathOfPointsOfSale),
          headers: RequestApi.defaultHeaders(
              withToken: true,
              withDeviceInfo: false,
              withLocalityIds: true,
              withLanguage: true
          )
      ).then((value) {

        if (value != null) {

          return List.from(jsonDecode(value.body)).map((e) => CompanyModelApi.fromMap(e)).toList();

        } else {

          return null;

        }

      });

    }

    return null;

  }

  static Future<bool> scannedOffer(body) async {

    var url = AppService.remoteConfig.buildUri(pathOfDocument);

    var resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ), body: body);


    if (resp != null) {

      if (resp.statusCode >= 200 || resp.statusCode < 300) {

        return Future.value(jsonDecode(resp.body) == 1);

      }

    }

    return Future.value(false);

  }

}
