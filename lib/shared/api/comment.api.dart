import 'dart:async';
import 'dart:convert';

import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/models/api/comment.model.api.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';

class CommentApi {

  static String pathOfDocument = 'comment';
  static String pathOfDocumentList = 'comments';

  static Map<String, FilterModel<CommentModelApi, CommentModelApi>> filterMap = Map();

  static dispose() {
    filterMap.forEach((key, value) {
      value.dispose();
    });
  }

  static Future refreshDocumentList(
      String filterItem, {
        int myLike,
        int myFollow,
        String search,
        int companyId,
        bool refreshTagList: false,
        bool clearDocument: true
      }
    ) async {

    filterMap[filterItem].reset(
        clearDocument: clearDocument,
        refreshTagList: refreshTagList
    );

    filterMap[filterItem].search = search;
    filterMap[filterItem].companyId = companyId;

    await getDocumentList(filterItem, refresh: true);

  }

  static Future<List<CommentModelApi>> getDocumentList(String filterItem, {bool refresh: false}) async {

    if ((!filterMap[filterItem].loader && !filterMap[filterItem].totalIsLessCountPage) || refresh) {

      filterMap[filterItem].beforeRequest();

      var resp;

      if (AppService.internetIsConnected && AppService.remoteConfig != null && (!filterMap[filterItem].totalIsLessCountPage || refresh)) {

          resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocumentList, json: filterMap[filterItem].toJson()), headers: RequestApi.defaultHeaders(
              withToken: AuthService.authModel.isUser,
              withDeviceInfo: false,
              withLocalityIds: true
          ));

      } else {
        await AppService.checkInternetConnect();

      }

      if (resp != null) {

        var data = jsonDecode(resp.body);

        await filterMap[filterItem].processingRequest(List.from(data['models']).map((doc) => CommentModelApi.fromMap(doc)).toList(), data['total']);

        if (filterMap[filterItem].search == null || filterMap[filterItem].search.length == 0) {

          filterMap[filterItem].saveDataToCache();

        }

      } else {

        List data = await filterMap[filterItem].dataFromCache();

        var list = data.map((doc) => CommentModelApi.fromMap(doc)).toList();
//
//        if (filterMap[filterItem].tagIdList != null && filterMap[filterItem].tagIdList.length > 0) {
//
//          list = list.where((element) {
//
//            return element.tagIdList.firstWhere((tagId) {
//              return filterMap[filterItem].tagIdList.contains(tagId);
//            }, orElse: () => -1) != -1;
//
//          }).toList();
//        }

        if (filterMap[filterItem].search != null && filterMap[filterItem].search.length > 0) {

          list = list.where((element) => element.comment.toLowerCase().trim().indexOf(filterMap[filterItem].search.toLowerCase().trim()) > -1 || (element.user.login != null ? element.user.login.toLowerCase().trim().indexOf(filterMap[filterItem].search.toLowerCase().trim()) > -1 : false)).toList();


        }

        filterMap[filterItem].processingRequest(list, list.length, count: list.length);

      }

    }

    filterMap[filterItem].afterRequest();

    return filterMap[filterItem].documentList;


  }

  static Future<CommentModelApi> getDocument(String filterItem, id) async {

    filterMap[filterItem].documentChanel.sink.add(null);

    if (AppService.internetIsConnected && AppService.remoteConfig != null) {

      var url = AppService.remoteConfig.buildUri(pathOfDocument, subPath: id.toString());

      var resp = await RequestApi.get(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withLocalityIds: true,
        withDeviceInfo: false,
      ));

      if (resp != null) {

        filterMap[filterItem]
          ..document = CommentModelApi.fromMap(jsonDecode(resp.body))
          ..documentChanel.sink.add(filterMap[filterItem].document);

      }

    } else {

      filterMap[filterItem]
        ..document = filterMap[filterItem].documentList.firstWhere((element) => element.id == id)
        ..documentChanel.sink.add(filterMap[filterItem].document);

    }

    return filterMap[filterItem].document;

  }




  static Future<CommentModelApi> saveDocument(CommentModelApi document, {bool isUpdate: false}) async {

    assert(document != null);

    var resp;

    if (AppService.internetIsConnected && AppService.remoteConfig != null) {

      Map<String, dynamic> data = document.toJson();

      data.remove('id');
      data.remove('offerId');
      data.remove('numberOfReplies');
      data.remove('userId');
      data.remove('user');
      data.remove('createdAt');
      data.remove('updatedAt');
      data.remove('status');
      data.remove('replies');

      if (document.scores.length > 0) {
        data.remove('score');
      } else {
        data.remove('scores');
        data['score'] = data['score'].toString();
      }

      data['for'] = data['for'].toString();
      data['companyId'] = data['companyId'].toString();
      data['comment'] = data['comment'].toString();

      if (isUpdate) {

        resp = await RequestApi.put(AppService.remoteConfig.buildUri(pathOfDocument, subPath: '${document.id}'), headers: RequestApi.defaultHeaders(
          withDeviceInfo: false,
        ), body: data);

      } else {

        resp = await RequestApi.post(AppService.remoteConfig.buildUri(pathOfDocumentList), headers: RequestApi.defaultHeaders(
          withDeviceInfo: false,
        ), body: data);

      }

    } else {
      await AppService.checkInternetConnect();
    }

    if (resp == null) {

      document = null;

    }

    return Future.value(document);

  }

  static selectDocument(String filterItem, CommentModelApi document, {bool searchInList}) async {
    if (searchInList) {
      if (filterMap[filterItem].documentList != null && filterMap[filterItem].documentList.length > 0) {
        filterMap[filterItem].document = filterMap[filterItem].documentList.firstWhere((element) => element.id == document.id, orElse: () => null) ?? document;
      } else {
        await getDocument(filterItem, document.id);
      }
    } else {
      filterMap[filterItem].document = document;
    }
    return filterMap[filterItem].document;
  }

  static void putIfNotExistFilterInMap(filterItem) {

    if (!filterMap.containsKey(filterItem)) {
      filterMap[filterItem] = FilterModel<CommentModelApi, CommentModelApi>(
          hivePath: 'hive-' + filterItem,
          tagIdList: List<int>(),
          count: AppService.remoteConfig.checkModule('companies') ? AppService.remoteConfig.modulesControl['companies'].defaultFilterValue['count'] : 50
      );
    }

  }

}
