import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/customs/cache-manager.custom.dart';
import 'package:SaloSale/shared/models/api/company.model.api.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/services/firebase/messaging.service.dart';
import 'package:http/http.dart';

class CompanyApi {

  static String pathOfLike = 'company-like';
  static String pathOfFollower = 'company-follower';
  static String pathOfDocument = 'company';
  static String pathOfDocumentList = 'companies';

  static Map<String, FilterModel<CompanyModelApi, CompanyModelApi>> filterMap = Map();

  static dispose() {
    filterMap.forEach((key, value) {
      value.dispose();
    });
  }

  static Future refreshDocumentList(
      String filterItem, {
        int myLike,
        int myFollow,
        String search,
        List<int> tagIdList,
        bool refreshTagList: false,
        bool clearDocument: true
      }
    ) async {

    filterMap[filterItem].reset(
        clearDocument: clearDocument,
        refreshTagList: refreshTagList
    );

    filterMap[filterItem].myLike = myLike;
    filterMap[filterItem].myFollow = myFollow;
    filterMap[filterItem].search = search;

    if (tagIdList != null || refreshTagList) {
      filterMap[filterItem].tagIdList = tagIdList ?? List<int>();
    }

    await getDocumentList(filterItem, refresh: true);

  }

  static Future<List<CompanyModelApi>> getDocumentList(String filterItem, {bool refresh: false}) async {

    if ((!filterMap[filterItem].loader && !filterMap[filterItem].totalIsLessCountPage) || refresh) {

      filterMap[filterItem].beforeRequest();

      var resp;

      if (AppService.internetIsConnected && AppService.remoteConfig != null && (!filterMap[filterItem].totalIsLessCountPage || refresh)) {

          resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocumentList, json: filterMap[filterItem].toJson()), headers: RequestApi.defaultHeaders(
              withToken: AuthService.authModel.isUser,
              withDeviceInfo: false,
              withLocalityIds: true
          ));

      } else {
        await AppService.checkInternetConnect();

      }

      if (resp != null) {

        var data = jsonDecode(resp.body);

        await filterMap[filterItem].processingRequest(List.from(data['models']).map((doc) => CompanyModelApi.fromMap(doc)).toList(), data['total']);

        if (filterMap[filterItem].search == null || filterMap[filterItem].search.length == 0) {

          filterMap[filterItem].saveDataToCache();

        }

      } else {

        List data = await filterMap[filterItem].dataFromCache();

        var list = data.map((doc) => CompanyModelApi.fromMap(doc)).toList();

        if (filterMap[filterItem].tagIdList != null && filterMap[filterItem].tagIdList.length > 0) {

          list = list.where((element) {

            return element.tagIdList.firstWhere((tagId) {
              return filterMap[filterItem].tagIdList.contains(tagId);
            }, orElse: () => -1) != -1;

          }).toList();
        }

        if (filterMap[filterItem].search != null && filterMap[filterItem].search.length > 0) {

          list = list.where((element) => element.name.toLowerCase().trim().indexOf(filterMap[filterItem].search.toLowerCase().trim()) > -1 || (element.latinName != null ? element.latinName.toLowerCase().trim().indexOf(filterMap[filterItem].search.toLowerCase().trim()) > -1 : false) || (element.shortName != null ? element.shortName.toLowerCase().trim().indexOf(filterMap[filterItem].search.toLowerCase().trim()) > -1 : false)).toList();


        }

        filterMap[filterItem].processingRequest(list, list.length, count: list.length);

      }

    }

    filterMap[filterItem].afterRequest();

    return filterMap[filterItem].documentList;


  }

  static Future<CompanyModelApi> getDocument(String filterItem, id) async {

    filterMap[filterItem].documentChanel.sink.add(null);

    if (AppService.internetIsConnected && AppService.remoteConfig != null) {

      var url = AppService.remoteConfig.buildUri(pathOfDocument, subPath: id.toString());

      var resp = await RequestApi.get(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withLocalityIds: true,
        withDeviceInfo: false,
      ));

      if (resp != null) {

        filterMap[filterItem]
          ..document = CompanyModelApi.fromMap(jsonDecode(resp.body))
          ..documentChanel.sink.add(filterMap[filterItem].document);

      }

    } else {

      filterMap[filterItem]
        ..document = filterMap[filterItem].documentList.firstWhere((element) => element.id == id)
        ..documentChanel.sink.add(filterMap[filterItem].document);

    }

    return filterMap[filterItem].document;

  }

  static Future<int> like(String filterItem, {bool isLike: true}) async {

    var url = AppService.remoteConfig.buildUri(pathOfLike, subPath: '${filterMap[filterItem].document.id}/like');

    Response resp;

    if (isLike) {

      resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
          withToken: true,
          withDeviceInfo: false
      ));

    } else {

      resp = await RequestApi.delete(url, headers: RequestApi.defaultHeaders(
          withToken: true,
          withDeviceInfo: false
      ));

    }

    if (resp != null) {

    }

    filterMap[filterItem].document.liked = jsonDecode(resp.body) == 1 ? isLike ? 1 : 0 : 0;
    filterMap[filterItem].documentChanel.add(filterMap[filterItem].document);

    return filterMap[filterItem].document.liked;

  }

  static Future<int> follower(String filterItem, int id, {bool isFollower: true, bool isSubscribe: true}) async {

    var url = AppService.remoteConfig.buildUri(pathOfLike, subPath: '${filterMap[filterItem].document.id}/follow');

    Response resp;

    if (isFollower) {

      resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
          withToken: true,
          withDeviceInfo: false
      ));

      FirebaseMessagingService.firebaseMessaging.subscribeToTopic('company_$id');

    } else {

      resp = await RequestApi.delete(url, headers: RequestApi.defaultHeaders(
          withToken: true,
          withDeviceInfo: false
      ));

      FirebaseMessagingService.firebaseMessaging.unsubscribeFromTopic('company_$id');

    }

    if (resp != null) {

    }

    filterMap[filterItem].document.follower.followed = jsonDecode(resp.body) == 1 ? isFollower ? 1 : 0 : 0;
    filterMap[filterItem].documentChanel.add(filterMap[filterItem].document);

    return filterMap[filterItem].document.liked;

  }

  static selectDocument(String filterItem, CompanyModelApi document, {bool searchInList}) async {
    if (searchInList) {
      if (filterMap[filterItem].documentList != null && filterMap[filterItem].documentList.length > 0) {
        filterMap[filterItem].document = filterMap[filterItem].documentList.firstWhere((element) => element.id == document.id, orElse: () => null) ?? document;
      } else {
        await getDocument(filterItem, document.id);
      }
    } else {
      filterMap[filterItem].document = document;
    }
    return filterMap[filterItem].document;
  }

  static Future<Uint8List> mapMarker(int id, {bool isOpen}) async {

    var file;

    try {

      file = await CacheManagerCustom().getSingleFile((AppService.remoteConfig?.routing != null ? AppService.remoteConfig.routing['map-marker-per-company'] : 'https://salo-sale.com/uploads/companies/') + id.toString() + '/' + (isOpen == null ? '' : isOpen ? 'green-dot-' : 'red-dot-') + (AppService.remoteConfig.modulesControl['companies'].defaultFileNameMapMarker ?? 'map-marker.png'));

    } on HttpException catch (_) {

      return null;

    }

    if (file != null && await file.exists()) {
      return file.readAsBytesSync();
    }
    return null;

  }

  static void putIfNotExistFilterInMap(filterItem) {

    if (!filterMap.containsKey(filterItem)) {
      filterMap[filterItem] = FilterModel<CompanyModelApi, CompanyModelApi>(
          hivePath: 'hive-' + filterItem,
          tagIdList: List<int>(),
          count: AppService.remoteConfig.checkModule('companies') ? AppService.remoteConfig.modulesControl['companies'].defaultFilterValue['count'] : 50
      );
    }

  }

}
