import 'dart:async';
import 'dart:convert';

import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/enums/cache-name.enum.dart';
import 'package:SaloSale/shared/models/api/event-list.model.api.dart';
import 'package:SaloSale/shared/models/api/offer.model.api.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:http/http.dart';

class EventApi {

  static String pathOfIncrementView = 'increment-view';
  static String pathOfComment = 'comment';
  static String pathOfLike = 'like';
  static String pathOfDocument = 'event';
  static String pathOfDocumentList = 'events';

  static Map<String, FilterModel<EventListModelApi, OfferModelApi>> filterMap = Map();

  static dispose() {
    filterMap.forEach((key, value) {
      value.dispose();
    });
  }

  static Future refreshDocumentList(String filterItem, {int myLike, int companyId, String search, List<int> tagIdList, bool refreshTagList: false}) async {

    filterMap[filterItem].reset();

    filterMap[filterItem].myLike = myLike;
    if (tagIdList != null || refreshTagList) {
      filterMap[filterItem].tagIdList = tagIdList;
    }
    filterMap[filterItem].search = search;
    filterMap[filterItem].companyId = companyId;

    await getDocumentList(filterItem, refresh: true);

  }

  static Future<List<EventListModelApi>> getDocumentList(String filterItem, {bool refresh: false}) async {

    if ((!filterMap[filterItem].loader && !filterMap[filterItem].totalIsLessCountPage) || refresh) {

      filterMap[filterItem].beforeRequest();

      var resp;

      if (AppService.internetIsConnected && AppService.remoteConfig != null) {

          resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocumentList, json: filterMap[filterItem].toJson()), headers: RequestApi.defaultHeaders(
              withToken: AuthService.authModel.isUser,
              withDeviceInfo: false,
              withLocalityIds: true
          ));

      } else {
        await AppService.checkInternetConnect();
      }

      if (resp != null) {

        var data = jsonDecode(resp.body);

        filterMap[filterItem].documentList = [];

        if (data['models'] is List<dynamic>) {

        } else {
          Map<String, dynamic>.from(data['models']).forEach((key, model) => filterMap[filterItem].documentList.add(EventListModelApi.fromMap(model)));

        }
        filterMap[filterItem].processingRequest(null, data['total']);
        filterMap[filterItem].saveDataToCache();

      } else {

        List data = await filterMap[filterItem].dataFromCache();

        filterMap[filterItem].processingRequest(data.map((doc) => EventListModelApi.fromMap(doc)).toList(), data.length, count: data.length);

      }

    }

    filterMap[filterItem].afterRequest();

    return filterMap[filterItem].documentList;

  }

  static Future<OfferModelApi> getDocument(String filterItem, id) async {

    filterMap[filterItem].documentChanel.sink.add(null);

    var url = AppService.remoteConfig.buildUri(pathOfDocument, subPath: id.toString());

    var resp = await RequestApi.get(url, headers: RequestApi.defaultHeaders(
      withToken: true,
      withDeviceInfo: false,
    ));

    if (resp != null) {

      filterMap[filterItem]
        ..document = OfferModelApi.fromMap(jsonDecode(resp.body))
        ..documentChanel.sink.add(filterMap[filterItem].document);

    }

    return filterMap[filterItem].document;

  }

  static Future<Response> incrementView(String filterItem) async {

    var url = AppService.remoteConfig.buildUri(pathOfIncrementView, subPath: '${filterMap[filterItem].document.id}/increment-view');

    var resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ));


    if (resp != null) {

    }

    return resp;

  }

  static Future<Response> comment(String filterItem) async {

    var url = AppService.remoteConfig.buildUri(pathOfComment, subPath: '${filterMap[filterItem].document.id}/comment');

    var resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ));

    if (resp != null) {

    }

    return resp;

  }

  static Future<int> like(String filterItem, {bool isLike: true}) async {

    var url = AppService.remoteConfig.buildUri(pathOfLike, subPath: '${filterMap[filterItem].document.id}/like');

    Response resp;

    if (isLike) {

      resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
          withToken: true,
          withDeviceInfo: false
      ));

    } else {

      resp = await RequestApi.delete(url, headers: RequestApi.defaultHeaders(
          withToken: true,
          withDeviceInfo: false
      ));

    }

    if (resp != null) {

    }

    filterMap[filterItem].document.liked = jsonDecode(resp.body) == 1 ? isLike ? 1 : 0 : 0;
    filterMap[filterItem].documentChanel.add(filterMap[filterItem].document);

    return filterMap[filterItem].document.liked;

  }

  static void putIfNotExistFilterInMap(filterItem) {

    if (!filterMap.containsKey(filterItem)) {
      filterMap[filterItem] = FilterModel<EventListModelApi, OfferModelApi>(
          type: 4,
          hivePath: CacheNameEnum.EVENTS.index.toString()
      );
    }

  }

}
