import 'dart:convert';

import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/models/api/loyalty-card.model.api.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';

class LoyaltyApi {

  static String pathOfDocument = 'loyalty-card';
  static String pathOfDocumentList = 'loyalty-cards';
  static String hiveName = 'loyaltyCards';

  static FilterModel<LoyaltyCardModelApi, LoyaltyCardModelApi> filter = FilterModel<LoyaltyCardModelApi, LoyaltyCardModelApi>(
    hivePath: hiveName
  );

  static dispose() {
    filter.dispose();
  }

  static Future refreshDocumentList({String search, bool checkNotSavedData: true}) async {

    filter.reset();
    filter.search = search;

    await getDocumentList(refresh: true, checkNotSavedData: checkNotSavedData);

  }

  static Future<List<LoyaltyCardModelApi>> getDocumentList({bool refresh: false, bool checkNotSavedData: true}) async {

    // TODO refrsh clear hive of loyalty cards

    if ((!filter.loader && !filter.totalIsLessCountPage) || refresh) {

      filter.beforeRequest();

      var resp;

      if (AppService.internetIsConnected && AppService.remoteConfig != null) {

        resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocumentList, json: filter.toJson()), headers: RequestApi.defaultHeaders(
            withToken: AuthService.authModel.isUser,
            withDeviceInfo: false,
            withLocalityIds: true
        ));

      } else {
        await AppService.checkInternetConnect();
      }

      if (resp != null) {

        var data = jsonDecode(resp.body);

        filter.processingRequest(List.from(data['models']).map((doc) => LoyaltyCardModelApi.fromMap(doc)).toList(), data['total']);
        filter.saveDataToCache(checkNotSavedData: checkNotSavedData);

      } else {


        List data = await filter.dataFromCache();

        var list = data.map((doc) => LoyaltyCardModelApi.fromMap(doc)).toList();

        if (filter.search != null) {

          list = list.where((element) => element.name.contains(filter.search) || element.comment.contains(filter.search)).toList();

        }

        filter.processingRequest(list, list.length, count: list.length);

      }

    }

    filter.afterRequest();

    return filter.documentList;

  }


  static Future<LoyaltyCardModelApi> saveDocument(LoyaltyCardModelApi document) async {

    assert(document != null);

    var resp;

    if (AppService.internetIsConnected && AppService.remoteConfig != null) {
      var data = document.toJson();

      data.remove('id');
      data.remove('userId');
      data.remove('createdAt');
      data.remove('updatedAt');
      data.remove('isNotSaved');
      data.remove('status');

      data['value'] = data['value'].toString();
      data['name'] = data['name'].toString();
      data['comment'] = data['comment'].toString();
      data['color'] = data['color'].toString();

      if (data['value'].length == 0) {
        data.remove('value');
      }

      if (data['comment'].length == 0) {
        data.remove('comment');
      }

      if (document.isNewModel) {

        print(AppService.remoteConfig.buildUri(pathOfDocumentList));

        resp = await RequestApi.post(AppService.remoteConfig.buildUri(pathOfDocumentList), headers: RequestApi.defaultHeaders(
          withDeviceInfo: false,
        ), body: data);

      } else {

        resp = await RequestApi.put(AppService.remoteConfig.buildUri(pathOfDocument, subPath: '${document.id}'), headers: RequestApi.defaultHeaders(
          withDeviceInfo: false,
        ), body: data);

      }

    } else {

      await AppService.checkInternetConnect();

      int index = filter.documentList.indexWhere((element) => element.id == document.id);

      if (!document.isNewModel) {

        filter.documentList.removeWhere((element) => element.id == document.id);

      }

      document.isNotSaved = true;
      filter.documentList.insert(index, document);

      await AppService.appBox.put(hiveName, json.encode(filter.documentList.map((e) => e.toJson()).toList()));

      resp = document;

    }

    if (resp == null) {

      document = null;

    }

    return Future.value(document);

  }

  static Future<bool> deleteDocument(int id) async {

    assert(id != null);

    var resp;

    if (AppService.internetIsConnected && AppService.remoteConfig != null) {

      resp = await RequestApi.delete(AppService.remoteConfig.buildUri(pathOfDocumentList, subPath: '/$id'), headers: RequestApi.defaultHeaders(
          withToken: AuthService.authModel.isUser,
          withDeviceInfo: false,
          withLocalityIds: true
      ));

    } else {
      await AppService.checkInternetConnect();
    }


    if (resp != null && resp.statusCode == 204) {

      filter.documentList.removeWhere((element) => element.id == id);

      filter.saveDataToCache();

    }


    return Future.value(true);

  }

}