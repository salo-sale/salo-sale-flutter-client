import 'dart:convert';

import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/models/api/notification.model.api.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:rxdart/rxdart.dart';

class NotificationApi {

  static String pathOfDocument = 'notification';
  static String pathOfDocumentList = 'notifications';

  static FilterModel<NotificationModelApi, NotificationModelApi> filter = FilterModel<NotificationModelApi, NotificationModelApi>(
    hivePath: 'notifications'
  );
  static PublishSubject<bool> notificationUnreadCounterChanel = PublishSubject<bool>();
  static int notificationUnreadCount = 0;

  static dispose() {
    filter.dispose();
    notificationUnreadCounterChanel.close();
  }

  static Future refreshDocumentList() async {

    filter.reset();
    await getDocumentList();

  }

  static Future getDocumentList({bool refresh: false}) async {

    if ((!filter.loader && !filter.totalIsLessCountPage) || refresh) {

      filter.beforeRequest();

      var resp;

      if (AppService.internetIsConnected && AppService.remoteConfig != null) {

          resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocumentList, json: filter.toJson()), headers: RequestApi.defaultHeaders(
              withToken: true,
              withDeviceInfo: false,
              withLocalityIds: true
          ));

      }

      if (resp != null) {

        var data = jsonDecode(resp.body);

        filter.processingRequest(List.from(data['models']).map((doc) => NotificationModelApi.fromMap(doc)).toList(), data['total']);

      } else {

        List data = await filter.dataFromCache();

        filter.processingRequest(data.map((doc) => NotificationModelApi.fromMap(doc)).toList(), data.length, count: data.length);

      }

    }

    filter.afterRequest();

    return filter.documentList;

  }

  static Future<NotificationModelApi> getDocument(id) async {

    filter.documentChanel.sink.add(null);

    var resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocument, subPath: id.toString()), headers: RequestApi.defaultHeaders(
      withLocalityIds: true,
      withDeviceInfo: false,
    ));


    if (resp != null) {

      filter
        ..document = NotificationModelApi.fromMap(jsonDecode(resp.body))
        ..documentChanel.sink.add(filter.document);

    }
    return filter.document;

  }

  static Future<void> getCheckUnread() async {

    var resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocument, subPath: 'check-unread'), headers: RequestApi.defaultHeaders(
      withToken: true,
      withDeviceInfo: false,
      withLocalityIds: true,
    ));


    if (resp != null) {


      notificationUnreadCount = jsonDecode(resp.body);
      notificationUnreadCounterChanel.add(true);

    }

  }

  static Future<bool> putReadNotification(String id) async {

    int returnValue = 0;

    var resp = await RequestApi.put(AppService.remoteConfig.buildUri(pathOfDocument, subPath: id), headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false,
        withLocalityIds: true
    ));


    if (resp != null) {

      returnValue = jsonDecode(resp.body);

      if (returnValue == 1) {

        filter.documentList.firstWhere((element) => element.id == id).read = 1;
        filter.documentListChanel.add(filter.documentList);

      }

    }

    return returnValue == 1;

  }

}
