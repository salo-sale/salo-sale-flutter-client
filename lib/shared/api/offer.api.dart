import 'dart:async';
import 'dart:convert';

import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/models/api/offer.model.api.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:http/http.dart';

class OfferApi {

  static String pathOfIncrementView = 'increment-view';
  static String pathOfBuyOffer = 'buy-offer';
  static String pathOfTakeIkoint = 'take-ikoint';
  static String pathOfComment = 'comment';
  static String pathOfLike = 'like';
  static String pathOfDocument = 'offer';
  static String pathOfDocumentList = 'offers';

  static Map<String, FilterModel<OfferModelApi, OfferModelApi>> filterMap = Map();

  static dispose() {
    filterMap.forEach((key, value) {
      value.dispose();
    });
  }

  static Future refreshDocumentList(
      String filterItem,
      {
        int myLike,
        int myOffers,
        int companyId,
        String search,
        List<int> tagIdList,
        bool refreshTagList: false
      }
    ) async {

    filterMap[filterItem].reset();

    filterMap[filterItem].myLike = myLike;
    if (tagIdList != null || refreshTagList) {
      filterMap[filterItem].tagIdList = tagIdList;
    }
    filterMap[filterItem].myOffers = myOffers;
    filterMap[filterItem].search = search;
    filterMap[filterItem].companyId = companyId;

    await getDocumentList(filterItem, refresh: true);

  }

  static Future getDocumentList(String filterItem, {bool refresh: false}) async {

    if ((!filterMap[filterItem].loader && !filterMap[filterItem].totalIsLessCountPage) || refresh) {

      filterMap[filterItem].beforeRequest();

      var resp;

      if (AppService.internetIsConnected && AppService.remoteConfig != null) {

        resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocumentList, json: filterMap[filterItem].toJson()), headers: RequestApi.defaultHeaders(
            withToken: AuthService.authModel.isUser,
            withDeviceInfo: false,
            withLocalityIds: true
        ));

      } else {
        await AppService.checkInternetConnect();
      }

      if (resp != null) {

        var data = jsonDecode(resp.body);

        filterMap[filterItem].processingRequest(List.from(data['models']).map((doc) => OfferModelApi.fromMap(doc)).toList(), data['total']);
        filterMap[filterItem].saveDataToCache();

      } else {


        List data = await filterMap[filterItem].dataFromCache();

        var list = data.map((doc) => OfferModelApi.fromMap(doc)).toList();

        if (filterMap[filterItem].search != null && filterMap[filterItem].search.length > 0) {

          list = list.where((element) => element.language.title.toLowerCase().trim().indexOf(filterMap[filterItem].search.toLowerCase().trim()) > -1 || element.company.name.toLowerCase().trim().indexOf(filterMap[filterItem].search.toLowerCase().trim()) > -1 || (element.company.latinName != null ? element.company.latinName.toLowerCase().trim().indexOf(filterMap[filterItem].search.toLowerCase().trim()) > -1 : false) || (element.company.shortName != null ? element.company.shortName.toLowerCase().trim().indexOf(filterMap[filterItem].search.toLowerCase().trim()) > -1 : false)).toList();

        }

        filterMap[filterItem].processingRequest(list, list.length, count: list.length);

      }

    }

    filterMap[filterItem].afterRequest();

    return filterMap[filterItem].documentList;

  }

  static Future<OfferModelApi> getDocument(String filterItem, id) async {

    filterMap[filterItem].documentChanel.sink.add(null);

    if (AppService.internetIsConnected && AppService.remoteConfig != null) {

      var url = AppService.remoteConfig.buildUri(pathOfDocument, subPath: id.toString());



      var resp = await RequestApi.get(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false,
      ));


      if (resp != null) {

        filterMap[filterItem]
          ..document = OfferModelApi.fromMap(jsonDecode(resp.body))
          ..documentChanel.sink.add(filterMap[filterItem].document);

      }

    } else {

      filterMap[filterItem]
        ..document = filterMap[filterItem].documentList.firstWhere((element) => element.id == id)
        ..documentChanel.sink.add(filterMap[filterItem].document);

    }

    return filterMap[filterItem].document;

  }

  static Future<Response> incrementView(String filterItem) async {

    var url = AppService.remoteConfig.buildUri(pathOfIncrementView, subPath: "/${filterMap[filterItem].document.id}/$pathOfIncrementView");

    var resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ));


    if (resp != null) {

    }

    return resp;

  }

  static Future<bool> buyOffer(int id) async {

    var url = AppService.remoteConfig.buildUri(pathOfBuyOffer, subPath: '$id/$pathOfBuyOffer');

    var resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ));


    bool result = false;
    if (resp != null) {

      result = jsonDecode(resp.body) == 1;

    }

    return result;

  }

  static Future<bool> takeIkoint(String filterItem) async {

    var url = AppService.remoteConfig.buildUri(pathOfTakeIkoint, subPath: '${filterMap[filterItem].document.id}/$pathOfTakeIkoint');

    Response resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ));


    if (resp != null) {


      var result = jsonDecode(resp.body);

      if (result == 0) {

      } else {

        filterMap[filterItem].document.canTakeIkoint = result;
        filterMap[filterItem].documentChanel.add(filterMap[filterItem].document);
        AuthService.initUserApiModel();

      }

    }

    return filterMap[filterItem].document.canTakeIkoint != null;

  }

  static Future<Response> comment(String filterItem) async {

    var url = AppService.remoteConfig.buildUri(pathOfComment, subPath: '${filterMap[filterItem].document.id}/$pathOfComment');

    var resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ));


    if (resp != null) {

    }

    return resp;

  }

  static Future<int> like(String filterItem, {bool isLike: true}) async {

    var url = AppService.remoteConfig.buildUri(pathOfLike, subPath: '${filterMap[filterItem].document.id}/$pathOfLike');

    Response resp;

    if (isLike) {

      resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
          withToken: true,
          withDeviceInfo: false
      ));

    } else {

      resp = await RequestApi.delete(url, headers: RequestApi.defaultHeaders(
          withToken: true,
          withDeviceInfo: false
      ));

    }


    if (resp != null) {

    }

    filterMap[filterItem].document.liked = jsonDecode(resp.body) == 1 ? isLike ? 1 : 0 : 0;
    filterMap[filterItem].documentChanel.add(filterMap[filterItem].document);

    return filterMap[filterItem].document.liked;

  }

  static void putIfNotExistFilterInMap(filterItem) {

    if (!filterMap.containsKey(filterItem)) {
      filterMap[filterItem] = FilterModel<OfferModelApi, OfferModelApi>(
          hivePath: 'hive-' + filterItem
      );
    }

  }

}
