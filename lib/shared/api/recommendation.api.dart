import 'dart:async';
import 'dart:convert';

import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/models/api/recommendation-banner.model.api.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:http/http.dart';

class RecommendationApi {

  static String pathOfClick = 'clicked-recommended';
  static String pathOfDocumentList = 'recommended-offers';
  static bool initialized = false;
  static bool loader = false;
  // TODO add filterMap
  static FilterModel<RecommendationBannerModelApi, RecommendationBannerModelApi> filter = FilterModel<RecommendationBannerModelApi, RecommendationBannerModelApi>(
    hivePath: 'recommendedOffers'
  );

  static dispose() {
    filter.dispose();
  }

  static Future refreshDocumentList({List<int> tagIdList}) async {

    filter.reset();
    RecommendationApi.filter.tagIdList = tagIdList;
    await getDocumentList(refresh: true);

  }

  static Future<List<RecommendationBannerModelApi>> getDocumentList({bool refresh: false}) async {

    if ((!filter.loader && !filter.totalIsLessCountPage) || refresh) {

      filter.documentList = [];

      filter.beforeRequest();

      var resp;
      List data;

      if (AppService.internetIsConnected && AppService.remoteConfig != null) {

          resp = await RequestApi.get(AppService.remoteConfig.buildUri(pathOfDocumentList), headers: RequestApi.defaultHeaders(
              withToken: false,
              withDeviceInfo: false,
              withLocalityIds: true
          ));

      }

      if (resp != null) {

        data = List.from(jsonDecode(resp.body));

      } else {

        data = await filter.dataFromCache();

      }

      filter.processingRequest(data.map((doc) => RecommendationBannerModelApi.fromMap(doc)).toList(), data.length);

    }

    filter.afterRequest();

    return filter.documentList;

  }

  static Future<Response> postClickedRecommendation() async {

    var url = AppService.remoteConfig.buildUri(pathOfClick);

    var resp = await RequestApi.post(url, headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ));

    if (resp != null) {

    }

    return resp;

  }

}
