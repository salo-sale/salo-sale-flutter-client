import "dart:async";
import 'dart:convert';
import 'dart:io';

import 'package:SaloSale/data.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

/// This is class doing request to server.
class RequestApi {

  static final List<dynamic> registerRequest = [];

  /// This is function is child of _get function and need for doing get request with serve.
  /// This function have 1 required and 1 optional parameters:
  /// @param url
  /// @param headers
  static Future<http.Response> get(url, {Map<String, String> headers, bool showError: true}) async {

    var resp = await _get(url, headers: headers, showError: showError);

    if (resp != null && resp.statusCode == 401) {

//      var r = await AuthService.refreshToken();
      resp = await _get(url, headers: headers);

    }

    return resp;

  }

  /// This is function is child of _post function and need for doing post request with serve.
  /// This function have 1 required and 2 optional parameters:
  /// @param url
  /// @param headers
  /// @param body
  static Future<http.Response> post(url, {Map<String, String> headers, body}) async {

    var resp = await _post(url, headers: headers, body: body);

    if (resp != null && resp.statusCode == 401) {

//      var r = await AuthService.refreshToken();
      resp = await _post(url, headers: headers, body: body);

    }

    return resp;

  }

  /// This is function is child of _put function and need for doing put request with serve.
  /// This function have 1 required and 2 optional parameters:
  /// @param url
  /// @param headers
  /// @param body
  static Future<http.Response> put(url, {Map<String, String> headers, Object body}) async {


    var resp = await _put(url, headers: headers, body: body);

    if (resp != null && resp.statusCode == 401) {

//      var r = await AuthService.refreshToken();
      resp = await _put(url, headers: headers, body: body);

    }

    return resp;

  }

  /// This is function is child of _delete function and need for doing delete request with serve.
  /// This function have 1 required and 2 optional parameters:
  /// @param url
  /// @param headers
  /// @param body
  static Future<http.Response> delete(url, {Map<String, String> headers, Object body}) async {

    var resp = await _delete(url, headers: headers);

    if (resp != null && resp.statusCode == 401) {

//      var r = await AuthService.refreshToken();
      resp = await _delete(url, headers: headers);

    }

    return resp;

  }

  /// This is function is private and need for doing get request with serve.
  /// This function have 1 required and 2 optional parameters:
  /// @param url
  /// @param headers
  static Future<http.Response> _get(url, {Map<String, String> headers, bool showError: true}) async {

    try {

      if (checkRegisterRequest(url)) {

        return await processResponse(await http.get(url, headers: headers), url, showError: showError);

      }

    } on SocketException catch (_) {
      AppService.changeStatusInternet(false);

    } catch (e) {

    // returns default error message
    return await processResponse(null, url);

    }

    return null;

  }

  /// This is function is private and need for doing post request with serve.
  /// This function have 1 required and 2 optional parameters:
  /// @param url
  /// @param headers
  /// @param body
  static Future<http.Response> _post(url, {Map<String, String> headers, body}) async {

    try {

      if (checkRegisterRequest(url)) {

        return await processResponse(await http.post(url, headers: headers, body: body), url);

      }

    } on SocketException catch (_) {
      AppService.changeStatusInternet(false);

    } catch (e) {

      // returns default error message
      return await processResponse(null, url);

    }

    return null;

  }

  /// This is function is private and need for doing put request with serve.
  /// This function have 1 required and 2 optional parameters:
  /// @param url
  /// @param headers
  /// @param body
  static Future<http.Response> _put(url, {Map<String, String> headers, body}) async {

    try {
      if (checkRegisterRequest(url)) {
        return await processResponse(
            await http.put(url, headers: headers, body: body), url);
      }

    } on SocketException catch (_) {

      AppService.changeStatusInternet(false);

    } catch (e) {

      // returns default error message
      return await processResponse(null, url);

    }

    return null;

  }

  /// This is function is private and need for doing delete request with serve.
  /// This function have 1 required and 2 optional parameters:
  /// @param url
  /// @param headers
  /// @param body
  static Future<http.Response> _delete(url, {Map<String, String> headers}) async {

    try {

      if (checkRegisterRequest(url)) {
        return await processResponse(await http.delete(url, headers: headers), url);
      }

    } on SocketException catch (_) {
      AppService.changeStatusInternet(false);

    } catch (e) {

      // returns default error message
      return await processResponse(null, url);

    }

    return null;

  }

  /// This is function need for add params to header request.
  /// @param withToken
  /// @param withDeviceInfo
  /// @param useDeviceUid
  /// @param sendEventId
  static Map<String, String> defaultHeaders({
    bool withLanguage: true,
    bool withClientData: true,
    bool withToken: true,
    bool withDeviceInfo: false,
    bool withDevicePlatform: false,
    bool withLocalityIds: false
  }) {

    var headers;

    headers = <String, String>{

//        "content-type": "multipart/form-data",
      "content-type": "application/x-www-form-urlencoded",
//      "App-Version": AuthService.deviceInfo.appVersion,

    };

    if (withClientData) {

      headers["client_id"] = clientId;
      headers["client_secret"] = clientSecret;

    }

    if (withLanguage) {

      headers["Accept-Language"] = AppService.deviceInfoModel.languageCode;

    }

    if (withLocalityIds) {

      headers["locality_ids"] = AppService.deviceInfoModel.localityModelApi.idList;

    }

    if (withToken) {

      headers["authorization"] = "Bearer " + (AuthService.authModel.accessToken ?? '');

    }

    if (withDeviceInfo) {

      headers["device_uuid"] = AppService.deviceInfoModel.uuid;


    }

    if (withDevicePlatform) {

      headers["Device-Platform"] = Platform.isIOS ? 'IOS' : 'ANDROID';


    }

    return headers;

  }

  static Future<http.Response> processResponse(http.Response response, url, {bool showError: true}) async {


    registerRequest.remove(url);

    if (response == null) {

      return defaultErrorResponse();

    }

    AppService.changeStatusInternet(true);

    // TODO status 401 need check refreshToken if server return false need clear local data

    if (response.statusCode == 401) {

      // TODO send refreshToken
      AuthService.clearUserAndAuthData();

    }

    if (response.statusCode >= 500) {

      return defaultErrorResponse();

    } else if (response.statusCode >= 400) {

      // If you write print with parse response.body via jsonDecode, when response.body.length is not working: -\_(0_0)_/-
      if (response.body != null && response.body.length > 0) {

        Map jsonMap = Map.from(jsonDecode(response.body));

        if (jsonMap != null && jsonMap.length > 0 && showError) {

          jsonMap.forEach((key, error) {

            AppService.message(MessageModel(
              title: key,
              message: error[0] ?? 'Sorry, there was an error',
              backgroundColor: Colors.red,
              duration: Duration(seconds: 10),
            ));

          });

        }

      } else {

        return defaultErrorResponse();

      }

    } else {


    }

    return response;


  }

  static http.Response defaultErrorResponse() {

//    getIt.get<AppServiceImplement>().message(MessageModel(
//      title: 'Error',
//      message: 'An unexpected error occurred',
//      backgroundColor: Colors.red,
//    ));

    return null;

  }

  static bool checkRegisterRequest(url) {

    print(url);

    if (!registerRequest.contains(url)) {

      registerRequest.add(url);

      return true;

    } else {

      if (!AppService.internetIsConnected) {

        registerRequest.clear();

      }

    }

    return false;

  }

}
