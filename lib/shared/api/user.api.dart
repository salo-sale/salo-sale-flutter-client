import 'package:SaloSale/shared/api/request.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:http/http.dart';

class UserApi {

  static Future<Response> getCheckInvitationCode(code) async {

    return await RequestApi.get(AppService.remoteConfig.buildUrl('check-invitation-code') + code, headers: RequestApi.defaultHeaders(
      withToken: false,
      withDeviceInfo: false
    ));

  }

  static Future<Response> putUserModelApi(Map data) async {


    return await RequestApi.put(AppService.remoteConfig.buildUrl('user') + data['id'], headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ), body: data);

  }

  static Future<Response> postInvitationUserCode(data) async {

    return await RequestApi.post(AppService.remoteConfig.buildUrl('invitation'), headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ), body: data);

  }

  static Future<Response> postConfirmEmail(data) async {

    print(AppService.remoteConfig.buildUrl('confirm-email'));
    return await RequestApi.post(AppService.remoteConfig.buildUrl('confirm-email'), headers: RequestApi.defaultHeaders(
        withToken: false,
        withDeviceInfo: false
    ), body: data);

  }

  static Future<Response> getSendVerification() async {

    return await RequestApi.get(AppService.remoteConfig.buildUrl('send-verification'), headers: RequestApi.defaultHeaders(
        withToken: true,
        withDeviceInfo: false
    ));

  }

}
