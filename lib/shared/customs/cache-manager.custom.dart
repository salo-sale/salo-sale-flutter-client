import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;

class CacheManagerCustom extends BaseCacheManager {
  static const key = "SaloSaleMapMarkers";

  static CacheManagerCustom _instance;

  factory CacheManagerCustom() {
    if (_instance == null) {
      _instance = new CacheManagerCustom._();
    }
    return _instance;
  }

  CacheManagerCustom._() : super(key,
      maxAgeCacheObject: Duration(days: 7));

  Future<String> getFilePath() async {
    var directory = await getTemporaryDirectory();
    return path.join(directory.path, key);
  }
}