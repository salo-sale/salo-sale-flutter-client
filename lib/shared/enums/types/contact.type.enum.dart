enum ContactTypeEnum {
  NOTHING,
  PHONE,
  TELEGRAM,
  INSTAGRAM,
  FACEBOOK,
  WEBSITE,
  EMAIL,
  VK,
  SLACK,
  LIKEDIN,
  OTHER
}