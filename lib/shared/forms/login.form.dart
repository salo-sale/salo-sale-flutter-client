class LoginForm {

  String avatar;
  String email;
  String password;
  String localityId;
  String fcmToken;

  String platform;
  String systemVersion;
  String appVersion;
  String model;

  String firstName;
  String lastName;

  String socialId;

  String accessToken;
  String authSocialNetworkId;
  String checkedAllRequiredAgreements;

  LoginForm({

//    this.email = 'ivankarbashevskyi@gmail.com',
//    this.password = '08021996v',
    this.avatar,
    this.email,
    this.password,
    this.fcmToken,
    this.localityId = '3',

    this.lastName,
    this.firstName,
    this.socialId,

    this.platform,
    this.systemVersion,
    this.appVersion,
    this.model,
    this.accessToken,
    this.authSocialNetworkId,
    this.checkedAllRequiredAgreements,

  });

  Map<String, dynamic> toJsonStructure() => {

    'avatar': avatar,
    'email': email,
    'password': password,
    'localityId': localityId,
    'fcmToken': fcmToken,
    'socialId': socialId,
    'firstName': firstName,
    'lastName': lastName,

    'platform': platform,
    'systemVersion': systemVersion,
    'appVersion': appVersion,
    'model': model,
    'accessToken': accessToken,
    'authSocialNetworkId': authSocialNetworkId,
    'checkedAllRequiredAgreements': checkedAllRequiredAgreements,

  };

}