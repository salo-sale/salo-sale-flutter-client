import 'dart:convert';

import 'package:SaloSale/shared/api/auth.api.dart';
import 'package:SaloSale/shared/models/api/agreement.model.api.dart';

class RegistrationModel {

  String email;
  String password;
  String passwordRepeat;
  String invitationCode;
  final List<AgreementModelApi> agreementList = [];

  bool passwordObscure;
  bool passwordRepeatObscure;

  String fcmToken;
  String platform;
  String systemVersion;
  String appVersion;
  String model;

  int localityId;
  List<int> agreements;
  int withLogin;
  int checkedAllRequiredAgreements;

  RegistrationModel({

    this.email,
    this.invitationCode,
    this.password,
    this.passwordRepeat,

    this.localityId,
    this.agreements,
    this.withLogin = 0,
    this.checkedAllRequiredAgreements = 1, // Change to 0 if need in registration page show agreements

    this.passwordObscure = true,
    this.passwordRepeatObscure = true,

    this.fcmToken,
    this.platform,
    this.systemVersion,
    this.appVersion,
    this.model,

  });

  bool checkPasswordMatch() => password == passwordRepeat;

  bool valid() {

    if (email.isNotEmpty && password.isNotEmpty && passwordRepeat.isNotEmpty && checkPasswordMatch()) {

      if (checkedAllRequiredAgreements == 0) {

        if (agreementList.isNotEmpty && agreementList.length > 0) {

          if (agreementList.firstWhere((agreement) => agreement.isRequired && !agreement.checked, orElse: () => null) == null) {

            return true;

          }

        }

      }

    }

    return false;

  }

  Map<String, dynamic> toJsonStructure() => withLogin == 1 ? {

    'email': email,
    'password': password,
    'invitationCode': invitationCode ?? '',
    'withLogin': withLogin.toString(),
    'localityId': localityId.toString(),
    'appVersion': appVersion,
    'checkedAllRequiredAgreements': checkedAllRequiredAgreements.toString(),
    'model': model,

    'fcmToken': fcmToken,
    'platform': platform,
    'systemVersion': systemVersion,

  } : {

    'email': email,
    'password': password,
    'invitationCode': invitationCode,
    'agreements': agreements,
    'withLogin': withLogin.toString(),
    'checkedAllRequiredAgreements': checkedAllRequiredAgreements.toString(),
    'localityId': localityId.toString()

  };

  Future<List<AgreementModelApi>> getAgreementList({bool refresh: false}) async {

    if (agreementList.length == 0 || refresh) {

      await AuthApi.getAgreementListData().then((res) {

        if (res != null) {

          if (res.statusCode == 200) {

            List.from(jsonDecode(res.body)).forEach((agreement) {

              agreementList.add(AgreementModelApi.fromMap(agreement));

            });

          }

        }

      });

    }

    return agreementList;

  }

}