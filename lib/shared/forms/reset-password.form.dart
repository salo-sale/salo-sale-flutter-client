import 'dart:convert';

import 'package:SaloSale/shared/api/auth.api.dart';

class ResetPasswordModel {

  String email;
  String oldPassword;
  String password;
  String passwordRepeat;
  String code;

  bool showCodeInput;
  bool showPasswordInputs;
  bool showOldPasswordInput;

  bool oldPasswordObscure;
  bool passwordObscure;
  bool passwordRepeatObscure;

  ResetPasswordModel({

    this.email,
    this.code,
    this.oldPassword,
    this.password,
    this.passwordRepeat,

    this.showCodeInput = false,
    this.showOldPasswordInput = false,
    this.showPasswordInputs = false,

    this.oldPasswordObscure = true,
    this.passwordObscure = true,
    this.passwordRepeatObscure = true,

  });

  bool checkPasswordMatch() => password == passwordRepeat;

  Future<bool> postForgotPassword(String email) async {

    assert(email.isNotEmpty);

    bool result = false;

    await AuthApi.postForgotPassword({
      "email": email
    }).then((res) {

      if (res != null) {

        if (res.statusCode == 200) {

          result = jsonDecode(res.body) == 1;

        }

      }

    });

    return result;

  }

  Future<bool> getCheckPasswordResetCode(String code) async {

    assert(code.isNotEmpty);

    bool result = false;

    await AuthApi.getCheckPasswordResetCode(code).then((res) {

      if (res != null) {

        if (res.statusCode == 200) {

          result = jsonDecode(res.body) == 1;

        }

      }

    });

    return result;

  }

  Future<bool> postResetPassword(data) async {

    assert(data.isNotEmpty);

    bool result = false;

    await AuthApi.postResetPassword(data).then((res) {

      if (res != null) {

        if (res.statusCode == 200) {

          result = jsonDecode(res.body) == 1;

        }

      }

    });

    return result;

  }

  Future<bool> postChangePassword(data) async {

    assert(data.isNotEmpty);

    bool result = false;

    await AuthApi.postChangePassword(data).then((res) {

      if (res != null) {

        if (res.statusCode == 200) {

          result = jsonDecode(res.body) == 1;

        }

      }

    });

    return result;

  }

}