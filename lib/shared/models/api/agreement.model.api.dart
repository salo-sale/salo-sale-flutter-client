class AgreementModelApi {

  final int id;
  final String agreement;
  final int required;
  final bool checked;

  AgreementModelApi({
    this.id,
    this.agreement,
    this.checked = false,
    this.required
  });

  factory AgreementModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return AgreementModelApi(
      id: json['id'],
      agreement: json['agreement'],
      required: json['required'],
    );
  }

  bool get isRequired => required == 1;

}