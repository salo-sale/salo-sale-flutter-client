import 'dart:convert';

import 'package:SaloSale/shared/models/comment-user.model.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/models/reply.model.dart';
import 'package:SaloSale/shared/models/score.model.dart';
import 'package:SaloSale/shared/utils/tools.dart';

class CommentModelApi implements FilterStandard {

  final int id;
  final int status;
  double score;
  final List<ScoreModel> scores;
  final List<ReplyModel> replies;
  final String createdAt;
  final String updatedAt;
  String forType;
  int companyId;
  int offerId;
  final CommentUserModel user;
  final int numberOfReplies;
  final String comment;

  CommentModelApi({

    this.id,
    this.status,
    this.score = 0.0,
    this.createdAt,
    this.updatedAt,
    this.user,
    this.numberOfReplies,
    this.comment,
    this.scores,
    this.replies,
    this.forType,
    this.companyId,
    this.offerId,

  });

  factory CommentModelApi.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);

    return CommentModelApi(
      id: json['id'],
      status: json['status'],
      score: Tools.universalParse<double>(json['score']) ?? 0.0,
      createdAt: json['createdAt'],
      updatedAt: json['updatedAt'],
      user: CommentUserModel.fromMap(json['user']),
      numberOfReplies: json['numberOfReplies'],
      comment: json['comment'],
      forType: json['for'],
      companyId: json['companyId'],
      offerId: json['offerId'],
      scores: List.from(json['scores'] ?? []).map((score) => ScoreModel.fromMap(score)).toList(),
      replies: List.from(json['replies'] ?? []).map((reply) => ReplyModel.fromMap(reply)).toList(),
    );

  }

  Map<String, dynamic> toJson() => {

    "id": id,
    "status": status,
    "score": score,
    "for": forType,
    "companyId": companyId,
    "offerId": offerId,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
    "user": jsonEncode(user),
    "numberOfReplies": numberOfReplies,
    "comment": comment,
    "scores": jsonEncode(scores),
    "replies": jsonEncode(scores),

  };

}