import 'package:SaloSale/shared/models/api/comment.model.api.dart';
import 'package:SaloSale/shared/models/company-score.model.dart';
import 'package:SaloSale/shared/models/contact.model.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/models/follower.model.dart';
import 'package:SaloSale/shared/models/image-url.model.dart';
import 'package:SaloSale/shared/models/point-of-sale.model.dart';
import 'package:SaloSale/shared/models/tag.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';

class CompanyModelApi implements FilterStandard {

  final int id;
  final String message;
  final String description;
  final String name;
  final String shortName;
  final String motto;
  final String latinName;
  final String primaryColor;
  final String accessType;
  final double score;
  final List<CompanyScoreModel> scores;
  final int numberOfComments;
  final List<int> tagIdList;

  final List<CommentModelApi> comments;
  final List<ContactModel> contactList;
  final List<PointOfSaleModel> pointOfSaleList;

  int liked;
  final FollowerModel follower;
  final ImageUrlModel logoUrl;
  final ImageUrlModel bannerUrl;

  String tagNames;

  List<TagModel> tagList;

  List<String> accessTypeList = [];

  CompanyModelApi({
    this.id,
    this.message,
    this.description,
    this.name,
    this.shortName,
    this.motto,
    this.latinName,
    this.primaryColor,
    this.tagIdList,
    this.accessType,
    this.contactList,
    this.pointOfSaleList,
    this.liked = 0,
    this.accessTypeList,
    this.follower,
    this.score,
    this.comments,
    this.numberOfComments,
    this.logoUrl,
    this.bannerUrl,
    this.scores,
  });

  factory CompanyModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    List<String> accessTypeList = [];

    // TODO if company hove seb site from other city this user can see this company?

    if (json['accessType'] != null && json['accessType'].toString().length > 0) {
      switch(json['accessType']) {
        case "ONLINE":
          accessTypeList.add('web');
          break;
        case "OFFLINE":
          accessTypeList.add('on map');
          break;
        case "ONLINE_OFFLINE":
          accessTypeList.add('on map');
          accessTypeList.add('web');
          break;
      }
    }

    return CompanyModelApi(
      accessTypeList: accessTypeList,
      id: Tools.universalParse<int>(json['id']),
      name: json['name'],
      description: json['description'],
      message: json['message'],
      shortName: json['shortName'],
      motto: json['motto'],
      accessType: json['accessType'],
      latinName: json['latinName'],
      numberOfComments: Tools.universalParse<int>(json['numberOfComments']),
      score: Tools.universalParse<double>(json['score']),
      primaryColor: json['primaryColor'],
      scores: List.from(json['scores'] ?? []).map((score) => CompanyScoreModel.fromMap(score)).toList(),
      tagIdList: List<int>.from(json['tagIdList'] ?? []), // TODO Any parse and change on backend from int to string
      comments: List.from(json['comments'] ?? []).map((comment) => CommentModelApi.fromMap(comment)).toList(),
      contactList: List.from(json['contactList'] ?? []).map((contact) => ContactModel.fromMap(contact)).toList(),
      pointOfSaleList: List.from(json['pointOfSaleList'] ?? []).map((pointOfSale) => PointOfSaleModel.fromMap(pointOfSale)).toList(),
      liked: Tools.universalParse<int>(json['liked']) ?? 0,
      follower: FollowerModel.fromMap(json['follower'], required: false),
      logoUrl: ImageUrlModel.fromMap(json['logoUrl'], required: false),
      bannerUrl: ImageUrlModel.fromMap(json['bannerUrl'], required: false),
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'shortName': shortName,
    'latinName': latinName,
    'message': message,
    'description': description,
    'motto': motto,
    'liked': liked,
    'numberOfComments': numberOfComments,
    'score': score,
    'accessType': accessType,
    'follower': follower.toJson(),
    'tagIdList': tagIdList,
    'scores': scores.map((c) => c.toJson()).toList(),
    'comments': comments.map((c) => c.toJson()).toList(),
    'contactList': contactList.map((c) => c.toJson()).toList(),
    'pointOfSaleList': pointOfSaleList.map((e) => e.toJson()).toList(),

    'primaryColor': primaryColor,
    'logoUrl': logoUrl.toJson(),
    'bannerUrl': bannerUrl.toJson(),

  };

  Map<String, dynamic> toJsonStructureForCache() => {
    'id': id,
    'name': name,
    'description': description,
    'message': message,
    'shortName': shortName,
    'latinName': latinName,
    'accessType': accessType,
    'numberOfComments': numberOfComments,
    'score': score,

    'primaryColor': primaryColor,
    'logoUrl': logoUrl.toJson(),
    'bannerUrl': bannerUrl.toJson(),

  };

  String getTagNames() {

    if (tagNames == null) {

      if (tagIdList != null && tagIdList.length > 0 && AppService.listsOfTagsMap['company'] != null && AppService.listsOfTagsMap['company'].tagList.length > 0) {

        tagNames = '';

        for (int i = 0; i < tagIdList.length; i++) {

          tagNames += (i == 0 ? '' : ', ') + AppService.listsOfTagsMap['company'].tagList.firstWhere((element) => element.id == tagIdList[i], orElse: () => null)?.name ?? '';

        }

        if (tagNames.length == 0) {

          tagNames = null;

        }

      }

    }

    return tagNames;

  }

}