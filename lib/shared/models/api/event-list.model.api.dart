import 'package:SaloSale/shared/models/event.model.dart';
import 'package:SaloSale/shared/models/filter.model.dart';

class EventListModelApi implements FilterStandard {

  final String data;
  List<EventModel> models = [];

  EventListModelApi({
    this.data,
    this.models,
  });

  factory EventListModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return EventListModelApi(
      data: json['data'],
      models: List.from(json['models'] ?? []).map((model) => EventModel.fromMap(model)).toList(),
    );
  }

  Map<String, dynamic> toJson() => {

    'data': data,
    'models': models.map((e) => e.toJson()).toList(),

  };

}