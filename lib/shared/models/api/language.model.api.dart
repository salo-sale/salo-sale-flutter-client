class LanguageModelApi {

  final String code;
  final String name;
  final String country;

  LanguageModelApi({
    this.code,
    this.name,
    this.country,
  });

  factory LanguageModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return LanguageModelApi(
      code: json['code'],
      name: json['name'],
      country: json['country'],
    );
  }

  Map<String, dynamic> toJsonStructure() => {

    'code': code,
    'name': name,
    'country': country,

  };

}