import 'package:SaloSale/shared/enums/types/tag-list.type.enum.dart';
import 'package:SaloSale/shared/models/tag.model.dart';

class ListOfTagsModelApi {

  final String name;
  final TagListTypeEnum type;
  final List<TagModel> tagList;

  ListOfTagsModelApi({
    this.name,
    this.type,
    this.tagList,
  });

  factory ListOfTagsModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return ListOfTagsModelApi(
      name: json['name'],
      type: json['type'],
      tagList: List.from(json['tags'] ?? []).map((tag) => TagModel.fromMap(tag)).toList(),
    );
  }

  Map<String, dynamic> toJsonStructure() => {

    'type': type,
    'name': name,
    'tags': tagList.map((e) => e.toJsonStructure()).toList(),

  };

}