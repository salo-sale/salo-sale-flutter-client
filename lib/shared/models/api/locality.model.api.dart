class LocalityModelApi {

  final int id;
  final int cityId;
  final int regionId;
  final int countryId;
  final List<int> parentIdList;
  final String name;
  final double longitude;
  final double latitude;
  final int zoom;

  LocalityModelApi({
    this.id,
    this.cityId,
    this.regionId,
    this.countryId,
    this.parentIdList,
    this.longitude,
    this.latitude,
    this.zoom,
    this.name
  });

  factory LocalityModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);
    return LocalityModelApi(
      id: json['id'],
      name: json['name'],
      cityId: json['cityId'],
      regionId: json['regionId'],
      countryId: json['countryId'],
      longitude: double.tryParse(json['longitude'].toString()),
      latitude: double.tryParse(json['latitude'].toString()),
      zoom: json['zoom'],
      parentIdList: List.from(json['parentIdList'] ?? []),
    );
  }

  Map<String, dynamic> toJsonStructure() => {

    'id': id,
    'name': name,
    'cityId': cityId,
    'regionId': regionId,
    'countryId': countryId,
    'parentIdList': parentIdList,
    'longitude': longitude,
    'latitude': latitude,
    'zoom': zoom,

  };

  String get idList => "[" + parentIdList.join(",").toString() + ',' + id.toString() + "]";

}