import 'dart:ui';

import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/utils/hex.color.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:flutter/material.dart';

class LoyaltyCardModelApi implements FilterStandard {

  final int id;
  final int userId;
  int status;
  String value;
  String name;
  String comment;
  final String createdAt;
  final String updatedAt;
  Color color;

  bool isNewModel;
  bool isNotSaved;

  LoyaltyCardModelApi({

    this.id,
    this.userId,
    this.status,
    this.value,
    this.name,
    this.comment,
    this.createdAt,
    this.updatedAt,
    this.color = Colors.brown,
    this.isNewModel = true,
    this.isNotSaved = true,

  });

  factory LoyaltyCardModelApi.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);

    return LoyaltyCardModelApi(
      id: Tools.universalParse<int>(json['id']),
      userId: Tools.universalParse<int>(json['userId']),
      status: Tools.universalParse<int>(json['status']),
      value: json['value'],
      name: json['name'],
      comment: json['comment'],
      createdAt: json['createdAt'],
      updatedAt: json['updatedAt'],
      color: HexColor(json['color']),
      isNotSaved: Tools.universalParse<bool>(json['isNotSaved'], ifNullUseValue: 'false'),
      isNewModel: false
    );

  }

  Map<String, dynamic> toJson() => {

    "id": id,
    "userId": userId,
    "status": status,
    "value": value,
    "name": name,
    "comment": comment,
    "createdAt": createdAt,
    "updatedAt": updatedAt,
    "isNotSaved": isNotSaved,
    "color": color.value.toRadixString(16).padLeft(8, '0').substring(2),

  };

}