import 'package:SaloSale/shared/utils/tools.dart';

class NotificationModelApi {

  final int id;
  final int companyId;
  final String title;
  final String body;
  final String createdAt;
  final String updatedAt;
  final int access;
  int read;

  NotificationModelApi({
    this.id,
    this.companyId,
    this.title,
    this.body,
    this.read,
    this.createdAt,
    this.updatedAt,
    this.access,
  });

  factory NotificationModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return NotificationModelApi(
      id: Tools.universalParse<int>(json['id']),
      companyId: Tools.universalParse<int>(json['companyId']),
      title: Tools.universalParse<String>(json['title']),
      body: Tools.universalParse<String>(json['body']),
      read: Tools.universalParse<int>(json['read']) ?? 0,
      createdAt: Tools.universalParse<String>(json['createdAt']),
      updatedAt: Tools.universalParse<String>(json['updatedAt']),
      access: Tools.universalParse<int>(json['access']),
    );
  }

  Map<String, dynamic> toJson() => {
    "id": this.id,
    "companyId": this.companyId,
    "title": this.title,
    "body": this.body,
    "read": this.read,
    "createdAt": this.createdAt,
    "updatedAt": this.updatedAt,
    "access": this.access,
  };

}