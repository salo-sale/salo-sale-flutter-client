
import 'package:SaloSale/shared/models/api/comment.model.api.dart';
import 'package:SaloSale/shared/models/api/company.model.api.dart';
import 'package:SaloSale/shared/models/filter.model.dart';
import 'package:SaloSale/shared/models/language-offer.model.dart';
import 'package:SaloSale/shared/models/link.model.dart';
import 'package:SaloSale/shared/models/point-of-sale.model.dart';
import 'package:SaloSale/shared/models/tag.model.dart';
import 'package:SaloSale/shared/models/vilidity.model.dart';
import 'package:SaloSale/shared/utils/tools.dart';

class OfferModelApi implements FilterStandard {

  final int id;
  CompanyModelApi company;
  final int limit;
  final int limitForOneUser;
  final int price;
  final int canBuy;
  final int accessForUse;
  final int accessForUser;
  final int type; // 1 offer 2 coupon 3 banner 4 event
  final int ikointForTake;
  final int amountOfUses;
  final int useValidity;
  final int redeemed;
  String canTakeIkoint; // If null and user is logged user can take ikoint if have date need show when user can take ikoint.
  final String updatedAt;

  final LanguageOffer language;

  final List<String> tagIdList;
  final List<int> localityIdList;
  final List<String> codeList;
  final List<LinkModel> linkList;
  final List<PointOfSaleModel> pointOfSaleList;
  final List<ValidityModel> validityList;
  final List<CommentModelApi> commentList;
  final List<OfferModelApi> similarOfferList;

  List<TagModel> tagList;

  int liked;

  OfferModelApi({

    this.updatedAt,
    this.id,
    this.company,
    this.limit,
    this.canBuy = 0,
    this.limitForOneUser,
    this.price,
    this.redeemed,
    this.accessForUse,
    this.accessForUser,
    this.type,
    this.ikointForTake,
    this.useValidity,
    this.language,
    this.canTakeIkoint,
    this.tagIdList,
    this.amountOfUses,
    this.localityIdList,
    this.codeList,
    this.linkList,
    this.pointOfSaleList,
    this.commentList,
    this.validityList,
    this.similarOfferList,
    this.liked,

  });

  factory OfferModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return OfferModelApi(
      id: Tools.universalParse<int>(json['id']),
      company: json['company'] != null ? CompanyModelApi.fromMap(json['company']) : null,
      limit: json['limit'],
      limitForOneUser: json['limitForOneUser'],
      price: json['price'],
      canBuy: Tools.universalParse<int>(json['canBuy'], ifNullUseValue: '0'),
      amountOfUses: json['amountOfUses'],
      redeemed: json['redeemed'],
      accessForUse: json['accessForUse'],
      accessForUser: json['accessForUser'],
      type: json['type'],
      ikointForTake: json['ikointForTake'],
      useValidity: json['useValidity'],
      language: json['language'] != null ? LanguageOffer.fromMap(json['language']) : null,
      canTakeIkoint: json['canTakeIkoint'],
      tagIdList: List<String>.from(json['tagIdList'] ?? []),
      localityIdList: List<int>.from(json['localityIdList'] ?? []),
      codeList: List<String>.from(json['codeList'] ?? []),
      linkList: List.from(json['linkList'] ?? []).map((link) => LinkModel.fromMap(link)).toList(),
      updatedAt: json['updatedAt'],
      liked: Tools.universalParse<int>(json['liked']),

      validityList: List.from(json['validityList'] ?? []).map((validity) => ValidityModel.fromMap(validity)).toList(),
      pointOfSaleList: List.from(json['pointOfSaleList'] ?? []).map((pointOfSale) => PointOfSaleModel.fromMap(pointOfSale)).toList(),
      commentList: List.from(json['commentList'] ?? []).map((comment) => CommentModelApi.fromMap(comment)).toList(),
      similarOfferList: List.from(json['similarOfferList'] ?? []).map((offer) => OfferModelApi.fromMap(offer)).toList(),
    );

  }

  Map<String, dynamic> toJson() => {

    'updatedAt': updatedAt,
    'id': id,
    'company': company.toJson(),
    'limit': limit,
    'limitForOneUser': limitForOneUser,
    'price': price,
    'amountOfUses': amountOfUses,
    'redeemed': redeemed,
    'accessForUse': accessForUse,
    'accessForUser': accessForUser,
    'type': type,
    'canBuy': canBuy,
    'ikointForTake': ikointForTake,
    'useValidity': useValidity,
    'language': language?.toJson() ?? null,
    'canTakeIkoint': canTakeIkoint,
    'tagIdList': tagIdList,
    'localityIdList': localityIdList,
    'codeList': codeList,
    'linkList': linkList,
    'pointOfSaleList': pointOfSaleList.map((e) => e.toJson()).toList(),
    'commentList': commentList.map((e) => e.toJson()).toList(),
    'validityList': validityList.map((e) => e.toJson()).toList(),
    'similarOfferList': similarOfferList.map((e) => e.toJson()).toList(),
    'liked': liked,

  };

  Map<String, dynamic> toJsonStructureForCache() => {

    'id': id,
    'price': price,
    'redeemed': redeemed,
    'type': type,

    'company': company.toJsonStructureForCache(),
    'language': language.toJson(),

    'localityIdList': localityIdList,
    'codeList': codeList,
    'linkList': linkList,

  };

}