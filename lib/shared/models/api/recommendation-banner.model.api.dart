import 'package:SaloSale/shared/models/image-url.model.dart';

class RecommendationBannerModelApi {

  final int id;
  final int offerId;
  final int localityId;
  final int position;
  final ImageUrlModel bannerUrl;

  RecommendationBannerModelApi({
    this.id,
    this.offerId,
    this.localityId,
    this.position,
    this.bannerUrl,
  });

  factory RecommendationBannerModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return RecommendationBannerModelApi(
      id: json['id'],
      offerId: json['offerId'],
      position: json['position'],
      bannerUrl: ImageUrlModel.fromMap(json['bannerUrl']),
    );
  }

}