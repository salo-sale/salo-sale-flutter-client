import 'dart:io' show Platform;

import 'package:SaloSale/data.dart';
import 'package:SaloSale/shared/models/remote-config-api-control.model.dart';
import 'package:SaloSale/shared/models/remote-config-module-control.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';

class RemoteConfigModelApi {

  final Map<String, RemoteConfigApiControlModel> apiControl;
  final Map<String, RemoteConfigModuleControlModel> modulesControl;
  final Map<String, bool> componentsControl;
  final Map<String, String> links;
  final Map<String, String> routing;
  final Map<String, String> paths;
  final Map<String, Map<String, dynamic>> versionControl;

  final DateTime createdAt;

  final bool isEmpty;

  Map<String, dynamic> version;
  String minActiveVersion;
  String inactiveVersion;
  bool blockedInactiveVersion;
  List<String> unsupportedVersion;

  RemoteConfigModelApi({
    this.apiControl,
    paths,
    this.modulesControl,
    this.versionControl,
    this.minActiveVersion,
    this.links,
    this.routing,
    this.inactiveVersion,
    this.blockedInactiveVersion,
    this.componentsControl,
    this.isEmpty = true,
    createdAt,
    this.version,
  }) : paths = paths ?? defaultPaths, createdAt = createdAt ?? DateTime.now();

  factory RemoteConfigModelApi.fromMap(Map<String, dynamic> json, {required: true}) {

    if (json == null || json['apiControl'] == null) {

      if (required) {

        assert(json != null);

      }

      return RemoteConfigModelApi();

    }

    return RemoteConfigModelApi(
      apiControl: Map<String, Map<String, dynamic>>.from(json['apiControl']).map((key, value) => MapEntry(key, RemoteConfigApiControlModel.fromMap(value))),
      modulesControl: Map<String, Map<String, dynamic>>.from(json['modulesControl']).map((key, value) => MapEntry(key, RemoteConfigModuleControlModel.fromMap(value))),
      versionControl: Map<String, Map<String, dynamic>>.from(json['versionControl']),
      routing: json['routing'] != null ? Map<String, String>.from(json['routing']) : null,
      paths: json['paths'] != null ? Map<String, String>.from(json['paths']) : defaultPaths,
      links: Map<String, String>.from(json['links']),
      isEmpty: false,
      createdAt: json['createdAt'] != null ? DateTime.parse(json['createdAt']) : DateTime.now(),
      componentsControl: Map<String, bool>.from(json['componentsControl']),
    );
  }

  Map<String, dynamic> toJsonStructure() => {

    'apiControl': apiControl.map((key, value) => MapEntry(key, value.toJsonStructure())),
    'modulesControl': modulesControl.map((key, value) => MapEntry(key, value.toJsonStructure())),
    'versionControl': versionControl,
    'minActiveVersion': minActiveVersion,
    'inactiveVersion': inactiveVersion,
    'links': links,
    'paths': paths,
    'routing': routing,
    'createdAt': createdAt.toIso8601String(),
    'componentsControl': componentsControl,
    'unsupportedVersion': unsupportedVersion,
    'blockedInactiveVersion': blockedInactiveVersion,

  };

  String buildHost(String api) => isEmpty ? '$protocol//$host' : '${apiControl[api].protocol}//${apiControl[api].hostname}';

  initAllVariable() {

    if (!isEmpty) {

      version = versionControl[Platform.operatingSystem.toLowerCase()];
      minActiveVersion = version['minActiveVersion'];
      inactiveVersion = version['inactiveVersion'];
      blockedInactiveVersion = version['blockedInactiveVersion'];
      unsupportedVersion = List<String>.from(version['unsupportedVersion']);

    }

  }

  /// @return bool
  bool checkInactiveAppVersion() {

    if (!isEmpty) {

      final String appVersion = AppService.deviceInfoModel.appVersion.substring(1);

      if (int.parse(appVersion.split('.')[0]) < int.parse(inactiveVersion.split('.')[0])) {

        return true;

      } else if (int.parse(appVersion.split('.')[0]) == int.parse(inactiveVersion.split('.')[0])) {

        if (int.parse(appVersion.split('.')[1]) < int.parse(inactiveVersion.split('.')[1])) {

          return true;

        } else if (int.parse(appVersion.split('.')[1]) == int.parse(inactiveVersion.split('.')[1])) {

          if (int.parse(appVersion.split('.')[2]) <= int.parse(inactiveVersion.split('.')[2])) {

            return true;

          }

        }

      }

    }

    return false;

  }

  bool checkModule(String key) {

    if (!isEmpty) {

      if (modulesControl == null || modulesControl.isEmpty) {
        return true;
      }

      if (modulesControl.containsKey(key)) {

        return modulesControl[key].use;

      }

    }

    return false;

  }

  // More one day
  bool get checkExpires => DateTime.now().difference(createdAt).inDays > 0;

  bool checkComponent(String key) {

    if (componentsControl == null || componentsControl.isEmpty) {
      return true;
    }

    if (componentsControl.containsKey(key)) {

      return componentsControl[key];

    }

    return false;

  }

  checkIsSupportVersion(version) {

    return !unsupportedVersion.contains(version);

  }

  /// pn - pathname default value is variable with name pathname and is const
  String buildUrl(String path, {String subPath: '', String pn, String h}) => (h ?? (protocol + '//' + host)) + (pn ?? '') + (isEmpty ? defaultPaths[path] : (paths[path] ?? defaultPaths[path])) + (subPath.length > 0 ? '/' + subPath : '');

  buildUri(String path, {Map<String, String> json, String subPath}) => Uri.https(host, (isEmpty ? defaultPaths[path] : (paths[path] ?? defaultPaths[path])) + (subPath ?? ''), json);

}