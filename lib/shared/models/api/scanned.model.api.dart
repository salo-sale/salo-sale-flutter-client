import 'package:SaloSale/shared/models/api/offer.model.api.dart';
import 'package:SaloSale/shared/models/api/user.model.api.dart';
import 'package:SaloSale/shared/models/user-purchased-offer.model.dart';

class ScannedModelApi {

  final int cashierUserId;
  final int offerId;
  final int id;
  final int userPurchasedOfferId;
  final String userEmail;
  final int pointOfSaleId;
  final String pointOfSaleAddress;
  final int status;
  final OfferModelApi offer;
  final UserPurchasedOffer userPurchasedOffer;
  final String createdAt;
  final String updatedAt;
  final UserModelApi cashierUser;

  ScannedModelApi({

    this.cashierUserId,
    this.offerId,
    this.id,
    this.userPurchasedOfferId,
    this.pointOfSaleId,
    this.status,
    this.userEmail,
    this.pointOfSaleAddress,
    this.offer,
    this.userPurchasedOffer,
    this.updatedAt,
    this.createdAt,
    this.cashierUser,

  });

  factory ScannedModelApi.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return ScannedModelApi(
      cashierUserId: json['cashierUserId'],
      offerId: json['offerId'],
      id: json['id'],
      userPurchasedOffer: UserPurchasedOffer.fromMap(json['userPurchasedOffer']),
      offer: OfferModelApi.fromMap(json['offer']),
      userPurchasedOfferId: json['userPurchasedOfferId'],
      pointOfSaleId: json['pointOfSaleId'],
      status: json['status'],
      userEmail: json['userEmail'],
      pointOfSaleAddress: json['pointOfSaleAddress'],
      updatedAt: json['updatedAt'],
      createdAt: json['createdAt'],
      cashierUser: UserModelApi.fromMap(json['cashierUser']),
    );
  }

}