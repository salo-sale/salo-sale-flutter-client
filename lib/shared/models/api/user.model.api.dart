
import 'package:SaloSale/shared/enums/status/user.status.enum.dart';
import 'package:SaloSale/shared/models/image-url.model.dart';

class UserModelApi {

  final int id;
  String firstName;
  String lastName;
  String email;
  String login;
  String phone;
  String birthday;
  ImageUrlModel avatar;
  final int ikoint;
  int gender;
  int invitationUserId;
  final UserStatusEnum status;
  final List<String> roles;
  final List<String> invitationCodes;

  final bool isEmpty;

  DateTime initDate;

  UserModelApi({
    this.id,
    this.firstName,
    this.lastName,
    this.email,
    this.login,
    this.phone,
    this.birthday,
    this.ikoint,
    this.gender,
    this.status,
    this.avatar,
    this.roles,
    this.invitationUserId,
    this.invitationCodes,
    initDate,

    this.isEmpty = true,
  }) : this.initDate = initDate ?? DateTime.now();

  factory UserModelApi.fromMap(Map<dynamic, dynamic> json, {required: true}) {

    if (json == null || json['id'] == null) {

      if (required) {

        assert(json != null);

      }

      return UserModelApi();

    }

    return UserModelApi(
        id: json['id'],
        firstName: json['firstName'],
        lastName: json['lastName'],
        email: json['email'],
        login: json['login'],
        phone: json['phone'],
        birthday: json['birthday'],
        avatar: ImageUrlModel.fromMap(Map<String, dynamic>.from(json['avatar']), required: false),
        ikoint: json['ikoint'],
        gender: json['gender'],
        invitationUserId: json['invitationUserId'],
        status: UserStatusEnum.values[int.parse(json['status'].toString())],
        roles: List<String>.from(json['roles'] ?? []),
        invitationCodes: List<String>.from(json['invitationCodes'] ?? []),
        isEmpty: false,
        initDate: json['initDate'] != null ? DateTime.parse(json['initDate']) : null
    );
  }

  Map<String, dynamic> toJsonStructure() => {

    'id': id,
    'firstName': firstName,
    'lastName': lastName,
    'email': email,
    'login': login,
    'phone': phone,
    'birthday': birthday,
    'ikoint': ikoint,
    'avatar': avatar?.toJson(),
    'gender': gender,
    'invitationUserId': invitationUserId,
    'status': status != null ? status.index : null,
    'roles': roles,
    'invitationCodes': invitationCodes,
    'initDate': initDate.toIso8601String(),

  };

  bool get emailIsConfirmed => status == UserStatusEnum.EMAIL_CONFIRMED;

}