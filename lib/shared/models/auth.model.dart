import 'package:SaloSale/shared/enums/status/auth.status.enum.dart';

class AuthModel {

  int id;
  String accessToken;
  String refreshToken;
  String expiresIn;

  AuthStatusEnum authStatus;

  final bool isEmpty;

  bool get isUser => authStatus == AuthStatusEnum.USER;
  bool get isGuest => authStatus == AuthStatusEnum.GUEST;

  int showCompensationBanner;
  int compensationCount;

  AuthModel({

    this.id,
    this.accessToken,
    this.refreshToken,
    this.expiresIn,

    this.authStatus = AuthStatusEnum.NOT_DETERMINED,

    this.isEmpty = true,

    this.showCompensationBanner,
    this.compensationCount,

  });

  bool get isShowCompensationBanner => showCompensationBanner == 1;

  factory AuthModel.fromMap(Map<dynamic, dynamic> json, {required: true}) {

    if (json == null) {

      if (required) {

        assert(json != null);

      }

      return AuthModel();

    }

    return AuthModel(
      id: json['id'],
      accessToken: json['accessToken'],
      refreshToken: json['refreshToken'],
      expiresIn: json['expiresIn'],
      authStatus: AuthStatusEnum.values[json['authStatus'] ?? 0],
      isEmpty: false,
    );

  }

  Map<String, dynamic> toJsonStructure() => {

    'id': id,
    'accessToken': accessToken,
    'refreshToken': refreshToken,
    'expiresIn': expiresIn,
    'authStatus': authStatus.index,

  };

  bool expiresInIsActive() {

    assert(!isEmpty);
    assert(isUser);
    assert(expiresIn.isNotEmpty);

    return DateTime.parse(expiresIn).compareTo(DateTime.now()) == 1;

  }

  void initLoginData(data) {

    id = data['id'];
    accessToken = data['accessToken'];
    refreshToken = data['refreshToken'];
    expiresIn = data['expiresIn'];

  }

}