import 'package:SaloSale/shared/utils/tools.dart';

class CommentUserModel {

  final int id;
  final String login;

  CommentUserModel({

    this.id,
    this.login,

  });

  factory CommentUserModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);

    return CommentUserModel(
      id: Tools.universalParse<int>(json['id']),
      login: json['login'],
    );

  }

  Map<String, dynamic> toJson() => {

    'id': id,
    'login': login,

  };

}