

import 'package:SaloSale/shared/utils/tools.dart';

class CompanyScoreModel {

  int id;
  int companyId;
  int tagId;
  double score;

  CompanyScoreModel({
    this.id,
    this.companyId,
    this.tagId,
    this.score,
  });

  factory CompanyScoreModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);

    return CompanyScoreModel(
      id: Tools.universalParse<int>(json['id']),
      companyId: Tools.universalParse<int>(json['companyId']),
      tagId: Tools.universalParse<int>(json['tagId']),
      score: Tools.universalParse<double>(json['score']),
    );
  }

  Map<String, dynamic> toJson() => {

    'id': id,
    'companyId': companyId,
    'tagId': tagId,
    'score': score,

  };

}