import 'package:SaloSale/shared/enums/types/contact.type.enum.dart';
import 'package:SaloSale/shared/utils/tools.dart';

class ContactModel {

  int id;
  int hideValue;
  String name;
  String value;
  ContactTypeEnum type;

  ContactModel({

    this.id,
    this.hideValue,
    this.name,
    this.value,
    this.type,

  });

  factory ContactModel.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return ContactModel(
      id: Tools.universalParse<int>(json['id']),
      hideValue: Tools.universalParse<int>(json['hideValue']),
      name: Tools.universalParse<String>(json['name']),
      value: json['value'],
      type: ContactTypeEnum.values[Tools.universalParse<int>(json['type']) ?? (ContactTypeEnum.values.length - 1)],
    );

  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'hideValue': hideValue,
    'name': name,
    'value': value,
    'type': type.index,

  };

}