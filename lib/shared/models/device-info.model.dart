
import 'package:SaloSale/shared/models/api/locality.model.api.dart';

class DeviceInfoModel {

  final String appVersion;
  final String platform;
  final String androidInfo;
  final String versionInfo;
  final String model;
  final String systemVersion;
  final String uuid;
  final String iosInfo;
  final String fcmToken;

  final bool isFirstOpen;
  final bool isEmpty;
  bool helpIsClose;

  LocalityModelApi localityModelApi;

  String languageCode;

  DeviceInfoModel({

    this.appVersion,
    this.platform,
    this.androidInfo,
    this.versionInfo,
    this.model,
    this.systemVersion,
    this.uuid,
    this.iosInfo,
    this.languageCode,
    this.fcmToken,

    this.isEmpty = true,
    this.isFirstOpen = false,
    this.helpIsClose = false,

    this.localityModelApi,

  });


  factory DeviceInfoModel.fromMap(Map<dynamic, dynamic> json, {required: true}) {

    if (json == null) {

      if (required) {

        assert(json != null);

      }

      return DeviceInfoModel();

    }

    return DeviceInfoModel(
      appVersion: json['appVersion'],
      platform: json['platform'],
      androidInfo: json['androidInfo'],
      versionInfo: json['versionInfo'],
      model: json['model'],
      languageCode: json['languageCode'],
      systemVersion: json['systemVersion'],
      uuid: json['uuid'],
      iosInfo: json['iosInfo'],
      fcmToken: json['fcmToken'],
      localityModelApi: json['localityModelApi'] != null ? LocalityModelApi.fromMap(Map<String, dynamic>.from(json['localityModelApi'])) : null,
      isEmpty: false,
      isFirstOpen: json['isFirstOpen'],
      helpIsClose: json['helpIsClose'] ?? false
    );

  }

  Map<String, dynamic> toJsonStructure() => {

    'appVersion': appVersion,
    'platform': platform,
    'androidInfo': androidInfo,
    'versionInfo': versionInfo,
    'isFirstOpen': isFirstOpen,
    'model': model,
    'languageCode': languageCode,
    'systemVersion': systemVersion,
    'uuid': uuid,
    'iosInfo': iosInfo,
    'fcmToken': fcmToken,
    'helpIsClose': helpIsClose,
    'localityModelApi': localityModelApi != null ? localityModelApi.toJsonStructure() : null,

  };

}