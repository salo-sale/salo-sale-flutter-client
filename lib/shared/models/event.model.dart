import 'package:SaloSale/shared/models/image-url.model.dart';

class EventModel {

  String id;
  String companyName;
  String companyId;
  String openTime;
  String closeTime;
  String weekday;
  String canUse;
  String isRange;
  String isFrom;
  String validityDate;
  String logoUrl;

  // TODO title
  // TODO description

  final List<ImageUrlModel> bannerUrl;

  EventModel({

    this.id,
    this.companyName,
    this.companyId,
    this.openTime,
    this.closeTime,
    this.weekday,
    this.canUse,
    this.isRange,
    this.isFrom,
    this.validityDate,
    this.logoUrl,
    this.bannerUrl,

  });

  factory EventModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);

    return EventModel(
      id: json['id'],
      companyName: json['companyName'],
      companyId: json['companyId'],
      openTime: json['openTime'],
      closeTime: json['closeTime'],
      weekday: json['weekday'],
      canUse: json['canUse'],
      isRange: json['isRange'],
      isFrom: json['isFrom'],
      validityDate: json['validityDate'],
      bannerUrl: List.from(json['bannerUrl'] ?? []).map((banner) => ImageUrlModel.fromMap(banner)).toList(),
      logoUrl: json['logoUrl'],
    );

  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'companyName': companyName,
    'companyId': companyId,
    'openTime': openTime,
    'closeTime': closeTime,
    'weekday': weekday,
    'canUse': canUse,
    'isRange': isRange,
    'isFrom': isFrom,
    'validityDate': validityDate,
    'bannerUrl': bannerUrl.map((e) => e.toJson()).toList(),
    'logoUrl': logoUrl,
  };

}