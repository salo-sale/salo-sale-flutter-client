import 'dart:async';
import 'dart:convert';

import 'package:SaloSale/shared/enums/types/scroll-event.type.enum.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

abstract class FilterStandard {
  toJson() {

  }
}

//<T extends BasicModelApi,D extends BasicModelApi>
//class BasicModelApi {
//
//  final int id;
//  bool isNotSaved;
//  factory BasicModelApi.fromMap() {
//
//  }
//
//}

class FilterModel<T,D> {

  List<T> documentList;
  D document;

  final String hivePath;

  static const int defaultCount = 25;
  static const int defaultPage = 1;
  static const int defaultTotal = 0;

  final PublishSubject<D> documentChanel = PublishSubject<D>();
  final BehaviorSubject<List<T>> documentListChanel = BehaviorSubject<List<T>>();
  final BehaviorSubject<ScrollEventTypeEnum> scrollEvent = BehaviorSubject<ScrollEventTypeEnum>();

  List<int> tagIdList = List<int>();
  List<int> localityIdList;
  List<String> languageCodeList;

  int pointOfSaleId;
  int companyId;

  String search;
  String orderBy;
  String sort;
  int type;

  int page = defaultPage;
  int count;
  int total = defaultTotal;

  int myFollow;
  int myLike;
  int myOffers;

  bool totalIsLessCountPage;
  bool loader;

  FilterModel({

    @required this.hivePath,

    this.tagIdList,
    this.localityIdList,
    this.languageCodeList,
    this.pointOfSaleId,
    this.companyId,
    this.search,
    this.page = defaultPage,
    this.count = defaultCount,
    this.orderBy,
    this.sort,
    this.type,
    this.total = defaultTotal,
    this.myOffers,
    this.myLike,
    this.myFollow,

    this.totalIsLessCountPage = false,
    this.loader = false,

  });

  factory FilterModel.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return FilterModel(
        hivePath: json['hivePath'],
        tagIdList: List<int>.from(json['tagIdList'] ?? []),
        localityIdList: List<int>.from(json['localityIdList'] ?? []),
        languageCodeList: List<String>.from(json['languageCodeList'] ?? []),
        pointOfSaleId: Tools.universalParse<int>(json['pointOfSaleId']),
        companyId: Tools.universalParse<int>(json['companyId']),
        search: json['search'],
        page: Tools.universalParse<int>(json['page']) ?? defaultPage,
        count: Tools.universalParse<int>(json['count']) ?? defaultCount,
        orderBy: json['orderBy'],
        sort: json['sort'],
        type: Tools.universalParse<int>(json['type']),
        myFollow: Tools.universalParse<int>(json['myFollow']),
        myOffers: Tools.universalParse<int>(json['myOffers']),
        myLike: Tools.universalParse<int>(json['myLike']),
        total: Tools.universalParse<int>(json['total']) ?? 0,
        totalIsLessCountPage: (Tools.universalParse<int>(json['total']) ?? defaultTotal) <= ((Tools.universalParse<int>(json['count']) ?? defaultCount) * (Tools.universalParse<int>(json['page']) ?? defaultPage)),
    );

  }

  Map<String, dynamic> toJsonStructure() => {

    "hivePath": hivePath,
    "tagIdList": tagIdList,
    "localityIdList": localityIdList,
    "languageCodeList": languageCodeList,
    "pointOfSaleId": pointOfSaleId,
    "companyId": companyId,
    "search": search,
    "page": page,
    "count": count,
    "orderBy": orderBy,
    "sort": sort,
    "total": total,
    "myOffers": myOffers,
    "myLike": myLike,
    "myFollow": myFollow,
    "type": type,

  };

  Map<String, String> toJson() {

    var jsonStructure = toJsonStructure();

    jsonStructure.remove('total');
    jsonStructure.remove('hivePath');

    jsonStructure.removeWhere((key, value) => ["", null].contains(value));

    return jsonStructure.map((key, value) => MapEntry(key, value.toString()));

  }

  checkTotalForCountPage() {

    totalIsLessCountPage = (total <= (count * page));

  }

  dispose() {

    documentChanel.close();
    documentListChanel.close();
    scrollEvent.close();

  }

  void reset({bool clearDocument: true, bool refreshTagList: true}) {

    if (clearDocument) {
      document = null;
    }
    documentList = null;

    documentChanel.add(document);
    documentListChanel.add(documentList);

    total = defaultTotal;
    page = defaultPage;
    count = defaultCount;
    totalIsLessCountPage = false;
    loader = false;

    if (refreshTagList) {
      tagIdList = List<int>();
    }

    localityIdList = null;
    languageCodeList = null;
    pointOfSaleId = null;
    companyId = null;
    search = null;
    orderBy = null;
    sort = null;
    type = null;

    myOffers = null;
    myLike = null;
    myFollow = null;

  }

  Future<List> dataFromCache() async {
    return List.from(json.decode(await AppService.appBox.get(hivePath) ?? '[]'));
  }

  Future saveDataToCache({bool checkNotSavedData: false}) async {
    if (documentList != null && documentList.length > 0) {
      List documentListCustom = [];
      if (checkNotSavedData) {
        documentListCustom = await dataFromCache();
      }
      await AppService.appBox.put(hivePath, json.encode(documentList.map((e) {

        Map<String, dynamic> jsonCustom = (e as FilterStandard).toJson();
        if (checkNotSavedData && documentListCustom.length > 0) {
          Map<String, dynamic> documentCustom = documentListCustom.firstWhere((element) => element['id'] == jsonCustom['id'], orElse: () => null);
          if (documentCustom != null && documentCustom['isNotSaved']) {
            jsonCustom = documentCustom;
          }
        }

        return jsonCustom;

      }).toList()));
    }
  }

  beforeRequest() {

    loader = true;
    refreshScrollEvent();

  }

  processingRequest(dataList, dataTotal, {int count}) {

    if (count != null) {
      this.count = count;
    }

    if (dataList != null) {

      if (documentList != null && documentList.length > 0) {

        documentList.addAll(dataList);

      } else {

        documentList = dataList;

      }

    }

    total = Tools.universalParse<int>(dataTotal);
  //        filter = FilterModel.fromMap(filter.toJsonStructure());

    checkTotalForCountPage();

    if (!totalIsLessCountPage) {

      page += 1;

    }

    documentListChanel.sink.add(documentList);

  }

  afterRequest() {

    Timer(Duration(seconds: 1), () {

      loader = false;
      refreshScrollEvent();
      documentListChanel.sink.add(documentList);

    });

  }

  refreshScrollEvent() {

    if (loader) {

      scrollEvent.sink.add(ScrollEventTypeEnum.LOADER);

    } else {

      if (documentList != null) {

        if (documentList.length == 0) {

          scrollEvent.sink.add(ScrollEventTypeEnum.EMPTY);

        } else {

          if (totalIsLessCountPage) {

            scrollEvent.sink.add(ScrollEventTypeEnum.FINISH_DATA);

          } else {

            resetScrollEvent();

          }

        }

      } else {

        resetScrollEvent();

      }
      
    }

  }

  clearDocumentListChanel() {

    documentListChanel.add(null);
    resetScrollEvent();

  }

  resetScrollEvent() {

    scrollEvent.add(ScrollEventTypeEnum.NOTHING);

  }

}