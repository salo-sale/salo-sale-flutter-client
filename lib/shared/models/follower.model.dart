class FollowerModel {

  int followed;
  int subscribed;

  FollowerModel({
    this.followed,
    this.subscribed,
  });

  factory FollowerModel.fromMap(Map<String, dynamic> json, {required: true}) {

    if (json == null || json['followed'] == null) {

      if (required) {

        assert(json != null);

      }

      return FollowerModel();

    }

    return FollowerModel(
      followed: int.tryParse(json['followed'].toString() ?? ''),
      subscribed: int.tryParse(json['subscribed'].toString() ?? ''),
    );
  }

  Map<String, dynamic> toJson() => {
    'followed': followed,
    'subscribed': subscribed,

  };

}