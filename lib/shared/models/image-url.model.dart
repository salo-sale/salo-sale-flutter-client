class ImageUrlModel {

  String requestScheme;
  String host;
  String path;
  String fileName;

  bool isEmpty;

  ImageUrlModel({
    this.requestScheme,
    this.host,
    this.path,
    this.fileName,
    this.isEmpty = true,
  });

  factory ImageUrlModel.fromMap(Map<String, dynamic> json, {required: true}) {

    if (json == null || json['requestScheme'] == null) {

      if (required) {

        assert(json != null);

      }

      return ImageUrlModel();

    }

    return ImageUrlModel(
      requestScheme: json['requestScheme'],
      host: json['host'],
      path: json['path'],
      fileName: json['fileName'],
      isEmpty: false,
    );
  }

  Map<String, dynamic> toJson() => {

    'requestScheme': requestScheme,
    'host': host,
    'path': path,
    'fileName': fileName,

  };

  String get buildUrl => (requestScheme ?? '') + (host ?? '') + (path ?? '') + (fileName ?? '');

}