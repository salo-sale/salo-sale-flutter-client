import 'package:SaloSale/shared/models/image-url.model.dart';

class ImageModel {

  int id;
  int position;
  int main;
  ImageUrlModel url;

  ImageModel({
    this.id,
    this.position,
    this.main,
    this.url,
  });

  factory ImageModel.fromMap(Map<String, dynamic> json, {required: true}) {

    if (json == null) {

      if (required) {

        assert(json != null);

      }

      return ImageModel();

    }

    return ImageModel(
      id: json['id'],
      position: json['position'],
      main: json['main'],
      url: ImageUrlModel.fromMap(json['url']),
    );
  }

  Map<String, dynamic> toJson() => {

    'id': id,
    'position': position,
    'main': main,
    'url': url.toJson(),

  };

}