
import 'package:SaloSale/shared/models/image.model.dart';

class LanguageOffer {

  final String languageCode;
  final String title;
  final String description;
  final String descriptionRequirements;
  final List<ImageModel> images;

//  bannerUrl: json['bannerUrl'] != null ? List.from(json['bannerUrl']).map((banner) => ImageModel.fromMap(banner)).toList() : [],

  LanguageOffer({
    this.languageCode,
    this.title,
    this.description,
    this.descriptionRequirements,
    this.images,
  });

  factory LanguageOffer.fromMap(Map<String, dynamic> json, {required: true}) {

    if (json == null) {

      if (required) {

        assert(json != null);

      }

      return LanguageOffer();

    }

    return LanguageOffer(
      languageCode: json['languageCode'],
      title: json['title'],
      description: json['description'],
      descriptionRequirements: json['descriptionRequirements'],
      images: List.from(json['images'] ?? []).map((image) => ImageModel.fromMap(image)).toList(),
    );
  }

  Map<String, dynamic> toJson() => {

    'languageCode': languageCode,
    'title': title,
    'description': description,
    'descriptionRequirements': descriptionRequirements,
    'images': images.map((image) => image.toJson()).toList(),

  };

}