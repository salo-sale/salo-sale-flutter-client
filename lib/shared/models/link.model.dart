

class LinkModel {

  String link;
  String name;

  LinkModel({
    this.link,
    this.name,
  });

  factory LinkModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);
    return LinkModel(
      name: json['name'],
      link: json['link'],
    );
  }

  Map<String, dynamic> toJson() => {

    'link': link,
    'name': name,

  };

}