import 'dart:ui';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MessageModel {

  String title;
  String message;
  Color backgroundColor;
  Duration duration;
  FlatButton mainButton;
  bool withoutTranslate;
  FlushbarPosition flushbarPosition;

  MessageModel({
    this.title,
    this.message,
    this.backgroundColor,
    this.duration,
    this.mainButton,
    this.flushbarPosition = FlushbarPosition.BOTTOM,
    this.withoutTranslate = false
  });

}