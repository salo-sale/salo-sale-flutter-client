import 'package:SaloSale/shared/models/api/notification.model.api.dart';
import 'package:SaloSale/shared/utils/tools.dart';

class NotificationListModel {

  int numberOfUnreadMessages;
  List<NotificationModelApi> list = [];

  NotificationListModel({
    this.list,
    this.numberOfUnreadMessages = 0
  });

  factory NotificationListModel.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return NotificationListModel(
      numberOfUnreadMessages: Tools.universalParse<int>(json['numberOfUnreadMessages']),
      list: List<NotificationModelApi>.from(json['list'] ?? []),
    );

  }

}