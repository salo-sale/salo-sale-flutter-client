
import 'package:SaloSale/shared/models/api/offer.model.api.dart';

class DownloadOfferListCacheModel {

  final int userId;
  final List<OfferModelApi> offerList;
  final DateTime initDate;
  DateTime lastUpdate;
  final bool isNew;

  DownloadOfferListCacheModel({
    this.userId,
    List<OfferModelApi> offerList,
    DateTime initDate,
    this.isNew = true,
    this.lastUpdate,
  }) : offerList = offerList ?? [], initDate = initDate ?? DateTime.now();

  factory DownloadOfferListCacheModel.fromMap(Map<dynamic, dynamic> json, {required: true}) {

    if (json == null) {

      if (required) {

        assert(json != null);

      }

      return DownloadOfferListCacheModel();

    }

    return DownloadOfferListCacheModel(
      userId: json['userId'],
      isNew: false,
      offerList: List.from(json['offerList'] ?? []).map((model) => OfferModelApi.fromMap(model)).toList(),
      initDate: json['initDate'] != null ? DateTime.parse(json['initDate']) : DateTime.now(),
      lastUpdate: json['lastUpdate'] != null ? DateTime.parse(json['lastUpdate']) : DateTime.now(),
    );

  }

  Map<String, dynamic> toJsonStructure() => {

    'userId': userId,
    'initDate': initDate.toIso8601String(),
    'lastUpdate': lastUpdate.toIso8601String(),
    'offerList': offerList.map((e) => e.toJsonStructureForCache()).toList()

  };

  void setNewOffer(OfferModelApi offer) {

    lastUpdate = DateTime.now();
    var elementIndex = offerList.indexWhere((element) => element.id == offer.id);
    if (elementIndex >= 0) {
      offerList.removeAt(elementIndex);
    }

    offerList.add(offer);

  }

}