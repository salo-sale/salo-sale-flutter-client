import 'package:SaloSale/shared/models/contact.model.dart';
import 'package:SaloSale/shared/models/image-url.model.dart';
import 'package:SaloSale/shared/models/work-hours.model.dart';
import 'package:SaloSale/shared/utils/tools.dart';

class PointOfSaleModel {

  final int id;
  final int companyId;
  final int localityId;
  final String comment;
  final double longitude;
  final double latitude;
  final int position;
  final DateTime dateNow;
  final String address;
  final String openTime;
  final String closeTime;

  List<ContactModel> contactList;
  List<WorkHoursModel> workHours;

  String companyName;
  ImageUrlModel companyLogo;

  PointOfSaleModel({
    this.id,
    this.companyId,
    this.localityId,
    this.comment,
    this.longitude,
    this.latitude,
    this.position,
    this.address,
    this.contactList,
    this.dateNow,
    this.workHours,
    this.openTime,
    this.closeTime,
  });

  factory PointOfSaleModel.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return PointOfSaleModel(
      id: int.parse(json['id'].toString()),
      companyId: json['companyId'],
      localityId: json['localityId'],
      comment: json['comment'],
      longitude: Tools.universalParse<double>(json['longitude']),
      latitude: Tools.universalParse<double>(json['latitude']),
      position: json['position'],
      address: json['address'],
      dateNow: new DateTime.now(),
      openTime: json['openTime'],
      closeTime: json['closeTime'],
      contactList: List.from(json['contactList'] ?? []).map((contact) => ContactModel.fromMap(contact)).toList(),
      workHours: List.from(json['workHours'] ?? []).map((workHours) => WorkHoursModel.fromMap(workHours)).toList(),
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'companyId': companyId,
    'localityId': localityId,
    'comment': comment,
    'longitude': longitude,
    'latitude': latitude,
    'position': position,
    'address': address,
    'openTime': openTime,
    'closeTime': closeTime,
    'contactList': contactList.map((e) => e.toJson()).toList(),
    'workHours': workHours.map((e) => e.toJson()).toList(),

  };

  bool isOpen() {

    if (workHours == null || workHours.length == 0) {

      return false;

    }

    WorkHoursModel day = workHours.firstWhere((element) {
      return (element.weekday == 0 ? 7 : element.weekday) == dateNow.weekday;
    }, orElse: () => null);

    if (day == null || day.openTime == null || day.closeTime == null) {

      return false;

    }

    int openM = Tools.universalParse<int>(day.openTime.split(':')[1]);
    int openH = Tools.universalParse<int>(day.openTime.split(':')[0]);

    int closeM = Tools.universalParse<int>(day.closeTime.split(':')[1]);
    int closeH = Tools.universalParse<int>(day.closeTime.split(':')[0]);

    if (openH > closeH) {
      closeH += 24;
    }

    if (openH < dateNow.hour &&
        dateNow.hour < closeH) {

      return true;

    } else if (
      (openH == dateNow.hour && openM <= dateNow.minute) ||
          ((dateNow.hour == closeH) &&
              dateNow.minute <= closeM)
    ) {
      return true;

    }

    return false;

  }

}