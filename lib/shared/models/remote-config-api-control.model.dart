class RemoteConfigApiControlModel {

  final String name;
  final String port;
  final String protocol;
  final String pathname;
  final String hostname;
  final String search;
  final Map<String, String> paths;

  RemoteConfigApiControlModel({
    this.name,
    this.port,
    this.protocol,
    this.pathname,
    this.hostname,
    this.search,
    this.paths,
  });

  String href({String pn}) => protocol + '//' + hostname + (pn ?? pathname);

  factory RemoteConfigApiControlModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);

    return RemoteConfigApiControlModel(
      name: json['name'],
      port: json['port'],
      protocol: json['protocol'],
      pathname: json['pathname'],
      hostname: json['hostname'],
      search: json['search'],
      paths: Map<String, String>.from(json['paths'] ?? {}),
    );

  }

  Map<String, dynamic> toJsonStructure() => {

    'name': name,
    'port': port,
    'protocol': protocol,
    'pathname': pathname,
    'hostname': hostname,
    'search': search,
    'paths': paths,

  };

}