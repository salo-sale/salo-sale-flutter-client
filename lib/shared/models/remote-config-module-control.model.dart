class RemoteConfigModuleControlModel {

  final String name;
  final bool use;
  final bool useMap;
  final String apiControlName;
  final String defaultFileNameMapMarker;
  final Map<String, dynamic> defaultFilterValue;

  RemoteConfigModuleControlModel({
    this.name,
    this.use,
    this.useMap = false,
    this.defaultFileNameMapMarker,
    this.apiControlName,
    this.defaultFilterValue,
  });

  factory RemoteConfigModuleControlModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);

    return RemoteConfigModuleControlModel(
      name: json['name'],
      use: json['use'],
      useMap: json['useMap'] ?? false,
      defaultFileNameMapMarker: json['defaultFileNameMapMarker'],
      apiControlName: json['apiControlName'],
      defaultFilterValue: Map<String, dynamic>.from(json['defaultFilterValue'] ?? {}),
    );

  }

  Map<String, dynamic> toJsonStructure() => {

    'name': name,
    'use': use,
    'useMap': useMap,
    'defaultFileNameMapMarker': defaultFileNameMapMarker,
    'apiControlName': apiControlName,
    'defaultFilterValue': defaultFilterValue,

  };

}