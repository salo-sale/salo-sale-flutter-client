

import 'package:SaloSale/shared/models/comment-user.model.dart';
import 'package:SaloSale/shared/utils/tools.dart';

class ReplyModel {

  int id;
  int commentId;
  int userId;
  CommentUserModel user;
  String reply;
  int status;
  String createdAt;
  String updatedAt;

  ReplyModel({
    this.id,
    this.commentId,
    this.userId,
    this.user,
    this.reply,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  factory ReplyModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);
    return ReplyModel(
      id: Tools.universalParse<int>(json['id']),
      commentId: Tools.universalParse<int>(json['commentId']),
      userId: Tools.universalParse<int>(json['userId']),
      user: CommentUserModel.fromMap(json['user']),
      reply: json['reply'],
      status: Tools.universalParse<int>(json['status']),
      createdAt: json['createdAt'],
      updatedAt: json['updatedAt'],
    );
  }

  Map<String, dynamic> toJson() => {

    'id': id,
    'commentId': commentId,
    'userId': userId,
    'user': user.toJson(),
    'reply': reply,
    'status': status,
    'createdAt': createdAt,
    'updatedAt': updatedAt,

  };

}