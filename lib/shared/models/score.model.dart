

import 'package:SaloSale/shared/utils/tools.dart';

class ScoreModel {

  final int id;
  final int commentId;
  final int tagId;
  double score;
  int status;
  final String createdAt;
  final String updatedAt;

  ScoreModel({
    this.id,
    this.commentId,
    this.tagId,
    this.score = 0.0,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  factory ScoreModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);
    return ScoreModel(
      id: Tools.universalParse<int>(json['id']),
      commentId: Tools.universalParse<int>(json['commentId']),
      tagId: Tools.universalParse<int>(json['tagId']),
      score: Tools.universalParse<double>(json['score']) ?? 0.0,
      status: Tools.universalParse<int>(json['status']),
      createdAt: json['createdAt'],
      updatedAt: json['updatedAt'],
    );
  }

  Map<String, dynamic> toJson() => {

    'id': id,
    'commentId': commentId,
    'tagId': tagId,
    'score': score,
    'status': status,
    'createdAt': createdAt,
    'updatedAt': updatedAt,

  };

}