
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:flutter/widgets.dart';
import 'package:SaloSale/data.dart';

class TagModel {

  final int id;
  final String icon;
  final IconData iconData;
  final String name;
  final int main;
  final int position;

  TagModel({
    this.id,
    this.icon,
    this.iconData,
    this.name,
    this.main,
    this.position,
  });

  bool get isMain => main == 1;

  factory TagModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);

    return TagModel(
      id: Tools.universalParse<int>(json['id']),
      icon: json['icon'],
      name: json['name'],
      main: Tools.universalParse<int>(json['main']),
      position: Tools.universalParse<int>(json['position']),
      iconData: Data.getIconByName(json['icon'] ?? ''),
    );

  }

  Map<String, dynamic> toJsonStructure() => {

    'id': id,
    'name': name,
    'icon': icon,
    'main': main,
    'position': position,

  };

}