import 'package:SaloSale/shared/models/api/user.model.api.dart';

class UserPurchasedOffer {

  final String code;
  final int offerId;
  final int userId;
  final int status;
  final int id;
  final UserModelApi user;
  final String createdAt;
  final String updatedAt;

  UserPurchasedOffer({

    this.createdAt,
    this.offerId,
    this.id,
    this.updatedAt,
    this.user,
    this.status,
    this.userId,
    this.code,

  });

  factory UserPurchasedOffer.fromMap(Map<String, dynamic> json) {

    assert(json != null);

    return UserPurchasedOffer(
        code: json['code'],
        offerId: json['offerId'],
        userId: json['userId'],
        status: json['status'],
        id: json['id'],
        user: UserModelApi.fromMap(json['user']),
        createdAt: json['createdAt'],
        updatedAt: json['updatedAt'],
    );
  }

}