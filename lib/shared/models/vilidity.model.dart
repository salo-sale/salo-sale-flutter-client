import 'package:SaloSale/shared/utils/tools.dart';

class ValidityModel {

  int id;
  String openTime;
  String closeTime;
  int weekend;
  String validityDate;
  int canUse;
  int isRange;
  int isFrom;

  ValidityModel({
    this.id,
    this.openTime,
    this.closeTime,
    this.weekend,
    this.validityDate,
    this.canUse,
    this.isRange,
    this.isFrom,
  });

  factory ValidityModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);
    return ValidityModel(
      id: Tools.universalParse<int>(json['id']),
      openTime: json['openTime'],
      closeTime: json['closeTime'],
      weekend: Tools.universalParse<int>(json['weekend']),
      validityDate: json['validityDate'],
      canUse: Tools.universalParse<int>(json['canUse']),
      isRange: Tools.universalParse<int>(json['isRange']),
      isFrom: Tools.universalParse<int>(json['isFrom']),
    );
  }

  Map<String, dynamic> toJson() => {

    'id': id,
    'openTime': openTime,
    'closeTime': closeTime,
    'weekend': weekend,
    'validityDate': validityDate,
    'canUse': canUse,
    'isRange': isRange,
    'isFrom': isFrom,

  };

}