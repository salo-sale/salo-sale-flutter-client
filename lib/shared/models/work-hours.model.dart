import 'package:SaloSale/shared/utils/tools.dart';

class WorkHoursModel {

  int id;
  String openTime;
  String closeTime;
  int weekday;
  int canUse;
  int isRange;
  int isFrom;
  int workDate;

  WorkHoursModel({
    this.id,
    this.openTime,
    this.closeTime,
    this.weekday,
    this.workDate,
    this.canUse,
    this.isRange,
    this.isFrom,
  });

  factory WorkHoursModel.fromMap(Map<dynamic, dynamic> json) {

    assert(json != null);
    return WorkHoursModel(
      id: Tools.universalParse<int>(json['id']),
      openTime: json['openTime'],
      closeTime: json['closeTime'],
      weekday: Tools.universalParse<int>(json['weekday']),
      workDate: json['workDate'],
      canUse: Tools.universalParse<int>(json['canUse']),
      isRange: Tools.universalParse<int>(json['isRange']),
      isFrom: Tools.universalParse<int>(json['isFrom']),
    );
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'openTime': openTime,
    'closeTime': closeTime,
    'weekday': weekday,
    'workDate': workDate,
    'canUse': canUse,
    'isRange': isRange,
    'isFrom': isFrom,

  };

}