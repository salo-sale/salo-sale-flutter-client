import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:SaloSale/data.dart';
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/app.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/models/api/language.model.api.dart';
import 'package:SaloSale/shared/models/api/list-of-tags.model.api.dart';
import 'package:SaloSale/shared/models/api/locality.model.api.dart';
import 'package:SaloSale/shared/models/api/remote-config.model.api.dart';
import 'package:SaloSale/shared/models/device-info.model.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/services/firebase/messaging.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/transparent.overlay.component.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:package_info/package_info.dart';
import 'package:rxdart/rxdart.dart';
import 'package:uni_links/uni_links.dart';
import 'package:uuid/uuid.dart';

class AppService {
  static final FirebaseAnalytics analytics = FirebaseAnalytics();
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  static DeviceInfoModel deviceInfoModel;

  static StreamSubscription uniLinksStream;
  static Box appBox;

  static bool needChangeLanguage = false;
  static bool loaderWindowIsShow;
  static PackageInfo packageInfo;
  static Map<String, Flushbar> flushbarMap;
  static bool initialized = false;
  static int currentIndexPage = Data.startDefaultIndexPage;

  static List<ListOfTagsModelApi> listsOfTags;
  static Map<String, ListOfTagsModelApi> listsOfTagsMap;
  static List<LocalityModelApi> localityList;
  static RemoteConfigModelApi remoteConfig;

  static Map<String, LanguageModelApi> languageMap = {
    'uk': LanguageModelApi.fromMap({
      'code': 'uk',
      'country': 'UA',
      'name': 'Українська',
    }),
    'ru': LanguageModelApi.fromMap({
      'code': 'ru',
      'country': 'RU',
      'name': 'Русский',
    }),
    'en': LanguageModelApi.fromMap({
      'code': 'en',
      'country': 'GB',
      'name': 'English',
    }),
  };

  ///
  /// Chanel's
  ///

  static final PublishSubject<bool> refreshLayout = PublishSubject<bool>();
  static final PublishSubject<bool> refreshAdvertisingBanners =
      PublishSubject<bool>();
  static final PublishSubject<bool> refreshContent = PublishSubject<bool>();
  static final PublishSubject<bool> refreshTags = PublishSubject<bool>();
  static final PublishSubject<bool> scrollToTop = PublishSubject<bool>();
  static final PublishSubject<bool> disconnect = PublishSubject<bool>();

  static final BehaviorSubject<SystemStepsTypeEnum> systemSteps =
      BehaviorSubject<SystemStepsTypeEnum>()..add(SystemStepsTypeEnum.OPEN_APP);

  static bool internetIsConnected = true;

  static void dispose() {
    refreshLayout.close();
    refreshAdvertisingBanners.close();
    refreshContent.close();
    refreshTags.close();
    disconnect.close();
    uniLinksStream.cancel();
    systemSteps.close();
    scrollToTop.close();
  }

  ///
  /// Init app
  ///

  static Future<void> init({bool refresh = false}) async {
    if (!initialized) {
      initialized = true;
      loaderWindowIsShow = false;

      localityList = [];
      listsOfTags = [];
      listsOfTagsMap = {};

      flushbarMap = {};

      packageInfo = await PackageInfo.fromPlatform();

      await initDeviceInfo(); // init device information
      await checkInternetConnect(refresh: true); // Init remote config
      await initRemoteConfig(); // init remote config
      await initAppData(); // init app data
      await changeLanguage(deviceInfoModel.languageCode,
          showLoader: false, refreshAllLayout: false);
      if (deviceInfoModel.localityModelApi == null &&
          localityList != null &&
          localityList.length > 0) {
        await changeLocality(
            localityList.firstWhere((element) => element.id == 3),
            showLoader: false);
      }

      // Added guest to fcm topic client_$platform_name and send to analytics app is open
      if (internetIsConnected) {
        await analytics.logAppOpen();
        await FirebaseMessagingService.firebaseMessaging.subscribeToTopic(
            'client_${deviceInfoModel.platform.toLowerCase()}');
      }
    }

    try {
      if (remoteConfig.checkInactiveAppVersion()) {
        await initRemoteConfig(refresh: true);
      }

      if (remoteConfig.checkInactiveAppVersion()) {
        Timer(Duration(seconds: 2), () {
          showOnTransparent(
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    FlutterI18n.translate(pageNavigatorKey.currentContext,
                        'Please update the mobile application'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext,
                          'Your version is no longer up-to-date'),
                      style: TextStyle(color: Colors.black),
                    ),
                  )
                ],
              ),
              hideCloseButton: remoteConfig.blockedInactiveVersion);
        });
      }
    } catch (e) {
      AppService.systemSteps.add(SystemStepsTypeEnum.REFRESH_APP);
    }

    if (refresh) {
      AppService.refreshAdvertisingBanners.sink.add(true);
      AppService.refreshContent.sink.add(true);
      AppService.refreshTags.sink.add(true);
    }
  }

  static Future<void> closeHelpPage() async {
    deviceInfoModel.helpIsClose = true;
    await putAppBox('deviceInfo', deviceInfoModel.toJsonStructure());
    await AuthService.updateAuthStatusChanel();
  }

  static String message(MessageModel messageModel) {
    String uuidString = Uuid().v4();
    flushbarMap.putIfAbsent(
        uuidString,
        () => Flushbar(
            borderRadius: 5.0,
            flushbarPosition: messageModel.flushbarPosition,
            margin:
                const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
            title: messageModel.withoutTranslate
                ? messageModel.title
                : FlutterI18n.translate(
                    pageNavigatorKey.currentContext, messageModel.title),
            messageText: Html(
              data: messageModel.withoutTranslate
                  ? messageModel.message
                  : FlutterI18n.translate(
                  pageNavigatorKey.currentContext, messageModel.message),
              defaultTextStyle: messageModel.backgroundColor == Colors.red ? TextStyle(
                color: Colors.white
              ) : TextStyle(
                  color: Colors.black
              ),
              linkStyle: messageModel.backgroundColor == Colors.red ? const TextStyle(
                color: Colors.yellowAccent,
              ) : const TextStyle(
                color: Colors.orange,
              ),
              onLinkTap: Tools.launchUrl,
            ),
            backgroundColor: messageModel.backgroundColor ?? Colors.red,
            duration: messageModel.duration ?? Duration(seconds: 5),
            mainButton: messageModel.mainButton ??
                FlatButton(
                  onPressed: () {
                    flushbarMap[uuidString].dismiss(true);
                  },
                  child: Icon(
                    FontAwesomeIcons.times,
                    color: Colors.white,
                  ),
                )));

    flushbarMap[uuidString].show(pageNavigatorKey.currentContext);

    return uuidString;
  }

  static void closeFlushbar(uuidString) {
    flushbarMap[uuidString].dismiss(true);
  }

  static void showOnTransparent(Widget child, {bool hideCloseButton = false}) {
    pageNavigatorKey.currentState.push(TransparentOverlayComponent(
        child: child, hideCloseButton: hideCloseButton));
  }

  static Future<bool> changeLocality(LocalityModelApi locality,
      {bool showLoader: true}) async {
    if (showLoader) {
      showLoaderWindow();
    }

    bool result = false;

    if (deviceInfoModel.localityModelApi != null && internetIsConnected) {
      await FirebaseMessagingService.firebaseMessaging.unsubscribeFromTopic(
          'locality_${deviceInfoModel.localityModelApi.id}');
    }
    deviceInfoModel.localityModelApi = locality;
    if (internetIsConnected) {
      await FirebaseMessagingService.firebaseMessaging
          .subscribeToTopic('locality_${deviceInfoModel.localityModelApi.id}');
    }

    await putAppBox('deviceInfo', deviceInfoModel.toJsonStructure());
//    appBox.put(CacheNameEnum.AUTH_MODEL.index.toString(), authModel.toJsonStructure());
    refreshLayout.sink.add(true);

    if (showLoader) {
      closeLoaderWindow();
    }

    return Future<bool>.value(result);
  }

  static Future<bool> changeLanguage(String languageCode,
      {bool showLoader = true, bool refreshAllLayout = true}) async {
    bool result = false;

    if (pageNavigatorKey.currentContext != null) {
      if (languageCode != null) {
        if (showLoader) {
          showLoaderWindow();
        }

        if (internetIsConnected) {
          await FirebaseMessagingService.firebaseMessaging
              .unsubscribeFromTopic('language_${deviceInfoModel.languageCode}');
        }

        deviceInfoModel.languageCode = languageCode;

        if (internetIsConnected) {
          await FirebaseMessagingService.firebaseMessaging
              .subscribeToTopic('language_${deviceInfoModel.languageCode}');
        }

        await FlutterI18n.refresh(
            pageNavigatorKey.currentContext, Locale(languageCode));
        await initAppData();
        if (deviceInfoModel.localityModelApi != null) {
          deviceInfoModel.localityModelApi = localityList.firstWhere(
              (locality) => locality.id == deviceInfoModel.localityModelApi.id);
        } else {
          await initLocalities();
        }

        if (refreshAllLayout) {
          refreshLayout.sink.add(true);
        }

        if (showLoader) {
          closeLoaderWindow();
        }

        await putAppBox('deviceInfo', deviceInfoModel.toJsonStructure());
      }
    } else {
      needChangeLanguage = true;
    }

    return Future<bool>.value(result);
  }

  static void showLoaderWindow() {
    if (!loaderWindowIsShow) {
      loaderWindowIsShow = true;

      // TODO currentContext check if is null

      showOnTransparent(
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              LoaderComponent(
                  refreshFunction: closeLoaderWindow, withTimer: false),
              Text(FlutterI18n.translate(
                  pageNavigatorKey.currentContext, 'Loading and three dots'))
            ],
          ),
          hideCloseButton: true);
    }
  }

  static void closeLoaderWindow() {
    if (loaderWindowIsShow) {
      pageNavigatorKey.currentState.pop();
      loaderWindowIsShow = false;
    }
  }

  static void showModalNeedRegistration(String message) {
    showOnTransparent(Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            FlutterI18n.translate(pageNavigatorKey.currentContext, message),
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black),
          ),
        ),
        FlatButton(
          onPressed: () {
//            Navigator.pop(pageNavigatorKey.currentContext);
            AuthService.logout();
          },
          child: Text(
            FlutterI18n.translate(
                pageNavigatorKey.currentContext, 'Go to login'),
            style: TextStyle(color: Colors.orange),
          ),
        )
      ],
    ));
  }

  static Future<List<ListOfTagsModelApi>> initListOfTags(
      {bool refresh: false}) async {
    if (listsOfTags.length == 0 || refresh) {
      listsOfTags =
          List.from(json.decode(await getAppBox('listsOfTags') ?? 'null') ?? [])
              .map((list) => ListOfTagsModelApi.fromMap(list))
              .toList();

      if (internetIsConnected) {
        await AppApi.getListOfTags().then((res) async {
          if (res != null && res.statusCode == 200) {
            listsOfTags = [];
            listsOfTags.addAll(List.from(jsonDecode(res.body))
                .map((list) => ListOfTagsModelApi.fromMap(list)));
            await putAppBox(
                'listsOfTags',
                json.encode(
                    listsOfTags.map((e) => e.toJsonStructure()).toList()));
          }

          refreshTags.sink.add(true);
        });
      }
    }

    return listsOfTags;
  }

  static Future<List<LocalityModelApi>> initLocalities(
      {bool refresh: false}) async {
    if (localityList.length == 0 || refresh) {
      localityList = List.from(
              json.decode(await getAppBox('localityList') ?? 'null') ?? [])
          .map((locality) => LocalityModelApi.fromMap(locality))
          .toList();

      if (internetIsConnected) {
        await AppApi.getLocalities().then((res) async {
          if (res != null && res.statusCode == 200) {
            localityList = List.from(jsonDecode(res.body))
                .map((locality) => LocalityModelApi.fromMap(locality))
                .toList();

            if (deviceInfoModel.localityModelApi == null) {
              deviceInfoModel.localityModelApi = localityList.first;
            }

            await putAppBox(
                'localityList',
                json.encode(
                    localityList.map((e) => e.toJsonStructure()).toList()));
          }
        });
      }
    }

    return localityList;
  }

  /// This function need for check internet connect!
  static Future<RemoteConfigModelApi> initRemoteConfig(
      {bool refresh: false}) async {
    if (remoteConfig == null || !internetIsConnected) {
      var data = await getAppBox('remoteConfig');
      remoteConfig = RemoteConfigModelApi.fromMap(json.decode(data ?? 'null'),
          required: false);
      if (!remoteConfig.isEmpty) {
        remoteConfig.initAllVariable();
      }
    }

    if (remoteConfig.isEmpty ||
        (remoteConfig != null && remoteConfig.checkExpires) ||
        refresh) {
      await AppApi.getRemoteConfig().then((res) async {
        if (res != null && res.statusCode == 200) {
          remoteConfig = RemoteConfigModelApi.fromMap(jsonDecode(res.body));
          remoteConfig.initAllVariable();
          await putAppBox(
              'remoteConfig', json.encode(remoteConfig.toJsonStructure()));
        }
      });
    }

    return remoteConfig;
  }

  /// This function need for check internet connect!
  static Future<bool> getPong() async {
    bool result = false;

    await AppApi.getPong().then((res) async {
      if (res != null && res.statusCode == 200) {
        result = jsonDecode(res.body) == 1;
      }
    });

    internetIsConnected = result;

    return internetIsConnected;
  }

  static initAppData({int index: 0}) async {
    localityList = [];
    listsOfTags = [];

    if (internetIsConnected) {
      await AppApi.getAppData().then((res) async {
        if (res != null && res.statusCode == 200) {
          var appData = jsonDecode(res.body);

          localityList.addAll(List.from(appData['localities'] ?? [])
              .map((locality) => LocalityModelApi.fromMap(locality)));
          listsOfTags.addAll(List.from(appData['listsOfTags'] ?? [])
              .map((list) => ListOfTagsModelApi.fromMap(list)));

          await putAppBox(
              'localityList',
              json.encode(
                  localityList.map((e) => e.toJsonStructure()).toList()));
          await putAppBox(
              'listsOfTags',
              json.encode(
                  listsOfTags.map((e) => e.toJsonStructure()).toList()));
        }
      });
    } else {
      await initListOfTags();
      await initLocalities();
    }

    initListOfTagsMap();
  }

  static void changeStatusInternet(bool status) {
    disconnect.sink.add(!status);
    internetIsConnected = status;
  }

  static Future<bool> checkInternetConnect({bool refresh: false}) async {
    if (!internetIsConnected || refresh) {
      await getPong();
    }

    return internetIsConnected;
  }

  static initUniLinks() {
    // Initialize uni_links
    if (uniLinksStream == null) {
      // Attach a listener to the stream
      uniLinksStream = getUriLinksStream().listen((Uri uri) {
        // Use the uri and warn the user, if it is not correct
//        var list = uri.queryParametersAll.entries.toList();
        var paths = uri.path.split('/');

//        print(uri);
//        print(uri.path);
//        print(uri.query);
//        print(uri.queryParameters);
//        print(uri.scheme);

        var id = paths.length > 1 ? paths[2].toString() : 0;
//        print(id);
//        print(paths[1]);
//        print((id != 0 ? '/$id' : ''));
//        print('---------------');

        pageNavigatorKey.currentState
            .pushNamed(paths[1] + (id != 0 ? '/$id' : ''));
      }, onError: (err) {
        // Handle exception by warning the user their action did not succeed
      });
    }
  }

  ///
  /// Init device information, if data from cache is null its first open
  ///
  static Future initDeviceInfo() async {
    // get device information from app box
    var appBoxDeviceInfo = await getAppBox('deviceInfo');

    if (deviceInfoModel != null) {
      appBoxDeviceInfo['helpIsClose'] = deviceInfoModel.helpIsClose;
    }

    deviceInfoModel =
        DeviceInfoModel.fromMap(appBoxDeviceInfo, required: false);

    if (deviceInfoModel.languageCode == null) {
      deviceInfoModel.languageCode = Platform.localeName.substring(0, 2);
    }

    if (deviceInfoModel.isEmpty ||
        deviceInfoModel.appVersion.substring(1) != packageInfo.version) {
      Map info = Map();
      info['appVersion'] = packageInfo.version;
      info['isFirstOpen'] = true;
      info['languageCode'] = deviceInfoModel.languageCode;

      if (internetIsConnected) {
        await FirebaseMessagingService.firebaseMessaging
            .getToken()
            .then((token) {
          info['fcmToken'] = token;
        });
      }

      if (Platform.isAndroid) {
        info['appVersion'] = 'a' + info['appVersion'];
        info['platform'] = 'ANDROID';
        var androidInfo = await deviceInfoPlugin.androidInfo;
        var versionInfo = androidInfo.version;
        info['model'] = [androidInfo.brand, androidInfo.model].join(";");
        info['systemVersion'] = versionInfo.release;
        info['uuid'] = androidInfo.androidId;
      } else if (Platform.isIOS) {
        info['appVersion'] = 'i' + info['appVersion'];
        info['platform'] = 'IOS';
        var iosInfo = await deviceInfoPlugin.iosInfo;
        info['model'] = iosInfo.utsname.machine;
        info['systemVersion'] =
            [iosInfo.systemName, iosInfo.systemVersion].join(";");
        info['uuid'] = iosInfo.identifierForVendor;
      }

      deviceInfoModel = DeviceInfoModel.fromMap(info);
    }

    await putAppBox('deviceInfo', deviceInfoModel.toJsonStructure());
  }

  // App Box

  static Future<void> initAppBox() async {
    if (appBox == null) {
      await Hive.initFlutter();
      appBox = await Hive.openBox("appBox");
    }
  }

  static getAppBox(String string) async {
    await initAppBox();

    return appBox.get(string);
  }

  static putAppBox(String string, data) async {
    await initAppBox();

    return appBox.put(string, data);
  }

  static void initListOfTagsMap() {
    if (listsOfTags != null && listsOfTags.length > 0) {
      listsOfTagsMap = {
        'company':
            listsOfTags.firstWhere((element) => element.name == 'company'),
        'offer': listsOfTags.firstWhere((element) => element.name == 'offer'),
        'event': listsOfTags.firstWhere((element) => element.name == 'event'),
        'score': listsOfTags.firstWhere((element) => element.name == 'score'),
      };
    }
  }
}
