import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:SaloSale/shared/api/auth.api.dart';
import 'package:SaloSale/shared/api/notification.api.dart';
import 'package:SaloSale/shared/api/user.api.dart';
import 'package:SaloSale/shared/enums/cache-name.enum.dart';
import 'package:SaloSale/shared/enums/status/auth.status.enum.dart';
import 'package:SaloSale/shared/forms/login.form.dart';
import 'package:SaloSale/shared/forms/registration.form.dart';
import 'package:SaloSale/shared/models/api/offer.model.api.dart';
import 'package:SaloSale/shared/models/api/user.model.api.dart';
import 'package:SaloSale/shared/models/auth.model.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/models/offer-list-cashe.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/firebase/messaging.service.dart';
import 'package:flutter/material.dart';
import "package:rxdart/rxdart.dart";

class AuthService {
  static bool initialized = false;

  ///
  /// Finish static methods and variables
  ///

  static BehaviorSubject<AuthStatusEnum> authStatusChanel =
      BehaviorSubject<AuthStatusEnum>();
  static PublishSubject<bool> refreshIkointCounter = PublishSubject<bool>();

  static DownloadOfferListCacheModel offerCacheModel =
      DownloadOfferListCacheModel(userId: AuthService.userModelApi.id);
  static AuthModel authModel = AuthModel();
  static UserModelApi userModelApi = UserModelApi();

  static Future<void> init() async {
    if (!initialized) {
      initialized = true;

      authModel = AuthModel.fromMap(
          await AppService.getAppBox(CacheNameEnum.AUTH_MODEL.index.toString()),
          required: false);

      userModelApi = UserModelApi.fromMap(
          await AppService.getAppBox(
              CacheNameEnum.USER_MODEL_API.index.toString()),
          required: false);

      /// IF IS NEW OBJECT
      if (authModel.isEmpty) {
        authModel.authStatus = AuthStatusEnum.GUEST;

        if (AppService.internetIsConnected) {
          await FirebaseMessagingService.firebaseMessaging
              .subscribeToTopic('no_authorization')
              .then((_) async {
            AppService.analytics.logLogin(loginMethod: 'LOGIN_LIKE_GUEST');
          });
        }
      } else if (authModel.isUser) {
        try {
          /// CHECK EXPIRES IN IF IS ACTIVE
          if (authModel.expiresInIsActive()) {
            authModel.authStatus = AuthStatusEnum.USER;

            if (userModelApi.isEmpty) {
              await initUserApiModel();
            }

            initOfferCache();
            NotificationApi.getCheckUnread();
          } else {
            await refreshToken();
          }
        } catch (e) {
          authModel.authStatus = AuthStatusEnum.GUEST;
        }
      }

      updateAuthStatusChanel();
    }
  }

  static Future<bool> refreshToken() {
    // TODO request to change refreshToke and change authStatus in authModel.

    return Future.value(false);
  }

  static Future<bool> login(LoginForm loginForm,
      {bool socialNetWork: false}) async {
    bool success = false;

    loginForm.platform = Platform.isIOS ? 'IOS' : 'ANDROID';
    loginForm.fcmToken =
        await FirebaseMessagingService.firebaseMessaging.getToken();

    if (AppService.deviceInfoModel != null) {
      loginForm.systemVersion = AppService.deviceInfoModel.systemVersion;
      loginForm.appVersion = AppService.deviceInfoModel.appVersion;
      loginForm.model = AppService.deviceInfoModel.model;
    }

    var data = loginForm.toJsonStructure();

    if (socialNetWork) {
      data.remove('password');

      if (data['email'] == null) {
        data.remove('email');
      }

      if (data['firstName'] == null) {
        data.remove('firstName');
      }

      if (data['avatar'] == null) {
        data.remove('avatar');
      }

      if (data['lastName'] == null) {
        data.remove('lastName');
      }

      if (data['authSocialNetworkId'] == '1') {
        data.remove('email');
        data.remove('firstName');
        data.remove('lastName');
        data.remove('socialId');
        data.remove('avatar');
      }

      if (data['authSocialNetworkId'] == '2') {
        data.remove('avatar');
      }

    } else {
      data.remove('firstName');
      data.remove('lastName');
      data.remove('socialId');
      data.remove('avatar');
      data.remove('accessToken');
      data.remove('authSocialNetworkId');
      data.remove('checkedAllRequiredAgreements');
    }

    print(data);

    await AuthApi.postLoginData(data, socialNetWork: socialNetWork)
        .then((res) async {

      if (res != null) {

        if (res.statusCode >= 200 && res.statusCode < 300) {

          Map<String, dynamic> loginData = Map<String, dynamic>.from(json.decode(res.body));

          await FirebaseMessagingService.firebaseMessaging
              .unsubscribeFromTopic('no_authorization')
              .then((_) {
            AppService.analytics.logLogin(loginMethod: 'LOGOUT_LIKE_USER');
          });

          // TODO showCompensationBanner with info compensation count


          if (loginData.containsKey('loginData')) {
            loginData = loginData['loginData'];
          }

          authModel = AuthModel.fromMap(loginData);
          authModel.authStatus = AuthStatusEnum.USER;

          await FirebaseMessagingService.firebaseMessaging
              .subscribeToTopic('user_' + authModel.id.toString())
              .then((_) async {
            await AppService.putAppBox(CacheNameEnum.AUTH_MODEL.index.toString(),
                authModel.toJsonStructure());
            await initUserApiModel();
            authStatusChanel.sink.add(authModel.authStatus);

            AppService.analytics.logLogin(loginMethod: 'LOGIN_LIKE_USER');
          });

          AppService.refreshLayout.add(true);

          success = true;
        }

      }
    });

    return Future.value(success);
  }

  static Future<void> logout(
      {bool withCheckAuthorization: true, goToRegistrationPage: false}) async {
    if (authModel.isUser) {
      await AuthApi.deleteLogoutData().then((res) async {
        await FirebaseMessagingService.firebaseMessaging
            .unsubscribeFromTopic('user_' + authModel.id.toString());
        AppService.analytics.logLogin(loginMethod: 'LOGOUT_LIKE_USER');
      });

      authModel.authStatus = AuthStatusEnum.GUEST;
      await FirebaseMessagingService.firebaseMessaging
          .subscribeToTopic('no_authorization')
          .then((_) async {
        AppService.analytics.logLogin(loginMethod: 'LOGIN_LIKE_GUEST');
//      await AppService.putAppBox(CacheNameEnum.AUTH_MODEL.index.toString(), authModel.toJsonStructure());
//      authStatusChanel.sink.add(authModel.authStatus);
      });
    }

    // TODO clear all offers, companies, events, notifications, cashier

//    NotificationApi.notificationUnreadCount = 0;
//    NotificationApi.notificationUnreadCounterChanel.add(true);

    clearUserAndAuthData();

    AppService.refreshLayout.add(true);
  }

  static Future<bool> registration(RegistrationModel registrationModel) async {
    bool successFinish = false;
    registrationModel.withLogin = 1;

    registrationModel.fcmToken = AppService.deviceInfoModel.fcmToken;
    registrationModel.platform = AppService.deviceInfoModel.platform;
    registrationModel.systemVersion = AppService.deviceInfoModel.systemVersion;
    registrationModel.appVersion = AppService.deviceInfoModel.appVersion;
    registrationModel.model = AppService.deviceInfoModel.model;

    await AuthApi.postRegistrationData(registrationModel.toJsonStructure())
        .then((res) async {
      if (res.statusCode == 200) {
        var data = jsonDecode(res.body);

        if (registrationModel.withLogin == 1) {
          authModel.initLoginData(data['loginData']);
          authModel.showCompensationBanner = data['showCompensationBanner'];
          authModel.compensationCount = data['compensationCount'];
          authModel.authStatus = AuthStatusEnum.USER;

          await AppService.putAppBox(CacheNameEnum.AUTH_MODEL.index.toString(),
              authModel.toJsonStructure());

          await initUserApiModel();
          authStatusChanel.sink.add(authModel.authStatus);
          AppService.refreshLayout.add(true);
        }

        successFinish = true;
      }
    });

    return successFinish;
  }

  static Future<void> checkAuthorized({bool refreshAuthStatus: true}) async {}

  static void dispose() {
    authStatusChanel.close();
    refreshIkointCounter.close();
  }

  static Future updateAuthStatusChanel() async {
    await AppService.putAppBox(
        CacheNameEnum.AUTH_MODEL.index.toString(), authModel.toJsonStructure());
    saveUserToCache();

    authStatusChanel.sink.add(authModel.authStatus);
  }

  static Future initUserApiModel() async {
    if (authModel.isGuest) {
      return null;
    }

    await AuthApi.getUser(authModel.id.toString()).then((res) {
      if (res != null) {
        if (res != null && res.statusCode >= 200 && res.statusCode < 300) {
          userModelApi = UserModelApi.fromMap(json.decode(res.body ?? 'null'));
          saveUserToCache();
          refreshIkointCounter.add(true);
        }
      }
    });
  }

  static void clearUserAndAuthData() {
    // Clear all hive cache names from enum
    CacheNameEnum.values.forEach((element) {
      AppService.appBox.delete(element.index.toString());
    });

    initialized = false;
    init();
  }

  static Future<void> initOfferCache() async {
    if (authModel.isUser) {
      var offerCache = await AppService.getAppBox(
          'offer_cache_for_user_' + authModel.id.toString());
      if (offerCache != null) {
        offerCacheModel = DownloadOfferListCacheModel.fromMap(
            json.decode(offerCache),
            required: false);
      }
    }
  }

  static Future<void> saveNewOfferToCache(OfferModelApi offer) async {
    if (authModel.isUser) {
      offerCacheModel.setNewOffer(offer);
      await AppService.putAppBox(
          'offer_cache_for_user_' + authModel.id.toString(),
          json.encode(offerCacheModel.toJsonStructure()));
      initOfferCache();
    }
  }

  static Future<void> saveUserToCache() async {
    await AppService.putAppBox(CacheNameEnum.USER_MODEL_API.index.toString(),
        userModelApi.toJsonStructure());
  }

  static Future<bool> checkInvitationCode(String code) async {
    bool result = false;

    await UserApi.getCheckInvitationCode(code).then((res) {
      if (res != null && res.statusCode >= 200 && res.statusCode < 300) {
        result = res.body.toLowerCase() == 'true';
      }
    });

    return result;
  }

  static Future<bool> putUserModeApi(UserModelApi userModelApi) async {
    bool result = false;

    Map data = userModelApi.toJsonStructure();
    data.remove('ikoint');
    data.remove('status');
    data.remove('roles');
    data.remove('invitationUserId');
    data.remove('birthday');
    data.remove('avatar');
    data.remove('initDate');
    data.remove('invitationCodes');

    data['id'] = data['id'].toString();
    data['gender'] = data['gender'].toString();

    await UserApi.putUserModelApi(data).then((res) {
      if (res != null && res.statusCode >= 200 && res.statusCode < 300) {
        AuthService.userModelApi.login = userModelApi.login;
        AuthService.userModelApi.phone = userModelApi.phone;
        AuthService.userModelApi.email = userModelApi.email;
        AuthService.userModelApi.firstName = userModelApi.firstName;
        AuthService.userModelApi.lastName = userModelApi.lastName;

        result = true;
      }
    });

    return result;
  }

  static Future<bool> postInvitation(String code) async {
    bool result = false;

    Map data = Map();
    data['code'] = code;

    await UserApi.postInvitationUserCode(data).then((res) {
      if (res != null && res.statusCode >= 200 && res.statusCode < 300) {
        if (json.decode(res.body ?? '0') != 0) {
          AuthService.userModelApi.invitationUserId = json.decode(res.body);
          AuthService.initUserApiModel();

          AppService.message(MessageModel(
              title: 'Invitations',
              message: 'Success set new invitation code',
              backgroundColor: Colors.green,
              duration: null,
              mainButton: null));

          result = true;
        } else {
          AppService.message(MessageModel(
              title: 'Invitations',
              message: 'Error',
              backgroundColor: Colors.red,
              duration: null,
              mainButton: null));
        }
      }
    });

    return result;
  }

  static Future<bool> postConfirmEmail(String verificationToken) async {

    assert(verificationToken != null && verificationToken.length > 0);

    Map<String, dynamic> data = Map<String, dynamic>()..addAll({
      "verificationToken": verificationToken
    });

    bool result = false;

    await UserApi.postConfirmEmail(data).then((res) {
      print('res: $res');
      if (res != null && res.statusCode >= 200 && res.statusCode < 300) {
        if (json.decode(res.body ?? '0') != 0) {
          AppService.message(MessageModel(
              title: 'Confirm email',
              message: 'Success',
              backgroundColor: Colors.green,
              duration: null,
              mainButton: null));

          result = true;
        } else {
          AppService.message(MessageModel(
              title: 'Confirm email',
              message: 'Error',
              backgroundColor: Colors.red,
              duration: null,
              mainButton: null));
        }
      }
    });

    return result;
  }

  static Future<bool> getSendVerification() async {
    bool result = false;

    AppService.showLoaderWindow();

    await UserApi.getSendVerification().then((res) {
      AppService.closeLoaderWindow();

      if (res != null && res.statusCode >= 200 && res.statusCode < 300) {
        if (json.decode(res.body ?? '0') != 0) {
          AppService.message(MessageModel(
              title: 'Confirmation',
              message: 'Sent successfully',
              backgroundColor: Colors.green,
              duration: null,
              mainButton: null));

          result = true;
        } else {
          AppService.message(MessageModel(
              title: 'Confirmation',
              message: 'Error',
              backgroundColor: Colors.red,
              duration: null,
              mainButton: null));
        }
      }
    });

    return result;
  }
}
