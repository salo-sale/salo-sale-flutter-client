import 'dart:io';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FirebaseMessagingService {

  static final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  static bool initialized = false;

  ///
  /// Init app
  ///

  static Future<void> init() async {

    if (!initialized) {

      initialized = true;

      if (Platform.isIOS) {

        await firebaseMessaging.requestNotificationPermissions(
          IosNotificationSettings(
            alert: true,
            badge: true,
            sound: true
          )
        );
        firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings settings) {



        });

      }

      firebaseMessaging.configure(

//        onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,

        /**
         *
            "notification": {
            "title": "Hello world",
            "body": "Hello world is body"
            }
         */
        onMessage: (Map<String, dynamic> data) {
          print(data);

          if (data['refresh'] == 'true') {

            AuthService.initUserApiModel();

          }

          _openNotification(data);
          _openSuccessNotification(data);

        },
        onLaunch: (Map<String, dynamic> data) async {
          print(data);

           if (data['refresh'] == 'true') {

             AuthService.initUserApiModel();

           }

           _openModulePage(data);
           _openSuccessNotification(data);

        },
        onResume: (Map<String, dynamic> data) async {
          print(data);

           if (data['refresh'] == 'true') {

             AuthService.initUserApiModel();

           }

           _openModulePage(data);
           _openSuccessNotification(data);

        }

      );

    }

  }

  static Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    if (message['refresh'] == 'true') {

      AuthService.initUserApiModel();

    }

    _openModulePage(message);
    _openSuccessNotification(message);

    // Or do other work.
  }

  static Future<bool> deleteInstanceID() async {

    var success = await firebaseMessaging.deleteInstanceID();

    return success;

  }

  static void _openModulePage(data) {

    if (data != null && data['openPage'] != null) {

      if (data['openPageId'] != null) {

        pageNavigatorKey.currentState.pushNamed('${data['openPage']}/${data['openPageId']}');

      } else {

        pageNavigatorKey.currentState.pushNamed('${data['openPage']}');

      }

    }

  }

  static void _openNotification(Map<String, dynamic> data) {

    bool isNotification = data["notification"] != null;

    if (isNotification || data["aps"]["alert"]["title"] != null) {

      String title = isNotification ? data["notification"]["title"] : data["aps"]["alert"]["title"];
      String body = isNotification ? data["notification"]["body"] : data["aps"]["alert"]["body"];
      String imgUrl = data["fcm_options"] != null ? data["fcm_options"]["image"] ?? null : null;

      Widget openPage = Container();

      if (data['openPage'] != null) {

        openPage = FlatButton(
          onPressed: () {
            _openModulePage(data);
          },
          child: Container(
            margin: const EdgeInsets.only(
              bottom: 15,
              top: 15
            ),
            child: Text(
              FlutterI18n.translate(pageNavigatorKey.currentContext, 'Open'),
              style: TextStyle(
                color: Colors.orange,
                fontSize: 20
              ),
            ),
          ),
        );

      }

      AppService.showOnTransparent(Column(
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.black
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 10
            ),
          ),
          imgUrl == null ? Container() : Padding(
            padding: const EdgeInsets.only(
              top: 10
            ),
            child: Image(
              image: NetworkImage(
                imgUrl
              ),
            ),
          ),
          Text(body, style: TextStyle(color: Colors.black),),
          openPage
        ],
      ));

    }

  }

  static void _openSuccessNotification(Map<String, dynamic> data) {


    if (data['showSuccessDialog'] == 'true') {

      AppService.showOnTransparent(Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                bottom: 25,
                top: 15
            ),
            child: Icon(
              FontAwesomeIcons.checkCircle,
              color: Colors.green,
              size: 100,
            ),
          ),
          Text(
            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Congratulations, you have successfully used the offer'),
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black
            ),
          )
        ],
      ));

    }

  }

}