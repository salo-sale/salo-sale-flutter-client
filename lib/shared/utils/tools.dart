import 'package:SaloSale/data.dart';
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/time-ago.dart' as timeago;
import 'package:fluro_fork/fluro_fork.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class Tools {

  static launchWebSite(url) async {
    if (url[0] != 'h' && url[6] != '/') {
      url = 'https://' + url;
    }
    launchUrl(url);
  }

  static launchPhone(phone) async {
    phone = phone.trim();
    if (await canLaunch('tel:$phone')) {
      await launch('tel:$phone');
    } else {
      throw 'Could not launch $phone';
    }
  }

  static launchEmail(email) async {
    if (await canLaunch('mailto:$email')) {
      await launch('mailto:$email');
    } else {
      throw 'Could not launch $email';
    }
  }

  static launchUrl(String url)  async {

    String originalUrl = url;

    if (url.contains(domain)) {
      if (url.contains('https://')) {
        url = url.substring(8);
      }
      if (url.contains('http://')) {
        url = url.substring(7);
      }
      url = url.substring(14);
      url = url.split('/').length > 1 ? url.split('/')[0] + '/' + (url.split('/')[1].contains('?') ? url.split('/')[1].split('?')[0] : url.split('/')[1]) : url;
    }

    if (Router.appRouter.match(url) != null) {
      pageNavigatorKey.currentState.pushNamed(url);
    } else {
      if (await canLaunch(originalUrl)) {

        await launch(originalUrl);

      } else {

        throw 'Could not launch $originalUrl';

      }
    }

  }

  static Future<void> launchMap(double latitude, double longitude) async {
    String googleUrl = 'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

  static bool compareDateToDate(DateTime from, DateTime to) {

    return from.year == to.year && from.month == to.month && from.day == to.day;

  }

  static String reverseString(String word) {

    if (word.isEmpty) {
      return '';
    }

    return new String.fromCharCodes(word.runes.toList().reversed);

  }

  static String convertOpposite(String word) {

    if (word.isEmpty) {
      return '';
    }

    String newWord = '';
    word.runes.forEach((int rune) {

      String char = new String.fromCharCode(rune);

      if (char == char.toLowerCase()) {

        char = char.toUpperCase();

      } else {

        char = char.toLowerCase();

      }

      newWord += char;
    });

    return newWord;

  }

  /// Функція відповідає за перетворення дати в мікросекундах в нормальний формат
  static String dateString(
      String dateNeedParse,
      {
        bool from: false,
        bool inAgo: false,
        bool oneDay: false
      }
  ) {

    DateTime date = DateTime.parse(dateNeedParse).toLocal();

    if (inAgo) {

      return timeago.format(date, locale: AppService.deviceInfoModel.languageCode);

    }

    DateTime now = DateTime.now();

    var customDate = date.day.toString().padLeft(2, "0") + "." + date.month.toString().padLeft(2, "0");

    if (now.year != date.year) {

      customDate = customDate + "." + date.year.toString().padLeft(2, "0");

    }

    if (oneDay) {

      if (compareDateToDate(date, now)) {

        return FlutterI18n.translate(pageNavigatorKey.currentContext, 'today');

      }

      if (compareDateToDate(date, DateTime(now.year, now.month, now.day + 1))) {

        return FlutterI18n.translate(pageNavigatorKey.currentContext, 'tomorrow');

      }

      if (compareDateToDate(date, DateTime(now.year, now.month, now.day + 2))) {

        return FlutterI18n.translate(pageNavigatorKey.currentContext, 'after tomorrow');

      }

      return customDate;

    }

    var prefix = FlutterI18n.translate(pageNavigatorKey.currentContext, 'to ');

    if (from) {

      prefix = FlutterI18n.translate(pageNavigatorKey.currentContext, 'from ');

    }

    return prefix + customDate;

  }

  static String imageUrl({String companyId, String offerId, String advertisingBannerId}) {

    String path = AppService.remoteConfig.buildHost('salo-sale') + 'uploads/companies/' + companyId;

    if (offerId != null) {

      return path + '/offers/' + offerId + '/' + AppService.deviceInfoModel.languageCode + '/images/1.jpg';

    } else if (advertisingBannerId != null) {

      return path + '/advertisingBanners/' + advertisingBannerId + '/' + AppService.deviceInfoModel.languageCode + '/images/1.jpg';

    } else {

      return path + '/logo.jpg';

    }

  }

  static String webString(String website) {

    if (website != null) {

      if (website.contains('//')) {

        website = website.split('//')[1];

      }

      if (website.contains('/')) {

        website = website.split('/')[0];

      }

    }

    return website ?? null;

  }

  static universalParse<T>(value, {bool required: false, String ifNullUseValue}) {

    if (required) {

      assert(value != null);

    }

    if (value == null || value.toString() == 'NULL') {

      if (ifNullUseValue != null) {

        value = ifNullUseValue;

      } else {

        return null;

      }

    }

    if (value.runtimeType == T) {

      return value;

    }

    switch(T) {

      case String:
        return value.toString().isEmpty ? null : value.toString();
        break;
      case int:
        return int.tryParse(value.toString());
        break;
      case double:
        return double.tryParse(value.toString());
        break;
      case bool:
        return value.toString() == 'true' ? true : false;
        break;

    }

  }

  static String timeString(DateTime initDate) {
    
    DateTime now = DateTime.now();

    String customDate = initDate.day.toString().padLeft(2, "0") + "." + initDate.month.toString().padLeft(2, "0");

    if (initDate.year == now.year) {

      if (initDate.month == now.month) {

        if (compareDateToDate(initDate, now)) {

          if (now.hour == initDate.hour) {

            if (now.minute == initDate.minute) {

              return FlutterI18n.translate(pageNavigatorKey.currentContext, 'just now');

            }

            return (now.minute - initDate.minute).toString() + ' ' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'min_ago');

          }

          return (now.hour - initDate.hour).toString() + ' ' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'h_ago');

        } else if (compareDateToDate(initDate, DateTime(now.year, now.month, now.day - 1))) {

          return FlutterI18n.translate(pageNavigatorKey.currentContext, 'yesterday');

        }

      } else {

        return customDate;

      }

    }

    return customDate + "." + initDate.year.toString().padLeft(2, "0");
    
  }



  static String dayName(String data) {

    if (data == null || data.length == 0) {
      return '';
    }

    if (data == DateFormat('yyyy-MM-dd').format(DateTime.now())) {

      return FlutterI18n.translate(pageNavigatorKey.currentContext, 'Today').toUpperCase();

    }

    if (data == DateFormat('yyyy-MM-dd').format(DateTime.now().add(Duration(days: 1)))) {

      return FlutterI18n.translate(pageNavigatorKey.currentContext, 'Tomorrow').toUpperCase();

    }


    return weekdayName(data);

  }

  static String weekdayName(String data) {

    if (data == null || data.toLowerCase() == 'null') {
      return '';
    }

    return DateFormat('EEEE', AppService.deviceInfoModel.languageCode).format(DateTime.parse(data)).toUpperCase();

  }

  static String weekdayNameJS(String weekday) {

    if (weekday == null || weekday.toLowerCase() == 'null') {
      return '';
    }

    return FlutterI18n.translate(pageNavigatorKey.currentContext, 'WEEKDAY_NAME_' + (weekday == '0' ? '7' : weekday));

  }

}