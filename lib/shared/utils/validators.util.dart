import 'package:flutter_i18n/flutter_i18n.dart';

class ValidatorsUtil {

  static final loginRegex = RegExp(r"^[a-zA-Z0-9_]{1,25}$");
  static final emailRegex = RegExp(r"^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$");

  static String email(context, value) {

    if (value.isEmpty) {
      return FlutterI18n.translate(context, "E-mail can't be empty");
    } else if (!emailRegex.hasMatch(value)) {
      return FlutterI18n.translate(context, "Incorrect email");
    } else if (value.toString().contains('+')) {
      return FlutterI18n.translate(context, "Incorrect email");
    }

    return null;

  }

  static String login(context, value) {

    if (value.isEmpty) {
      return FlutterI18n.translate(context, "Login can't be empty");
    } else if (!loginRegex.hasMatch(value)) {
      return FlutterI18n.translate(context, "Incorrect login");
    } else if (value.toString().length > 25) {
      return FlutterI18n.translate(context, "Maximum 25 characters");
    }

    return null;

  }

  static String phone(context, value) {

    if (value.isEmpty) {
      return FlutterI18n.translate(context, "Phone can't be empty");
    }

    return null;

  }

  static String password(context, value) {

    if (value.isEmpty) {
      return FlutterI18n.translate(context, "Password can't be empty");
    } else if (value.length < 5) {
      return FlutterI18n.translate(context, "Password is short, should be at least 6 characters long");
    }

    return null;

  }

  static String passwordRepeat(context, value, password) {

    if (value.isEmpty) {
      return FlutterI18n.translate(context, "Password repeat can't be empty");
    } else if (value != password) {
      return FlutterI18n.translate(context, "Passwords do not match");
    }

    return null;

  }

  static String checkEmptyValue(context, value) {


    if (value.isEmpty) {
      return FlutterI18n.translate(context, "Can't be empty");
    }

    return null;

  }

}