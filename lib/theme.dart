import "package:flutter/material.dart";

const colorPrimary = MaterialColor(0xFFFF9800, {
  100: Color(0xFF93F6A3),
  200: Color(0xFF74D787),
  300: Color(0xFF56B86B),
  400: Color(0xFF369B50),
  500: Color(0xFF097E36),
  600: Color(0xFF005813),
  700: Color(0xFF004E08),
  800: Color(0xFF033606),
  900: Color(0xFF042105),
});

const whiteMaterialColor = MaterialColor(0xFFFFFFFF, {
  50: Color(0xFFFFFFFF),
  100: Color(0xFFFFFFFF),
  200: Color(0xFFFFFFFF),
  300: Color(0xFFFFFFFF),
  400: Color(0xFFFFFFFF),
  500: Color(0xFFFFFFFF),
  600: Color(0xFFFFFFFF),
  700: Color(0xFFFFFFFF),
  800: Color(0xFFFFFFFF),
  900: Color(0xFFFFFFFF),
});

ThemeData lightCustomTheme = ThemeData(
  fontFamily: 'FiraSans',
  brightness: Brightness.light,
  primarySwatch: whiteMaterialColor,
  primaryColor: Color(0xFFDDDDDD),
  cursorColor: Colors.orange,
  appBarTheme: AppBarTheme(
    color: Colors.white,
  ),
  buttonTheme: ButtonThemeData(
    textTheme: ButtonTextTheme.normal,
  ),
  inputDecorationTheme: InputDecorationTheme(
    filled: true,
    fillColor: Color(0xffFFFFFF),
    contentPadding: const EdgeInsets.only(
      top: 5,
      left: 15,
      bottom: 5
    ),
    hintStyle: TextStyle(
      color: Colors.grey,
    ),
    labelStyle: TextStyle(
      color: Colors.grey,
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey.shade200,
        width: 1.0,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey.shade300,
        width: 1.0,
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: colorPrimary,
        width: 1.0,
      ),
    ),
  ),
);

ThemeData darkCustomTheme = ThemeData(
  brightness: Brightness.dark,
  accentColor: Color(0xFF333333),
  primarySwatch: whiteMaterialColor,
  primaryColor: Color(0xff212121),
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: Color(0xff555555),
  ),
  inputDecorationTheme: InputDecorationTheme(
    filled: true,
    fillColor: Color(0xff555555),
    contentPadding: const EdgeInsets.only(
        top: 5,
        left: 15,
        bottom: 5
    ),
    hintStyle: TextStyle(
      color: Colors.white,
    ),
    labelStyle: TextStyle(
      color: Colors.grey,
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey,
        width: 1.0,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Color(0xff212121),
        width: 1.0,
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: colorPrimary,
        width: 1.0,
      ),
    ),
  ),

);