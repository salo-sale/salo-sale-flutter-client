
import 'package:barcode/barcode.dart';

String buildBarcode(
    Barcode bc,
    String data, {
      String filename,
      double width,
      double height,
    }) => bc.toSvg(data, width: width ?? 200, height: height ?? 80);