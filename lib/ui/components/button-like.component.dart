
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget buttonLikeComponent({
  Function onTap,
  int liked = 0,
  bool likeIsClicked = false
}) {

  Color primaryColor = AuthService.authModel.isUser && AppService.internetIsConnected ? Color(0xFFEE1F1F) : Colors.grey.shade400;

  Widget btn = Container(
    margin: const EdgeInsets.only(
        right: 5
    ),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7.0),
        border: Border.all(
            color: primaryColor,
            width: 1
        ),
        color: liked == 1 ? primaryColor : Colors.transparent
    ),
    padding: const EdgeInsets.all(10.0),
    child: Center(
      child: likeIsClicked ? LoaderComponent(
        withTimer: false,
        circularColor: liked == 1 ? Colors.white : primaryColor,
        circularMargin: const EdgeInsets.all(0),
      ) : Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Icon(
            liked == 1 ? FontAwesomeIcons.solidHeart : FontAwesomeIcons.heart,
            size: 20.0,
            color: liked == 1 ? Colors.white : primaryColor,
          ),
//          Padding(
//            padding: const EdgeInsets.only(
//              left: 10,
//            ),
//            child: Text(
//              FlutterI18n.translate(pageNavigatorKey.currentContext, 'Like'),
//              style: TextStyle(
//                  fontSize: 18,
//                  color: liked == 1 ? Colors.white : primaryColor,
//                  fontWeight: FontWeight.w400
//              ),
//            ),
//          )
        ],
      ),
    ),
  );


  return GestureDetector(
    onTap: AuthService.authModel.isGuest ? () {

      AppService.message(MessageModel(
          title: 'Like',
          message: 'You must be logged in',
          backgroundColor: Colors.orangeAccent,
          duration: null,
          mainButton: null
      ));

    } : AppService.internetIsConnected ? onTap : () {

      AppService.message(MessageModel(
          title: 'Like',
          message: 'TEXT_DISCONNECT',
          backgroundColor: Colors.orangeAccent,
          duration: null,
          mainButton: null
      ));

    },
    onDoubleTap: () {

    },
    child: btn,
  );

}