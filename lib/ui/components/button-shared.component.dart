import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget buttonSharedComponent({
  Function onTap
}) {

  return GestureDetector(
    onTap: onTap,
    child:
    Container(
      margin: const EdgeInsets.only(
          left: 5
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7.0),
          border: Border.all(
              color: Color(0xFF509CC7),
              width: 1
          ),
          color: Colors.transparent
      ),
      padding: const EdgeInsets.all(10.0),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
//            Padding(
//              padding: const EdgeInsets.only(
//                right: 10,
//              ),
//              child: Text(
//                FlutterI18n.translate(pageNavigatorKey.currentContext, 'Share'),
//                style: TextStyle(
//                    fontSize: 18,
//                    color: const Color(0xFF509CC7),
//                    fontWeight: FontWeight.w400
//                ),
//              ),
//            ),
            Icon(
              FontAwesomeIcons.bullhorn,
              size: 20.0,
              color: const Color(0xFF509CC7),
            ),
          ],
        ),
      ),
    ),
  );

}