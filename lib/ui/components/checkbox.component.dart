import 'package:SaloSale/shared/utils/tools.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CheckboxComponent extends StatefulWidget {

  final String title;
  final bool isRequired;
  final bool checked;
  const CheckboxComponent({Key key, this.title, this.checked = false, this.changeChecked, this.isRequired}) : super(key: key);

  final CheckboxCallbackComponent changeChecked;

  @override
  State<StatefulWidget> createState() => _CheckboxComponent();

}

class _CheckboxComponent extends State<CheckboxComponent> {

  bool _checked;

  @override
  void initState() {
    _checked = widget.checked;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () {

              setState(() {

                _checked = !_checked;
                widget.changeChecked(_checked);

              });

            },
            child: Container(
              margin: const EdgeInsets.only(
                right: 10.0
              ),
              padding: const EdgeInsets.all(
                5.0
              ),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: Colors.grey.shade300
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    5
                  )
                )
              ),
              width: 25,
              height: 25,
              child: _checked ? Icon(
                FontAwesomeIcons.check,
                size: 12.5,
                color: Colors.orange,
              ) : null,
            ),
          ),
          widget.isRequired ? Container(
            child: Center(
              child: Text(
                '*',
                style: TextStyle(
                  color: Colors.red
                ),
              ),
            ),
          ) : Container(),
          GestureDetector(
            onTap: () {

              setState(() {

                _checked = !_checked;
                widget.changeChecked(_checked);

              });

            },
            child: Html(
              data: widget.title,
              linkStyle: const TextStyle(
                color: Colors.orange,
              ),
              onLinkTap: Tools.launchUrl,
            ),
          ),
        ],
      ),
    );
  }

}

typedef CheckboxCallbackComponent = void Function(bool checked);