
import 'dart:math';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/ui/pages/auth/main.auth.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CommentInputComponent extends StatefulWidget {

  final FocusNode myFocusNode;
  final TextEditingController commentInputController;
  final Function clickSuffix;
  final Function onFieldSubmitted;
  final String commentLabel;
  final bool withSliver;
  final bool withPadding;

  CommentInputComponent({
    Key key,
    this.myFocusNode,
    this.commentInputController,
    this.clickSuffix,
    this.onFieldSubmitted,
    this.commentLabel,
    this.withSliver = true,
    this.withPadding = true,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CommentInputComponent();

}

class _CommentInputComponent extends State<CommentInputComponent> {

  bool _validate = false;

  @override
  initState() {

    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    if (AuthService.authModel.isEmpty) {

      return FlatButton(
        padding: const EdgeInsets.only(
            top: 20,
            left: 10,
            right: 10,
            bottom: 20
        ),
        onPressed: () async {
          await Navigator.push(context, MaterialPageRoute(
            builder: (context) => MainAuthPage(),
          ));
          setState(() {

          });
        },
        child: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'You must be logged in to post a comment'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            color: Colors.grey
          ),
        ),
      );

    }

    if (AuthService.userModelApi.login == null || AuthService.userModelApi.login.length == 0) {

      return FlatButton(
        padding: const EdgeInsets.only(
          top: 20,
          left: 10,
          right: 10,
          bottom: 20
        ),
        onPressed: () async {
          await pageNavigatorKey.currentState.pushNamed('profile/edit');
          setState(() {

          });
        },
        child: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'To leave a comment, you must be login'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
            color: Colors.grey
          ),
        ),
      );

    }


    Widget child = TextField(
      key: Key((Random().nextInt(100)).toString()),
      maxLines: null,
      keyboardType: TextInputType.multiline,
      focusNode: widget.myFocusNode,
      cursorColor: Colors.orange,
      controller: widget.commentInputController,
      decoration: InputDecoration(
        errorText: _validate ? FlutterI18n.translate(pageNavigatorKey.currentContext, 'The comment cannot be less than 5 characters') : null,
        prefixIcon: Icon(
          FontAwesomeIcons.comment,
          size: 18,
          color: Colors.grey.shade300,
        ),
        suffixIcon: IconButton(
          icon: Icon(
            FontAwesomeIcons.paperPlane,
            size: 18,
            color: Colors.orange,
          ),
          onPressed: () {
            if (widget.commentInputController.text.trim().length > 4) {
              setState(() {
                _validate = false;
              });
              widget.clickSuffix(widget.commentInputController.text.trim());
            } else {
              setState(() {
                _validate = true;
              });
            }
          },
        ),
        contentPadding: const EdgeInsets.only(
            top: 25,
            bottom: 0,
            left: 20,
            right: 20
        ),
        labelText: widget.commentLabel,
        labelStyle: TextStyle(
            fontWeight: FontWeight.w500,
            color: Colors.grey.shade500
        ),
        fillColor: Colors.white,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          // width: 0.0 produces a thin "hairline" border
          borderSide: BorderSide(color: Colors.grey.shade400, width: 0.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide: BorderSide(
              width: 0.0,
              color: Colors.orange
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide: BorderSide(
              width: 0.0,
              color: Colors.grey.shade300
          ),
        ),
      ),
      onSubmitted: (value) {
        if (value.trim().length > 4) {
          setState(() {
            _validate = false;
          });
          widget.onFieldSubmitted(value.trim());
        } else {
          setState(() {
            _validate = true;
          });
        }
      },
    );

    if (widget.withPadding) {

      child = Padding(
        padding: const EdgeInsets.all(10),
        child: child,
      );

    }

    if (widget.withSliver) {


      child = SliverToBoxAdapter(
        child: child,
      );

    }


    return child;

  }

}