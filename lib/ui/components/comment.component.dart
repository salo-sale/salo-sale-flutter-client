import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/models/api/comment.model.api.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/starts.component.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class CommentComponent extends StatelessWidget {

  final CommentModelApi comment;

  CommentComponent({
    this.comment,
  });

  @override
  Widget build(BuildContext context) {

    if (comment == null) {

      return Container();

    }

    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  width: 1,
                  color: Colors.grey.shade200
              )
          )
      ),
      child: ListTile(
        onLongPress: () {

        },
        onTap: () => Navigator.pushNamed(context, 'comments/${comment.id}'),
        contentPadding: const EdgeInsets.all(0),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '@${comment.user.login}',
                  style: TextStyle(
//                            fontSize: 16,
                      fontWeight: FontWeight.w500
                  ),
                ),
                Row(
                  children: [
                    StartsComponent(
                      value: comment.score,
                      iconSize: 14.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 5
                      ),
                      child: Text(
                        comment.score.toString(),
                        style: TextStyle(
                            fontWeight: FontWeight.w500
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              padding: const EdgeInsets.only(
                  top: 10,
                  bottom: 10
              ),
              child: Text(
                comment.comment,
              ),
            ),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  Tools.dateString(comment.createdAt, inAgo: true),
                  style: TextStyle(
                      color: Colors.grey.shade400
                  ),
                ),
                comment.numberOfReplies > 0 ? Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'Replies') + ' (${comment.numberOfReplies})',
                  style: TextStyle(
                      color: Colors.orange
                  ),
                ) : Container(),
              ],
            )
          ],
        ),
      ),
    );

  }

}