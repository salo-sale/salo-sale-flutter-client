import 'package:SaloSale/shared/models/api/company.model.api.dart';
import 'package:SaloSale/shared/models/api/list-of-tags.model.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/starts.component.dart';
import 'package:SaloSale/ui/pages/company/detail.company.page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CompanyCardComponent extends StatefulWidget {

  final CompanyModelApi companyModelApi;
  final String companyId;
  final String filterItem;
  final ListOfTagsModelApi tagList;

  CompanyCardComponent({
    this.filterItem,
    this.companyModelApi,
    this.companyId,
    this.tagList,
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CompanyCardComponent();

}

class _CompanyCardComponent extends State<CompanyCardComponent> {

  @override
  initState() {

    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailCompanyPage(
                companyId: widget.companyModelApi.id,
                filterItem: widget.filterItem
            ),
          ),
        );
      },
      child: Container(
        height: 150.0,
        padding: const EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(15.0),
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).primaryColor,
              blurRadius: 10.0,
              // has the effect of softening the shadow
              spreadRadius: 0.0,
              // has the effect of extending the shadow
              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
        ),
        margin: const EdgeInsets.only(
          bottom: 15.0,
          left: 10.0,
          right: 10.0,
        ),
        child: Row(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(
                  right: 10.0
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0)
              ),
              width: 112.5,
              height: 112.5,
              child: CachedNetworkImage(
                imageUrl: widget.companyModelApi.logoUrl.buildUrl,
                imageBuilder: (context, imageProvider) => Container(
                  width: double.infinity,
                  height: 35.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(25.0)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: imageProvider,
                    ),
                  ),
                ),
                placeholder: (context, url) => Container(
                  width: double.infinity,
                  height: 35.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(25.0)),
                    color: Theme.of(context).accentColor,
                  ),
                  child: LoaderComponent(
                    withTimer: false,
                  ),
                ),
                errorWidget: (context, url, error) => Container(
                  width: double.infinity,
                  height: 35.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(25.0)),
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 0.0,
                        bottom: 10.0
                    ),
                    child: Text(
                      widget.companyModelApi.name,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  AppService.remoteConfig.checkComponent('accessTypeList') ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: widget.companyModelApi.accessTypeList.map((type) {
                      return Padding(
                        padding: const EdgeInsets.only(
                            right: 10
                        ),
                        child: Text(
                          type,
                          style: TextStyle(
                              color: type == 'web' ? Colors.green.shade200 : Colors.lightBlue.shade200,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      );
                    }).toList(),
                  ) : Container(),
                  Center(
                    child: Text(
                      widget.companyModelApi.getTagNames() ?? '',
                      softWrap: true,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 10
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '${widget.companyModelApi.score} ', // FlutterI18n.translate(pageNavigatorKey.currentContext, 'Score') +
                            style: TextStyle(
                                color: Colors.grey.shade600
                            ),
                          ),
                          StartsComponent(
                            value: widget.companyModelApi.score,
                            iconSize: 20.0,
                          ),
                          Text(
                            ' (${widget.companyModelApi.numberOfComments})',
                            style: TextStyle(
                                color: Colors.grey.shade600
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );

  }

}