
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class ContainerTextComponent extends StatelessWidget {

  final String title;
  final String text;
  final EdgeInsets margin;
  final Widget child;
  final bool show;

  ContainerTextComponent({
    this.title,
    this.text,
    this.margin = const EdgeInsets.all(0),
    this.show = true,
    Widget child,
  }) : this.child = child ?? Html(
    data: text ?? '',
    linkStyle: const TextStyle(
      color: Colors.orange,
      decorationColor: Colors.orangeAccent,
      decoration: TextDecoration.underline,
    ),
    onLinkTap: Tools.launchUrl,
  );

  @override
  Widget build(BuildContext context) {

    if ((text == null && child == null) || !show) {

      return Container();

    }

    return Column(
      children: <Widget>[
        Padding(
          padding: margin,
          child: Text(
            FlutterI18n.translate(pageNavigatorKey.currentContext, title ?? ''),
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 20,
                color: Colors.grey
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          margin: margin,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(7)),
            boxShadow: [
              BoxShadow(
                color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
                blurRadius: 10.0,
                // has the effect of softening the shadow
                spreadRadius: 0.0,
                // has the effect of extending the shadow
                offset: Offset(
                  0.0, // horizontal, move right 10
                  0.0, // vertical, move down 10
                ),
              )
            ],
          ),
          child: child,
        ),
      ],
      crossAxisAlignment: CrossAxisAlignment.start,
    );

  }

}