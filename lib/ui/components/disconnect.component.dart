import 'package:SaloSale/routing.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget disconnectComponent(String text) => Padding(
    padding: const EdgeInsets.only(
        top: 15.0,
        left: 10.0,
        right: 10.0,
        bottom: 25
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(
              top: 10.0,
              bottom: 15.0
          ),
          child: Icon(
            FontAwesomeIcons.wifi,
            size: 50,
            color: Colors.grey.shade300,
          ),
        ),
        Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, text),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.grey,
            fontSize: 20
          ),
        ),
      ],
    ),
  );