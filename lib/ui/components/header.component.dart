import 'package:SaloSale/routing.dart';

import 'package:SaloSale/ui/components/ikoint.component.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';


class HeaderComponent extends StatelessWidget {

  final String titleOfPage;
  final Color color;
  final Widget action;
  final Widget title;
  final bool forCustomScroll;

  HeaderComponent({

    this.titleOfPage,
    this.action,
    this.title,
    this.forCustomScroll = true,
    this.color = const Color(0xFF343434),

  });

  @override
  Widget build(BuildContext context) {

    Widget _header = Padding(
      padding: const EdgeInsets.only(
          left: 15,
          right: 15,
          top: 10,
          bottom: 15
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          title ?? _title(),
          action ?? IkointComponent()
        ],
      ),
    );

    if (forCustomScroll) {

      return SliverToBoxAdapter(
        child: _header,
      );

    } else {

      return _header;

    }

  }

  _title() {

    return Text(
      FlutterI18n.translate(pageNavigatorKey.currentContext, titleOfPage),
      style: TextStyle(
        fontSize: 25,
        color: color,
        fontWeight: FontWeight.bold
      ),
    );

  }

}