import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/salo_sale_icons_icons.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/ikoint.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/menu-item.component.dart';
import 'package:SaloSale/ui/pages/auth/main.auth.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeDrawerComponent extends StatefulWidget implements Widget {

  final IconData iconOfPage = FontAwesomeIcons.thLarge;

  HomeDrawerComponent({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomeDrawerComponent();

}

class _HomeDrawerComponent extends State<HomeDrawerComponent> {

  List<Widget> items;

  @override
  void initState() {

    super.initState();

    if (AppService.internetIsConnected) {

      AppService.analytics.logEvent(name: 'OPEN_MENU_PAGE');

    }

    _refresh();

  }

  @override
  void dispose() {

    super.dispose();

  }


  Future _refresh({bool doRequest: true}) {

    initItems();

    if (AuthService.authModel.isUser && doRequest) {

      return AuthService.initUserApiModel();

    } else {

      return Future.value(true);

    }

  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      body: RefreshIndicator(
        color: Colors.orange,
        onRefresh: _refresh,
        child: ListView.builder(
          // Let the ListView know how many items it needs to build.
          itemCount: items.length,
          // Provide a builder function. This is where the magic happens.
          // Convert each item into a widget based on the type of item it is.
          itemBuilder: (context, index) {
            return items[index];
          },
        ),
      ),
    );

  }

  initItems() {

    items = [
      _header(),
    ];

    if (AuthService.authModel.isUser) {

      items.add(menuItemComponent(
        context: context,
        trailing: GestureDetector(
          onTap: () => pageNavigatorKey.currentState.pushNamed('profile/edit'),
          child: Container(
            height: 50,
            color: Colors.transparent,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'SHORT_EDIT'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.orange,
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                  ),
                ),
              ],
            ),
          ),
        ),
        padding: const EdgeInsets.all(0),
        customTitle: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                right: 10,
                top: 2.5
              ),
              child: Icon(
                FontAwesomeIcons.solidUserCircle,
                size: 65,
                color: Colors.grey,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                AuthService.userModelApi.lastName != null && AuthService.userModelApi.firstName != null ? Text(
                  (AuthService.userModelApi.lastName ?? '') + ' ' + (AuthService.userModelApi.firstName ?? ''),
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.w500
                  ),
                ) : Container(),
                AuthService.userModelApi.login != null ? Text(
                  AuthService.userModelApi.login ?? '',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.w500
                  ),
                ) : Container(),
                AuthService.userModelApi.email != null ? Text(
                  AuthService.userModelApi.email ?? '',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.grey,
                  ),
                ) : Container(),
              ],
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.start,
        ),
        onTap: () async {

//          if (await Navigator.push(
//            context,
//            MaterialPageRoute<bool>(
//              builder: (context) => EditProfileMenuHomePage(),
//            ),
//          ) ?? false) {
//
//            _refresh(doRequest: false);
//
//          }

        },
      ));


      items.add(Container(
        padding: const EdgeInsets.only(
          left: 15,
          right: 15,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            IkointComponent(
                bigCounter: true
            ),
            Text(
              FlutterI18n.translate(pageNavigatorKey.currentContext, 'Last update') + ': ' + Tools.timeString(AuthService.userModelApi.initDate),
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.grey
              ),
            ),
            _phone(),
            _birthday()
          ],
        ),
      ));

      if (!AuthService.userModelApi.emailIsConfirmed) {

        if (AuthService.userModelApi.email != null && AuthService.userModelApi.email.length > 0) {

          items.add(Container(
            decoration: BoxDecoration(
              border: Border(top: BorderSide(width: 1, color: Colors.grey.shade300)),
            ),
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    FlutterI18n.translate(pageNavigatorKey.currentContext, 'Your email has not been verified'),
                    softWrap: true,
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.white
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    AuthService.getSendVerification();
                  },
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Send confirmation email'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                          color: Colors.orange
                      ),
                    ),
                  ),
                )
              ],
            ),
          ));

        } else {

          items.add(Container(
            decoration: BoxDecoration(
              border: Border(top: BorderSide(width: 1, color: Colors.grey.shade300)),
            ),
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    FlutterI18n.translate(pageNavigatorKey.currentContext, 'You need to specify an email'),
                    softWrap: true,
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.white
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () => pageNavigatorKey.currentState.pushNamed('profile/edit'),
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Specify an email'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                          color: Colors.orange
                      ),
                    ),
                  ),
                )
              ],
            ),
          ));

        }

      }

      if (AuthService.userModelApi.login == null || AuthService.userModelApi.login.length == 0) {

        items.add(Container(
          decoration: BoxDecoration(
            border: Border(top: BorderSide(width: 1, color: Colors.grey.shade300)),
          ),
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.orange,
                  borderRadius: BorderRadius.circular(5),
                ),
                padding: const EdgeInsets.all(10),
                child: Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'Add your login to have more features'),
                  softWrap: true,
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.white
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => pageNavigatorKey.currentState.pushNamed('profile/edit'),
                child: Container(
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    FlutterI18n.translate(pageNavigatorKey.currentContext, 'Add'),
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                        fontSize: 20,
                      color: Colors.orange
                    ),
                  ),
                ),
              )
            ],
          ),
        ));

      }

    }

    items.addAll([
      menuItemComponent(
        context: context,
        title: AppService.deviceInfoModel?.localityModelApi?.name ?? '',
        iconData: FontAwesomeIcons.mapMarkerAlt,
        url: 'feedback',
        onTap: () => Navigator.pushNamed(context, 'change-locality'),
      ),
      menuItemComponent(
        context: context,
        title: AppService.languageMap[AppService.deviceInfoModel.languageCode].name,
        iconData: FontAwesomeIcons.globe,
        url: '',
        onTap: () => Navigator.pushNamed(context, 'change-language'),
      ),
    ]);

    if (AuthService.authModel.isUser) {

      items.add(menuItemComponent(
          context: context,
          title: 'Change password',
          iconData: FontAwesomeIcons.key,
          url: 'change-password'
      ));

      if (AppService.remoteConfig.checkModule('buyOffer')) {

        items.add(menuItemComponent(
            context: context,
            title: 'My offers',
            iconData: SaloSaleIcons.percentage,
            url: 'my-offers'
        ));

      }

      items.addAll([
//        _menuItem(
//            title: 'Favorite',
//            iconData: FontAwesomeIcons.heart,
//            url: 'likes'
//        ),
        menuItemComponent(
            context: context,
            title: 'My follows',
            iconData: FontAwesomeIcons.store,
            url: 'follows'
        ),
        menuItemComponent(
            context: context,
            title: 'Invitations',
            iconData: FontAwesomeIcons.users,
            url: 'friends'
        ),
        menuItemComponent(
            context: context,
            title: 'Loyalty cards',
            iconData: FontAwesomeIcons.creditCard,
            url: 'loyalty-cards'
        )
      ]);

      if (AuthService.userModelApi.roles.indexOf('CASHIER') > -1) {

        items.add(
            menuItemComponent(
                context: context,
                title: 'Cashier',
                iconData: FontAwesomeIcons.cashRegister,
                url: 'cashier'
            )
        );

      }

    }

    items.addAll([
//      _menuItem(
//          title: 'Settings',
//          iconData: FontAwesomeIcons.sun,
//          url: 'settings'
//      ),
      menuItemComponent(
          context: context,
          title: 'Contact',
          iconData: FontAwesomeIcons.solidComment,
          url: 'feedback'
      ),
      menuItemComponent(
          context: context,
          title: 'About us',
          iconData: FontAwesomeIcons.info,
          url: 'about'
      ),
      menuItemComponent(
          context: context,
          title: 'Help',
          iconData: FontAwesomeIcons.question,
          url: 'help'
      ),
      menuItemComponent(
          context: context,
          title: AuthService.authModel.isUser ? 'Logout' : 'Login',
          colorText: AuthService.authModel.isUser ? Colors.red : Colors.orange,
          colorIcon: AuthService.authModel.isUser ? Colors.red : Colors.orange,
          iconData: FontAwesomeIcons.signOutAlt,
          onTap: () {
            if (AuthService.authModel.isGuest) {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => MainAuthPage(),
              ));
//              setState(() {
//
//              });
              return;
            }
            if (AppService.internetIsConnected) {
              showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return AlertDialog(
                      title: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Exit'),
                      ),
                      content: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Are you sure you want to sign out of your account?')
                      ),
                      actions: <Widget>[
                        FlatButton(
                          child: Text(
                            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Cancel'),
                            style: TextStyle(
                                color: Colors.black
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          onPressed: () async {

                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (context) {
                                  return AlertDialog(
                                    content: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        LoaderComponent(
                                          withTimer: false,
                                        ),
                                      ],
                                    ),
                                  );
                                }
                            );
                            await AuthService.logout().then((_) {

                              Navigator.pop(context);
                              Navigator.pop(context);

                            });
                          },
                          child: Text(
                            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Yes'),
                            style: TextStyle(
                                color: Colors.red
                            ),
                          ),
                        ),
                      ],
                    );
                  }
              );
            } else {
              showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return AlertDialog(
                      title: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Exit')
                      ),
                      content: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'You cannot log out because you need to have an internet connection')
                      ),
                      actions: <Widget>[
                        FlatButton(
                          child: Text(
                            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Ok'),
                            style: TextStyle(
                                color: Colors.orange
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    );
                  }
              );
            }
          }
      ),
      Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border(top: BorderSide(width: 1, color: Colors.grey.shade300)),
        ),
        child: Html(
          data: FlutterI18n.translate(pageNavigatorKey.currentContext, 'html_agreements'),
          customTextAlign: (node) => TextAlign.center,
          defaultTextStyle: TextStyle(
              color: Colors.grey
          ),
          linkStyle: TextStyle(
            color: Colors.grey,
            decoration: TextDecoration.underline,
          ),
          onLinkTap: Tools.launchUrl,
        ),
      ),
      Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border(top: BorderSide(width: 1, color: Colors.grey.shade300)),
        ),
        child: Center(
          child: Text(
              AppService?.deviceInfoModel?.appVersion ?? ''
          ),
        ),
      ),
//        _menuItem(
//            'Rules',
//            Color(0xFFFCC56F),
//            Colors.white,
//            Colors.white,
//            FontAwesomeIcons.file,
//            'rules'
//        ),
//        _menuItem(
//            'Help',
//            Color(0xFF2B7075),
//            Colors.white,
//            Colors.white,
//            FontAwesomeIcons.infoCircle,
//            'help'
//        ),
    ]);


    setState(() {

    });

  }

  _phone() {

//    if (AuthService.userModelApi.phone == null || AuthService.userModelApi.phone.isEmpty) {
//
//      return Container(
//        decoration: BoxDecoration(
//          color: Colors.lime,
//          borderRadius: BorderRadius.all(Radius.circular(7))
//        ),
//        padding: const EdgeInsets.all(10),
//        margin: const EdgeInsets.only(
//          top: 10,
//          bottom: 10
//        ),
//        child: Row(
//          mainAxisAlignment: MainAxisAlignment.spaceAround,
//          mainAxisSize: MainAxisSize.max,
//          children: <Widget>[
//            Icon(
//              FontAwesomeIcons.phoneAlt,
//              color: Colors.lime.shade700,
//              size: 50,
//            ),
//            Padding(
//              padding: const EdgeInsets.only(
//
//              ),
//              child: Text(
//                FlutterI18n.translate(context, 'Add phone and get 50 ikoint'),
//                style: TextStyle(
//                  color: Colors.lime.shade900,
//                  fontWeight: FontWeight.w500
//                ),
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.only(
//
//              ),
//              child: Icon(
//                FontAwesomeIcons.times,
//                color: Colors.lime.shade900,
//              ),
//            ),
//          ],
//        ),
//      );
//
//    }

    return Text(
      AuthService.userModelApi.phone ?? '',
      style: TextStyle(
        fontSize: 15,
        color: Colors.white,
      ),
    );

  }

  _birthday() {

//    if (AuthService.userModelApi.birthday == null || AuthService.userModelApi.birthday.isEmpty) {
//
//      return Container(
//        decoration: BoxDecoration(
//            color: Colors.teal,
//            borderRadius: BorderRadius.all(Radius.circular(7))
//        ),
//        padding: const EdgeInsets.all(10),
//        margin: const EdgeInsets.only(
//            top: 10,
//            bottom: 10
//        ),
//        child: Row(
//          mainAxisAlignment: MainAxisAlignment.spaceAround,
//          mainAxisSize: MainAxisSize.max,
//          children: <Widget>[
//            Icon(
//              FontAwesomeIcons.birthdayCake,
//              color: Colors.teal.shade700,
//              size: 50,
//            ),
//            Padding(
//              padding: const EdgeInsets.only(
//
//              ),
//              child: Text(
//                FlutterI18n.translate(context, 'Add birthday and get 25 ikoint'),
//                style: TextStyle(
//                    color: Colors.teal.shade900,
//                    fontWeight: FontWeight.w500
//                ),
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.only(
//
//              ),
//              child: Icon(
//                FontAwesomeIcons.times,
//                color: Colors.teal.shade900,
//              ),
//            ),
//          ],
//        ),
//      );
//
//    }

    return Text(
      AuthService.userModelApi.birthday ?? '',
      style: TextStyle(
        fontSize: 15,
        color: Colors.white,
      ),
    );

  }

  _header() {

    return menuItemComponent(
      icon: Container(),
      context: context,
      customTitle: Text(
        FlutterI18n.translate(pageNavigatorKey.currentContext, 'Menu'),
        style: TextStyle(
            color: Colors.grey,
            fontSize: 25,
            fontWeight: FontWeight.w500
        ),
      ),
      trailing: IconButton(
        alignment: Alignment.centerRight,
        onPressed: () => Navigator.pop(context),
        icon: Icon(
            FontAwesomeIcons.angleRight
        ),
      ),
      onTap: () => Navigator.pop(context),
    );

  }

}