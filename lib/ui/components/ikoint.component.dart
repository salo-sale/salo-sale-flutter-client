import 'package:SaloSale/routing.dart';
import 'package:SaloSale/salo_sale_icons_icons.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/ui/pages/auth/main.auth.page.dart';
import 'package:SaloSale/ui/pages/ikoint-history/list.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class IkointComponent extends StatelessWidget {
  final bool bigCounter;
  final EdgeInsets margin;
  final bool useOnTapForBig;
  final bool showLoginIfGuest;

  IkointComponent({
    this.bigCounter = false,
    this.margin,
    this.useOnTapForBig = true,
    this.showLoginIfGuest = false,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: AuthService.refreshIkointCounter,
      initialData: false,
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.data) {
          AuthService.refreshIkointCounter.add(false);
//          return Container(
//            height: bigCounter ? 75 : 0,
//            child: LoaderComponent()
//          );
        }
        if (AuthService.authModel.isUser) {
          if (bigCounter) {
            Widget container = Container(
              height: 75,
              decoration: BoxDecoration(
//                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(7))),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      '${AuthService.userModelApi.ikoint ?? ''}',
                      style: TextStyle(
                          fontSize: 50,
                          fontWeight: FontWeight.w500,
                          color: Colors.orange),
                    ),
                  ),
                  Icon(
                    SaloSaleIcons.ikoint_logo,
                    color: Colors.orange,
                    size: 50,
                  )
                ],
              ),
            );

            if (useOnTapForBig) {
              container = GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => IkointHistoryPage(),
                  ));
                },
                onDoubleTap: () {},
                child: container,
              );
            }

            return container;
          }

          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => IkointHistoryPage(),
              ));
            },
            onDoubleTap: () {},
            child: Container(
              margin: margin,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Text(
                      '${AuthService.userModelApi.ikoint ?? ''}',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Colors.orange),
                    ),
                  ),
                  Icon(
                    SaloSaleIcons.ikoint_logo,
                    color: Colors.orange,
                  )
                ],
              ),
            ),
          );
        } else {
          if (showLoginIfGuest) {
            return GestureDetector(
              onDoubleTap: () {},
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MainAuthPage(),
                    ));
              },
              child: Container(
                height: 50,
                width: 75,
                padding: const EdgeInsets.only(right: 10),
                color: Colors.transparent,
                child: Center(
                  child: Text(
                    FlutterI18n.translate(
                        pageNavigatorKey.currentContext, 'Login'),
                    style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.w500,
                        fontSize: 18),
                  ),
                ),
              ),
            );
          }

          return Container();
        }
      },
    );
  }
}
