
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ImageComponent extends StatelessWidget {

  final bool withHero;
  final bool withAspectRation;
  final bool useImageBuilder;
  final double rounded;
  final String url;
  final double aspectRation;
  final String tag;
  final double height;
  final double width;
  final EdgeInsets margin;
  final Function onClick;

  ImageComponent({
    this.onClick,
    this.margin,
    this.rounded = 0,
    this.withHero = true,
    this.withAspectRation = true,
    this.useImageBuilder = true,
    this.url,
    this.height = 150,
    this.width = double.infinity,
    this.aspectRation = 1, //16/9
    this.tag
  });

  _image({ImageProvider imageProvider, bool error = false}) {

    return Container(
      height: height,
      width: width,
      margin: margin,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(rounded)),
        image: imageProvider != null ? DecorationImage(
          image: imageProvider,
          fit: BoxFit.cover,
        ) : null,
      ),
      child: imageProvider == null ? error ? Center(
        child: Icon(
          FontAwesomeIcons.image,
          color: Colors.grey.shade400,
        ),
      ) : LoaderComponent(
        withTimer: false,
      ) : null,
    );

  }

  @override
  Widget build(BuildContext context) {

    Widget _img = CachedNetworkImage(
        imageUrl: url,
        imageBuilder: useImageBuilder ? (context, imageProvider) => _image(imageProvider: imageProvider) : null,
        placeholder: (context, url) => Container(
          height: height,
          width: width,
          margin: margin,
          child: LoaderComponent(
            withTimer: false,
          )
        ),
        errorWidget: (context, url, error) => _image(error: true),
        width: double.infinity,
        fit: BoxFit.cover
    );

    if (withHero) {

      _img = Hero(
        transitionOnUserGestures: true,
        tag: tag,
        child: _img,
      );

    }

    if (withAspectRation) {

      _img = AspectRatio(
        aspectRatio: aspectRation,
        child: _img,
      );

    }

    if (onClick != null) {

      return GestureDetector(
        onTap: onClick,
        onDoubleTap: () {

        },
        child: _img,
      );

    }

    return _img;

  }

}