import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";
import 'package:flutter_i18n/flutter_i18n.dart';

// ignore: must_be_immutable
class LoaderComponent extends StatelessWidget {

  final double fontSize;
  final double circularWidth;
  final double circularHeight;
  final double iconSize;
  final EdgeInsetsGeometry circularMargin;
  final EdgeInsetsGeometry circularPadding;
  final Color circularColor;
  final double circularStrokeWidth;
  final Function refreshFunction;
  final bool withTimer;
  final bool forSliver;
  final bool inBox;

  bool _timerIsInitialized = false;
  bool _timerIsFinish = false;

  LoaderComponent({
    Color circularColor,
    double circularWidth,
    double circularHeight,
    double circularStrokeWidth,
    EdgeInsetsGeometry circularMargin,
    this.inBox = false,
    this.refreshFunction,
    this.circularPadding,
    this.iconSize = 50.0,
    this.fontSize = 20.0,
    this.forSliver = false,
    this.withTimer = true
  }): circularColor = circularColor ?? Colors.orange,
        circularWidth = circularWidth ?? 20,
        circularHeight = circularHeight ?? 20,
        circularStrokeWidth = circularStrokeWidth ?? 3,
        circularMargin = circularMargin ?? const EdgeInsets.all(10.0);

  @override
  Widget build(BuildContext context) {

    return FutureBuilder(
      builder: (context, snapshot) {

        Widget child = _timerIsFinish == false ? Center(
          child: Container(
            margin: circularMargin,
            padding: circularPadding,
            width: circularWidth,
            height: circularHeight,
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(circularColor),
              strokeWidth: circularStrokeWidth,
            ),
          ),
        ) : Container();

        if (withTimer) {

          if (_timerIsInitialized && _timerIsFinish) {

            child = GestureDetector(
              onTap: refreshFunction,
              child: Container(
                margin: circularMargin,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.refresh,
                      size: iconSize,
                      color: Colors.grey
                          .shade300,
                    ),
                    Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Refresh'),
                      style: TextStyle(
                          fontSize: fontSize,
                          color: Colors.grey
                      ),
                    )
                  ],
                ),
              ),
            );

          }

        }

        if (inBox) {

          child = Container(
            height: 350,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Theme.of(context).primaryColor,
                  blurRadius: 10.0,
                  // has the effect of softening the shadow
                  spreadRadius: 0.0,
                  // has the effect of extending the shadow
                  offset: Offset(
                    0.0, // horizontal, move right 10
                    0.0, // vertical, move down 10
                  ),
                )
              ],
            ),
            child: child,
          );

        }

        if (forSliver) {

          child = SliverToBoxAdapter(
            child: child,
          );

        }

        return child;

      },
      initialData: null,
      future: initTimer(),
    );
  }

  Future<bool> initTimer() async {

    if (withTimer) {

      if (!_timerIsInitialized) {
        _timerIsInitialized = true;
        await Future.delayed(const Duration(seconds: 5), () {


          _timerIsFinish = true;

        });
      }

    }

    return Future<bool>.value(true);

  }
}