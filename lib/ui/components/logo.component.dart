
import 'package:flutter/widgets.dart';
import 'package:SaloSale/data.dart';

Widget logo() => Container(
    margin: const EdgeInsets.only(
        bottom: 30
    ),
    child: Text(
      Data.nameOfApp,
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Color(0xCCC21E01),
          fontWeight: FontWeight.bold,
          fontSize: 50
      ),
    ),
  );