

import 'package:SaloSale/routing.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

Widget menuItemComponent({
  String title,
  String afterTitle,
  Color colorBg,
  Color colorText: Colors.black,
  Color colorIcon: const Color(0xFF787878),
  Widget icon,
  Widget subTitle,
  IconData iconData,
  String url,
  Function onTap,
  Function onLongPress,
  Widget customTitle,
  Widget trailing,
  BuildContext context,
  bool useTranslate = true,
  EdgeInsets padding = const EdgeInsets.only(
      left: 15
  )
}) => Container(
  decoration: BoxDecoration(
    color: colorBg,
    border: Border(top: BorderSide(width: 1, color: Colors.grey.shade300)),
  ),
  child: ListTile(
    trailing: trailing ?? null,
    title: Row(
      children: <Widget>[
        icon ?? (iconData != null ? Icon(
          iconData,
          size: 20,
          color: colorIcon,
        ) : Container()),
        Flexible(
          child: Padding(
            padding: padding,
            child: customTitle ?? Text(
              (useTranslate ? FlutterI18n.translate(pageNavigatorKey.currentContext, title) : title) + (afterTitle ?? ''),
              style: TextStyle(
                  fontSize: 20,
                  color: colorText
              ),
            ),
          ),
        ),
      ],
    ),
    onLongPress: onLongPress,
    onTap: onTap ?? () {
//      pageNavigatorKey.currentState.pushNamed(url);
      Navigator.pushNamed(context, url);
    },
    subtitle: subTitle ?? null,
  ),
);