import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/notification.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/ui/pages/auth/main.auth.page.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NotificationIconComponent extends StatefulWidget {
  final bool showLogin;

  const NotificationIconComponent({
    Key key,
    this.showLogin = true,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NotificationIconComponent();
}

class _NotificationIconComponent extends State<NotificationIconComponent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (AuthService.authModel.isUser) {
      return StreamBuilder(
        stream: NotificationApi.notificationUnreadCounterChanel.stream,
        builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
//            if (snapshot.hasData && snapshot.data) {
//              NotificationApi.notificationUnreadCounterChanel.add(false);
//              return LoaderComponent();
//            }

          return Badge(
            position: BadgePosition.topRight(top: 5, right: 5),
            showBadge: AppService.currentIndexPage != 4
                ? snapshot.hasData
                    ? NotificationApi.notificationUnreadCount > 0
                    : false
                : false,
            badgeContent: Text(
              snapshot.hasData
                  ? NotificationApi.notificationUnreadCount.toString()
                  : '0',
              style: TextStyle(color: Colors.white),
            ),
            child: IconButton(
                onPressed: () {
                  Navigator.pushNamed(context, 'notifications');
                },
                icon: Icon(FontAwesomeIcons.bell)),
          );
        },
      );
    } else {
      if (widget.showLogin) {
        return GestureDetector(
          onDoubleTap: () {},
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => MainAuthPage(),
                ));
          },
          child: Container(
            height: 50,
            width: 75,
            padding: const EdgeInsets.only(right: 10),
            color: Colors.transparent,
            child: Center(
              child: Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'Login'),
                style: TextStyle(
                    color: Colors.orange,
                    fontWeight: FontWeight.w500,
                    fontSize: 18),
              ),
            ),
          ),
        );
      } else {
        return Container();
      }
    }
  }
}
