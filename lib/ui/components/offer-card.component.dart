import 'package:SaloSale/shared/models/api/offer.model.api.dart';
import 'package:SaloSale/shared/models/event.model.dart';
import 'package:SaloSale/ui/components/image.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/pages/offer/detail.offer.page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class OfferCardComponent extends StatefulWidget {

  final OfferModelApi offerModelApi;
  final EventModel eventModel;
  final String offerId;
  final int count;
  final String lastTook;
  final String filterItem;
  final bool fromCompany;
  final bool fromOffer;
  final bool showHeader;

  OfferCardComponent({
    this.eventModel,
    this.offerModelApi,
    this.filterItem,
    this.offerId,
    this.lastTook = '',
    this.count = 0,
    this.showHeader = true,
    this.fromOffer = false,
    this.fromCompany = false,
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OfferCardComponent();

}

class _OfferCardComponent extends State<OfferCardComponent> {

  bool likeIsClicked;

  String url = '';
  String id = '';

  @override
  initState() {

    likeIsClicked = false;

    if (widget.offerModelApi != null) {
      if (widget.offerModelApi.language != null) {
        if (widget.offerModelApi.language.images.length > 0) {
          url = widget.offerModelApi.language.images[0].url.buildUrl;
        }
      }
      id = widget.offerModelApi.id.toString();
    }

    if (widget.eventModel != null) {
      if (widget.eventModel.bannerUrl != null) {
        if (widget.eventModel.bannerUrl.length > 0) {
          url = widget.eventModel.bannerUrl[0].buildUrl;
        }
      }
      id = widget.eventModel.id.toString();
    }

    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    Widget child;

    if (widget.showHeader) {

      child = Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                left: 10.0,
                right: 10.0,
                top: 10,
                bottom: 10.0
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    _showLogo(widget.offerModelApi.company.id.toString()),
                    Text(
                      widget.offerModelApi.company.name,
                      style: TextStyle(
                          fontWeight: FontWeight.w500
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          _banner(),
          Container(
            padding: const EdgeInsets.all(10),
            child: Text(
              widget.offerModelApi?.language?.title ?? '',
              textAlign: TextAlign.left,
            ),
          )
        ],
      );

    } else {

      child = _banner();

    }

    return GestureDetector(
      onTap: () {
        var page = MaterialPageRoute(
          builder: (context) => DetailOfferPage(
            filterItem: widget.filterItem,
            offerId: int.parse(id),
            fromOffer: widget.fromOffer,
          ),
        );
        if (widget.fromCompany || widget.fromOffer) {
          Navigator.pushReplacement(context, page);
        } else {
          Navigator.push(context, page);
        }
      },
      child: Container(
        margin: const EdgeInsets.only(
          bottom: 15.0,
        ),
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
  //                  borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).primaryColor,
              blurRadius: 10.0,
              // has the effect of softening the shadow
              spreadRadius: 0.0,
              // has the effect of extending the shadow
              offset: Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
        ),
        child: child,
      ),
    );

  }

  Widget _showLogo(String documentID) {

    return Container(
      width: 30,
      margin: const EdgeInsets.only(
        right: 10.0,
      ),
      child: CachedNetworkImage(
        imageUrl: widget.offerModelApi.company.logoUrl.buildUrl,
        imageBuilder: (context, imageProvider) => Container(
          width: double.infinity,
          height: 30.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(50.0)),
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        placeholder: (context, url) => Container(
          width: double.infinity,
          height: 30.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(50.0)),
            color: Colors.white,
          ),
          child: LoaderComponent(
            withTimer: false,
          ),
        ),
        errorWidget: (context, url, error) => Container(
          width: double.infinity,
          height: 30.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(50.0)),
            color: Colors.grey,
          ),
        ),
      ),
    );

  }

  _banner() {

    return ImageComponent(
      useImageBuilder: false,
      withAspectRation: false,
      tag: (widget.fromOffer ? 'offer_similar_banner_' : 'offer_banner_') + id,
      url: url,
      withHero: false,
    );

  }

}