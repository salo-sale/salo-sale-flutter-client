import 'dart:async';


import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/recommendation.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/models/api/recommendation-banner.model.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/disconnect.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/pages/offer/detail.offer.page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:SaloSale/data.dart';

class RecommendedOffersComponent extends StatefulWidget {

  final bool withSilver;

  RecommendedOffersComponent({
    Key key,
    this.withSilver = true,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _RecommendedOffersComponent();

}

class _RecommendedOffersComponent extends State<RecommendedOffersComponent> with TickerProviderStateMixin  {

  @override
  initState() {

    super.initState();

    AppService.systemSteps.stream.listen((event) {

      if (event == SystemStepsTypeEnum.INITIALIZED) {

        RecommendationApi.getDocumentList();

      }

    });

  }

  int _current = 0;

  @override
  Widget build(BuildContext context) {

    Widget child = StreamBuilder<bool>(
        stream: AppService.refreshAdvertisingBanners.stream,
        builder: (context, snapshot) {

          if (snapshot.hasData && snapshot.data) {

            Timer(Duration(seconds: 1), () {
              _refresh();
            });

            return AspectRatio(
              child: LoaderComponent(
                refreshFunction: _refresh,
              ),
              aspectRatio: 16/9,
            );

          } else {

            return StreamBuilder<List<RecommendationBannerModelApi>>(
                stream: RecommendationApi.filter.documentListChanel.stream,
                builder: (BuildContext context, AsyncSnapshot<List<RecommendationBannerModelApi>> snapshot) {

                  Widget _slideShow;

                  if (snapshot.hasData) {

                    if (snapshot.data.length > 0) {

                      _slideShow = Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CarouselSlider(
                            enableInfiniteScroll: true,
                            viewportFraction: 1.0,
                            autoPlay: snapshot.data.length > 1,
                            enlargeCenterPage: true,
                            onPageChanged: (index) {
                              setState(() {
                                _current = index;
                              });
                            },
                            autoPlayInterval: Duration(
                                seconds: 5
                            ),
                            items: snapshot.data.map((item) {
                              return Builder(
                                builder: (BuildContext context) {
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => DetailOfferPage(
                                              offerId: item.offerId,
                                              fromAdvertisingBanners: true
                                          ),
                                        ),
                                      );
                                    },
                                    child: ClipRRect(
                                      child: Hero(
                                        tag: Data.prefixRecommendedOffer + item.offerId.toString(),
                                        transitionOnUserGestures: true,
                                        child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(Radius.circular(50)),
                                            ),
                                            child: CachedNetworkImage(
                                                imageUrl: item.bannerUrl.buildUrl,
                                                key: Key(item.bannerUrl.buildUrl),
                                                placeholder: (context, url) => LoaderComponent(
                                                  withTimer: false,
                                                ),
                                                errorWidget: (context, url, error) => Container(
                                                  width: double.infinity,
                                                  child: Center(
                                                    child: Icon(
                                                      FontAwesomeIcons.image,
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                ),
                                                width: double.infinity,
                                                fit: BoxFit.cover
                                            )
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              );
                            }).toList(),
                          ),
                          Container(
                            width: double.infinity,
                            height: 50,
                            child: Center(
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                children: snapshot.data.map((url) {
                                  int index = snapshot.data.indexOf(url);
                                  return Container(
                                    width: 8.0,
                                    height: 8.0,
                                    margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: _current == index
                                          ? Color.fromRGBO(0, 0, 0, 0.9)
                                          : Color.fromRGBO(0, 0, 0, 0.4),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ],
                      );

                    } else {

                      _slideShow = null;

                    }

                  } else {

                    _slideShow = AspectRatio(
                      child: Container(
                        color: Colors.grey,
                      ),
                      aspectRatio: 16/9,
                    );

                  }

                  if (_slideShow != null) {

                    return _slideShow;

                  } else {

                    if (!AppService.internetIsConnected) {

                      return disconnectComponent('TEXT_DISCONNECT_RECOMMENDED_COMPONENT');

                    }

                    return Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0,
                          left: 10.0,
                          right: 10.0
                      ),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10.0,
                                bottom: 15.0
                            ),
                            child: Icon(
                              FontAwesomeIcons.grinBeamSweat,
                              size: 50,
                              color: Colors.grey.shade300,
                            ),
                          ),
                          Text(
                            FlutterI18n.translate(pageNavigatorKey.currentContext, 'TEXT_FINISH_RECOMMENDATION'),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.grey
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                child: FlatButton(
                                  highlightColor: Colors.grey.shade200,
                                  splashColor: Colors.grey.shade200,
                                  hoverColor: Colors.grey.shade200,
                                  focusColor: Colors.grey.shade200,
                                  onPressed: () {
                                    Tools.launchUrl(AppService.remoteConfig.links['form']);
                                  },
                                  child: Text(
                                    FlutterI18n.translate(pageNavigatorKey.currentContext, 'Yes'),
                                    style: TextStyle(
                                        color: Colors.orange
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    );

                  }

                }
            );

          }
        }
    );

    if (widget.withSilver) {

      child = SliverToBoxAdapter(
        child: child,
      );

    }

    return child;

  }

  void _refresh() {

    AppService.refreshAdvertisingBanners.sink.add(false);

    RecommendationApi.refreshDocumentList(tagIdList: RecommendationApi.filter.tagIdList);

  }

}