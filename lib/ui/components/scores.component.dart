
import 'package:SaloSale/shared/models/company-score.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/ui/components/starts.component.dart';
import 'package:flutter/widgets.dart';

class ScoresComponent extends StatelessWidget {

  final List<CompanyScoreModel> scores;

  ScoresComponent({
    this.scores,
  });

  @override
  Widget build(BuildContext context) {

    if (scores == null || scores.length == 0) {

      return Container();

    }

    return Container(
      height: (((scores.length / 3) + .5).toInt() * 50).toDouble(),
      padding: const EdgeInsets.all(10.0),
      child: GridView.count(
        // Create a grid with 2 columns. If you change the scrollDirection to
        // horizontal, this produces 2 rows.
        crossAxisCount: 3,
        physics: new NeverScrollableScrollPhysics(),
        // Generate 100 widgets that display their index in the List.
        children: List.generate(scores.length, (i) {
          return Container(
            height: 50,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        (AppService.listsOfTagsMap['score']?.tagList?.firstWhere((element) => element.id == scores[i].tagId)?.name ?? ''),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                        '${scores[i].score} '
                    ),
                    StartsComponent(
                      value: scores[i].score,
                      iconSize: 14.0,
                    ),
                  ],
                )
              ],
            ),
          );
        }),
      ),
    );

  }

}