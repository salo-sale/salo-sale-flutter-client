import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/enums/types/scroll-event.type.enum.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rxdart/rxdart.dart';

class ScrollEventComponent extends StatelessWidget {

  final ValueStream<ScrollEventTypeEnum> stream;
  final String text;
  final String textEmpty;
  final bool useSliver;
  final IconData icon;
  final IconData iconEmpty;
  final Widget defaultRenderWidget;

  ScrollEventComponent({
    this.text,
    this.textEmpty,
    this.stream,
    this.icon,
    this.iconEmpty,
    this.useSliver = true,
    Widget defaultRenderWidget,
  }) : this.defaultRenderWidget = defaultRenderWidget ?? Container(
    padding: const EdgeInsets.only(
      top: 20,
      left: 10,
      right: 10,
      bottom: 100
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, text ?? "FOOTER_SCROLL_CONTENT"),
          style: TextStyle(
            color: Colors.orange,
            fontWeight: FontWeight.w500,
            fontSize: 25.0,
          ),
          textAlign: TextAlign.center,
        ),
        Padding(
          padding: const EdgeInsets.only(
              top: 10
          ),
          child: Icon(
            icon ?? FontAwesomeIcons.solidGrinWink,
            color: Colors.orange,
            size: 40,
          ),
        )
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {

    return StreamBuilder<ScrollEventTypeEnum>(
        stream: stream,
        builder: (BuildContext context, AsyncSnapshot<ScrollEventTypeEnum> snapshot) {

          Widget returnWidget = Container();

          if (snapshot.hasData) {

            if (snapshot.data == ScrollEventTypeEnum.LOADER) {

              returnWidget = Container(
                  height: 150.0,
                  child: LoaderComponent(
                    withTimer: false,
                  )
              );

            } else if (snapshot.data == ScrollEventTypeEnum.EMPTY) {

              returnWidget = Container(
                padding: const EdgeInsets.only(
                    top: 20,
                    left: 10,
                    right: 10,
                    bottom: 100
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, textEmpty ?? "FOOTER_SCROLL_CONTENT_EMPTY"),
                      style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.w500,
                        fontSize: 25.0,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 10
                      ),
                      child: Icon(
                        iconEmpty ?? FontAwesomeIcons.solidGrinWink,
                        color: Colors.orange,
                        size: 40,
                      ),
                    )
                  ],
                ),
              );

            } else {

              returnWidget = defaultRenderWidget;

            }


          }

          if (useSliver) {

            returnWidget = SliverToBoxAdapter(
              child: returnWidget,
            );

          }

          return returnWidget;

        },
      );

  }

}