
import 'dart:math';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

Widget searchInputComponent(
  myFocusNode, 
  searchInputController, 
  Function clickSuffix, 
  Function onFieldSubmitted, 
  {
    String searchTitle, 
    bool withSliver: true, 
    bool withCityName: true,
    bool withPadding: true
  }
) {

  if (searchTitle == null) {
    searchTitle = FlutterI18n.translate(pageNavigatorKey.currentContext, 'Search in');
  }

  if (withCityName) {
    searchTitle +=  ' ' + (AppService.deviceInfoModel?.localityModelApi?.name ?? '');
  }

  Widget child = TextField(
        key: Key((Random().nextInt(100)).toString()),
        focusNode: myFocusNode,
        cursorColor: Colors.orange,
        controller: searchInputController,
        decoration: InputDecoration(
          prefix: Padding(
            padding: const EdgeInsets.only(
                right: 10
            ),
            child: Icon(
              FontAwesomeIcons.search,
              size: 18,
              color: Colors.grey.shade300,
            ),
          ),
          suffix: GestureDetector(
            onTap: clickSuffix,
            child: Container(
              width: 25,
              height: 25,
              color: Colors.transparent,
              child: Icon(
                FontAwesomeIcons.times,
                size: 18,
                color: Colors.orange,
              ),
            ),
          ),
          contentPadding: const EdgeInsets.only(
              top: 0,
              bottom: 0,
              left: 20,
              right: 20
          ),
          labelText: searchTitle,
          labelStyle: TextStyle(
              fontWeight: FontWeight.w500,
              color: Colors.grey.shade500
          ),
          fillColor: Colors.white,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
            // width: 0.0 produces a thin "hairline" border
            borderSide: BorderSide(color: Colors.grey.shade400, width: 0.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
            borderSide: BorderSide(
                width: 0.0,
                color: Colors.orange
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
            borderSide: BorderSide(
                width: 0.0,
                color: Colors.grey.shade300
            ),
          ),
        ),
        onSubmitted: onFieldSubmitted,
    );

  if (withPadding) {

    child = Padding(
      padding: const EdgeInsets.only(
          left: 10,
          right: 10,
          bottom: 10
      ),
      child: child,
    );

  }

  if (withSliver) {


    child = SliverToBoxAdapter(
      child: child,
    );

  }

  return child;

}