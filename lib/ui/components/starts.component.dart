import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StartsComponent extends StatelessWidget {

  final double value;
  final double iconSize;
  const StartsComponent({Key key, this.value = 0.0, this.iconSize = 40.0})
      : assert(value != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(5, (index) {
        double rating = value - index;
        return Icon(
          rating > 0 ? rating > 0.5 ? Icons.star : Icons.star_half : Icons.star_border,
          color: rating > 0 ? Colors.orange : Colors.grey.shade400,
          size: iconSize,
        );
      }),
    );
  }


}