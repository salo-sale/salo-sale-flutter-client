

import 'package:SaloSale/shared/models/tag.model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

Widget tagListComponent(List<TagModel> tagList) {

  // TODO click on tag and open page with filter sleected tag

  if (tagList == null || tagList.length == 0) {
    return Container();
  }

  return Container(
    margin: const EdgeInsets.only(
      bottom: 10
    ),
    width: double.infinity,
    decoration: BoxDecoration(
//        color: Colors.grey.shade300
        color: Colors.transparent
    ),
    height: 50,
    child: ListView(
      scrollDirection: Axis.horizontal,
      children: tagList.map((tag) {

        return GestureDetector(
          onTap: () {

          },
          child: Container(
            padding: const EdgeInsets.only(
                left: 10,
                right: 10,
                top: 10,
                bottom: 10
            ),
            decoration: BoxDecoration(
//                color: Colors.grey.shade300
                color: Colors.transparent
            ),
            child: Row(
              children: <Widget>[
                Text(
                    '#'
                ),
                Text(
                    tag.name
                )
              ],
            ),
          ),
        );

      }).toList(),
    ),
  );

}