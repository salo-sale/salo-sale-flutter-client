
import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';

import 'package:SaloSale/shared/models/tag.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class TagsComponent extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _TagsComponent();
  final String tagListName;
  final bool withSilver;
  final List<int> selectedTagIdList;

  const TagsComponent({
    Key key,
    this.onSelectedTagIdList,
    this.selectedTagIdList,
    this.tagListName,
    this.withSilver = true
  }) : super(key: key);

  final SelectedTagIdListCallbackComponent onSelectedTagIdList;

}

class _TagsComponent extends State<TagsComponent> {

  final List<TagModel> tags = List<TagModel>();
  List<int> selectedTagIdList = List<int>();

  StreamSubscription refreshTags;

  bool initialized = false;

  @override
  void dispose() {
    initialized = true;
    refreshTags?.cancel();
    super.dispose();
  }

  @override
  void initState() {

    super.initState();

    if (widget.selectedTagIdList != null && widget.selectedTagIdList.length > 0) {

      selectedTagIdList = widget.selectedTagIdList;

    }

    AppService.systemSteps.stream.listen((event) {

      if (event == SystemStepsTypeEnum.INITIALIZED) {
        if (!initialized) {
          initialized = true;
          selectedTagIdList.add(0);
          initTags();

          refreshTags = AppService.refreshTags.listen((refresh) {

            if (refresh) {

              initTags();

            }

          });
        }
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    Widget child = Column(
      children: <Widget>[
        Container(
//            color: Colors.white,
          height: 65.0,
          padding: const EdgeInsets.only(
              top: 5,
              bottom: 10
          ),
          child: tags != null && tags.length > 1 ? ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: tags.length,
            itemBuilder: (context, index) {

              var tag = tags[index];

              var isSelected = selectedTagIdList.firstWhere((tagId) => tagId == tag.id, orElse: () => null) != null;

              return GestureDetector(
                onTap: () {
                  if (!isSelected) {
                    if (tag.id == 0) {
                      selectedTagIdList.clear();
                    } else {
                      selectedTagIdList.removeWhere((tagId) => tagId == 0);
                    }
                    selectedTagIdList.add(tag.id);
                  } else {
                    if (tag.id != 0) {
                      selectedTagIdList.removeWhere((tagId) =>
                      tagId == tag.id);
                    }
                  }
                  if (selectedTagIdList.length == 0) {
                    selectedTagIdList.add(0);
                  }
                  widget.onSelectedTagIdList(selectedTagIdList.length == 1 ? selectedTagIdList[0] == 0 ? null : selectedTagIdList : selectedTagIdList);
                  setState(() {});
                },
                child: Container(
                  color: Colors.transparent,
                  padding: const EdgeInsets.only(
                      left: 15,
                      right: 15,
                      top: 0,
                      bottom: 0
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        tag.iconData,
                        color: isSelected ? Colors.orange : Color(0xFF8E8E93),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 5
                        ),
                        child: Text(
                          tag.name,
//                              FlutterI18n.currentLocale(context) == null ? tag.name : FlutterI18n.currentLocale(context).languageCode != 'uk' ? tag.nameEn : tag.name ?? FlutterI18n.translate(context, tag.nameEn),
                          style: TextStyle(
                              color: isSelected ? Colors.orange : Colors.grey,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );

            },
          ) : LoaderComponent(
            withTimer: false,
            circularMargin: const EdgeInsets.only(
                top: 5,
                bottom: 5
            ),
          ),
        ),
      ],
    );
    if (widget.withSilver) {
      child = SliverToBoxAdapter(
        child: child,
      );
    }
    return child;
  }

  void initTags() {

    if (tags.length == 0) {

      tags.add(TagModel.fromMap({
        'id': '0',
        'name': FlutterI18n.translate(pageNavigatorKey.currentContext, 'All'),
        'icon': 'All',
        'main': '1',
        'position': '1'
      }));

    }


    if (AppService.listsOfTags != null && AppService.listsOfTags.length != 0) {

      tags.addAll(AppService.listsOfTags.firstWhere((listOfTags) => listOfTags.name == widget.tagListName).tagList);

    }

    setState(() {


    });

  }

}

typedef SelectedTagIdListCallbackComponent = void Function(List<int> tagIdList);