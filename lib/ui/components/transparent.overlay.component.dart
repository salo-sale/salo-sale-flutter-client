import 'package:SaloSale/routing.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class TransparentOverlayComponent extends ModalRoute<void> {
  TransparentOverlayComponent({this.child, this.hideCloseButton});
  Widget child;
  bool hideCloseButton;

  @override
  Duration get transitionDuration => Duration(milliseconds: 350);

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => false;

  @override
  Color get barrierColor => Colors.black.withOpacity(0.6);

  @override
  String get barrierLabel => null;

  @override
  bool get maintainState => true;

  @override
  Widget buildPage(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      ) {
    // This makes sure that text and other content follows the material style
    return Material(
      type: MaterialType.transparency,
      // make sure that the overlay content is not cut off
      child: _buildOverlayContent(context),
    );
  }

  Widget _buildOverlayContent(BuildContext context) {
    return child;
  }

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // You can add your own animations for the overlay content
    return FadeTransition(
      opacity: animation,
      child: ScaleTransition(
        scale: animation,
        child: Center(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                color: Colors.white
            ),
            padding: const EdgeInsets.all(15.0),
            width: MediaQuery.of(pageNavigatorKey.currentContext).size.width * 0.8,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                child,
                hideCloseButton ? Container() : FlatButton(
                  textColor: Colors.orange,
                  onPressed: () {
                    pageNavigatorKey.currentState.pop();
                  },
                  child: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Close')
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}