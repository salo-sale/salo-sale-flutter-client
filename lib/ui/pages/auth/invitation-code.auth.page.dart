import 'package:SaloSale/routing.dart';

import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/validators.util.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
class InvitationCodeAuthPage extends StatefulWidget {

  InvitationCodeAuthPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _InvitationCodeAuthPage();

}

class _InvitationCodeAuthPage extends State<InvitationCodeAuthPage> {

  final _formKey = GlobalKey<FormState>();
  final TextEditingController invitationCodeController = TextEditingController();

  String _errorMessage;
  bool _isLoading;

  @override
  void initState() {

    _errorMessage = "";
    _isLoading = false;

    super.initState();

  }

  @override
  void dispose() {

    invitationCodeController.dispose();
    super.dispose();

  }

  // Check if form is valid before perform login
  bool _saveAndValidate() {

    final form = _formKey.currentState;

    form.save();

    if (form.validate()) {

      return true;

    }

    setState(() {

      _isLoading = false;

    });

    return false;

  }

  void _validateAndSubmit() async {

    setState(() {

      _errorMessage = "";
      _isLoading = true;

    });

    if (_saveAndValidate()) {

      try {

        if (invitationCodeController.text.isNotEmpty) {

          AuthService.checkInvitationCode(invitationCodeController.text).then((value) {


            if (value) {
              Navigator.pop(context, invitationCodeController.text);
              AppService.message(MessageModel(
                  title: 'Invitation code',
                  message: 'You can use this code',
                  backgroundColor: Colors.green,
                  duration: null,
                  mainButton: null
              ));
            } else {
              AppService.message(MessageModel(
                  title: 'Invitation code',
                  message: 'This code is inactive, your friend may not have yet verified their inbox',
                  backgroundColor: Colors.red,
                  duration: null,
                  mainButton: null
              ));
            }
          });
          AppService.analytics.logSignUp(signUpMethod: 'CHECK_INVITATION');

        }

        setState(() {

          _isLoading = false;

        });

      } catch (e) {

        AppService.message(MessageModel(
            title: 'Invitation code',
            message: 'An unknown error occurred, please try again later',
            backgroundColor: Colors.red,
            duration: null,
            mainButton: null
        ));

        setState(() {

          _isLoading = false;

        });

      }

    }
  }
  ///
  /// Build
  ///

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          FlutterI18n.translate(context, 'Invitation code'),
          style: TextStyle(
              fontSize: 22.5,
              fontWeight: FontWeight.bold
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back_ios,),
        ),
      ),
      body: Stack(
        children: <Widget>[
          _body(),
          _circularProgress(),
        ],
      ),
    );

  }

  ///
  /// Body - ListView
  ///

  Widget _body(){
    return Form(
      key: _formKey,
      child: ListView(
        padding: const EdgeInsets.only(
            left: 15,
            right: 15,
            bottom: 15
        ),
        shrinkWrap: true,
        children: <Widget>[
          _optionalInvitationCodeInput(),
          _buttonInvitationCode(),
          _primaryButton(),
          _showErrorMessage(),
        ],
      ),
    );
  }

  ///
  /// Show error message
  ///

  Widget _showErrorMessage() {
    if (_errorMessage != null && _errorMessage.length > 0) {
      return Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return Container(
        height: 0.0,
      );
    }
  }

  ///
  /// Primary button
  ///

  Widget _primaryButton() {

    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 50.0,
          child: RaisedButton(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)
            ),
            color: Color(0xFF343434),
            child: Text(
                FlutterI18n.translate(context, 'Use'),
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white
                )
            ),
            onPressed: () {

              if (AppService.internetIsConnected) {

                _validateAndSubmit();

              } else {

                AppService.message(MessageModel(
                    title: 'Invitation code',
                    message: 'TEXT_DISCONNECT',
                    backgroundColor: Colors.orangeAccent,
                    duration: null,
                    mainButton: null
                ));

              }

            },
          ),
        )
    );

  }

  ///
  /// Loader
  ///

  Widget _circularProgress() {

    if (_isLoading) {

      return Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.orange,
          )
      );

    } else {

      return Container(
        height: 0.0,
        width: 0.0,
      );

    }

  }

  Widget _buttonInvitationCode() {

    return GestureDetector(
      onTap: scan,
      child: Container(
        padding: const EdgeInsets.symmetric(
            vertical: 15,
            horizontal: 10
        ),
        margin: const EdgeInsets.symmetric(
            vertical: 10
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(7)),
            color: Color(0xCCC21E01)
        ),
        child: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Open the scanner'),
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w500
          ),
        ),
      ),
    );

  }

  ///
  /// Option invitation code
  /// With qr-scanner
  ///

  Widget _optionalInvitationCodeInput() {

    return Padding(
      padding: const EdgeInsets.only(
          top: 15.0
      ),
      child: TextFormField(
        cursorColor: Colors.orange,
        controller: invitationCodeController,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(
              right: 50.0,
              top: 15.0,
              left: 15.0,
              bottom: 15.0
          ),
          hintText: FlutterI18n.translate(context, 'Invitation code'),
            prefixIcon: Icon(
            FontAwesomeIcons.qrcode,
            color: Colors.grey,
          )
        ),
        validator: (value) {
          return ValidatorsUtil.checkEmptyValue(context, value);
        },
      ),
    );

  }

  Future<void> scan() async {
    BarcodeScanner.scan().then((code) {

      setState(() {

        invitationCodeController.text = code;

      });
    });

  }
}