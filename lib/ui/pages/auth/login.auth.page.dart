import 'package:SaloSale/routing.dart';

import 'package:SaloSale/shared/forms/login.form.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/validators.util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginAuthPage extends StatefulWidget {

  LoginAuthPage({Key key}) :super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginAuthPage();

}

class _LoginAuthPage extends State<LoginAuthPage> {

  final _formKey = GlobalKey<FormState>();

  final LoginForm _loginForm = LoginForm();

  bool _isLoading;

  bool passwordObscure;

  @override
  void initState() {

    _isLoading = false;
    passwordObscure = true;
    AppService.analytics.logEvent(name: 'OPEN_LOGIN_PAGE');

    super.initState();

  }

  // Check if form is valid before perform login
  bool _validateAndSave() {

    final form = _formKey.currentState;

    if (form.validate()) {

      form.save();

      return true;

    }

    setState(() {

      _isLoading = false;

    });

    return false;

  }

  void _validateAndSubmit({bool withOutValidate: false}) async {

    setState(() {

      _isLoading = true;

    });

    if (withOutValidate || _validateAndSave()) {

      await AuthService.login(_loginForm).then((success) {

        if (success) {

          Navigator.pop(context);
          Navigator.pop(context);

        }

      });

      setState(() {

        _isLoading = false;

      });

    }
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
              FontAwesomeIcons.angleLeft,
              color: Colors.grey
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          FlutterI18n.translate(context, 'Login'),
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 20
          ),
        ),
      ),
      body: SafeArea(
        top: false,
        child: Stack(
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                _body(),
                _circularProgress(),
              ],
            ),
            _primaryButton(),
          ],
        ),
      ),
    );

  }

  Widget _body(){
    return Form(
      key: _formKey,
      child: ListView(
        padding: const EdgeInsets.only(
          top: 20,
          left: 15.0,
          right: 15.0,
          bottom: 15.0
        ),
        shrinkWrap: true,
        children: <Widget>[
          _emailInput(),
          _passwordInput(),
          _resetPasswordLink(),
//          _loginViaSocialNetWork(),
        ],
      ),
    );
  }

  Widget _emailInput() {
    return TextFormField(
      maxLines: 1,
      cursorColor: Colors.orange,
      keyboardType: TextInputType.emailAddress,
      autofocus: true,
      initialValue: _loginForm.email,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, 'E-mail'),
        prefixIcon: Icon(
          Icons.mail,
          color: Colors.grey,
        )
      ),
      validator: (value) {
        return ValidatorsUtil.email(context, value.trim().toLowerCase());
      },
      onSaved: (value) => _loginForm.email = value.trim().toLowerCase(),
    );
  }

  Widget _passwordInput() {

    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        cursorColor: Colors.orange,
        initialValue: _loginForm.password,
        obscureText: passwordObscure,
        autofocus: false,
        decoration: InputDecoration(
          suffixIcon: GestureDetector(
            onTap: () {
              setState(() {
                passwordObscure = !passwordObscure;
              });
            },
            child: Container(
              color: Colors.transparent,
              child: Icon(
                passwordObscure ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                color: Colors.grey,
                size: 12.5,
              ),
            ),
          ),
          hintText: FlutterI18n.translate(context, 'Password'),
          prefixIcon: Icon(
            Icons.lock,
            color: Colors.grey,
          )
        ),
        validator: (value) {
          return ValidatorsUtil.password(context, value);
        },
        onSaved: (value) => _loginForm.password = value,
      ),
    );

  }

  Widget _resetPasswordLink() {

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        GestureDetector(
          onTap: () {
            pageNavigatorKey.currentState.pushNamed('auth/reset-password');
          },
          child: Container(
            padding: const EdgeInsets.only(
              top: 12.5,
            ),
            child: Text(
              FlutterI18n.translate(context, 'Reset password'),
              style: TextStyle(
                  color: Colors.orange,
                  fontSize: 20
              ),
            ),
          ),
        )
      ],
    );

  }

  Widget _primaryButton() {

    return Positioned.fill(
      child: Align(
        alignment: Alignment.bottomCenter,
        child: GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 10
            ),
            decoration: BoxDecoration(
              color: Colors.orange,
            ),
            child: Text(
              FlutterI18n.translate(context, 'Sign in'),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 20.0,
                color: Colors.white
              )
            ),
          ),
          onTap: () {
            if (AppService.internetIsConnected) {

              _validateAndSubmit();
            } else {


              AppService.message(MessageModel(
                  title: 'Login',
                  message: 'TEXT_DISCONNECT',
                  backgroundColor: Colors.orangeAccent,
                  duration: null,
                  mainButton: null
              ));

            }
          },
        ),
      ),
    );

  }

  Widget _circularProgress() {

    if (_isLoading) {

      return Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.orange,
        )
      );

    } else {

      return Container(
        height: 0.0,
        width: 0.0,
      );

    }

  }

}