import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/app.api.dart';
import 'package:SaloSale/shared/forms/login.form.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/services/firebase/messaging.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/logo.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class MainAuthPage extends StatefulWidget {
  MainAuthPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MainAuthPage();
}

class _MainAuthPage extends State<MainAuthPage> {
  final Future<bool> _isAvailableFuture = SignInWithApple.isAvailable();

  final LoginForm _loginForm = LoginForm();

  bool loader = false;
  bool supportsAppleSignIn = false;

  final GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
    ],
  );

  Future<void> _handleSignIn() async {
    try {

      BuildContext dialogContext;
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            dialogContext = context;
            return AlertDialog(
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  LoaderComponent(
                    withTimer: false,
                  ),
                ],
              ),
            );
          });

      bool success = false;
      String message;

      try {

        GoogleSignInAccount account = await _googleSignIn.signIn();

        _loginForm.accessToken = (await account.authentication).accessToken;
        _loginForm.socialId = account.id;
        _loginForm.email = account.email;
        _loginForm.avatar = account.photoUrl ?? null;
        if (account.displayName.split(' ').length > 1) {
          _loginForm.firstName = account.displayName.split(' ')[0];
          _loginForm.lastName = account.displayName.split(' ')[1];
        } else {
          _loginForm.firstName = account.displayName;
        }

        _loginForm.authSocialNetworkId = '3';
        _loginForm.checkedAllRequiredAgreements = '1';

        await AuthService.login(_loginForm, socialNetWork: true).then((_success) {
          success = _success;

          if (success) {
            Navigator.pop(dialogContext);
            Navigator.pop(context);
          }
        });

        if (!success) {
          Navigator.pop(dialogContext);
          AppService.message(MessageModel(
              title: 'Continue with Google',
              message: message ?? 'Sorry, there was an error',
              backgroundColor: Colors.red,
              duration: null,
              mainButton: null));
        }
      } catch (e) {
        Navigator.pop(dialogContext);
        AppService.message(MessageModel(
            title: 'Continue with Google',
            message: e.toString(),
            backgroundColor: Colors.orangeAccent,
            duration: null,
            mainButton: null));
      }

    } catch (error) {
      print(error);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  Future _refresh() async {
    return await AppService.checkInternetConnect(refresh: true).then((value) {
      setState(() {});
    });
  }

  Base64Codec base64 = const Base64Codec();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        color: Colors.orange,
        onRefresh: _refresh,
        child: SafeArea(
          top: false,
          child: StreamBuilder<bool>(
              stream: AppService.refreshLayout.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data) {
                  Timer(Duration(milliseconds: 500), () {
                    AppService.refreshLayout.sink.add(false);
                  });

                  return LoaderComponent(
                    withTimer: false,
                  );
                } else {
                  return _body();
                }
              }),
        ),
      ),
    );
  }

  Widget _body() {
    return ListView(
      padding:
          const EdgeInsets.only(top: 75, left: 15.0, right: 15.0, bottom: 15.0),
      shrinkWrap: true,
      children: <Widget>[
        logo(),
        _connectCommunicate(),
        _localityAndLanguage(),
        _loginViaSocialNetWork(),
        _registrationLink(),
        Text(
          FlutterI18n.translate(context, 'Already have an account') + ' ?',
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.grey.shade400, fontWeight: FontWeight.w500),
        ),
        _primaryButton(),
        _demoAuthButton(),
        Html(
          data: FlutterI18n.translate(context, 'html_agreements'),
          customTextAlign: (node) => TextAlign.center,
          defaultTextStyle: TextStyle(color: Colors.grey),
          linkStyle: TextStyle(
            color: Colors.grey,
            decoration: TextDecoration.underline,
          ),
          onLinkTap: Tools.launchUrl,
        ),
        Center(
          child: GestureDetector(
            onTap: () => Navigator.pushNamed(context, 'help'),
            child: Container(
              margin: const EdgeInsets.only(top: 25),
              color: Colors.transparent,
              child: Text(
                FlutterI18n.translate(
                        pageNavigatorKey.currentContext, 'How it works') +
                    "?",
                style: TextStyle(
                    color: Colors.grey.shade400,
                    fontWeight: FontWeight.w500,
                    fontSize: 16),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _primaryButton() {
    return Padding(
        padding: const EdgeInsets.only(top: 25.0, bottom: 10),
        child: GestureDetector(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(7)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.orange.shade300,
                  offset: Offset(0.0, 0.0),
                  blurRadius: 2.5,
                ),
              ],
            ),
            child: Text(FlutterI18n.translate(context, 'Sign in'),
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400,
                    color: Colors.orange)),
          ),
          onTap: () => pageNavigatorKey.currentState.pushNamed('auth/login'),
        ));
  }

  Widget _demoAuthButton() {
    return Padding(
        padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
        child: Container(
          child: FlatButton(
            color: Colors.transparent,
            textColor: Colors.orange,
            child: Text(
              FlutterI18n.translate(context, 'I just want to look around'),
              style: TextStyle(fontWeight: FontWeight.w300),
            ),
            onPressed: () async {
              setState(() {
                AppService.showLoaderWindow();
              });

              Navigator.pop(context);

              setState(() {
                AppService.closeLoaderWindow();
              });
            },
          ),
        ));
  }

  Widget _loginViaSocialNetWork() {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 10
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//        Expanded(
//          flex: 1,
//          child: GestureDetector(
//            onTap: () async {
//              BuildContext dialogContext;
//              showDialog(
//                  context: context,
//                  barrierDismissible: false,
//                  builder: (context) {
//                    dialogContext = context;
//                    return AlertDialog(
//                      content: Column(
//                        mainAxisSize: MainAxisSize.min,
//                        children: <Widget>[
//                          LoaderComponent(
//                            withTimer: false,
//                          ),
//                        ],
//                      ),
//                    );
//                  });
//
//              final result = await FacebookLogin().logIn(['email']);
//
//              switch (result.status) {
//                case FacebookLoginStatus.loggedIn:
//                  _loginForm.accessToken = result.accessToken.token;
//                  _loginForm.authSocialNetworkId = '1';
//                  _loginForm.checkedAllRequiredAgreements = '1';
//                  await AuthService.login(_loginForm, socialNetWork: true)
//                      .then((success) {
//                    Navigator.pop(dialogContext);
//
//                    if (success) {
//                      Navigator.pop(context);
//                    }
//                  });
//                  break;
//                case FacebookLoginStatus.cancelledByUser:
//                  Navigator.pop(dialogContext);
//                  break;
//                case FacebookLoginStatus.error:
//                  Navigator.pop(dialogContext);
//                  break;
//              }
//            },
//            child: Container(
//              decoration: BoxDecoration(
//                //                color: Color(0xFF3B5998),
//                borderRadius: BorderRadius.all(Radius.circular(7)),
//                border: Border.all(width: 1, color: Color(0xFF3B5998)),
//              ),
//              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
//              margin: const EdgeInsets.only(
//                  bottom: 10,
//                  right: 5
//              ),
//              child: Row(
//                mainAxisAlignment: MainAxisAlignment.center,
//                children: <Widget>[
//                  Icon(
//                    FontAwesomeIcons.facebook,
//                    color: Color(0xFF3B5998),
//                    //                    color: Colors.white,
//                  ),
////                  Padding(
////                    padding: const EdgeInsets.only(left: 10),
////                    child: Text(
////                      FlutterI18n.translate(
////                          context, 'Continue with Facebook'),
////                      style: TextStyle(
////                          color: Color(0xFF3B5998),
////                          //                        color: Colors.white,
////                          fontSize: 20.0,
////                          fontWeight: FontWeight.w500),
////                    ),
////                  ),
//                ],
//              ),
//            ),
//          ),
//        ),

          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.only(
                right: 5
              ),
              child: SizedBox(
                height: 45,
                child: FlatButton(
                  onPressed: () async {
                    BuildContext dialogContext;
                    showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (context) {
                          dialogContext = context;
                          return AlertDialog(
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                LoaderComponent(
                                  withTimer: false,
                                ),
                              ],
                            ),
                          );
                        });

                    final result = await FacebookLogin().logIn(['email']);

                    switch (result.status) {
                      case FacebookLoginStatus.loggedIn:
                        _loginForm.accessToken = result.accessToken.token;
                        _loginForm.authSocialNetworkId = '1';
                        _loginForm.checkedAllRequiredAgreements = '1';
                        await AuthService.login(_loginForm, socialNetWork: true)
                            .then((success) {
                          Navigator.pop(dialogContext);

                          if (success) {
                            Navigator.pop(context);
                          }
                        });
                        break;
                      case FacebookLoginStatus.cancelledByUser:
                        Navigator.pop(dialogContext);
                        break;
                      case FacebookLoginStatus.error:
                        Navigator.pop(dialogContext);
                        break;
                    }
                  },
                  color: Colors.white,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Color(0xFF3B5998),
                  splashColor: Color(0xFF3B5998),
                  shape: RoundedRectangleBorder(
                      side: BorderSide(
                          color: Color(0xFF3B5998),
                          width: 1,
                          style: BorderStyle.solid
                      ),
                      borderRadius: BorderRadius.circular(7)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.facebook,
                        color: Color(0xFF3B5998),
                      ),
//                          Padding(
//                            padding: const EdgeInsets.only(left: 10),
//                            child: Text(
//                              FlutterI18n.translate(
//                                  context, 'Continue with Apple'),
//                              style: TextStyle(
//                                  color: Colors.black,
//                                  fontSize: 20.0,
//                                  fontWeight: FontWeight.w500),
//                            ),
//                          ),
                    ],
                  ),
                ),
              ),
            ),
          ),
//        Expanded(
//          flex: 1,
//          child: GestureDetector(
//            onTap: _handleSignIn,
//            child: Container(
//              decoration: BoxDecoration(
////                color: Color(0xFF3B5998),
//                borderRadius: BorderRadius.all(Radius.circular(7)),
//                border: Border.all(width: 1, color: Color(0xFFDB4437)),
//              ),
//              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
//              margin: const EdgeInsets.only(
//                bottom: 10,
//                left: 5,
//                right: 5
//              ),
//              child: Row(
//                mainAxisAlignment: MainAxisAlignment.center,
//                children: <Widget>[
//                  Icon(
//                    FontAwesomeIcons.google,
//                    color: Color(0xFFDB4437),
//                  ),
////                  Padding(
////                    padding: const EdgeInsets.only(left: 10),
////                    child: Text(
////                      FlutterI18n.translate(
////                          context, 'Continue with Google'),
////                      style: TextStyle(
////                          color: Color(0xFFDB4437),
////                          fontSize: 20.0,
////                          fontWeight: FontWeight.w500
////                      ),
////                    ),
////                  ),
//                ],
//              ),
//            ),
//          ),
//        ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.only(
                  right: 5,
                  left: 5
              ),
              child: SizedBox(
                height: 45,
                child: FlatButton(
                  onPressed: _handleSignIn,
                  color: Colors.white,
                  textColor: Colors.white,
                  disabledColor: Colors.grey,
                  disabledTextColor: Color(0xFFDB4437),
                  splashColor: Color(0xFFDB4437),
                  shape: RoundedRectangleBorder(
                      side: BorderSide(
                          color: Color(0xFFDB4437),
                          width: 1,
                          style: BorderStyle.solid
                      ),
                      borderRadius: BorderRadius.circular(7)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.google,
                        color: Color(0xFFDB4437),
                      ),
//                          Padding(
//                            padding: const EdgeInsets.only(left: 10),
//                            child: Text(
//                              FlutterI18n.translate(
//                                  context, 'Continue with Apple'),
//                              style: TextStyle(
//                                  color: Colors.black,
//                                  fontSize: 20.0,
//                                  fontWeight: FontWeight.w500),
//                            ),
//                          ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          _loginViaApple(),
        ],
      ),
    );
  }

  _loginViaApple() {
    if (Platform.isIOS) {
      return Expanded(
        flex: 1,
        child: Padding(
          padding: const EdgeInsets.only(
              left: 5
          ),
          child: FutureBuilder(
            future: _isAvailableFuture,
            builder: (context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return SizedBox(
                  height: 45,
                  child: FlatButton(
                    onPressed: logIn,
                    color: Colors.white,
                    textColor: Colors.white,
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    splashColor: Colors.black,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                          color: Colors.black,
                          width: 1,
                          style: BorderStyle.solid
                      ),
                      borderRadius: BorderRadius.circular(7)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.apple,
                          color: Colors.black,
                        ),
//                          Padding(
//                            padding: const EdgeInsets.only(left: 10),
//                            child: Text(
//                              FlutterI18n.translate(
//                                  context, 'Continue with Apple'),
//                              style: TextStyle(
//                                  color: Colors.black,
//                                  fontSize: 20.0,
//                                  fontWeight: FontWeight.w500),
//                            ),
//                          ),
                      ],
                    ),
                  ),
                );
              }
              return Container();
            },
          ),
        ),
      );
    }

    return Container();
  }

  Widget _registrationLink() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 25),
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, 'auth/registration');
        },
        child: Container(
          decoration: BoxDecoration(
              color: Colors.orange,
//          border: Border.all(width: 1, color: Color(0xFFCD4A33)),
              borderRadius: BorderRadius.all(Radius.circular(7))),
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
          child: Text(
            FlutterI18n.translate(context, 'Sign up'),
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white,
                fontSize: 20.0,
                fontWeight: FontWeight.w500),
          ),
        ),
      ),
    );
  }

  Widget _localityAndLanguage() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, 'change-locality'),
            child: Container(
              color: Colors.transparent,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Icon(
                      FontAwesomeIcons.mapMarkerAlt,
                      color: Colors.orange,
                    ),
                  ),
                  Text(
                    AppService.deviceInfoModel.localityModelApi.name,
                    style: TextStyle(color: Colors.orange),
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, 'change-language'),
            child: Container(
              color: Colors.transparent,
              child: Row(
                children: <Widget>[
                  Text(
                    AppService
                        .languageMap[AppService.deviceInfoModel.languageCode]
                        .name,
                    style: TextStyle(color: Colors.orange),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Icon(
                      FontAwesomeIcons.globe,
                      color: Colors.orange,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void logIn() async {
    BuildContext dialogContext;
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          dialogContext = context;
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                LoaderComponent(
                  withTimer: false,
                ),
              ],
            ),
          );
        });

    bool success = false;
    String message;

    try {
      final credential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      );

      _loginForm.accessToken = credential?.authorizationCode ?? null;
      _loginForm.socialId = credential?.userIdentifier ?? null;
      _loginForm.email = credential?.email ?? null;
      _loginForm.firstName = credential?.givenName ?? null;
      _loginForm.lastName = credential?.familyName ?? null;

      _loginForm.authSocialNetworkId = '2';
      _loginForm.checkedAllRequiredAgreements = '1';

      await AuthService.login(_loginForm, socialNetWork: true).then((_success) {
        success = _success;

        if (success) {
          Navigator.pop(dialogContext);
          Navigator.pop(context);
        }
      }, onError: (error) async {
        success = false;

        await AppApi.postFeedback({
          "subject": "Проблема з авторизаціє через Apple (0)",
          "text": (await buildDataToString(_loginForm, socialNetWork: true)).toString() + '\n' + error.toString(),
        }).then((result) {

        });
        AppService.message(MessageModel(
            title: 'Continue with Apple',
            message: 'Error in data transfer',
            backgroundColor: Colors.deepOrangeAccent,
            duration: null,
            mainButton: null));

      });

      if (!success) {

        await AppApi.postFeedback({
          "subject": "Проблема з авторизаціє через Apple (1)",
          "text": (await buildDataToString(_loginForm, socialNetWork: true)).toString(),
        }).then((result) {

        });

        Navigator.pop(dialogContext);
        // AppService.message(MessageModel(
        //     title: 'Continue with Apple',
        //     message: message ?? 'Sorry, there was an error',
        //     backgroundColor: Colors.red,
        //     duration: null,
        //     mainButton: null));
      }
    } catch (e) {

      await AppApi.postFeedback({
        "subject": "Проблема з авторизаціє через Apple (2)",
        "text": (await buildDataToString(_loginForm, socialNetWork: true)).toString() + '\n' + e.toString(),
      }).then((result) {

      });

      Navigator.pop(dialogContext);
      AppService.message(MessageModel(
          title: 'Continue with Apple',
          message: e.toString(),
          backgroundColor: Colors.orangeAccent,
          duration: null,
          mainButton: null));
    }
  }

  _connectCommunicate() {
    if (AppService.internetIsConnected) {
      return Container();
    }

    if (loader) {
      return LoaderComponent();
    }

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(right: 10, left: 10),
            width: double.infinity,
            child: Text(
              FlutterI18n.translate(
                  pageNavigatorKey.currentContext, 'TEXT_DISCONNECT'),
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.orange,
                  fontSize: 20,
                  fontWeight: FontWeight.w500),
            ),
          ),
          FlatButton(
            onPressed: () {
              setState(() {
                loader = true;
              });
              AppService.checkInternetConnect(refresh: true).then((value) {
                Timer(Duration(seconds: 2), () {
                  setState(() {
                    loader = value;
                  });
                });
              });
            },
            padding: const EdgeInsets.only(top: 10, bottom: 10, left: 10),
            child: Text(
              FlutterI18n.translate(pageNavigatorKey.currentContext,
                  'Try to connect to the server'),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
          )
        ],
      ),
    );
  }

  buildDataToString(LoginForm loginForm,
      {bool socialNetWork: false}) async {



    loginForm.platform = Platform.isIOS ? 'IOS' : 'ANDROID';
    loginForm.fcmToken =
        await FirebaseMessagingService.firebaseMessaging.getToken();

    if (AppService.deviceInfoModel != null) {
      loginForm.systemVersion = AppService.deviceInfoModel.systemVersion;
      loginForm.appVersion = AppService.deviceInfoModel.appVersion;
      loginForm.model = AppService.deviceInfoModel.model;
    }

    var data = loginForm.toJsonStructure();

    if (socialNetWork) {
      data.remove('password');

      if (data['email'] == null) {
        data.remove('email');
      }

      if (data['firstName'] == null) {
        data.remove('firstName');
      }

      if (data['avatar'] == null) {
        data.remove('avatar');
      }

      if (data['lastName'] == null) {
        data.remove('lastName');
      }

      if (data['authSocialNetworkId'] == '1') {
        data.remove('email');
        data.remove('firstName');
        data.remove('lastName');
        data.remove('socialId');
        data.remove('avatar');
      }

      if (data['authSocialNetworkId'] == '2') {
        data.remove('avatar');
      }

    } else {
      data.remove('firstName');
      data.remove('lastName');
      data.remove('socialId');
      data.remove('avatar');
      data.remove('accessToken');
      data.remove('authSocialNetworkId');
      data.remove('checkedAllRequiredAgreements');
    }

    print("${json.encode(data)}");
    print(json.encode(data));
    print(json.encode(data).runtimeType);

    return json.encode(data);

  }

}
