import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/forms/registration.form.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/validators.util.dart';
import 'package:SaloSale/ui/pages/auth/invitation-code.auth.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:SaloSale/data.dart';

class RegistrationAuthPage extends StatefulWidget {

  RegistrationAuthPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _RegistrationAuthPage();

}

class _RegistrationAuthPage extends State<RegistrationAuthPage> {

  final _formKey = GlobalKey<FormState>();

  final RegistrationModel _registrationModel = RegistrationModel(
    agreements: [],
    localityId: 3,
  );

  String _errorMessage;
  bool _isLoading;

  @override
  void initState() {

    _errorMessage = "";
    _isLoading = false;

    super.initState();

  }

  @override
  void dispose() {

    super.dispose();

  }

  // Check if form is valid before perform login
  bool _saveAndValidate() {

    final form = _formKey.currentState;

    form.save();

    if (form.validate()) {

      return true;

    }

    setState(() {

      _isLoading = false;

    });

    return false;

  }

  void _validateAndSubmit() async {

    setState(() {

      _errorMessage = "";
      _isLoading = true;

    });

    if (_saveAndValidate()) {

      try {

        await AuthService.registration(_registrationModel).then((result) {

          if (result) {

            if (_registrationModel.invitationCode != null && _registrationModel.invitationCode.length > 0) {
              AppService.analytics.logSignUp(signUpMethod: 'USE_INVITATION');
            }
            AppService.analytics.logSignUp(signUpMethod: 'REGISTRATION_IS_SUCCESS');

            Navigator.pop(context);
            Navigator.pop(context);

          }

        });

        setState(() {

          _isLoading = false;

        });

      } catch (e) {

        AppService.message(MessageModel(
            title: 'Registration form',
            message: 'An unknown error occurred, please try again later',
            backgroundColor: Colors.red,
            duration: null,
            mainButton: null
        ));

        setState(() {

          _isLoading = false;

        });
//
//        setState(() {
//
//          _isLoading = false;
//
//          if (_isIos) {
//
//            _errorMessage = e.details;
//
//          } else {
//
//            _errorMessage = e.message;
//
//          }
//
//          message = _errorMessage;
//
//        });

      }

    }
  }
  ///
  /// Build
  ///

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          FlutterI18n.translate(context, 'Registration form'),
          style: TextStyle(
            fontSize: 22.5,
            fontWeight: FontWeight.bold
          ),
        ),
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back_ios,),
        ),
      ),
      body: Stack(
        children: <Widget>[
          _body(),
          _circularProgress(),
        ],
      ),
    );

  }

  ///
  /// Body - ListView
  ///

  Widget _body(){
    return Form(
      key: _formKey,
      child: ListView(
        padding: const EdgeInsets.only(
          left: 15,
          right: 15,
          bottom: 15
        ),
        shrinkWrap: true,
        children: <Widget>[
          _emailInput(),
          _passwordInput(),
          _passwordRepeatInput(),
          _titleComponent(),
          _buttonInvitationCode(),
          _primaryButton(),
          _showErrorMessage(),
        ],
      ),
    );
  }

  ///
  /// Show error message
  ///

  Widget _showErrorMessage() {
    if (_errorMessage != null && _errorMessage.length > 0) {
      return Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return Container(
        height: 0.0,
      );
    }
  }


  ///
  /// E-mail input
  ///

  Widget _emailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        initialValue: _registrationModel.email,
        autofocus: true,
        cursorColor: Colors.orange,
        decoration: InputDecoration(
          hintText: FlutterI18n.translate(context, 'E-mail'),
          prefixIcon: Icon(
            Icons.mail,
            color: Colors.grey,
          )
        ),
        validator: (value) {
          return ValidatorsUtil.email(context, value.trim().toLowerCase());
        },
        onSaved: (value) => _registrationModel.email = value.trim().toLowerCase(),
      ),
    );
  }

  ///
  /// Password input
  ///

  Widget _passwordInput() {

    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        obscureText: _registrationModel.passwordObscure,
        initialValue: _registrationModel.password,
        autofocus: false,
        cursorColor: Colors.orange,
        decoration: InputDecoration(
          suffixIcon: GestureDetector(
            onTap: () {
              setState(() {
                _registrationModel.passwordObscure = !_registrationModel.passwordObscure;
              });
            },
            child: Container(
              color: Colors.transparent,
              child: Icon(
                _registrationModel.passwordObscure ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                color: Colors.grey,
                size: 12.5,
              ),
            ),
          ),
          hintText: FlutterI18n.translate(context, 'Password'),
          prefixIcon: Icon(
            Icons.lock,
            color: Colors.grey,
          )
        ),
        validator: (value) {
          return ValidatorsUtil.password(context, value);
        },
        onSaved: (value) => _registrationModel.password = value,
      ),
    );

  }

  ///
  /// Password repeat input
  ///

  Widget _passwordRepeatInput() {

    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        obscureText: _registrationModel.passwordRepeatObscure,
        initialValue: _registrationModel.passwordRepeat,
        autofocus: false,
        cursorColor: Colors.orange,
        decoration: InputDecoration(
          suffixIcon: GestureDetector(
            onTap: () {
              setState(() {
                _registrationModel.passwordRepeatObscure = !_registrationModel.passwordRepeatObscure;
              });
            },
            child: Container(
              color: Colors.transparent,
              child: Icon(
                _registrationModel.passwordRepeatObscure ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                color: Colors.grey,
                size: 12.5,
              ),
            ),
          ),
          hintText: FlutterI18n.translate(context, 'Password repeat'),
          prefixIcon: Icon(
            Icons.lock,
            color: Colors.grey,
          )
        ),
        validator: (value) {
          return ValidatorsUtil.passwordRepeat(context, value, _registrationModel.password);
        },
        onSaved: (value) => _registrationModel.passwordRepeat = value,
      ),
    );

  }

  ///
  /// Primary button
  ///

  Widget _primaryButton() {

    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 50.0,
          child: RaisedButton(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)
            ),
            color: Color(0xFF343434),
            child: Text(
                FlutterI18n.translate(context, 'Sign up'),
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white
                )
            ),
            onPressed: () {
              if (AppService.internetIsConnected) {

                _validateAndSubmit();

              } else {

                AppService.message(MessageModel(
                    title: 'Registration form',
                    message: 'TEXT_DISCONNECT',
                    backgroundColor: Colors.orangeAccent,
                    duration: null,
                    mainButton: null
                ));

              }
            },
          ),
        )
    );

  }

  ///
  /// Loader
  ///

  Widget _circularProgress() {

    if (_isLoading) {

      return Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.orange,
          )
      );

    } else {

      return Container(
        height: 0.0,
        width: 0.0,
      );

    }

  }

  ///
  /// Title component
  ///

  Widget _titleComponent() {
    return Padding(
      padding: const EdgeInsets.only(
          top: 25.0,
          bottom: 10
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            FlutterI18n.translate(context, 'Your starting capital') + ': ',
          ),
          Row(
            children: <Widget>[
              Text(
                (_registrationModel.invitationCode != null && _registrationModel.invitationCode.length > 0 ? (Data.invitationCodeRegistrationIkoint + Data.startRegistrationIkoint).toString() : Data.startRegistrationIkoint.toString() ) + ' ' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'ikoint_2'),
                style: TextStyle(
                    color: Colors.orange,
                    fontWeight: FontWeight.bold
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _buttonInvitationCode() {

    Widget invitationButton = GestureDetector(
      onTap: () async {

        AppService.analytics.logSignUp(signUpMethod: 'OPEN_INVITATION');

        _registrationModel.invitationCode = await Navigator.push(context, MaterialPageRoute<String>(
          builder: (context) => InvitationCodeAuthPage(),
        )
        );

        if (_registrationModel.invitationCode != null && _registrationModel.invitationCode.length > 0) {

          setState(() {

          });

        }

      },
      child: Container(
        padding: const EdgeInsets.symmetric(
            vertical: 15,
            horizontal: 10
        ),
        margin: const EdgeInsets.symmetric(
            vertical: 10
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(7)),
            color: Color(0xCCC21E01)
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              FlutterI18n.translate(context, 'I have an invite code') + ' ',
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w500
              ),
            ),
            Text(
              _registrationModel.invitationCode != null && _registrationModel.invitationCode.length > 0 ? ' ' + _registrationModel.invitationCode : ' + ' + Data.invitationCodeRegistrationIkoint.toString() + ' ' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'ikoint_2'),
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Color(0xFF343434),
                  fontWeight: FontWeight.w500
              ),
            ),
          ],
        ),
      ),
    );
    Widget deleteInvitationCode = Container();

    if (_registrationModel.invitationCode != null && _registrationModel.invitationCode.length > 0) {

      deleteInvitationCode = GestureDetector(
        onTap: () {

          setState(() {
            _registrationModel.invitationCode = null;
          });

        },
        child: Container(
          padding: const EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 10
          ),
          margin: const EdgeInsets.symmetric(
              vertical: 10
          ),
          color: Colors.transparent,
          child: Text(
            FlutterI18n.translate(context, 'Delete invitation code'),
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.orange,
                fontWeight: FontWeight.w500
            ),
          ),
        ),
      );

    }

    return Column(
      children: <Widget>[
        invitationButton,
        deleteInvitationCode
      ],
    );

  }


}