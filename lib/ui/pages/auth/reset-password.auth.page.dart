
import 'dart:io';

import 'package:SaloSale/shared/forms/reset-password.form.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/validators.util.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ResetPasswordAuthPage extends StatefulWidget {

  ResetPasswordAuthPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ResetPasswordAuthPage();

}

class _ResetPasswordAuthPage extends State<ResetPasswordAuthPage> {

  // TODO need finish flushbar and routing when password is changed return to login page and show comment you success chage password

  final _formKey = GlobalKey<FormState>();

  final ResetPasswordModel _resetPasswordModel = ResetPasswordModel();

  final TextEditingController verificationCodeController = TextEditingController();

  bool _isLoading;

  @override
  void initState() {

    _isLoading = false;
    AppService.analytics.logEvent(name: 'OPEN_RESET_PASSWORD');

    super.initState();

  }

  // Check if form is valid before perform login
  bool _validateAndSave() {

    final form = _formKey.currentState;

    form.save();

    if (form.validate()) {

      return true;

    }

    setState(() {

      _isLoading = false;

    });

    return false;

  }

  void _validateAndSubmit() async {

    setState(() {

      _isLoading = true;

    });

    if (_validateAndSave()) {

      String message;
      Color color = Colors.red;

      try {

        if (_resetPasswordModel.showCodeInput) {

          if (_resetPasswordModel.showPasswordInputs) {

            await _resetPasswordModel.postResetPassword({
              "password_reset_token": _resetPasswordModel.code,
              "password": _resetPasswordModel.password
            }).then((result) {

              if (result) {

                _resetPasswordModel.showPasswordInputs = result;
                AppService.analytics.logEvent(name: 'USE_RESET_PASSWORD');

                message = "Password changed successfully";
                color = Colors.green;
                _formKey.currentState.reset();

                // TODO pop to login page
                Navigator.of(context).pop();

              }

              setState(() {

                _isLoading = false;

              });

            });

          } else {

            await _resetPasswordModel.getCheckPasswordResetCode(_resetPasswordModel.code).then((result) {

              if (result) {

                _resetPasswordModel.showPasswordInputs = result;

                message = "The code has been verified, you can now enter a new one";
                color = Colors.green;
                _formKey.currentState.reset();

              }

              setState(() {

                _isLoading = false;

              });

            });

          }

        } else {

          await _resetPasswordModel.postForgotPassword(_resetPasswordModel.email).then((result) {

            if (result) {

              _resetPasswordModel.showCodeInput = result;

              message = "The e-mail has been sent, please check your e-mail";
              color = Colors.green;
              _formKey.currentState.reset();

            }

            setState(() {

              _isLoading = false;

            });

          });

        }

      } catch (e) {

        setState(() {

          _isLoading = false;

          if (Platform.isIOS) {

            message = e.details;

          } else {

            message = e.message;

          }

        });

      }


      AppService.message(MessageModel(
          title: 'Reset password form',
          message: message ?? '-',
          backgroundColor: color,
          duration: null,
          mainButton: null
      ));

    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
              FontAwesomeIcons.angleLeft,
              color: Colors.grey
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          FlutterI18n.translate(context, 'Reset password form'),
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 20
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          _body(),
          _circularProgress(),
        ],
      ),
    );

  }

  Widget _body() {
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            _emailInput(),
            _iHaveCode(),
            _codeInput(),
            _passwordInput(),
            _passwordRepeatInput(),
            _primaryButton(),
          ],
        ),
      )
    );
  }

  ///
  /// Password input
  ///

  Widget _passwordInput() {

    if (!_resetPasswordModel.showPasswordInputs) {
      return Container();
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        cursorColor: Colors.orange,
        obscureText: _resetPasswordModel.passwordObscure,
        initialValue: _resetPasswordModel.password,
        autofocus: false,
        decoration: InputDecoration(
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  _resetPasswordModel.passwordObscure = !_resetPasswordModel.passwordObscure;
                });
              },
              child: Container(
                color: Colors.transparent,
                child: Icon(
                  _resetPasswordModel.passwordObscure ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                  color: Colors.grey,
                  size: 12.5,
                ),
              ),
            ),
            hintText: FlutterI18n.translate(context, 'Password'),
            icon: Icon(
              Icons.lock,
              color: Colors.grey,
            )
        ),
        validator: (value) {
          return ValidatorsUtil.password(context, value);
        },
        onSaved: (value) => _resetPasswordModel.password = value,
      ),
    );

  }

  ///
  /// Password repeat input
  ///

  Widget _passwordRepeatInput() {

    if (!_resetPasswordModel.showPasswordInputs) {
      return Container();
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        cursorColor: Colors.orange,
        obscureText: _resetPasswordModel.passwordRepeatObscure,
        initialValue: _resetPasswordModel.passwordRepeat,
        autofocus: false,
        decoration: InputDecoration(
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  _resetPasswordModel.passwordRepeatObscure = !_resetPasswordModel.passwordRepeatObscure;
                });
              },
              child: Container(
                color: Colors.transparent,
                child: Icon(
                  _resetPasswordModel.passwordRepeatObscure ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                  color: Colors.grey,
                  size: 12.5,
                ),
              ),
            ),
            hintText: FlutterI18n.translate(context, 'Password repeat'),
            icon: Icon(
              Icons.lock,
              color: Colors.grey,
            )
        ),
        validator: (value) {
          return ValidatorsUtil.passwordRepeat(context, value, _resetPasswordModel.password);
        },
        onSaved: (value) => _resetPasswordModel.passwordRepeat = value,
      ),
    );

  }


  Widget _emailInput() {

    if (_resetPasswordModel.showCodeInput) {
      return Container();
    }

    return TextFormField(
      maxLines: 1,
      cursorColor: Colors.orange,
      keyboardType: TextInputType.emailAddress,
      autofocus: true,
      decoration: InputDecoration(
          hintText: FlutterI18n.translate(context, "E-mail"),
          icon: Icon(
            Icons.mail,
            color: Colors.grey,
          )),
      validator: (value) => ValidatorsUtil.email(context, value.trim().toLowerCase()),
      onSaved: (value) => _resetPasswordModel.email = value.trim().toLowerCase(),
    );
  }

  Widget _codeInput() {

    if (!_resetPasswordModel.showCodeInput) {
      return Container();
    }

    return Stack(
        children: <Widget>[
          TextFormField(
            cursorColor: Colors.orange,
            controller: verificationCodeController,
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(
                    right: 50.0,
                    top: 15.0,
                    left: 15.0,
                    bottom: 15.0
                ),
                hintText: FlutterI18n.translate(context, 'Verification code'),
                icon: Icon(
                  FontAwesomeIcons.qrcode,
                  color: Colors.grey,
                )
            ),
            onSaved: (value) => _resetPasswordModel.code = value,
          ),
          Positioned(
            right: 0.0,
            bottom: 0.0,
            child: IconButton(
                icon: Icon(
                  MdiIcons.qrcodeScan,
                  color: Colors.orange,
                ),
                onPressed: scan
            ),
          )
        ]
    );
  }


  Future<void> scan() async {
    BarcodeScanner.scan().then((code) {

        setState(() {

          verificationCodeController.text = code;

        });
    });

  }

  Widget _primaryButton() {

    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 50.0,
          child: RaisedButton(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)
            ),
            color: Colors.orange,
            child: Text(
                FlutterI18n.translate(context, _resetPasswordModel.showCodeInput ? _resetPasswordModel.showPasswordInputs ? 'Change password' : 'Check verification code' : "Send mail"),
                style: TextStyle(
                    fontSize: 20.0,
                )
            ),
            onPressed: () {
              if (AppService.internetIsConnected) {

                _validateAndSubmit();
              } else {



                AppService.message(MessageModel(
                    title: 'Reset password form',
                    message: 'TEXT_DISCONNECT',
                    backgroundColor: Colors.orangeAccent,
                    duration: null,
                    mainButton: null
                ));

              }
            },
          ),
        )
    );

  }

  Widget _iHaveCode() {

    if (!_resetPasswordModel.showCodeInput) {

      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              setState(() {
                _resetPasswordModel.showCodeInput = true;
              });
            },
            child: Container(
              padding: const EdgeInsets.only(
                top: 12.5,
              ),
              child: Text(
                FlutterI18n.translate(context, 'I have a code'),
                style: TextStyle(
                    color: Colors.orange,
                    fontSize: 20
                ),
              ),
            ),
          )
        ],
      );

    }

    return Container();

  }

  Widget _circularProgress() {

    if (_isLoading) {

      return Center(
          child: CircularProgressIndicator()
      );

    } else {

      return Container(
        height: 0.0,
        width: 0.0,
      );

    }

  }

}