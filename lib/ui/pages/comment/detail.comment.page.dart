import 'dart:async';

import 'package:SaloSale/data.dart';
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/comment.api.dart';
import 'package:SaloSale/shared/models/api/comment.model.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/disconnect.component.dart';
import 'package:SaloSale/ui/components/starts.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:share/share.dart';

class DetailCommentPage extends StatefulWidget implements Widget {

  final int commentId;

  DetailCommentPage({Key key, this.commentId}) : super(key: key ?? Key('comment-detail'));

  @override
  State<StatefulWidget> createState() => _DetailCommentPage();

}

class _DetailCommentPage extends State<DetailCommentPage> {

  List<Widget> items;
  String filterItem = 'comment';
  CommentModelApi comment;

  ScrollController _listViewController = ScrollController();

  @override
  void initState() {

    super.initState();
    initItems();

    AppService.analytics.logEvent(name: 'OPEN_PAGE_POINT_OF_SALE');
    _refresh();

    CommentApi.putIfNotExistFilterInMap(filterItem);

    CommentApi.filterMap[filterItem].documentChanel.listen((data) {
      comment = data;
      initItems();
    });

    if (AppService.internetIsConnected) {

      CommentApi.getDocument(filterItem, widget.commentId);

    } else {
    }

  }

  @override
  void dispose() {
    _listViewController.dispose();
    super.dispose();
  }

  Future _refresh() {

    return Future.value(true);

  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Comment')
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            onPressed: () => Share.share('https://$domain/comments/${widget.commentId}'),
            icon: Icon(
              Icons.share,
              color: Colors.orange,
            ),
          )
        ],
      ),
      key: _scaffoldKey,
      body: AppService.internetIsConnected ? ListView.builder(
        controller: _listViewController,
        itemBuilder: (context, index) => items[index],
        itemCount: items.length,
      ) : disconnectComponent('TEXT_DISCONNECT_RECOMMENDED_COMPONENT'),
    );

  }



  initItems() {

    items = [];

    if (comment != null) {


      items.add(Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
            border: Border(
                bottom: BorderSide(
                    width: 1,
                    color: Colors.grey.shade200
                )
            )
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '@${comment.user.login}',
                  style: TextStyle(
//                            fontSize: 16,
                      fontWeight: FontWeight.w500
                  ),
                ),
                Row(
                  children: [
                    StartsComponent(
                      value: comment.score,
                      iconSize: 14.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 5
                      ),
                      child: Text(
                        comment.score.toString(),
                        style: TextStyle(
                            fontWeight: FontWeight.w500
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Container(
              padding: const EdgeInsets.only(
                  top: 10,
                  bottom: 10
              ),
              child: Text(
                comment.comment,
              ),
            ),
            comment.scores.length > 0 ? Padding(
              padding: const EdgeInsets.only(
                bottom: 10,
                top: 5
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    FlutterI18n.translate(pageNavigatorKey.currentContext, 'Scores'),
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 20
                    ),
                  ),

                  Container(
                    height: (((comment.scores.length / 3) + .5).toInt() * 50).toDouble(),
                    padding: const EdgeInsets.only(
                      top: 10
                    ),
                    child: GridView.count(
                      // Create a grid with 2 columns. If you change the scrollDirection to
                      // horizontal, this produces 2 rows.
                      crossAxisCount: 3,
                      // Generate 100 widgets that display their index in the List.
                      children: List.generate(comment.scores.length, (i) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  child: Text(
                                    (AppService.listsOfTagsMap['score']?.tagList?.firstWhere((element) => element.id == comment.scores[i].tagId)?.name ?? ''),
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                    '${comment.scores[i].score} '
                                ),
                                StartsComponent(
                                  value: comment.scores[i].score,
                                  iconSize: 12.0,
                                ),
                              ],
                            )
                          ],
                        );
                      }),
                    ),
                  )

//                  Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    children: comment.scores.map((score) {
//
//                      return Container(
//                        child: Row(
//                          children: [
//                            Text(
//                              (AppService.listsOfTagsMap['score']?.tagList?.firstWhere((element) => element.id == score.tagId)?.name ?? '') + ': ',
//                              textAlign: TextAlign.left,
//                              style: TextStyle(
//                                fontSize: 16,
//                                fontWeight: FontWeight.w400
//                              ),
//                            ),
//                            SmoothStarRating(
//                                rating: score.score,
//                                size: 14.0,
//                                isReadOnly: true,
//                                defaultIconData: Icons.star,
//                                color: Colors.orange,
//                                borderColor: Colors.grey.shade300,
//                                spacing: 0.0
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.only(
//                                  left: 5
//                              ),
//                              child: Text(
//                                score.score.toString(),
//                                style: TextStyle(
//                                    fontWeight: FontWeight.w500
//                                ),
//                              ),
//                            ),
//                          ],
//                        ),
//                      );
//                    }).toList(),
//                  ),
                ],
              ),
            ) : Container(),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  Tools.dateString(comment.createdAt, inAgo: true),
                  style: TextStyle(
                      color: Colors.grey.shade400
                  ),
                ),
              ],
            )
          ],
        ),
      ));

      if (comment.numberOfReplies > 0) {

        items.add(_replies());

      }

    }


    setState(() {

    });

  }


  _replies() {


    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(7),
        boxShadow: [
          BoxShadow(
            color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
            blurRadius: 10.0,
            spreadRadius: 0.0,
            offset: Offset(
              0.0, // horizontal, move right 10
              0.0, // vertical, move down 10
            ),
          )
        ],
      ),
//        padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.all(10),
      child: Column(
        children: [

          Container(
            height: 50,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(
                left: 10,
                right: 10
            ),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        width: 1,
                        color: Colors.grey.shade200
                    )
                )
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RichText(
                  text: TextSpan(
                      text: FlutterI18n.translate(pageNavigatorKey.currentContext, 'Replies'),
                      style: TextStyle(
                          fontFamily: 'FiraSans',
                          fontWeight: FontWeight.w500,
                          color: Colors.grey,
                          fontSize: 20
                      ),
                      children: <TextSpan>[
                        TextSpan(
                            text: ' (${comment.numberOfReplies})',
                            style: TextStyle(
                                fontFamily: 'FiraSans',
                                fontWeight: FontWeight.w400,
                                color: Colors.grey,
                                fontSize: 15
                            )
                        )
                      ]
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: comment.replies.map((reply) => Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          width: 1,
                          color: Colors.grey.shade200
                      )
                  )
              ),
              child: ListTile(
                contentPadding: const EdgeInsets.all(0),
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '@${reply.user.login}',
                          style: TextStyle(
//                            fontSize: 16,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      ],
                    ),
                    Container(
                      padding: const EdgeInsets.only(
                          top: 10,
                          bottom: 10
                      ),
                      child: Text(
                        reply.reply,
                      ),
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          Tools.dateString(reply.createdAt, inAgo: true),
                          style: TextStyle(
                              color: Colors.grey.shade400
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )).toList(),
          )
        ],
      ),
    );

  }


}