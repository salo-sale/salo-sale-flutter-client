import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/comment.api.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/models/api/comment.model.api.dart';
import 'package:SaloSale/shared/models/api/company.model.api.dart';
import 'package:SaloSale/shared/models/score.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/ui/components/comment-input.component.dart';
import 'package:SaloSale/ui/components/comment.component.dart';
import 'package:SaloSale/ui/components/disconnect.component.dart';
import 'package:SaloSale/ui/components/ikoint.component.dart';
import 'package:SaloSale/ui/components/scores.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:rxdart/rxdart.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class ListCommentPage extends StatefulWidget implements Widget {

  final int companyId;

  ListCommentPage({
    Key key,
    this.companyId
  }) : super(key: key ?? Key('comment-list'));

  @override
  State<StatefulWidget> createState() => _ListCommentPage();

}

class _ListCommentPage extends State<ListCommentPage> {

  List<Widget> items;
  String filterItem = 'comment';
  CompanyModelApi companyModelApi;
  final FocusNode commentFocusNode = FocusNode();
  final TextEditingController commentInputController = TextEditingController();

  final ScrollController _listViewController = ScrollController();

  final PublishSubject<bool> scrollToTopSinglePage = PublishSubject<bool>();

  @override
  void initState() {

    items = [];

    super.initState();

    CommentApi.putIfNotExistFilterInMap(filterItem);
    CompanyApi.putIfNotExistFilterInMap(filterItem);

    AppService.analytics.logEvent(name: 'OPEN_PAGE_POINT_OF_SALE');
    _refresh();

    scrollToTopSinglePage.stream.listen((success) {

      if (success) {

        if (_listViewController.offset > 0) {

          _listViewController.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.easeOut);

        } else {
          Navigator.pop(context);
        }

      }

    });

    CommentApi.filterMap[filterItem].documentListChanel.listen((documentList) {

      setState(() {
        initItems(documentList);
      });
    });

    if (AppService.internetIsConnected) {

      CompanyApi.getDocument(filterItem, widget.companyId).then((_companyModelApi) {

        _updateCompanyModelApi(_companyModelApi);

        CommentApi.refreshDocumentList(filterItem, companyId: widget.companyId);

      });

    }

  }

  _updateCompanyModelApi(CompanyModelApi _companyModelApi) {

    if (_companyModelApi != null) {

      setState(() {
        companyModelApi = _companyModelApi;
//        companyModelApi.tagList = AppService.listsOfTags.firstWhere((listOfTags) => listOfTags.name == 'company').tagList.where((tag) => companyModelApi.tagIdList.contains(tag.id.toString())).toList();
      });

    }

  }

  @override
  void dispose() {
    scrollToTopSinglePage?.close();
    _listViewController.dispose();
    commentInputController.dispose();
    commentFocusNode.dispose();
    super.dispose();
  }

  Future _refresh() async {

    return await CommentApi.refreshDocumentList(filterItem, companyId: widget.companyId);

  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        centerTitle: false,
        title: GestureDetector(
          onTap: () => scrollToTopSinglePage.sink.add(true),
          child: Container(
            color: Colors.transparent,
            child: Text(
              companyModelApi != null ? companyModelApi.name : FlutterI18n.translate(pageNavigatorKey.currentContext, 'Loading and three dots'),
              style: TextStyle(
                  fontSize: 18
              ),
            ),
          ),
        ),
        actions: <Widget>[
          IkointComponent(
            showLoginIfGuest: true,
            margin: const EdgeInsets.only(
                right: 10
            ),
          )
        ],
      ),
      key: _scaffoldKey,
      body: AppService.internetIsConnected ? RefreshIndicator(
        color: Colors.orange,
        onRefresh: _refresh,
        child: ListView.builder(
          controller: _listViewController,
          itemBuilder: (context, index) => items[index],
          itemCount: items.length,
        ),
      ) : disconnectComponent('TEXT_DISCONNECT_RECOMMENDED_COMPONENT'),
    );

  }

  initItems(List<CommentModelApi> documentList) {

    items = [
      Container(
        padding: const EdgeInsets.all(10),
        child: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Comments') + (companyModelApi != null ? ' (${companyModelApi.numberOfComments})' : ''),
          style: TextStyle(
            fontSize: 20
          ),
        ),
      )
    ];

    items.addAll([
      ScoresComponent(
        scores: companyModelApi.scores
      ),
      CommentInputComponent(
          myFocusNode: commentFocusNode,
          commentInputController: commentInputController,
          onFieldSubmitted: (term) {
          },
          clickSuffix: (text) {

            CommentModelApi commentModel = CommentModelApi(
                comment: text,
                companyId: companyModelApi.id,
                forType: 'COMPANY',
                scores: companyModelApi.scores.map((e) => ScoreModel(
                    tagId: e.tagId,
                    score: 0.0
                )).toList()
            );

            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context) {
                  return AlertDialog(
                    title: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Comment'),
                    ),
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Please rate') + ': ${companyModelApi.name}',
                        ),
                        ...(companyModelApi.scores.length > 0 ? companyModelApi.scores.map((e) => Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                                bottom: 10,
                              ),
                              child: Text(
                                (AppService.listsOfTagsMap['score']?.tagList?.firstWhere((element) => element.id == e.tagId)?.name ?? ''),
                                style: TextStyle(
                                    fontSize: 18
                                ),
                              ),
                            ),
                            SmoothStarRating(
                              rating: commentModel.scores.firstWhere((element) => element.tagId == e.tagId).score,
                              size: 40,
                              onRated: (score) {
                                setState(() {
                                  commentModel.scores.firstWhere((element) => element.tagId == e.tagId).score = score;
                                });
                              },
                              defaultIconData: Icons.star,
                              color: Colors.orange,
                              borderColor: Colors.grey.shade300,
                            ),
                          ],
                        )).toList() : [SmoothStarRating(
                          rating: commentModel.score,
                          size: 40,
                          onRated: (score) {
                            setState(() {
                              commentModel.score = score;
                            });
                          },
                          defaultIconData: Icons.star,
                          color: Colors.orange,
                          borderColor: Colors.grey.shade300,
                        )]),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 10
                          ),
                          child: Text(
                            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Swipe from left to right or vice versa to select a score'),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.grey
                            ),
                          ),
                        )
                      ],
                    ),
                    actions: <Widget>[
                      FlatButton(
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Cancel'),
                          style: TextStyle(
                              color: Colors.grey
                          ),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                      FlatButton(
                        onPressed: () async {

                          CommentApi.saveDocument(commentModel).then((value) {

                            if (value != null) {

                              Navigator.pop(context);
                              commentInputController.clear();
                              _refresh();

                            }

                          });

                        },
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Send'),
                          style: TextStyle(
                              color: Colors.orange
                          ),
                        ),
                      ),
                    ],
                  );
                }
            );

            commentFocusNode.unfocus();
          },
          commentLabel: FlutterI18n.translate(pageNavigatorKey.currentContext, 'LABEL_COMMENT_INPUT'),
          withSliver: false
      ),
    ]);

    if (documentList != null && documentList.length > 0) {
      documentList.forEach((comment) {
        items.add(CommentComponent(
            comment: comment
        ));
      });
    }

  }


}