import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/comment.api.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/api/event.api.dart';
import 'package:SaloSale/shared/api/offer.api.dart';
import 'package:SaloSale/shared/enums/types/contact.type.enum.dart';
import 'package:SaloSale/shared/models/api/comment.model.api.dart';
import 'package:SaloSale/shared/models/api/company.model.api.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/models/score.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/button-like.component.dart';
import 'package:SaloSale/ui/components/button-shared.component.dart';
import 'package:SaloSale/ui/components/comment-input.component.dart';
import 'package:SaloSale/ui/components/comment.component.dart';
import 'package:SaloSale/ui/components/ikoint.component.dart';
import 'package:SaloSale/ui/components/image.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/notification-icon.component.dart';
import 'package:SaloSale/ui/components/offer-card.component.dart';
import 'package:SaloSale/ui/components/scores.component.dart';
import 'package:SaloSale/ui/components/scroll-event.component.dart';
import 'package:SaloSale/ui/components/starts.component.dart';
import 'package:SaloSale/ui/pages/company/map.page.dart';
import 'package:SaloSale/ui/pages/company/point-of-sale.company.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';
import 'package:share/share.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class DetailCompanyPage extends StatefulWidget {
  final int companyId;
  final bool openOffers;
  final String filterItem;

  DetailCompanyPage(
      {Key key, this.openOffers = false, this.companyId, this.filterItem})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _DetailCompanyPage();
}

class _DetailCompanyPage extends State<DetailCompanyPage> {
  StreamSubscription subscription;

  Stream streamSubscription;
  Stream scrollEvent;
  final ScrollController _scrollController = ScrollController();
  final FocusNode commentFocusNode = FocusNode();
  final TextEditingController commentInputController = TextEditingController();
  bool likeIsClicked;
  bool _loader;
  String filterCompanyItem = 'company-detail';
  String filterEventItem = 'company-detail';
  String filterOfferItem = 'company-detail';

  int selectedIndex;
  ScrollController _listViewPointsOfSaleController = ScrollController();

  CompanyModelApi companyModelApi;

  final PublishSubject<bool> scrollToTopSinglePage = PublishSubject<bool>();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  initState() {
    if (widget.filterItem != null && widget.filterItem.length > 0) {
      filterCompanyItem = widget.filterItem;
    }

    _loader = false;

    likeIsClicked = false;

    OfferApi.putIfNotExistFilterInMap(filterOfferItem);
    EventApi.putIfNotExistFilterInMap(filterEventItem);
    CompanyApi.putIfNotExistFilterInMap(filterCompanyItem);

    scrollToTopSinglePage.stream.listen((success) {
      if (success) {
        if (_scrollController.offset > 0) {
          _scrollController.animateTo(0,
              duration: Duration(milliseconds: 500), curve: Curves.easeOut);
        } else {
          Navigator.pop(context);
        }
      }
    });

    CompanyApi.getDocument(filterCompanyItem, widget.companyId)
        .then((_companyModelApi) {
      if (_companyModelApi != null) {
        AppService.analytics
            .logEvent(name: 'OPEN_COMPANY' + _companyModelApi.id.toString());

        AppService.analytics.logViewItem(
            itemId: widget.companyId.toString(),
            itemName: _companyModelApi.name,
            itemCategory: 'company');
        _updateCompanyModelApi(_companyModelApi);
        subscription = CompanyApi
            .filterMap[filterCompanyItem].documentChanel.stream
            .listen((__companyModelApi) =>
                _updateCompanyModelApi(__companyModelApi));

        if (widget.openOffers) {
          setState(() {
            _loader = true;
          });
          scrollEvent = OfferApi.filterMap['company-detail'].scrollEvent.stream;
          streamSubscription =
              OfferApi.filterMap['company-detail'].documentListChanel.stream;
          OfferApi.filterMap['company-detail'].clearDocumentListChanel();

          OfferApi.refreshDocumentList('company-detail',
                  companyId: companyModelApi.id, refreshTagList: true)
              .then((value) {
            setState(() {
              _loader = false;
              selectedIndex = 0;
            });
          });
        }
      } else {
        AppService.showOnTransparent(
          Container(
            child: Text(
              FlutterI18n.translate(pageNavigatorKey.currentContext, 'No data'),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
          ),
        );
      }
    });

    super.initState();
  }

  _updateCompanyModelApi(CompanyModelApi _companyModelApi) {
    if (_companyModelApi != null) {
      setState(() {
        companyModelApi = _companyModelApi;
//        companyModelApi.tagList = AppService.listsOfTags.firstWhere((listOfTags) => listOfTags.name == 'company').tagList.where((tag) => companyModelApi.tagIdList.contains(tag.id.toString())).toList();
      });
    }
  }

  @override
  void dispose() {
    scrollToTopSinglePage?.close();
    subscription?.cancel();
    _listViewPointsOfSaleController.dispose();
    OfferApi.refreshDocumentList('company-detail');
    super.dispose();
  }

// TODO ПОтрібно подумати, можливо краще змінити логіку, потипу сухати коли приходять оновлення і тоді тільки робив сетСтате і як б все оновиться

  Future<void> _refresh() {
    setState(() {});
    return CompanyApi.getDocument(filterCompanyItem, companyModelApi.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        titleSpacing: 0,
        centerTitle: false,
        title: GestureDetector(
          onTap: () => scrollToTopSinglePage.sink.add(true),
          child: Container(
            color: Colors.transparent,
            child: Text(
              FlutterI18n.translate(
                  pageNavigatorKey.currentContext, 'Shops and services'),
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
        actions: <Widget>[
          IkointComponent(
            showLoginIfGuest: true,
            margin: const EdgeInsets.only(right: 10),
          ),
          NotificationIconComponent(
            showLogin: false,
          ),
        ],
      ),
      body: SafeArea(
          child: companyModelApi == null
              ? LoaderComponent(
                  withTimer: false,
                )
              : _content()),
    );
  }

  _content() {
    return StreamBuilder<bool>(
        stream: AppService.refreshLayout.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data) {
            Timer(Duration(seconds: 1), () {
              _refresh();
            });
            return LoaderComponent(
              forSliver: false,
              inBox: true,
              circularMargin: const EdgeInsets.only(top: 40, bottom: 40),
              refreshFunction: _refresh,
            );
          } else {
            return RefreshIndicator(
              color: Colors.orange,
              onRefresh: _refresh,
              child: ListView(controller: _scrollController, children: <Widget>[
                companyModelApi.bannerUrl != null &&
                        !companyModelApi.bannerUrl.isEmpty
                    ? ImageComponent(
                        onClick: () {
                          // TODO open gallery
                        },
                        tag: 'company_banner_' + companyModelApi.id.toString(),
                        url: companyModelApi.bannerUrl.buildUrl,
                        aspectRation: 16 / 9,
                        withHero: false,
                      )
                    : Container(),
                Container(
                    decoration: BoxDecoration(color: Colors.white),
                    height: 150,
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      children: <Widget>[
                        ImageComponent(
                          tag: 'company_' + companyModelApi.id.toString(),
                          url: companyModelApi.logoUrl.buildUrl,
                          height: 130,
                          width: 130,
                          margin: const EdgeInsets.only(right: 10),
                          withAspectRation: false,
                          withHero: false,
                        ),
                        _rightBlockOfTop()
                      ],
                    )),
                _likeAndSubscribe(),
                _motto(),
                _companyDescription(),
                _companyMessage(),
                _contacts(),
                _pointsOfSale(),
                _comments(),
                _offersOrEvents(),
              ]),
            );
          }
        });
  }

  _motto() {
    if (companyModelApi.motto != null) {
      return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Center(
          child: Text(
            companyModelApi.motto ?? '',
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18),
          ),
        ),
      );
    }

    return Container();
  }

  _rightBlockOfTop() {
    var items = <Widget>[
      Flexible(
        child: Center(
          child: Text(
            companyModelApi.name,
            textAlign: TextAlign.center,
            softWrap: true,
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
          ),
        ),
      ),
    ];

    if (companyModelApi.getTagNames() != null) {
      items.add(Flexible(
        child: Center(
          child: Text(
            companyModelApi.getTagNames(),
            textAlign: TextAlign.center,
            softWrap: true,
          ),
        ),
      ));
    }

    items.add(Flexible(
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '${companyModelApi.score} ', // FlutterI18n.translate(pageNavigatorKey.currentContext, 'Score') +
              style: TextStyle(color: Colors.grey.shade600),
            ),
            StartsComponent(
              value: companyModelApi.score,
              iconSize: 20.0,
            ),
            Text(
              ' (${companyModelApi.numberOfComments})',
              style: TextStyle(color: Colors.grey.shade600),
            ),
          ],
        ),
      ),
    ));

    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: items,
      ),
    );
  }

  _companyDescription() {
    if (companyModelApi.description == null ||
        companyModelApi.description.length == 0) {
      return Container();
    }

    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(7)),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
            blurRadius: 10.0,
            spreadRadius: 0.0,
            offset: Offset(
              0.0, // horizontal, move right 10
              0.0, // vertical, move down 10
            ),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            FlutterI18n.translate(
                pageNavigatorKey.currentContext, 'Description'),
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 20, color: Colors.grey, fontWeight: FontWeight.w500),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              companyModelApi.description,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 17,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _likeAndSubscribe() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      padding: const EdgeInsets.all(10),
      child: Row(
        children: <Widget>[
          Expanded(flex: 4, child: _showFollowButton()),
          Expanded(
              child: buttonLikeComponent(
                  onTap: () {
                    setState(() {
                      likeIsClicked = true;
                    });
                    CompanyApi.like(filterCompanyItem,
                            isLike: !(companyModelApi.liked == 1))
                        .then((value) {
                      setState(() {
                        likeIsClicked = false;
                      });
                    });
                  },
                  liked: companyModelApi.liked,
                  likeIsClicked: likeIsClicked)),
          Expanded(
              child: buttonSharedComponent(
                  onTap: () => Share.share(
                      companyModelApi.name +
                          ' \n salo-sale.com/companies/' +
                          companyModelApi.id.toString() +
                          ' \n',
                      subject: companyModelApi.motto ?? ''))),
        ],
      ),
    );
  }

  _showFollowButton() {
    Color primaryColor =
        !AppService.internetIsConnected || AuthService.authModel.isGuest
            ? Colors.grey.shade400
            : Colors.orange;

    Widget child = Container(
      margin: const EdgeInsets.only(
        right: 10,
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7.0),
          border: Border.all(color: primaryColor, width: 1),
          color: companyModelApi.follower.followed == 1
              ? primaryColor
              : Colors.transparent),
      padding: const EdgeInsets.all(10.0),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                right: 10,
              ),
              child: Text(
                FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'Subscribe'),
                style: TextStyle(
                    fontSize: 18,
                    color: companyModelApi.follower.followed == 1
                        ? Colors.white
                        : primaryColor,
                    fontWeight: FontWeight.w400),
              ),
            ),
            Icon(
              companyModelApi.follower.followed == 1
                  ? FontAwesomeIcons.solidBell
                  : FontAwesomeIcons.bell,
              size: 20.0,
              color: companyModelApi.follower.followed == 1
                  ? Colors.white
                  : primaryColor,
            ),
          ],
        ),
      ),
    );

    return GestureDetector(
      onTap: AuthService.authModel.isGuest
          ? () {
              AppService.message(MessageModel(
                  title: 'Subscription',
                  message: 'You must be logged in',
                  backgroundColor: Colors.orangeAccent,
                  duration: null,
                  mainButton: null));
            }
          : AppService.internetIsConnected
              ? () {
                  CompanyApi.follower(filterCompanyItem, companyModelApi.id,
                      isFollower: !(companyModelApi.follower.followed == 1),
                      isSubscribe: true);
                }
              : () {
                  AppService.message(MessageModel(
                      title: 'Subscription',
                      message: 'TEXT_DISCONNECT',
                      backgroundColor: Colors.orangeAccent,
                      duration: null,
                      mainButton: null));
                },
      child: child,
    );
  }

  _companyMessage() {
    if (companyModelApi.message == null ||
        companyModelApi.message.length == 0) {
      return Container();
    }

    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(7)),
        color: Colors.blueGrey,
        boxShadow: [
          BoxShadow(
            color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
            blurRadius: 10.0,
            spreadRadius: 0.0,
            offset: Offset(
              0.0, // horizontal, move right 10
              0.0, // vertical, move down 10
            ),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Message') +
                ' !',
            textAlign: TextAlign.left,
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.w500),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              companyModelApi.message,
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 17,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _contacts() {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(top: 10.0, left: 10, right: 10),
      padding: const EdgeInsets.all(2.5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(7)),
        boxShadow: [
          BoxShadow(
            color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
            blurRadius: 10.0,
            spreadRadius: 0.0,
            offset: Offset(
              0.0, // horizontal, move right 10
              0.0, // vertical, move down 10
            ),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 40,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Row(
              children: <Widget>[
                RichText(
                  text: TextSpan(
                      text: FlutterI18n.translate(
                          pageNavigatorKey.currentContext, 'Contacts'),
                      style: TextStyle(
                          fontFamily: 'FiraSans',
                          fontWeight: FontWeight.w500,
                          color: Colors.grey,
                          fontSize: 20),
                      children: <TextSpan>[
                        TextSpan(
                            text:
                                ' (${companyModelApi.contactList.length.toString() ?? '0'})',
                            style: TextStyle(
                                fontFamily: 'FiraSans',
                                fontWeight: FontWeight.w400,
                                color: Colors.grey,
                                fontSize: 15))
                      ]),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 5),
            child: Column(
              children: companyModelApi.contactList.map((contact) {
                var icon = FontAwesomeIcons.font;

                switch (contact.type) {
                  case ContactTypeEnum.NOTHING:
                    icon = FontAwesomeIcons.atom;
                    break;
                  case ContactTypeEnum.PHONE:
                    icon = FontAwesomeIcons.phone;
                    break;
                  case ContactTypeEnum.TELEGRAM:
                    icon = FontAwesomeIcons.telegram;
                    break;
                  case ContactTypeEnum.INSTAGRAM:
                    icon = FontAwesomeIcons.instagram;
                    break;
                  case ContactTypeEnum.FACEBOOK:
                    icon = FontAwesomeIcons.facebook;
                    break;
                  case ContactTypeEnum.WEBSITE:
                    icon = FontAwesomeIcons.globe;
                    break;
                  case ContactTypeEnum.EMAIL:
                    icon = Icons.alternate_email;
                    break;
                  case ContactTypeEnum.VK:
                    icon = FontAwesomeIcons.vk;
                    break;
                  case ContactTypeEnum.SLACK:
                    icon = FontAwesomeIcons.slack;
                    break;
                  case ContactTypeEnum.LIKEDIN:
                    icon = FontAwesomeIcons.linkedin;
                    break;
                  case ContactTypeEnum.OTHER:
                    icon = FontAwesomeIcons.question;
                    break;
                }

                return Container(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Icon(
                          icon,
                          color: Colors.orange,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          switch (contact.type) {
                            case ContactTypeEnum.NOTHING:
                              Share.share(contact.value);
                              break;
                            case ContactTypeEnum.PHONE:
                              Tools.launchPhone(contact.value);
                              break;
                            case ContactTypeEnum.TELEGRAM:
                              Tools.launchWebSite(
                                  'telegram.org/' + contact.value);
                              break;
                            case ContactTypeEnum.INSTAGRAM:
                              Tools.launchWebSite(
                                  'instagram.com/' + contact.value);
                              break;
                            case ContactTypeEnum.FACEBOOK:
                              Tools.launchWebSite(
                                  'facebook.com/' + contact.value);
                              break;
                            case ContactTypeEnum.WEBSITE:
                              Tools.launchWebSite(contact.value);
                              break;
                            case ContactTypeEnum.EMAIL:
                              Tools.launchEmail(contact.value);
                              break;
                            case ContactTypeEnum.VK:
                              Tools.launchWebSite('vk.com/' + contact.value);
                              break;
                            case ContactTypeEnum.SLACK:
                              Tools.launchWebSite('slack.com/' + contact.value);
                              break;
                            case ContactTypeEnum.LIKEDIN:
                              Tools.launchWebSite(
                                  'linkedin.com/' + contact.value);
                              break;
                            case ContactTypeEnum.OTHER:
                              Tools.launchUrl(contact.value);
                              break;
                          }
                        },
                        child: Column(
                          children: <Widget>[
                            contact.name != null && contact.name.length > 0
                                ? Text(
                                    contact.name,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.orange,
                                    ),
                                  )
                                : Container(),
                            contact.hideValue == 0
                                ? Text(
                                    contact.value,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      color: Colors.orange,
                                    ),
                                  )
                                : Container(),
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                        ),
                      )
                    ],
                  ),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }

  _pointsOfSale() {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(top: 10.0, left: 10, right: 10),
//      padding: const EdgeInsets.all(2.5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(7)),
        boxShadow: [
          BoxShadow(
            color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
            blurRadius: 10.0,
            spreadRadius: 0.0,
            offset: Offset(
              0.0, // horizontal, move right 10
              0.0, // vertical, move down 10
            ),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 50,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(7)),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RichText(
                  text: TextSpan(
                      text: FlutterI18n.translate(
                          pageNavigatorKey.currentContext, 'Points of sale'),
                      style: TextStyle(
                          fontFamily: 'FiraSans',
                          fontWeight: FontWeight.w500,
                          color: Colors.grey,
                          fontSize: 20),
                      children: <TextSpan>[
                        TextSpan(
                            text:
                                ' (${companyModelApi.pointOfSaleList.length.toString() ?? '0'})',
                            style: TextStyle(
                                fontFamily: 'FiraSans',
                                fontWeight: FontWeight.w400,
                                color: Colors.grey,
                                fontSize: 15))
                      ]),
                ),
                GestureDetector(
                  onTap: () async {
                    String uuidFlushbar = AppService.message(MessageModel(
                        title: 'Map',
                        message: 'Loading and three dots',
                        backgroundColor: Colors.black54,
                        duration: null,
                        mainButton: null));
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => MapPage(
                            useSelectedCompany: true,
                            flushbarUuid: uuidFlushbar,
                            filterCompanyItem: filterCompanyItem,
                          ),
                        ));
                  },
                  onDoubleTap: () {},
                  child: Container(
                    color: Colors.transparent,
                    child: Text(
                      FlutterI18n.translate(
                          pageNavigatorKey.currentContext, 'See on map'),
                      style: TextStyle(color: Colors.orange),
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(width: 1, color: Colors.grey.shade200))),
            height: companyModelApi.pointOfSaleList.length > 4
                ? 250
                : double.parse(
                    (75 * companyModelApi.pointOfSaleList.length).toString()),
            child: ListView(
              physics: companyModelApi.pointOfSaleList.length <= 4
                  ? const NeverScrollableScrollPhysics()
                  : const AlwaysScrollableScrollPhysics(),
              controller: _listViewPointsOfSaleController,
              children: companyModelApi.pointOfSaleList.map((pos) {
                bool haveWorkHours =
                    pos.workHours != null && pos.workHours.length > 0;
                bool isOpen = haveWorkHours ? pos.isOpen() : true;

                return Container(
                  height: 75,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              width: 1, color: Colors.grey.shade200))),
                  child: ListTile(
                    title: Text(
                      pos.address ??
                          (FlutterI18n.translate(
                                  pageNavigatorKey.currentContext, 'Point') +
                              ': ' +
                              pos.id.toString()),
                      textAlign: TextAlign.left,
                    ),
                    subtitle: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 5),
                          child: Icon(
                            FontAwesomeIcons.solidCircle,
                            size: 10,
                            color: haveWorkHours
                                ? isOpen ? Colors.green : Colors.red
                                : Colors.green,
                          ),
                        ),
                        Text(
                          FlutterI18n.translate(
                              pageNavigatorKey.currentContext,
                              haveWorkHours
                                  ? (isOpen ? 'Opened' : 'Closed')
                                  : '24H'),
                          style: TextStyle(
                              color: haveWorkHours
                                  ? isOpen ? Colors.green : Colors.red
                                  : Colors.green),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                    leading: Icon(
                      FontAwesomeIcons.mapMarkerAlt,
                      color: Colors.orange,
                      size: 35,
                    ),
                    onTap: () {
                      String uuidFlushbar = AppService.message(MessageModel(
                          title: 'Map',
                          message: 'Loading and three dots',
                          backgroundColor: Colors.black54,
                          duration: null,
                          mainButton: null));

                      Navigator.push(
                          context,
                          MaterialPageRoute<String>(
                            builder: (context) => PointOfSaleCompanyPage(
                              pos: pos,
                              isOpen: isOpen,
                              fromCompanyDetail: true,
                              companyId: companyModelApi.id,
                              companyName: companyModelApi.name,
                              companyNumberOfComments:
                                  companyModelApi.numberOfComments,
                              companyScore: companyModelApi.score,
                              companyLogoUrl: companyModelApi.logoUrl.buildUrl,
                              haveWorkHours: haveWorkHours,
                              flushbarUuid: uuidFlushbar,
                            ),
                          ));
                    },
                  ),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }

  _comments() {
    Widget child = Container(
      padding: const EdgeInsets.all(10),
      child: Text(FlutterI18n.translate(
          pageNavigatorKey.currentContext, 'EMPTY_COMMENTS_COMMUNICATE')),
    );

    if (companyModelApi.numberOfComments != null &&
        companyModelApi.numberOfComments > 0) {
      child = Column(
        children: [
          Container(
            height: (companyModelApi.comments.length < 4
                    ? companyModelApi.comments.length * 100
                    : 300)
                .toDouble(),
            child: ListView(
              children: companyModelApi.comments
                  .map((comment) => CommentComponent(comment: comment))
                  .toList(),
            ),
          ),
          FlatButton(
            onPressed: () => Navigator.pushNamed(
                context, 'companies/${companyModelApi.id}/comments'),
            padding: const EdgeInsets.only(
              left: 75,
              right: 75,
            ),
            child: Text(FlutterI18n.translate(
                pageNavigatorKey.currentContext, 'View all comments')),
          )
        ],
      );
    }

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(7),
        boxShadow: [
          BoxShadow(
            color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
            blurRadius: 10.0,
            spreadRadius: 0.0,
            offset: Offset(
              0.0, // horizontal, move right 10
              0.0, // vertical, move down 10
            ),
          )
        ],
      ),
//        padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.all(10),
      child: Column(
        children: [
          Container(
            height: 50,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.only(left: 10, right: 10),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(width: 1, color: Colors.grey.shade200))),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RichText(
                  text: TextSpan(
                      text: FlutterI18n.translate(
                          pageNavigatorKey.currentContext, 'Comments'),
                      style: TextStyle(
                          fontFamily: 'FiraSans',
                          fontWeight: FontWeight.w500,
                          color: Colors.grey,
                          fontSize: 20),
                      children: <TextSpan>[
                        TextSpan(
                            text: ' (${companyModelApi.numberOfComments})',
                            style: TextStyle(
                                fontFamily: 'FiraSans',
                                fontWeight: FontWeight.w400,
                                color: Colors.grey,
                                fontSize: 15))
                      ]),
                ),
              ],
            ),
          ),
          ScoresComponent(scores: companyModelApi.scores),
          CommentInputComponent(
              myFocusNode: commentFocusNode,
              commentInputController: commentInputController,
              onFieldSubmitted: (term) {},
              clickSuffix: (text) {
                CommentModelApi commentModel = CommentModelApi(
                    comment: text,
                    companyId: companyModelApi.id,
                    forType: 'COMPANY',
                    scores: companyModelApi.scores
                        .map((e) => ScoreModel(tagId: e.tagId, score: 0.0))
                        .toList());

                showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (context) {
                      return AlertDialog(
                        title: Text(
                          FlutterI18n.translate(
                              pageNavigatorKey.currentContext, 'Comment'),
                        ),
                        content: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              FlutterI18n.translate(
                                      pageNavigatorKey.currentContext,
                                      'Please rate') +
                                  ': ${companyModelApi.name}',
                            ),
                            ...(companyModelApi.scores.length > 0
                                ? companyModelApi.scores
                                    .map((e) => Column(
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                top: 10,
                                                bottom: 10,
                                              ),
                                              child: Text(
                                                (AppService
                                                        .listsOfTagsMap['score']
                                                        ?.tagList
                                                        ?.firstWhere(
                                                            (element) =>
                                                                element.id ==
                                                                e.tagId)
                                                        ?.name ??
                                                    ''),
                                                style: TextStyle(fontSize: 18),
                                              ),
                                            ),
                                            SmoothStarRating(
                                              rating: commentModel.scores
                                                  .firstWhere((element) =>
                                                      element.tagId == e.tagId)
                                                  .score,
                                              size: 40,
                                              onRated: (score) {
                                                setState(() {
                                                  commentModel.scores
                                                      .firstWhere((element) =>
                                                          element.tagId ==
                                                          e.tagId)
                                                      .score = score;
                                                });
                                              },
                                              defaultIconData: Icons.star,
                                              color: Colors.orange,
                                              borderColor: Colors.grey.shade300,
                                            ),
                                          ],
                                        ))
                                    .toList()
                                : [
                                    SmoothStarRating(
                                      rating: commentModel.score,
                                      size: 40,
                                      onRated: (score) {
                                        setState(() {
                                          commentModel.score = score;
                                        });
                                      },
                                      defaultIconData: Icons.star,
                                      color: Colors.orange,
                                      borderColor: Colors.grey.shade300,
                                    )
                                  ]),
                            Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Text(
                                FlutterI18n.translate(
                                    pageNavigatorKey.currentContext,
                                    'Swipe from left to right or vice versa to select a score'),
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.grey),
                              ),
                            )
                          ],
                        ),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(
                              FlutterI18n.translate(
                                  pageNavigatorKey.currentContext, 'Cancel'),
                              style: TextStyle(color: Colors.grey),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                          FlatButton(
                            onPressed: () async {
                              CommentApi.saveDocument(commentModel)
                                  .then((value) {
                                if (value != null) {
                                  Navigator.pop(context);
                                  commentInputController.clear();
                                  _refresh();
                                }
                              });
                            },
                            child: Text(
                              FlutterI18n.translate(
                                  pageNavigatorKey.currentContext, 'Send'),
                              style: TextStyle(color: Colors.orange),
                            ),
                          ),
                        ],
                      );
                    });

                commentFocusNode.unfocus();
              },
              commentLabel: FlutterI18n.translate(
                  pageNavigatorKey.currentContext, 'LABEL_COMMENT_INPUT'),
              withSliver: false),
          child,
        ],
      ),
    );
  }

  _offersOrEvents() {
    return StreamBuilder(
      stream: streamSubscription,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        List cards = [];

        if (!_loader) {
          if (snapshot.hasData && snapshot.data.length > 0) {
            switch (selectedIndex) {
              case 0:
                cards.addAll(snapshot.data
                    .map((offer) => OfferCardComponent(
                          offerModelApi: offer,
                        ))
                    .toList());
                break;
              case 1:
                cards.addAll(snapshot.data.map((event) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(
                            left: 10, right: 10, bottom: 10),
                        child: Row(
                          children: <Widget>[
                            Text(
                              Tools.dayName(event.data),
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.w500),
                            ),
                            Text(
                              DateFormat('dd-MM-yyyy')
                                  .format(DateTime.parse(event.data)),
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        ),
                      ),
                      Column(
                        children: event.models.map<Widget>((event) {
                          return OfferCardComponent(
                            showHeader: false,
                            offerModelApi: null,
                            eventModel: event,
                          );
                        }).toList(),
                      )
                    ],
                  );
                }).toList());

                break;
            }
          }
        }

        if (scrollEvent != null) {
          cards.add(ScrollEventComponent(
            stream: scrollEvent,
            useSliver: false,
          ));
        }

        return Column(children: <Widget>[
          Container(
            margin: const EdgeInsets.all(10),
            width: double.infinity,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      if (selectedIndex != 0) {
                        setState(() {
                          _loader = true;
                        });
                        scrollEvent = OfferApi
                            .filterMap['company-detail'].scrollEvent.stream;
                        streamSubscription = OfferApi
                            .filterMap['company-detail']
                            .documentListChanel
                            .stream;
                        OfferApi.filterMap['company-detail']
                            .clearDocumentListChanel();

                        await OfferApi.refreshDocumentList('company-detail',
                            companyId: companyModelApi.id,
                            refreshTagList: true);
                        setState(() {
                          _loader = false;
                          selectedIndex = 0;
                        });
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color:
                              selectedIndex == 0 ? Colors.orange : Colors.white,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(6.0),
                              topLeft: Radius.circular(6.0)),
                          border: Border(
                            top: BorderSide(width: 1, color: Colors.orange),
                            bottom: BorderSide(width: 1, color: Colors.orange),
                            left: BorderSide(width: 1, color: Colors.orange),
                            right: BorderSide(width: 1, color: Colors.orange),
                          )),
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        FlutterI18n.translate(
                            pageNavigatorKey.currentContext, 'Offers'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: selectedIndex == 0
                                ? Colors.white
                                : Colors.orange,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ),
//                Expanded(
//                  child: Container(
//                    decoration: BoxDecoration(
////                      color: Colors.orange,
//                      color: Colors.white,
//                      border: Border.symmetric(
//                        vertical: BorderSide(
//                          width: 1,
//                          color: Colors.orange
//                        )
//                      )
//                    ),
//                    padding: const EdgeInsets.all(10),
//                    child: Text(
//                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Or'),
//                      textAlign: TextAlign.center,
//                      style: TextStyle(
//                        color: Colors.grey,
////                        color: Colors.white,
//                        fontWeight: FontWeight.w500
//                      ),
//                    ),
//                  ),
//                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      if (selectedIndex != 1) {
                        setState(() {
                          _loader = true;
                        });
                        scrollEvent = EventApi
                            .filterMap[filterEventItem].scrollEvent.stream;
                        streamSubscription = EventApi.filterMap[filterEventItem]
                            .documentListChanel.stream;
                        EventApi.filterMap[filterEventItem]
                            .clearDocumentListChanel();

                        await EventApi.refreshDocumentList(filterEventItem,
                            companyId: companyModelApi.id,
                            refreshTagList: true);
                        setState(() {
                          _loader = false;
                          selectedIndex = 1;
                        });
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color:
                              selectedIndex == 1 ? Colors.orange : Colors.white,
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(6.0),
                              topRight: Radius.circular(6.0)),
                          border: Border(
                            top: BorderSide(width: 1, color: Colors.orange),
                            bottom: BorderSide(width: 1, color: Colors.orange),
                            right: BorderSide(width: 1, color: Colors.orange),
                            left: BorderSide(width: 1, color: Colors.orange),
                          )),
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        FlutterI18n.translate(
                            pageNavigatorKey.currentContext, 'Events'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: selectedIndex == 1
                                ? Colors.white
                                : Colors.orange,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          ...cards,
        ]);
      },
    );
  }
}
