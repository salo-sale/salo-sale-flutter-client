import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/api/recommendation.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/models/api/company.model.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/company-card.component.dart';
import 'package:SaloSale/ui/components/header.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/recommended-offers.component.dart';
import 'package:SaloSale/ui/components/scroll-event.component.dart';
import 'package:SaloSale/ui/components/search-input.component.dart';
import 'package:SaloSale/ui/components/tags.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ListCompanyPage extends StatefulWidget implements Widget {

  ListCompanyPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ListCompanyPage();

}

class _ListCompanyPage extends State<ListCompanyPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final ScrollController _scrollController = ScrollController();
  final FocusNode myFocusNode = FocusNode();
  final String filterItem = 'company-list';

  final TextEditingController searchInputController = TextEditingController();

  StreamSubscription scrollToTop;
  StreamSubscription refreshLayout;

  bool initialized = false;

  @override
  void dispose() {

    initialized = true;
    _scrollController?.dispose();
    searchInputController?.dispose();
    scrollToTop?.cancel();
    refreshLayout?.cancel();
    super.dispose();

  }

  @override
  void initState() {

    CompanyApi.putIfNotExistFilterInMap(filterItem);

    AppService.systemSteps.stream.listen((event) {

      if (event == SystemStepsTypeEnum.INITIALIZED) {

        if (!initialized) {

          initialized = true;

          CompanyApi.refreshDocumentList(filterItem, refreshTagList: true);

          _scrollController.addListener(() {

            if (_scrollController.position.maxScrollExtent <= (_scrollController.offset + 250)) {

              CompanyApi.getDocumentList(filterItem);

            }

          });

          scrollToTop = AppService.scrollToTop.stream.listen((success) {

            if (success && _scrollController.offset > 0) {

              _scrollController.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.easeOut);

            }

          });

          refreshLayout = AppService.refreshLayout.stream.listen((refreshContent) {

            if (refreshContent) {

              this._refresh(refreshAdvertisingBanner: false);

            }

          });

        }

      }

    });

    super.initState();

  }

  Future<void> _refresh({bool refreshAdvertisingBanner: true}) {

    if (refreshAdvertisingBanner) {

      AppService.refreshAdvertisingBanners.sink.add(true);

    }

    return CompanyApi.refreshDocumentList(filterItem, search: searchInputController.text);

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      body: RefreshIndicator(
          color: Colors.orange,
          onRefresh: _refresh,
          child: CustomScrollView(
              controller: _scrollController,
              slivers: <Widget>[
                HeaderComponent(
                  titleOfPage: 'Shops and services',
                ),
                searchInputComponent(
                    myFocusNode,
                    searchInputController,
                    () {
                      searchInputController.clear();
                      myFocusNode.unfocus();
                      _refresh(refreshAdvertisingBanner: false);
                    },
                    (term) {
                      AppService.analytics.logEvent(name: 'USE_SEARCH_COMPANY_LIST');
                      CompanyApi.refreshDocumentList(filterItem, search: term);
                    },
                ),
                TagsComponent(
                  tagListName: 'company',
                  selectedTagIdList: CompanyApi.filterMap[filterItem].tagIdList,
                  onSelectedTagIdList: (list) {
                    CompanyApi.refreshDocumentList(filterItem, tagIdList: list, refreshTagList: list == null);
                    RecommendationApi.refreshDocumentList(tagIdList: list);
                  },
                ),
                RecommendedOffersComponent(),
                _titleOfCompanyList(),
                /// Тунель для завантаження пропозиції
                _addNewCompany(),
                StreamBuilder<bool>(
                    stream: AppService.refreshContent.stream,
                    builder: (context, snapshot) {

                      if (snapshot.hasData && snapshot.data) {
                        Timer(Duration(seconds: 1), () {
                          _refresh();
                        });

                        return _loader();
                      } else {
                        return StreamBuilder<List<CompanyModelApi>>(
                            stream: CompanyApi.filterMap[filterItem].documentListChanel.stream,
                            builder: (BuildContext context, AsyncSnapshot<List<CompanyModelApi>> snapshot) {

                                if (snapshot.hasData && snapshot.data.length > 0) {
                                  return SliverList(
                                    delegate: SliverChildBuilderDelegate(
                                          (BuildContext context,
                                          int index) {
                                            CompanyModelApi companyModelApi = snapshot.data[index];

                                        return CompanyCardComponent(
                                          companyModelApi: companyModelApi,
                                          filterItem: filterItem,
                                        );
                                      },
                                      childCount: snapshot.data.length,
                                    ),
                                  );

                                }

                              return _loader();
                            }
                        );
                      }
                    }
                ),
                ScrollEventComponent(stream: CompanyApi.filterMap[filterItem].scrollEvent.stream,),
              ]
          )
      ),
    );

  }

  Widget _loader() {
    return LoaderComponent(
      forSliver: true,
      circularMargin: const EdgeInsets.only(
          top: 40,
          bottom: 40
      ),
      refreshFunction: _refresh,
    );
  }

  _addNewCompany() {

    return SliverToBoxAdapter(
      child: GestureDetector(
        onTap: () {
          Tools.launchUrl(AppService.remoteConfig.links['form']);
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.all(
            10
          ),
          padding: const EdgeInsets.all(
            10
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(7)),
            border: Border.all(width: 1, color: Colors.orange)
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'Add your business'),
                style: TextStyle(
                  color: Colors.orange,
                  fontWeight: FontWeight.w500,
                  fontSize: 20
                ),
              ),
              Icon(
                FontAwesomeIcons.plus,
                color: Colors.orange,
              )
            ],
          ),
        ),
      ),
    );

  }

  Widget _titleOfCompanyList() {

    return SliverToBoxAdapter(
      child: Container(
        margin: const EdgeInsets.only(
            bottom: 5
        ),
        padding: const EdgeInsets.only(
            bottom: 10,
            left: 10,
            right: 10
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.only(
                      left: 5.0
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'The list'),
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.pushNamed(context, 'map');
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.orange,
                      borderRadius: BorderRadius.all(Radius.circular(12.5)),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: Colors.orange,
                          offset: Offset(0, 0),
                          blurRadius: 3,
                        ),
                      ],
                    ),
                    padding: const EdgeInsets.only(
                        top: 10,
                        left: 12.5,
                        right: 12.5,
                        bottom: 10
                    ),
                    child: Row(
                      children: <Widget>[
                        Text(
                          FlutterI18n.translate(context, 'Open map'),
                          style: TextStyle(
                            fontSize: 14.0,
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 10
                          ),
                          child: Icon(
                            FontAwesomeIcons.mapMarkedAlt,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );

  }


}