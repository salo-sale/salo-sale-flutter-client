import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/models/api/company.model.api.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/models/point-of-sale.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/pages/company/point-of-sale.company.page.dart';
import 'package:SaloSale/ui/pages/company/tags.company.component.dart';
import 'package:badges/badges.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapPage extends StatefulWidget {
  final bool useSelectedCompany;
  final String flushbarUuid;
  final String filterCompanyItem;
  MapPage({
    Key key,
    this.useSelectedCompany = false,
    this.flushbarUuid,
    this.filterCompanyItem
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MapPage();
}

class _MapPage extends State<MapPage> {

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Completer<GoogleMapController> _controller = Completer();
  bool disposed;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final FocusNode myFocusNode = FocusNode();

  final TextEditingController searchInputController = TextEditingController();
  bool screenIsNotStandard = false;
  bool openSelectCompany = false;
  bool openTags = false;
  bool loaded = false;
  bool firstOpen = true;
  bool initialized = false;

  StreamSubscription documentListChanel;

  LatLng target;
  String filterMapItem = 'map';

  @override
  initState() {

    disposed = false;

    target = LatLng(AppService.deviceInfoModel.localityModelApi.latitude, AppService.deviceInfoModel.localityModelApi.longitude);

    if (widget.useSelectedCompany) {

      filterMapItem = widget.filterCompanyItem;
      CompanyApi.putIfNotExistFilterInMap(filterMapItem);

      if (CompanyApi.filterMap[filterMapItem].document != null) {
        if (CompanyApi.filterMap[filterMapItem].document.pointOfSaleList
            .length == 1) {
          target = LatLng(
              CompanyApi.filterMap[filterMapItem].document.pointOfSaleList[0]
                  .latitude,
              CompanyApi.filterMap[filterMapItem].document.pointOfSaleList[0]
                  .longitude);
        }
      }

    } else {

      if (CompanyApi.filterMap.containsKey(filterMapItem)) {

        loaded = true;

      } else {

        CompanyApi.putIfNotExistFilterInMap(filterMapItem);

      }

    }

    AppService.systemSteps.stream.listen((event) {

      if (event == SystemStepsTypeEnum.INITIALIZED) {
        if (!initialized) {
          initialized = true;

          screenIsNotStandard = MediaQuery.of(pageNavigatorKey.currentContext).viewPadding.top > 0 && MediaQuery.of(pageNavigatorKey.currentContext).viewPadding.bottom > 0;

          if (widget.useSelectedCompany) {

            if (CompanyApi.filterMap[filterMapItem].document != null) {

              searchInputController.text = CompanyApi.filterMap[filterMapItem].document.name;
              updateMap([CompanyApi.filterMap[filterMapItem].document]);
              openSelectCompany = false;

            }

          } else {

            documentListChanel = CompanyApi.filterMap[filterMapItem].documentListChanel.stream.listen((data) {
              loaded = false;
              updateMap(data);
            });

            if (CompanyApi.filterMap[filterMapItem].documentList == null || CompanyApi.filterMap[filterMapItem].documentList.length == 0) {

              openTags = true;
              firstOpen = false;

            }

          }

          setState(() {

          });

        }
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    documentListChanel?.cancel();
    searchInputController?.dispose();
    disposed = true;
//    CompanyApi.refreshDocumentList(filterMapItem, clearDocument: false);
    super.dispose();
  }

  /// Віджет який з'являється тоді коли починаєш скролити до низу
  @override
  Widget build(BuildContext context) {

    var body = Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          loaded && firstOpen ? Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _loader(),
            ],
          ) : GoogleMap(
            mapType: MapType.normal,
            markers: Set<Marker>.of(markers.values),
            initialCameraPosition: CameraPosition(
              target: target,
              zoom: double.parse(AppService.deviceInfoModel.localityModelApi.zoom.toString()),
            ),
            onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
            },
          ),
          Positioned(
            top: 10,
            right: 15,
            left: 15,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color(0xFF888888),
                    blurRadius: 5.0,
                    // has the effect of softening the shadow
                    spreadRadius: 0.0,
                    // has the effect of extending the shadow
                    offset: Offset(
                      0.0, // horizontal, move right 10
                      0.0, // vertical, move down 10
                    ),
                  )
                ],
              ),
              child: Row(
                children: <Widget>[
                  IconButton(
                    splashColor: Colors.grey,
                    icon: Icon(
                      FontAwesomeIcons.angleLeft,
                      color: Colors.orange,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  Expanded(
                    child: TextField(
                      controller: searchInputController,
                      focusNode: myFocusNode,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.go,
                      onSubmitted: (value) {

                        setState(() {
                          if (value != null && value.length > 0) {
                            searchInputController.text = value;
                            CompanyApi.refreshDocumentList(filterMapItem, search: value);
                            openSelectCompany = true;
                          } else {
                            CompanyApi.refreshDocumentList(filterMapItem);
                          }
                        });
                      },
                      enabled: !widget.useSelectedCompany,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        contentPadding: EdgeInsets.symmetric(horizontal: 15),
                        hintText: FlutterI18n.translate(pageNavigatorKey.currentContext, 'What will we look for?'),
                        suffix: GestureDetector(
                          onTap: () {
                            setState(() {
                              CompanyApi.refreshDocumentList(filterMapItem);
                              searchInputController.clear();
                              myFocusNode.unfocus();
                            });
                          },
                          child: Container(
                            width: 25,
                            height: 25,
                            color: Colors.transparent,
                            child: Icon(
                              FontAwesomeIcons.times,
                              size: 18,
                              color: widget.useSelectedCompany ? Colors.transparent : Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        openTags = !openTags;
                      });
                    },
                    child: Badge(
                      position: BadgePosition(
                        top: 0,
                        right: 5
                      ),
                      showBadge: openTags ? false : CompanyApi.filterMap[filterMapItem] != null ? CompanyApi.filterMap[filterMapItem].tagIdList.length > 0 : false,
                      badgeContent: Text(
                        CompanyApi.filterMap[filterMapItem] != null ? CompanyApi.filterMap[filterMapItem].tagIdList.length.toString() : '0',
                        style: TextStyle(
                            color: Colors.white
                        ),
                      ),
                      child: Container(
                        color: Colors.transparent,
                        padding: const EdgeInsets.only(
                          right: 15,
                          left: 15,
                          top: 15,
                          bottom: 15
                        ),
                        child: Icon(
                          FontAwesomeIcons.filter,
                          color: widget.useSelectedCompany ? Colors.transparent : Colors.orange,
                          size: 18,
                        ),
                      ),
                    )
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 75,
            right: 15,
            left: 15,
            child: AnimatedContainer(
              height: openTags ? 100 : 0,
              child: Container(
                padding: const EdgeInsets.all(0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFF888888),
                      blurRadius: 5.0,
                      // has the effect of softening the shadow
                      spreadRadius: 0.0,
                      // has the effect of extending the shadow
                      offset: Offset(
                        0.0, // horizontal, move right 10
                        0.0, // vertical, move down 10
                      ),
                    )
                  ],
                ),
                child: TagsCompanyComponent(
                  selectedTagIdList: CompanyApi.filterMap[filterMapItem] != null ? CompanyApi.filterMap[filterMapItem].tagIdList : [],
                  filterItem: filterMapItem,
                ),
              ),
              duration: Duration(
                milliseconds: 250,
              ),
            ),
          ),
          Positioned(
            top: openTags ? 195 : 75,
            right: 15,
            left: 15,
            child: AnimatedContainer(
              height: openSelectCompany ? 200 : 0,
              child: openSelectCompany ? Container(
                padding: const EdgeInsets.all(0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xFF888888),
                      blurRadius: 5.0,
                      // has the effect of softening the shadow
                      spreadRadius: 0.0,
                      // has the effect of extending the shadow
                      offset: Offset(
                        0.0, // horizontal, move right 10
                        0.0, // vertical, move down 10
                      ),
                    )
                  ],
                ),
                child: StreamBuilder<List<CompanyModelApi>>(
                    stream: CompanyApi.filterMap[filterMapItem].documentListChanel.stream,
                    builder: (BuildContext context, AsyncSnapshot<List<CompanyModelApi>> snapshot) {

                      if (snapshot.hasData && snapshot.data.length > 0) {

                        return ListView(
                          children: ListTile.divideTiles(
                            context: context,
                            tiles: snapshot.data.map((companyModelApi) {
                              return ListTile(
                                onTap: () {
                                  CompanyApi.selectDocument(filterMapItem, companyModelApi);
                                  setState(() {
                                    searchInputController.text = companyModelApi.name;
                                    openSelectCompany = false;
                                    openTags = false;
                                    updateMap([companyModelApi]);
                                  });
                                },
                                title: Row(
                                  children: <Widget>[
                                    _showLogo(companyModelApi),
                                    Text(
                                      companyModelApi.name,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }).toList(),
                          ).toList(),
                          shrinkWrap: true,
                        );

                      }

                      return _loader();
                    }
                ),
              ) : Container(),
              duration: Duration(
                milliseconds: 250,
              ),
            ),
          ),
        ],
      ),
    );

    if (screenIsNotStandard) {

      return SafeArea(
        child: body,
      );
    }

    return body;

  }

  Future<void> updateMap(List<CompanyModelApi> data) async {
    if (!disposed && data != null && data.length > 0) {
      if (widget.useSelectedCompany) {

      } else {

        if (!loaded) {
          loaded = true;
          AppService.showLoaderWindow();
        }

      }
      markers.clear();

//      await data.forEach((company) async {
//      });

      for (CompanyModelApi company in data) {

        var mapMarker;
        await CompanyApi.mapMarker(company.id).then((companyMapMarker) async {
          if (companyMapMarker != null) {
            mapMarker = companyMapMarker;
          }
        });

        for (PointOfSaleModel pos in company.pointOfSaleList) {

          bool haveWorkHours = pos.workHours != null && pos.workHours.length > 0;
          bool isOpen = haveWorkHours ? pos.isOpen() : true;

          if (isOpen) {
            await CompanyApi.mapMarker(company.id, isOpen: isOpen).then((posMapMarker) async {

              if (posMapMarker != null) {
                mapMarker = posMapMarker;
              }

            });

          }

          markers[MarkerId(pos.id.toString())] = Marker(
            onTap: () {

              String uuidFlushbar =  AppService.message(MessageModel(
                  title: 'Map',
                  message: 'Loading and three dots',
                  backgroundColor: Colors.black54,
                  duration: null,
                  mainButton: null
              ));

              Navigator.push(context, MaterialPageRoute<String>(
                builder: (context) => PointOfSaleCompanyPage(
                  pos: pos,
                  isOpen: isOpen,
                  companyId: company.id,
                  companyName: company.name,
                  companyScore: company.score,
                  companyNumberOfComments: company.numberOfComments,
                  companyLogoUrl: company.logoUrl.buildUrl,
                  haveWorkHours: haveWorkHours,
                  flushbarUuid: uuidFlushbar,
                ),
              ));

            },
            consumeTapEvents: false,
            infoWindow: InfoWindow(
                title: company.name,
                snippet: pos.address
            ),
            markerId: MarkerId(pos.id.toString()),
            position: LatLng(pos.latitude, pos.longitude),
            icon: BitmapDescriptor.fromBytes(mapMarker) ?? BitmapDescriptor.defaultMarker,
//          icon: BitmapDescriptor.fromBytes(bytes.buffer.asUint8List())
          );

        }


      }

      if (!disposed) {
        Timer(Duration(seconds: 2), () {
          setState(() {
            if (widget.useSelectedCompany) {
              if (widget.flushbarUuid != null && widget.flushbarUuid.length > 0) {
                AppService.closeFlushbar(widget.flushbarUuid);
              }
            } else {
              if (loaded) {
                loaded = false;
                firstOpen = false;
                AppService.closeLoaderWindow();
              }
            }
          });
        });
      }
    }

  }

  Widget _showLogo(CompanyModelApi companyModelApi) {

    return Container(
      width: 30,
      margin: const EdgeInsets.only(
        right: 10.0,
      ),
      child: CachedNetworkImage(
        imageUrl: companyModelApi.logoUrl.buildUrl,
        imageBuilder: (context, imageProvider) => Container(
          width: double.infinity,
          height: 30.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(50.0)),
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        placeholder: (context, url) => Container(
          width: double.infinity,
          height: 30.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(50.0)),
            color: Colors.white,
          ),
          child: LoaderComponent(
            withTimer: false,
          ),
        ),
        errorWidget: (context, url, error) => Container(
          width: double.infinity,
          height: 30.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(50.0)),
            color: Colors.grey,
          ),
        ),
      ),
    );

  }

  Widget _loader() {
    return LoaderComponent(
      forSliver: false,
      circularMargin: const EdgeInsets.only(
          top: 40,
          bottom: 40
      ),
      refreshFunction: () {
        setState(() {

        });
      },
    );
  }

}