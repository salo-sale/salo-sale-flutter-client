import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/models/point-of-sale.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/disconnect.component.dart';
import 'package:SaloSale/ui/components/image.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/menu-item.component.dart';
import 'package:SaloSale/ui/components/starts.component.dart';
import 'package:SaloSale/ui/pages/company/detail.company.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class PointOfSaleCompanyPage extends StatefulWidget implements Widget {

  final PointOfSaleModel pos;
  final int companyId;
  final int companyNumberOfComments;
  final double companyScore;
  final String companyName;
  final String companyLogoUrl;
  final bool fromCompanyDetail;
  final bool isOpen;
  final bool haveWorkHours;
  final String flushbarUuid;

  PointOfSaleCompanyPage({
    Key key,
    this.flushbarUuid,
    this.companyLogoUrl,
    this.pos,
    this.companyNumberOfComments,
    this.companyScore,
    this.fromCompanyDetail = false,
    this.haveWorkHours,
    this.isOpen,
    this.companyName,
    this.companyId
  }) : super(key: key ?? Key('Point-of-sale'));

  @override
  State<StatefulWidget> createState() => _PointOfSaleCompanyPage();

}

class _PointOfSaleCompanyPage extends State<PointOfSaleCompanyPage> {

  List<Widget> items;
  LatLng target;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Completer<GoogleMapController> _controller = Completer();
  ScrollController _listViewController = ScrollController();

  @override
  void initState() {

    super.initState();
    var mapMarker;
    target = LatLng(widget.pos.latitude, widget.pos.longitude);
    initItems();

    AppService.analytics.logEvent(name: 'OPEN_PAGE_POINT_OF_SALE');
    _refresh();
    if (AppService.internetIsConnected) {

      CompanyApi.mapMarker(widget.companyId).then((mapMarkerCompany) async {
        if (mapMarkerCompany != null) {
          mapMarker = mapMarkerCompany;
        }
        await CompanyApi.mapMarker(widget.companyId, isOpen: widget.isOpen).then((posMapMarker) async {
          if (posMapMarker != null) {
            mapMarker = posMapMarker;
          }
        });

        markers[MarkerId(widget.pos.id.toString())] = Marker(
          consumeTapEvents: false,
          infoWindow: InfoWindow(
              title: widget.companyName,
              snippet: widget.pos.address
          ),
          markerId: MarkerId(widget.pos.id.toString()),
          position: LatLng(widget.pos.latitude, widget.pos.longitude),
          icon: BitmapDescriptor.fromBytes(mapMarker) ?? BitmapDescriptor.defaultMarker,
        );
        initItems();
        AppService.closeFlushbar(widget.flushbarUuid);
      });

    } else {
      AppService.closeFlushbar(widget.flushbarUuid);
    }

  }

  @override
  void dispose() {
    _listViewController.dispose();
    super.dispose();
  }

  Future _refresh() {

    return Future.value(true);

  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Point of sale')
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            onPressed: () => Tools.launchMap(widget.pos.latitude, widget.pos.longitude),
            icon: Icon(
              FontAwesomeIcons.map,
              color: Colors.orange,
            ),
          )
        ],
      ),
      key: _scaffoldKey,
      body: AppService.internetIsConnected ? ListView.builder(
        controller: _listViewController,
        itemBuilder: (context, index) => items[index],
        itemCount: items.length,
      ) : disconnectComponent('TEXT_DISCONNECT_RECOMMENDED_COMPONENT'),
    );

  }

  initItems() {

    items = [];

    if (AppService.internetIsConnected) {
      items.add(Container(
        height: 250,
        color: Colors.grey.shade300,
        child: GoogleMap(
          mapType: MapType.normal,
          markers: Set<Marker>.of(markers.values),
          initialCameraPosition: CameraPosition(
            target: target,
            zoom: double.parse(AppService.deviceInfoModel.localityModelApi.zoom.toString()),
          ),
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
        ),
      ));
    } else {
      items.add(Container(
        height: 250,
        color: Colors.grey.shade300,
      ));
    }

    items.add(menuItemComponent(
      context: context,
      onTap: () {
      },
      icon: Icon(
        FontAwesomeIcons.solidCircle,
//          size: 10,
        color: widget.haveWorkHours ? widget.isOpen ? Colors.green : Colors.red : Colors.green,
      ),
      colorIcon: widget.haveWorkHours ? widget.isOpen ? Colors.green : Colors.red : Colors.green,
      colorText: widget.haveWorkHours ? widget.isOpen ? Colors.green : Colors.red : Colors.green,
      title: widget.haveWorkHours ? (widget.isOpen ? 'Opened' : 'Closed') : '24H',
    ));
    items.add(
        menuItemComponent(
          colorBg: Colors.white,
          context: context,
          onTap: () {
            if (widget.fromCompanyDetail) {
              Navigator.pop(context);
            } else {
              pageNavigatorKey.currentState.push(
                MaterialPageRoute(
                  builder: (context) => DetailCompanyPage(
                    companyId: widget.companyId,
                  ),
                ),
              );
            }
          },
          useTranslate: false,
          icon: ImageComponent(
            tag: 'company_${widget.companyId}',
            url: widget.companyLogoUrl ?? '',
            height: 75,
            width: 75,
            margin: const EdgeInsets.only(
              right: 0,
              top: 10,
              bottom: 10,
            ),
            withAspectRation: false,
            withHero: false,
          ),
          customTitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                  widget.companyName
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    '${widget.companyScore} ', // FlutterI18n.translate(pageNavigatorKey.currentContext, 'Score') +
                    style: TextStyle(
                        color: Colors.grey.shade600
                    ),
                  ),
                  StartsComponent(
                    value: widget.companyScore,
                    iconSize: 20.0,
                  ),
                  Text(
                    ' (${widget.companyNumberOfComments})',
                    style: TextStyle(
                        color: Colors.grey.shade600
                    ),
                  ),
                ],
              ),
            ],
          ),
        ));
    items.add(menuItemComponent(
      context: context,
      onLongPress: () {
        Clipboard.setData(ClipboardData(text: widget.pos.address ?? (FlutterI18n.translate(pageNavigatorKey.currentContext, 'Point') + ': ' + widget.pos.id.toString())));
        _scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'Copied to Clipboard')
              ),
            )
        );
      },
      padding: const EdgeInsets.all(0),
      useTranslate: false,
//      title: 'Україна, Чернівецька обл., м. Чернівці, 58013, вул. Південно-кільцева, 9/81',
      title: widget.pos.address ?? (FlutterI18n.translate(pageNavigatorKey.currentContext, 'Point') + ': ' + widget.pos.id.toString()),
    ));

    if (widget.pos.workHours != null) {

      if (widget.pos.workHours.length > 0) {

        items.addAll(widget.pos.workHours.map((work) => menuItemComponent(
          context: context,
          onTap: () {
          },
          padding: const EdgeInsets.all(0),
          customTitle: RichText(
            text: TextSpan(
              style: TextStyle(
                fontFamily: 'FiraSans',
                fontWeight: FontWeight.w500,
                color: Colors.black,
                fontSize: 20
              ),
              text: Tools.weekdayNameJS(work.weekday.toString()) + ': ',
              children: [
                TextSpan(
                  text: (work.openTime.split(':')[0] + ':' + work.openTime.split(':')[1]) + ' - ' + (work.closeTime.split(':')[0] + ':' + work.closeTime.split(':')[1]),
                  style: TextStyle(
                      fontFamily: 'FiraSans',
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                      fontSize: 20
                  ),
                )
              ]
            ),

          )
        )));

      }

    } else {

      items.add(LoaderComponent(
        forSliver: false,
      ));

    }

    setState(() {

    });

  }


}