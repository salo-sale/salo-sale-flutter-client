import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/models/tag.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class TagsCompanyComponent extends StatefulWidget implements Widget {

  final List<int> selectedTagIdList;

  final String filterItem;

  TagsCompanyComponent({
    Key key,
    this.selectedTagIdList,
    @required this.filterItem
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TagsCompanyComponent();

}

class _TagsCompanyComponent extends State<TagsCompanyComponent> {

  List<Widget> items;
  List<int> selectedTagIdList = List<int>();


//  TagModel.fromMap({
//  'id': '0',
//  'name': FlutterI18n.translate(pageNavigatorKey.currentContext, 'All'),
//  'icon': 'All',
//  'main': '1',
//  'position': '1'
//  })
  List<TagModel> list = [];

  @override
  void initState() {

    super.initState();
    selectedTagIdList = widget.selectedTagIdList;
    AppService.analytics.logEvent(name: 'OPEN_PAGE_TAGS_COMPANY');
    list.addAll(AppService.listsOfTags.firstWhere((listOfTags) => listOfTags.name == 'company').tagList);

  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
//    if (CompanyApi.filter.tagIdList.length != selectedTagIdList.length) {
//      selectedTagIdList = CompanyApi.filter.tagIdList;
//    }
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10
            ),
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'Tags'),
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500
                  ),
                ),
//                Text(
//                  CompanyApi.filterMap[widget.filterItem].tagIdList.length.toString() +'/'+ list.length.toString(),
//                  style: TextStyle(
//                      fontSize: 15,
//                      fontWeight: FontWeight.w500
//                  ),
//                ),
              ],
            ),
          ),
          Flexible(
            child: Container(
              height: 50,
              width: double.infinity,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: list.length,
                itemBuilder: (context, index) {

                  var tag = list[index];
                  bool isSelected = selectedTagIdList.firstWhere((tagId) => tagId == tag.id, orElse: () => null) != null;

                  return GestureDetector(
                    onTap: () {
                      if (!isSelected) {
                        selectedTagIdList.clear();
                        selectedTagIdList.add(tag.id);
                        setState(() {
                          CompanyApi.refreshDocumentList(widget.filterItem, tagIdList: selectedTagIdList, search: CompanyApi.filterMap[widget.filterItem].search);
                        });
                      } else {
//                        selectedTagIdList.removeWhere((tagId) => tagId == tag.id);
                      }
                    },
                    child: Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.only(
                          left: 15,
                          right: 15,
                          top: 0,
                          bottom: 0
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            tag.iconData,
                            color: isSelected ? Colors.orange : Color(0xFF8E8E93),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 5
                            ),
                            child: Text(
                              tag.name,
//                              FlutterI18n.currentLocale(context) == null ? tag.name : FlutterI18n.currentLocale(context).languageCode != 'uk' ? tag.nameEn : tag.name ?? FlutterI18n.translate(context, tag.nameEn),
                              style: TextStyle(
                                  color: isSelected ? Colors.orange : Colors.grey,
                                  fontWeight: FontWeight.w500
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );

  }

}