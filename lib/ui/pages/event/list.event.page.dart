import 'dart:async';


import 'package:SaloSale/routing.dart';
import 'package:SaloSale/salo_sale_icons_icons.dart';
import 'package:SaloSale/shared/api/event.api.dart';
import 'package:SaloSale/shared/api/recommendation.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/models/api/event-list.model.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/recommended-offers.component.dart';
import 'package:SaloSale/ui/components/header.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/offer-card.component.dart';
import 'package:SaloSale/ui/components/scroll-event.component.dart';
import 'package:SaloSale/ui/components/search-input.component.dart';
import 'package:SaloSale/ui/components/tags.component.dart';
import 'package:SaloSale/ui/pages/menu/feedback.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';

class ListEventPage extends StatefulWidget implements Widget {

  ListEventPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ListEventPage();

}

class _ListEventPage extends State<ListEventPage> with SingleTickerProviderStateMixin {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final FocusNode myFocusNode = FocusNode();

  final TextEditingController searchInputController = TextEditingController();

  final ScrollController _scrollController = ScrollController();
  final String filterItem = 'events';

  StreamSubscription scrollToTop;
  StreamSubscription refreshLayout;

  bool initialized = false;

  @override
  void dispose() {
    initialized = true;
    _scrollController?.dispose();
    searchInputController?.dispose();
    scrollToTop?.cancel();
    refreshLayout?.cancel();
    super.dispose();
  }

  @override
  void initState() {

    EventApi.putIfNotExistFilterInMap(filterItem);

    AppService.systemSteps.stream.listen((event) {

      if (event == SystemStepsTypeEnum.INITIALIZED) {

        if (!initialized) {

          initialized = true;

          EventApi.refreshDocumentList(filterItem, refreshTagList: true);

          _scrollController.addListener(() {

            if (_scrollController.position.maxScrollExtent <= (_scrollController.offset + 250)) {

              EventApi.getDocumentList(filterItem);

            }

          });

          scrollToTop = AppService.scrollToTop.stream.listen((success) {

            if (success && _scrollController.offset > 0) {

              _scrollController.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.easeOut);

            }

          });

          refreshLayout = AppService.refreshLayout.stream.listen((refreshContent) {

            if (refreshContent) {

              this._refresh(refreshAdvertisingBanner: false);

            }

          });

        }

      }

    });

    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          RefreshIndicator(
              color: Colors.orange,
              onRefresh: _refresh,
              child: CustomScrollView(
                  controller: _scrollController,
                  slivers: <Widget>[
                    HeaderComponent(
                      titleOfPage: 'Events',
                    ),
                    searchInputComponent(
                        myFocusNode,
                        searchInputController,
                            () {
                          searchInputController.clear();
                          myFocusNode.unfocus();
                          _refresh(refreshAdvertisingBanner: false);
                        },
                            (term) {
                              AppService.analytics.logEvent(name: 'USE_SEARCH_EVENT_LIST');
                          EventApi.refreshDocumentList(filterItem, search: term);
                        },
//                        searchTitle: FlutterI18n.translate(pageNavigatorKey.currentContext, 'Search events in')
                    ),
                    TagsComponent(
                      tagListName: 'event',
                      onSelectedTagIdList: (list) {
                        EventApi.refreshDocumentList(filterItem, tagIdList: list);
                        RecommendationApi.refreshDocumentList(tagIdList: list);
                      },
                    ),
                    RecommendedOffersComponent(),
                    _addNewEvent(),
                    /// Тунель для завантаження пропозиції
                    StreamBuilder<bool>(
                        stream: AppService.refreshContent.stream,
                        builder: (context, snapshot) {

                          if (snapshot.hasData && snapshot.data) {
                            Timer(Duration(seconds: 1), () {
                              _refresh();
                            });

                            return _loader();
                          } else {
                            return StreamBuilder<List<EventListModelApi>>(
                                stream: EventApi.filterMap[filterItem].documentListChanel.stream,
                                builder: (BuildContext context, AsyncSnapshot<List<EventListModelApi>> snapshot) {

                                  if (snapshot.hasData) {

                                    if (snapshot.data.length > 0) {


                                      return SliverList(
                                        delegate: SliverChildBuilderDelegate((BuildContext context, int index) {

                                          EventListModelApi eventListModel = snapshot.data[index];

                                          return Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                margin: const EdgeInsets.only(
                                                    left: 10,
                                                    right: 10,
                                                    bottom: 10
                                                ),
                                                child: Row(
                                                  children: <Widget>[
                                                    Text(
                                                      Tools.dayName(eventListModel.data),
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                          fontSize: 25,
                                                          fontWeight: FontWeight.w500
                                                      ),
                                                    ),
                                                    Text(
                                                      DateFormat('dd-MM-yyyy').format(DateTime.parse(eventListModel.data)),
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontWeight: FontWeight.w500
                                                      ),
                                                    ),
                                                  ],
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                ),
                                              ),
                                              Column(
                                                children: eventListModel.models.map<Widget>((event) {
                                                  return OfferCardComponent(
                                                    showHeader: false,
                                                    offerModelApi: null,
                                                    eventModel: event,
                                                  );
                                                }).toList(),
                                              )
                                            ],
                                          );
                                        },
                                          childCount: snapshot.data.length,
                                        ),
                                      );

                                    }

                                    return SliverToBoxAdapter(
                                      child: Container(
                                      ),
                                    );

                                  }

                                  return _loader();
                                }
                            );
                          }
                        }
                    ),
                    ScrollEventComponent(stream: EventApi.filterMap[filterItem].scrollEvent.stream,),
                  ]
              )
            ),
        ],
      ),
    );

  }

  /// Фіункція для оновлення даних
  Future<void> _refresh({bool refreshAdvertisingBanner: true}) {

    if (refreshAdvertisingBanner) {

      AppService.refreshAdvertisingBanners.sink.add(true);

    }

    return EventApi.refreshDocumentList(filterItem);

  }

  _addNewEvent() {

    if (!AppService.remoteConfig.checkModule('addNewEvent') || !AppService.remoteConfig.modulesControl['addNewEvent'].use) {
      return SliverToBoxAdapter(
        child: Container(),
      );
    }

    return SliverToBoxAdapter(
      child: GestureDetector(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => FeedbackMenuHomePage(
              text: FlutterI18n.translate(pageNavigatorKey.currentContext, 'STANDARD_TEXT_NEW_EVENT') + '${AppService.remoteConfig.modulesControl['addNewOffer'].defaultFilterValue['ikoint']} ik' + '\n' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'STANDARD_FORM_IN_TEXTAREA'),
            ),
          ));
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.all(
              10
          ),
          padding: const EdgeInsets.all(
              10
          ),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(7)),
              border: Border.all(width: 1, color: Colors.orange)
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'BTN_ADD_NEW_EVENT'),
                style: TextStyle(
                    color: Colors.orange,
                    fontWeight: FontWeight.w500,
                    fontSize: 20
                ),
              ),

              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        right: 10
                    ),
                    child: Text(
                      '+ ${AppService.remoteConfig.modulesControl['addNewOffer'].defaultFilterValue['ikoint']}' ?? '',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Colors.orange
                      ),
                    ),
                  ),
                  Icon(
                    SaloSaleIcons.ikoint_logo,
                    color: Colors.orange,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );

  }

  Widget _loader() {
    return LoaderComponent(
      forSliver: true,
      circularMargin: const EdgeInsets.only(
          top: 40,
          bottom: 40
      ),
      refreshFunction: _refresh,
    );
  }

}