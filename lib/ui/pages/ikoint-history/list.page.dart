import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/ui/components/ikoint.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class IkointHistoryPage extends StatefulWidget {

  IkointHistoryPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _IkointHistoryPage();

}

class _IkointHistoryPage extends State<IkointHistoryPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  initState() {

    AppService.analytics.logEvent(name: 'OPEN_IKOINT_HOSTORY_PAGE');

    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
            FlutterI18n.translate(context, 'History')
        ),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10),
            child: Center(
              child: Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'TOP_MESSAGE_IN_HISTORY_PAGE'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500
                ),
              ),
            ),
          ),
          IkointComponent(
            useOnTapForBig: false,
            bigCounter: true
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: Center(
              child: Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'BOTTOM_MESSAGE_IN_HISTORY_PAGE'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey,
                  fontWeight: FontWeight.w500
                ),
              ),
            ),
          ),
        ],
      ),
    );

  }

}