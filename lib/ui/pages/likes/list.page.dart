import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/api/event.api.dart';
import 'package:SaloSale/shared/api/offer.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/company-card.component.dart';
import 'package:SaloSale/ui/components/disconnect.component.dart';
import 'package:SaloSale/ui/components/header.component.dart';
import 'package:SaloSale/ui/components/offer-card.component.dart';
import 'package:SaloSale/ui/components/scroll-event.component.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class LikesPage extends StatefulWidget implements Widget {

  LikesPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LikesPage();

}

class _LikesPage extends State<LikesPage> with SingleTickerProviderStateMixin {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _loader;
  final String filterOfferItem = 'likes-offers';
  final String filterEventItem = 'likes-events';
  final String filterCompanyItem = 'likes-companies';

  final ScrollController _scrollController = ScrollController();

  StreamSubscription scrollToTop;
  Stream streamSubscription;
  Stream scrollEvent;

  int selectedIndex;

  @override
  void initState() {

    _loader = false;
    selectedIndex = null;
    EventApi.putIfNotExistFilterInMap(filterEventItem);
    OfferApi.putIfNotExistFilterInMap(filterOfferItem);
    CompanyApi.putIfNotExistFilterInMap(filterCompanyItem);

    _scrollController.addListener(() async {

      if (_scrollController.position.maxScrollExtent <= (_scrollController.offset + 250)) {

        switch(selectedIndex) {
          case 0:
            OfferApi.getDocumentList(filterOfferItem);
            break;
          case 1:
            CompanyApi.getDocumentList(filterCompanyItem);
            break;
          case 2:
            EventApi.getDocumentList(filterEventItem);
            break;
        }

      }

    });

    scrollToTop = AppService.scrollToTop.stream.listen((success) {

      if (success && _scrollController.offset > 0) {

        _scrollController.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.easeOut);

      }

    });

    AppService.analytics.logEvent(name: 'OPEN_LIKES_PAGE');

    super.initState();

  }

  @override
  void dispose() {
    _scrollController?.dispose();
    scrollToTop?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      body: AuthService.authModel.isGuest ? _guestContent() : _content(),
    );

  }

  Future<void> _refresh() {

    switch(selectedIndex) {
      case 0:
        OfferApi.refreshDocumentList('offer-likes', myLike: 1, refreshTagList: true);
        break;
      case 1:
        CompanyApi.refreshDocumentList(filterCompanyItem, myLike: 1, refreshTagList: true);
        break;
      case 2:
        EventApi.refreshDocumentList(filterEventItem, myLike: 1, refreshTagList: true);
        break;
    }

    return Future.value();

  }

  Widget _content() {

    return RefreshIndicator(
      color: Colors.orange,
      onRefresh: _refresh,
      child: StreamBuilder(
        stream: streamSubscription,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          List cards = [];

          if (!_loader) {

            switch(selectedIndex) {
              case 0:
                if (snapshot.hasData && snapshot.data.length > 0) {
                  cards.addAll(snapshot.data.map((offer) =>
                      OfferCardComponent(offerModelApi: offer,)).toList());
                }
                break;
              case 1:
                if (snapshot.hasData && snapshot.data.length > 0) {
                  cards.addAll(snapshot.data.map((company) =>
                      CompanyCardComponent(companyModelApi: company,))
                      .toList());
                }
                break;
              case 2:

                if (snapshot.hasData && snapshot.data.length > 0) {
                  cards.addAll(snapshot.data.map((event) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(
                              left: 10,
                              right: 10,
                              bottom: 10
                          ),
                          child: Row(
                            children: <Widget>[
                              Text(
                                Tools.dayName(event.data),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w500
                                ),
                              ),
                              Text(
                                DateFormat('dd-MM-yyyy').format(
                                    DateTime.parse(event.data)),
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w500
                                ),
                              ),
                            ],
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          ),
                        ),
                        Column(
                          children: event.models.map<Widget>((event) {
                            return OfferCardComponent(
                              showHeader: false,
                              eventModel: event,
                            );
                          }).toList(),
                        )
                      ],
                    );
                  }).toList());
                }

                break;
              default:
                if (AppService.internetIsConnected) {

                  cards.add(Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 75,
                        horizontal: 10
                    ),
                    child: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Select one of the items to view your favorite offers, events, shops and services'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 18
                      ),
                    ),
                  ));

                } else {

                  cards.add(disconnectComponent('TEXT_DISCONNECT'));

                }
                break;
            }

          }

          return ListView(
              controller: _scrollController,
              children: <Widget>[
                HeaderComponent(
                  forCustomScroll: false,
                  titleOfPage: 'Favorite',
                ),
                _selectTypeNode(),
                ...cards,
                scrollEvent != null ? ScrollEventComponent(
                  stream: scrollEvent,
                  iconEmpty: FontAwesomeIcons.heart,
                  textEmpty: 'EMPTY_FAVORITE_LIST',
                  useSliver: false,
                ) : Container(),
              ]
          );
        },
      ),
    );

  }


  _selectTypeNode() {

    return Container(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      if (selectedIndex != 0) {
                        setState(() {
                          _loader = true;
                        });
                        scrollEvent = OfferApi.filterMap[filterOfferItem].scrollEvent.stream;
                        OfferApi.filterMap[filterOfferItem].clearDocumentListChanel();
                        streamSubscription = OfferApi.filterMap[filterOfferItem].documentListChanel.stream;
                        await OfferApi.refreshDocumentList(filterOfferItem, myLike: 1, refreshTagList: true);
                        setState(() {
                          selectedIndex = 0;
                          _loader = false;
                        });
                      }
                    },
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: selectedIndex == 0 ? Colors.orange : Colors.white,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(6.0),
                              topLeft: Radius.circular(6.0)
                          ),
                          border: Border(
                            top: BorderSide(
                                width: 1,
                                color: Colors.orange
                            ),
                            bottom: BorderSide(
                                width: 1,
                                color: Colors.orange
                            ),
                            left: BorderSide(
                                width: 1,
                                color: Colors.orange
                            ),
                            right: BorderSide(
                                width: 1,
                                color: Colors.orange
                            ),
                          )
                      ),
                      padding: const EdgeInsets.all(10),
                      child: Center(
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Offers'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: selectedIndex == 0 ? Colors.white : Colors.orange,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      if (selectedIndex != 1) {
                        setState(() {
                          _loader = true;
                        });
                        scrollEvent = CompanyApi.filterMap[filterCompanyItem].scrollEvent.stream;
                        CompanyApi.filterMap[filterCompanyItem].clearDocumentListChanel();
                        streamSubscription = CompanyApi.filterMap[filterCompanyItem].documentListChanel.stream;
                        await CompanyApi.refreshDocumentList(filterCompanyItem, myLike: 1, refreshTagList: true);
                        setState(() {
                          selectedIndex = 1;
                          _loader = false;
                        });
                      }
                    },
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                        color: selectedIndex == 1 ? Colors.orange : Colors.white,
                        border: Border.symmetric(
                          vertical: BorderSide(
                            width: 1,
                            color: Colors.orange
                          )
                        )
                      ),
                      padding: const EdgeInsets.all(2.5),
                      child: Center(
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Shops and services'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: selectedIndex == 1 ? Colors.white : Colors.orange,
                            fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () async {
                      if (selectedIndex != 2) {
                        setState(() {
                          _loader = true;
                        });
                        scrollEvent = EventApi.filterMap[filterEventItem].scrollEvent.stream;
                        EventApi.filterMap[filterEventItem].clearDocumentListChanel();
                        streamSubscription = EventApi.filterMap[filterEventItem].documentListChanel.stream;
                        await EventApi.refreshDocumentList(filterEventItem, myLike: 1, refreshTagList: true);
                        setState(() {
                          selectedIndex = 2;
                          _loader = false;
                        });
                      }
                    },
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: selectedIndex == 2 ? Colors.orange : Colors.white,
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(6.0),
                              topRight: Radius.circular(6.0)
                          ),
                          border: Border(
                            top: BorderSide(
                                width: 1,
                                color: Colors.orange
                            ),
                            bottom: BorderSide(
                                width: 1,
                                color: Colors.orange
                            ),
                            right: BorderSide(
                                width: 1,
                                color: Colors.orange
                            ),
                            left: BorderSide(
                                width: 1,
                                color: Colors.orange
                            ),
                          )
                      ),
                      padding: const EdgeInsets.all(10),
                      child: Center(
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Events'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: selectedIndex == 2 ? Colors.white : Colors.orange,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );

  }

  Widget _guestContent() {

    return Column(
      children: <Widget>[
        HeaderComponent(
          forCustomScroll: false,
          titleOfPage: 'Favorite',
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Text(
            FlutterI18n.translate(pageNavigatorKey.currentContext, 'like_page_guest'),
            style: TextStyle(
              fontSize: 20
            ),
          ),
        ),
      ],
    );

  }

}