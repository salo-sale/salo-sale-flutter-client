
import 'package:SaloSale/data.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AboutMenuHomePage extends StatefulWidget {

  AboutMenuHomePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AboutMenuHomePage();

}

class _AboutMenuHomePage extends State<AboutMenuHomePage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    AppService.analytics.logEvent(name: 'OPEN_ABOUT_SALO_SALE_PAGE');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          FlutterI18n.translate(context, 'About us')
        ),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            slivers: <Widget>[
              SliverToBoxAdapter(
                child: Stack(
                  children: <Widget>[
                    Container(
                      height: 175,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.orange,
                      ),
                      height: 75,
                    ),
                    Positioned.fill(
                      bottom: 50,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100.0),
                            color: Colors.white,
                            image: DecorationImage(
                                image: AssetImage(
                                  'assets/images/ms-icon-150x150.png',
                                )
                            ),
                            border: Border.all(width: 5, color: Colors.orange),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.only(
                    bottom: 25,
                    left: 10,
                    right: 10
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          FlutterI18n.translate(context, 'Motto Salo-Sale'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20
                        ),
                      ),
                      Text(
                        FlutterI18n.translate(context, 'Website'),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20
                        ),
                      ),
                      GestureDetector(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.globe,
                              color: Colors.orange,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 10.0
                              ),
                              child: Text(
                                'salo-sale.com',
                                style: TextStyle(
                                  color: Colors.orange
                                ),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                          Tools.launchWebSite('salo-sale.com');
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20
                        ),
                      ),
                      Text(
                        FlutterI18n.translate(context, 'Social network'),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Tools.launchWebSite('instagram.com/salo_sale_com');
                        },
                        child: Row(
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.instagram,
                              color: Colors.orange,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 10.0
                              ),
                              child: Text(
                                'salo_sale_com',
                                style: TextStyle(
                                  color: Colors.orange
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 20
                        ),
                      ),
                      GestureDetector(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.facebook,
                              color: Colors.orange,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 10.0
                              ),
                              child: Text(
                                'salo.sale.world',
                                style: TextStyle(
                                  color: Colors.orange
                                ),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                          Tools.launchWebSite('facebook.com/salo.sale.world');
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 20
                        ),
                      ),
                      GestureDetector(
                        child: Row(
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.telegram,
                              color: Colors.orange,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 10.0
                              ),
                              child: Text(
                                Data.nameOfApp,
                                style: TextStyle(
                                  color: Colors.orange
                                ),
                              ),
                            )
                          ],
                        ),
                        onTap: () {
                          Tools.launchWebSite('https://t.me/joinchat/AAAAAFVOISNuHdRqI1JW_g');
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );

  }

}