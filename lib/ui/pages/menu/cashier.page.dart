import 'dart:async';
import 'dart:convert';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/salo_sale_icons_icons.dart';
import 'package:SaloSale/shared/api/auth.api.dart';
import 'package:SaloSale/shared/api/cashier.api.dart';
import 'package:SaloSale/shared/api/offer.api.dart';
import 'package:SaloSale/shared/models/api/company.model.api.dart';
import 'package:SaloSale/shared/models/api/offer.model.api.dart';
import 'package:SaloSale/shared/models/api/scanned.model.api.dart';
import 'package:SaloSale/shared/models/api/user.model.api.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/models/point-of-sale.model.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/disconnect.component.dart';
import 'package:SaloSale/ui/components/image.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/pages/offer/detail.offer.page.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CashierMenuHomePage extends StatefulWidget {

  CashierMenuHomePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CashierMenuHomePage();

}

class _CashierMenuHomePage extends State<CashierMenuHomePage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool pointOfsAleListIsInitialized;
  OfferModelApi newScannedOffer;
  UserModelApi newScannedClient;
  StreamSubscription scrollToTop;
  StreamSubscription documentListChanel;

  Map<String, int> newScannedOfferData;
  bool _showSelectPointOfSale;
  final ScrollController _scrollController = ScrollController();
  PointOfSaleModel _selectedPointOfSale;
  List<CompanyModelApi> companyModelApiList = [];
  List<ScannedModelApi> scannedOfferList;

  @override
  void dispose() {
    pointOfsAleListIsInitialized = false;
    scrollToTop?.cancel();
    documentListChanel?.cancel();
    super.dispose();
  }

  @override
  initState() {
    OfferApi.putIfNotExistFilterInMap('offer-cashier');

    _selectedPointOfSale = null;

    pointOfsAleListIsInitialized = false;
    _showSelectPointOfSale = false;

    CashierApi.getPointsOfSale(isInitialized: pointOfsAleListIsInitialized).then((value) {

      if (value != null && value.length > 0) {

        if (!pointOfsAleListIsInitialized) {
          pointOfsAleListIsInitialized = true;
        }

        setState(() {
          companyModelApiList = value;
        });

      }

    });

    scrollToTop = AppService.scrollToTop.stream.listen((success) {

      if (success && _scrollController.offset > 0) {

        _scrollController.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.easeOut);

      }

    });

    _scrollController.addListener(() {

      if (_scrollController.position.maxScrollExtent <= (_scrollController.offset + 250)) {

        CashierApi.getDocumentList();

      }

    });

    documentListChanel = CashierApi.filter.documentListChanel.stream.listen((_scannedOfferList) {
      setState(() {
        scannedOfferList = _scannedOfferList;
      });
    });

    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
            FlutterI18n.translate(context, 'Cashier')
        ),
        centerTitle: true,
      ),
      body: RefreshIndicator(
          color: Colors.orange,
        onRefresh: _refresh,
        child: AppService.internetIsConnected ? ListView(
          controller: _scrollController,
          children: <Widget>[
            _animateSelectPointOfSale(),
            _buttonForScanningQrCode(),
            FutureBuilder(
              future: _showNewScannedOffer(),
              builder: (context, AsyncSnapshot<Widget> snapshot) {

                if (snapshot.hasData) {

                  return snapshot.data;

                }

                return Container();

              },
            ),
            ..._showScannedOffer(),
          ],
        ) : disconnectComponent('TEXT_DISCONNECT'),
      ),
    );

  }

  Future<void> _refresh() {

    return CashierApi.refreshDocumentList(pointOfSaleId: _selectedPointOfSale.id);

  }

  _showSelectedPointOfSale(pos) {

    if (pos == null) {

      return Text(
        FlutterI18n.translate(pageNavigatorKey.currentContext, 'Select point of sale'),
        style: TextStyle(
            fontWeight: FontWeight.w500,
            color: Colors.grey,
            fontSize: 20
        ),
      );

    } else {

      return Row(
        children: <Widget>[
          ImageComponent(
            url: pos.companyLogo.buildUrl ?? '',
            height: 30,
            width: 30,
            rounded: 50,
            margin: const EdgeInsets.only(
                right: 10
            ),
            withAspectRation: false,
            withHero: false,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                pos.companyName ?? '',
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontWeight: FontWeight.w500
                ),
              ),
              Text(
                AppService.deviceInfoModel.localityModelApi.name + ', ' + pos.address ?? '',
                textAlign: TextAlign.left,
              ),
            ],
          ),
        ],
      );

    }

  }

  _animateSelectPointOfSale() {

    if (companyModelApiList.length == 0) {
      return LoaderComponent(
        refreshFunction: () {
          setState(() {
            pointOfsAleListIsInitialized = false;
          });
        },
      );
    }


    List<PointOfSaleModel> pointOfSaleList = [];

    companyModelApiList.forEach((company) {

      if (company.pointOfSaleList != null && company.pointOfSaleList.length > 0) {

        pointOfSaleList.addAll(company.pointOfSaleList.map((e) {
          e.companyLogo = company.logoUrl;
          e.companyName = company.name;
          return e;
        }).toList());

      }

    });

    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(
          top: 10.0,
          left: 10,
          right: 10
      ),
      padding: const EdgeInsets.all(2.5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(7)),
        boxShadow: [
          BoxShadow(
            color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
            blurRadius: 10.0,
            // has the effect of softening the shadow
            spreadRadius: 0.0,
            // has the effect of extending the shadow
            offset: Offset(
              0.0, // horizontal, move right 10
              0.0, // vertical, move down 10
            ),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              setState(() {
                _showSelectPointOfSale = !_showSelectPointOfSale;
              });
            },
            child: Container(
              height: 40,
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.only(
                  left: 10,
                  right: 10
              ),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _showSelectedPointOfSale(_selectedPointOfSale),
                  Icon(
                    _showSelectPointOfSale ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
                    color: Colors.grey,
                  )
                ],
              ),
            ),
          ),
          AnimatedContainer(
            height: double.parse((_showSelectPointOfSale ? pointOfSaleList.length > 4 ? 250 : (pointOfSaleList.length * 50) : 0).toString()),
            child: Container(
              child: ListView(
                children: pointOfSaleList.map((pos) {
                  return GestureDetector(
                    onTap: () {
                      if (_selectedPointOfSale == null || _selectedPointOfSale.id != pos.id) {
                        setState(() {
                          _showSelectPointOfSale = false;
                          _selectedPointOfSale = pos;
                          _refresh();
                        });
                      }
                    },
                    child: Container(
                      height: 60,
                      decoration: BoxDecoration(
                          color: Colors.transparent
                      ),
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          _showSelectedPointOfSale(pos),
                          Icon(
                            _selectedPointOfSale != null && _selectedPointOfSale.id == pos.id ? SaloSaleIcons.radio_checked : SaloSaleIcons.radio_unchecked,
                            color: _selectedPointOfSale != null && _selectedPointOfSale.id == pos.id ? Colors.green : Colors.grey,
                          )
                        ],
                      ),
                    ),
                  );
                }).toList(),
              ),
            ),
            duration: Duration(
              milliseconds: 250,
            ),
          )
        ],
      ),
    );

  }

  _buttonForScanningQrCode() {

    if (_selectedPointOfSale == null) {

      return Container();

    }

    return GestureDetector(
      onTap: barcodeScanning,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.blueGrey,
          borderRadius: BorderRadius.all(Radius.circular(7))
        ),
        width: double.infinity,
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(10),
        child: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Open the scanner'),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 20
          ),
        ),
      ),
    );

  }

  Future barcodeScanning() async {

    String barcode = '';

    try {
      barcode = await BarcodeScanner.scan();
      setState(() {
        newScannedOfferData = Map.from(jsonDecode(utf8.fuse(base64).decode(Tools.reverseString(Tools.convertOpposite(barcode)))));
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        AppService.message(MessageModel(
            title: 'Camera',
            message: 'No camera permission!',
            backgroundColor: Colors.red,
            duration: null,
            mainButton: null
        ));
      } else {
        AppService.message(MessageModel(
            title: 'Error',
            message: 'Unknown error: $e',
            backgroundColor: Colors.red,
            duration: null,
            mainButton: null
        ));
      }
    } on FormatException {
      AppService.message(MessageModel(
          title: 'Camera',
          message: 'Nothing captured.',
          backgroundColor: Colors.yellow,
          duration: null,
          mainButton: null
      ));
    } catch (e) {
      AppService.message(MessageModel(
          title: 'Error',
          message: 'Unknown error: $e',
          backgroundColor: Colors.red,
          duration: null,
          mainButton: null
      ));
    }
  }

  Future<Widget> _showNewScannedOffer() async {

    if (newScannedOfferData == null) {

      return Future.value(Container());

    }

    if (newScannedOffer == null && newScannedClient == null) {

      newScannedOffer = await OfferApi.getDocument('offer-cashier', newScannedOfferData['offer_id']);
      newScannedClient = UserModelApi.fromMap(jsonDecode((await AuthApi.getUser(newScannedOfferData['user_id'].toString())).body));

    }

    return Future.value(Container(
      decoration: BoxDecoration(
          color: Colors.teal,
          borderRadius: BorderRadius.all(Radius.circular(7))
      ),
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'New invoice'),
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontSize: 25
                ),
              ),
              GestureDetector(
                onTap: () {

                  setState(() {

                    newScannedClient = null;
                    newScannedOffer = null;
                    newScannedOfferData = null;

                  });

                },
                onDoubleTap: () {

                },
                child: Container(
                  child: Icon(
                    FontAwesomeIcons.times,
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
          Container(
            decoration: BoxDecoration(
//              color: Colors.teal.shade900,
//              borderRadius: BorderRadius.all(Radius.circular(7))
            ),
            margin: const EdgeInsets.only(
              top: 10
            ),
//            padding: const EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                ImageComponent(
                  url: newScannedOffer.company.logoUrl.buildUrl ?? '',
                  height: 40,
                  width: 40,
                  rounded: 50,
                  margin: const EdgeInsets.only(
                      right: 10
                  ),
                  withAspectRation: false,
                  withHero: false,
                ),
                Column(
                  children: <Widget>[
                    Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Provider'),
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 16
                      ),
                    ),
                    Text(
                      newScannedOffer.company.name,
                      softWrap: true,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 16
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 10
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'Offer'),
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 18
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: Text(
                  newScannedOffer.language.title,
                  softWrap: true,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      fontSize: 18
                  ),
                ),
              ),
            ],
          ),
          GestureDetector(
            onTap: () {

              Navigator.push(context, MaterialPageRoute(
                builder: (context) => DetailOfferPage(
                  offerId: newScannedOffer.id
                ),
              ));

            },
            onDoubleTap: () {

            },
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.teal.shade700,
                  borderRadius: BorderRadius.all(Radius.circular(7))
              ),
              margin: const EdgeInsets.only(
                  top: 10
              ),
              padding: const EdgeInsets.all(10),
              child: Center(
                child: Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'Open offer'),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w500
                  ),
                ),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
//              color: Colors.teal.shade900,
//              borderRadius: BorderRadius.all(Radius.circular(7))
            ),
            margin: const EdgeInsets.only(
                top: 10
            ),
//            padding: const EdgeInsets.all(10),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    right: 10
                  ),
                  child: Icon(
                    FontAwesomeIcons.solidUserCircle,
                    size: 40,
                    color: Colors.white,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    newScannedClient.lastName != null && newScannedClient.firstName != null ? Text(
                      newScannedClient.lastName + '' + newScannedClient.firstName,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 16
                      ),
                    ) : Container(),
                    Text(
                      newScannedClient.email,
                      softWrap: true,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: 16
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          GestureDetector(
            onTap: () async {
              String uuidFlushbar =  AppService.message(MessageModel(
                  title: 'Cashier',
                  message: 'Loading and three dots',
                  backgroundColor: Colors.black54,
                  duration: null,
                  mainButton: null
              ));
              Map<String, dynamic> body = {
                'offerId': newScannedOffer.id.toString(),
                'userId': newScannedClient.id.toString(),
                'cashierUserId': AuthService.authModel.id.toString(),
                'pointOfSaleId': _selectedPointOfSale.id.toString()
              };
              await CashierApi.scannedOffer(body).then((value) async {

                AppService.closeFlushbar(uuidFlushbar);

                if (value) {

                  AppService.message(MessageModel(
                      title: 'New invoice',
                      message: 'Is success created',
                      backgroundColor: Colors.green,
                      duration: null,
                      mainButton: null
                  ));

                  setState(() {

                    newScannedClient = null;
                    newScannedOffer = null;
                    newScannedOfferData = null;

                  });

                } else {

                  AppService.message(MessageModel(
                      title: 'New invoice',
                      message: 'Error',
                      backgroundColor: Colors.red,
                      duration: null,
                      mainButton: null
                  ));

                }

              });

            },
            onDoubleTap: () {

            },
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(7))
              ),
              margin: const EdgeInsets.only(
                  top: 15
              ),
              padding: const EdgeInsets.all(10),
              child: Center(
                child: Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'Confirm'),
                  style: TextStyle(
                    color: Colors.orange,
                    fontSize: 20,
                    fontWeight: FontWeight.w500
                  ),
                ),
              ),
            ),
          ),
        ],
      ),

    ));

  }

  List<Widget> _showScannedOffer() {

    if (_selectedPointOfSale == null) {
      return [Container()];
    }

    if (scannedOfferList == null) {
      return [Container()];
    }

    List<Widget> widgets = [
      Container(
        padding: const EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              FlutterI18n.translate(pageNavigatorKey.currentContext, 'Invoices') + ':',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500
              ),
            ),
            Text(
              CashierApi.filter.total.toString() ?? '0',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500
              ),
            ),
          ],
        ),
      )
    ];
    
    if (CashierApi.filter.total > 0) {
      
      widgets.addAll(scannedOfferList.map((_scannedOffer) {

        return Container(
          decoration: BoxDecoration(
              border: Border.all(width: 1, color: Colors.grey.shade300),
              borderRadius: BorderRadius.all(Radius.circular(7))
          ),
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          right: 10
                      ),
                      child: Icon(
                        FontAwesomeIcons.hashtag,
                        color: Colors.grey.shade400,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          _scannedOffer.id.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 22.5
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          right: 10
                      ),
                      child: Icon(
                        FontAwesomeIcons.user,
                        color: Colors.grey.shade400,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Cashier') + ': ' + (_scannedOffer.cashierUser.firstName ?? '') + ' ' + (_scannedOffer.cashierUser.lastName ?? ''),
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                            _scannedOffer.cashierUser.email.toString()
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          right: 10
                      ),
                      child: Icon(
                        FontAwesomeIcons.user,
                        color: Colors.grey.shade400,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Client') + ': ' + (_scannedOffer.userPurchasedOffer.user.firstName ?? '') + ' ' + (_scannedOffer.userPurchasedOffer.user.lastName ?? ''),
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                            _scannedOffer.userPurchasedOffer.user.email.toString()
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          right: 10
                      ),
                      child: Icon(
                        SaloSaleIcons.percentage,
                        color: Colors.grey.shade400,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Offer'),
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Text(
                            _scannedOffer.offer?.language?.title ?? ''
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 10
                        ),
                        child: Icon(
                          FontAwesomeIcons.calendarAlt,
                          color: Colors.grey.shade400,
                        ),
                      ),
                      Text(
                          _scannedOffer.createdAt
                      ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                          _scannedOffer.offer.price.toString()
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 10
                        ),
                        child: Icon(
                          SaloSaleIcons.ikoint_logo,
                          color: Colors.grey.shade400,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              GestureDetector(
                onTap: () {

                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) => DetailOfferPage(
                        offerId: _scannedOffer.offerId
                    ),
                  ));

                },
                onDoubleTap: () {

                },
                child: Container(
                  margin: const EdgeInsets.only(
                      top: 10
                  ),
                  decoration: BoxDecoration(
                      color: Colors.orange,
                      borderRadius: BorderRadius.all(Radius.circular(7))
                  ),
                  padding: const EdgeInsets.all(10),
                  width: double.infinity,
                  child: Center(
                    child: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Open offer'),
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );

      }).toList());
      
    } else {
      
      widgets.add(Container(
        padding: const EdgeInsets.all(10),
        child: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'No data'),
          style: TextStyle(
            fontSize: 20,
            color: Colors.grey,
            fontWeight: FontWeight.w500
          ),
        ),
      ));
      
    }
    
    return widgets;

  }

}

