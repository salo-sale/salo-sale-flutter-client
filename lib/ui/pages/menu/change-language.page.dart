import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/services/app.service.dart';

import 'package:SaloSale/ui/components/disconnect.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ChangeLanguageMenuPage extends StatefulWidget implements Widget {

  ChangeLanguageMenuPage({Key key}) : super(key: key ?? Key('Change-language'));

  @override
  State<StatefulWidget> createState() => _ChangeLanguageMenuPage();

}

class _ChangeLanguageMenuPage extends State<ChangeLanguageMenuPage> {

  List<Widget> items = [];
  bool initialized = false;

  @override
  void initState() {

    super.initState();

    if (AppService.internetIsConnected) {

      AppService.analytics.logEvent(name: 'OPEN_PAGE_CHANGE_LANGUAGE');

    }

    AppService.systemSteps.stream.listen((event) {

      if (event == SystemStepsTypeEnum.INITIALIZED) {

        if (!initialized) {
          initialized = true;

          initItems();

        }

      }

    });

  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Languages')
        ),
        centerTitle: true,
      ),
      key: _scaffoldKey,
      body: AppService.internetIsConnected ? ListView.builder(
        // Let the ListView know how many items it needs to build.
        itemCount: items.length,
        // Provide a builder function. This is where the magic happens.
        // Convert each item into a widget based on the type of item it is.
        itemBuilder: (context, index) {
          return items[index];
        },
      ) : disconnectComponent('TEXT_DISCONNECT_RECOMMENDED_COMPONENT'),
    );

  }

  initItems() {
    items = [];
    setState(() {

      AppService.languageMap.forEach((key, language) {
        items.add(_menuItem(
            onTap: () async {
              if (language.code != AppService.deviceInfoModel.languageCode) {
                await AppService.changeLanguage(language.code).then((value) {
                  initItems();
                });
              }
            },
            title: language.name,
            iconData: language.code == AppService.deviceInfoModel.languageCode ? FontAwesomeIcons.checkCircle : FontAwesomeIcons.circle,
            colorIcon: language.code == AppService.deviceInfoModel.languageCode ? Colors.orange : Colors.grey
        ));
      });

    });

  }

  Widget _menuItem({
    String title,
    String afterTitle,
    Color colorBg,
    Color colorText: Colors.black,
    Color colorIcon: const Color(0xFF787878),
    Widget icon,
    IconData iconData,
    String url,
    Function onTap
  }) {

    return Container(
      decoration: BoxDecoration(
        border: Border(top: BorderSide(width: 1, color: Colors.grey.shade300)),
      ),
      child: ListTile(
        title: Row(
          children: <Widget>[
            icon ?? Icon(
              iconData,
              size: 20,
              color: colorIcon,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15
              ),
              child: Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, title) + (afterTitle ?? ''),
                style: TextStyle(
                  fontSize: 20,
                  color: colorText
                ),
              ),
            ),
          ],
        ),
        onTap: onTap ?? () {
          pageNavigatorKey.currentState.pushNamed(url);
        },
      ),
    );

  }

}