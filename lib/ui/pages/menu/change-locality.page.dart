
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/models/api/locality.model.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';

import 'package:SaloSale/ui/components/disconnect.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ChangeLocalityMenuPage extends StatefulWidget implements Widget {

  ChangeLocalityMenuPage({Key key}) : super(key: key ?? Key('Change-locality'));

  @override
  State<StatefulWidget> createState() => _ChangeLocalityMenuPage();

}

class _ChangeLocalityMenuPage extends State<ChangeLocalityMenuPage> {

  List<Widget> items;

  @override
  void initState() {

    super.initState();
    AppService.analytics.logEvent(name: 'OPEN_PAGE_CHANGE_LANGUAGE');
    _refresh();

    items = [
      LoaderComponent(
        forSliver: false,
      )
    ];

  }


  Future _refresh() {

    return AppService.initLocalities(refresh: true).then((list) => initItems(list));

  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Localizations')
        ),
        centerTitle: true,
      ),
      key: _scaffoldKey,
      body: AppService.internetIsConnected ? RefreshIndicator(
          color: Colors.orange,
          onRefresh: _refresh,
          child: ListView.builder(
            // Let the ListView know how many items it needs to build.
            itemCount: items.length,
            // Provide a builder function. This is where the magic happens.
            // Convert each item into a widget based on the type of item it is.
            itemBuilder: (context, index) {
              return items[index];
            },
          ),
      ) : disconnectComponent('TEXT_DISCONNECT_RECOMMENDED_COMPONENT'),
    );

  }

  initItems(List<LocalityModelApi> list) {

    items = [];

    if (list != null) {

      if (list.length > 0) {

        items.addAll(list.map((locality) => _menuItem(
          onTap: () async {
            if (locality.id != AppService.deviceInfoModel.localityModelApi.id) {
              await AppService.changeLocality(locality);
              setState(() {
                initItems(list);
              });
            }
          },
          title: locality.name,
          iconData: locality.id == AppService.deviceInfoModel.localityModelApi.id ? FontAwesomeIcons.checkCircle : FontAwesomeIcons.circle,
          colorIcon: locality.id == AppService.deviceInfoModel.localityModelApi.id ? Colors.orange : Colors.grey
        )));

      } else {

        items.add(Container(
          child: Text(
              FlutterI18n.translate(pageNavigatorKey.currentContext, 'There are no localizations ​​in the list, possibly because there is no connection to the server',)
          ),
        ));

      }

    } else {

      items.add(LoaderComponent(
        forSliver: false,
      ));

    }

    setState(() {

    });

  }

  Widget _menuItem({
    String title,
    String afterTitle,
    Color colorBg,
    Color colorText: Colors.black,
    Color colorIcon: const Color(0xFF787878),
    Widget icon,
    IconData iconData,
    String url,
    Function onTap
  }) {

    return Container(
      decoration: BoxDecoration(
        border: Border(top: BorderSide(width: 1, color: Colors.grey.shade300)),
      ),
      child: ListTile(
        title: Row(
          children: <Widget>[
            icon ?? Icon(
              iconData,
              size: 20,
              color: colorIcon,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15
              ),
              child: Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, title) + (afterTitle ?? ''),
                style: TextStyle(
                  fontSize: 20,
                  color: colorText
                ),
              ),
            ),
          ],
        ),
        onTap: onTap ?? () {
          pageNavigatorKey.currentState.pushNamed(url);
        },
      ),
    );

  }

}