
import 'dart:io';

import 'package:SaloSale/shared/forms/reset-password.form.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/validators.util.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ChangePasswordPage extends StatefulWidget {

  final String code;

  ChangePasswordPage({
    Key key,
    this.code
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChangePasswordPage();

}

class _ChangePasswordPage extends State<ChangePasswordPage> {

  final _formKey = GlobalKey<FormState>();

  final ResetPasswordModel _changePasswordModel = ResetPasswordModel();

  final TextEditingController verificationCodeController = TextEditingController();

  bool _isLoading;

  @override
  void initState() {

    _isLoading = false;
    _changePasswordModel.code = widget.code; // TODO when code is not null when make reset password when null make change
    _changePasswordModel.email = AuthService.userModelApi.email;
    _changePasswordModel.showPasswordInputs = true;
    _changePasswordModel.showOldPasswordInput = _changePasswordModel.code == null;
    _changePasswordModel.showCodeInput = ! _changePasswordModel.showOldPasswordInput;

    AppService.analytics.logEvent(name: 'OPEN_CHANGE_PASSWORD');

    super.initState();

  }

  // Check if form is valid before perform login
  bool _validateAndSave() {

    final form = _formKey.currentState;

    form.save();

    if (form.validate()) {

      return true;

    }

    setState(() {

      _isLoading = false;

    });

    return false;

  }

  void _validateAndSubmit() async {

    setState(() {

      _isLoading = true;

    });

    if (_validateAndSave()) {

      String message;
      Color color = Colors.red;

      try {

        // if (_changePasswordModel.showCodeInput) {
        //   bool success = false;
        //   await _changePasswordModel.getCheckPasswordResetCode(
        //       _changePasswordModel.code).then((result) {
        //     if (result) {
        //       success = true;
        //       _changePasswordModel.showPasswordInputs = result;
        //
        //       message =
        //       "The code has been verified, you can now enter a new one";
        //       color = Colors.green;
        //       _formKey.currentState.reset();
        //     }
        //
        //     setState(() {
        //       _isLoading = false;
        //     });
        //   });
        //   if (!success) {
        //     return
        //   }
        // }

        // TODO change param of data if we have code in _changePasswordModel (value length > 0)

        await _changePasswordModel.postChangePassword({
          "oldPassword": _changePasswordModel.oldPassword,
          "password": _changePasswordModel.password
        }).then((result) {

          if (result) {

            _changePasswordModel.showPasswordInputs = result;
            AppService.analytics.logEvent(name: 'USE_CHANGE_PASSWORD');

            message = "Password changed successfully";
            color = Colors.green;
            _formKey.currentState.reset();

            Navigator.of(context).pop();

          }

          setState(() {

            _isLoading = false;

          });

        });

      } catch (e) {

        setState(() {

          _isLoading = false;

          if (Platform.isIOS) {

            message = e.details;

          } else {

            message = e.message;

          }

        });

      }


      AppService.message(MessageModel(
          title: 'Change password',
          message: message ?? 'Error',
          backgroundColor: color,
          duration: null,
          mainButton: null
      ));

    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
              FontAwesomeIcons.angleLeft,
              color: Colors.grey
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          FlutterI18n.translate(context, 'Change password'),
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 20
          ),
        ),
      ),
      body: ListView(
        children: <Widget>[
          _body(),
          _circularProgress(),
        ],
      ),
    );

  }

  Widget _body() {
    return Container(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              _codeInput(),
              _oldPasswordInput(),
              _passwordInput(),
              _passwordRepeatInput(),
              _primaryButton(),
            ],
          ),
        )
    );
  }

  ///
  /// Password input
  ///

  Widget _oldPasswordInput() {

    if (!_changePasswordModel.showOldPasswordInput) {
      return Container();
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        cursorColor: Colors.orange,
        obscureText: _changePasswordModel.oldPasswordObscure,
        initialValue: _changePasswordModel.oldPassword,
        autofocus: false,
        decoration: InputDecoration(
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  _changePasswordModel.oldPasswordObscure = !_changePasswordModel.oldPasswordObscure;
                });
              },
              child: Container(
                color: Colors.transparent,
                child: Icon(
                  _changePasswordModel.oldPasswordObscure ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                  color: Colors.grey,
                  size: 12.5,
                ),
              ),
            ),
            hintText: FlutterI18n.translate(context, 'Old password'),
            icon: Icon(
              Icons.lock,
              color: Colors.grey,
            )
        ),
        validator: (value) {
          return ValidatorsUtil.password(context, value);
        },
        onSaved: (value) => _changePasswordModel.oldPassword = value,
      ),
    );

  }

  ///
  /// Password input
  ///

  Widget _passwordInput() {

    if (!_changePasswordModel.showPasswordInputs) {
      return Container();
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        cursorColor: Colors.orange,
        obscureText: _changePasswordModel.passwordObscure,
        initialValue: _changePasswordModel.password,
        autofocus: false,
        decoration: InputDecoration(
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  _changePasswordModel.passwordObscure = !_changePasswordModel.passwordObscure;
                });
              },
              child: Container(
                color: Colors.transparent,
                child: Icon(
                  _changePasswordModel.passwordObscure ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                  color: Colors.grey,
                  size: 12.5,
                ),
              ),
            ),
            hintText: FlutterI18n.translate(context, 'New password'),
            icon: Icon(
              Icons.lock,
              color: Colors.grey,
            )
        ),
        validator: (value) {
          return ValidatorsUtil.password(context, value);
        },
        onSaved: (value) => _changePasswordModel.password = value,
      ),
    );

  }

  ///
  /// Password repeat input
  ///

  Widget _passwordRepeatInput() {

    if (!_changePasswordModel.showPasswordInputs) {
      return Container();
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: TextFormField(
        maxLines: 1,
        cursorColor: Colors.orange,
        obscureText: _changePasswordModel.passwordRepeatObscure,
        initialValue: _changePasswordModel.passwordRepeat,
        autofocus: false,
        decoration: InputDecoration(
            suffixIcon: GestureDetector(
              onTap: () {
                setState(() {
                  _changePasswordModel.passwordRepeatObscure = !_changePasswordModel.passwordRepeatObscure;
                });
              },
              child: Container(
                color: Colors.transparent,
                child: Icon(
                  _changePasswordModel.passwordRepeatObscure ? FontAwesomeIcons.eye : FontAwesomeIcons.eyeSlash,
                  color: Colors.grey,
                  size: 12.5,
                ),
              ),
            ),
            hintText: FlutterI18n.translate(context, 'New password repeat'),
            icon: Icon(
              Icons.lock,
              color: Colors.grey,
            )
        ),
        validator: (value) {
          return ValidatorsUtil.passwordRepeat(context, value, _changePasswordModel.password);
        },
        onSaved: (value) => _changePasswordModel.passwordRepeat = value,
      ),
    );

  }

  Widget _codeInput() {

    if (!_changePasswordModel.showCodeInput) {
      return Container();
    }

    return Stack(
        children: <Widget>[
          TextFormField(
            cursorColor: Colors.orange,
            controller: verificationCodeController,
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(
                    right: 50.0,
                    top: 15.0,
                    left: 15.0,
                    bottom: 15.0
                ),
                hintText: FlutterI18n.translate(context, 'Verification code'),
                icon: Icon(
                  FontAwesomeIcons.qrcode,
                  color: Colors.grey,
                )
            ),
            onSaved: (value) => _changePasswordModel.code = value,
          ),
          Positioned(
            right: 0.0,
            bottom: 0.0,
            child: IconButton(
                icon: Icon(
                  MdiIcons.qrcodeScan,
                  color: Colors.orange,
                ),
                onPressed: scan
            ),
          )
        ]
    );
  }


  Future<void> scan() async {
    BarcodeScanner.scan().then((code) {

      setState(() {

        verificationCodeController.text = code;

      });
    });

  }

  Widget _primaryButton() {

    return Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 50.0,
          child: RaisedButton(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)
            ),
            color: Colors.orange,
            child: Text(
                FlutterI18n.translate(context, "Change password"),
                style: TextStyle(
                  fontSize: 20.0,
                )
            ),
            onPressed: () {
              if (AppService.internetIsConnected) {

                _validateAndSubmit();
              } else {



                AppService.message(MessageModel(
                    title: 'Change password',
                    message: 'TEXT_DISCONNECT',
                    backgroundColor: Colors.orangeAccent,
                    duration: null,
                    mainButton: null
                ));

              }
            },
          ),
        )
    );

  }

  Widget _circularProgress() {

    if (_isLoading) {

      return Center(
          child: CircularProgressIndicator()
      );

    } else {

      return Container(
        height: 0.0,
        width: 0.0,
      );

    }

  }

}