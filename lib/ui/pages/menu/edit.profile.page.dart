
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/models/api/user.model.api.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/validators.util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class EditProfileMenuHomePage extends StatefulWidget {

  EditProfileMenuHomePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _EditProfileMenuHomePage();

}

class _EditProfileMenuHomePage extends State<EditProfileMenuHomePage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController textPhoneController = TextEditingController();
  final MaskTextInputFormatter maskFormatter = MaskTextInputFormatter(mask: '+## (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });

  final UserModelApi userModelApi = AuthService.userModelApi;

  @override
  void initState() {
    textPhoneController.text = userModelApi.phone;
    AppService.analytics.logEvent(name: 'OPEN_EDIT_PROFILE_PAGE');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          FlutterI18n.translate(context, 'Edit profile')
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _emailInput(),
                _loginInput(),
                _firstNameInput(),
                _lastNameInput(),
//                  _birthdayInput(),
                _numberPhoneInput(),
                _button()
              ],
            ),
          ),
        ),
      ),
    );

  }

  _loginInput() {

    if (AuthService.userModelApi.login != null && AuthService.userModelApi.login.length > 0) {

      return
        Padding(
          padding: const EdgeInsets.symmetric(
              vertical: 10.0
          ),
          child: Text(
            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Your login') + ': ${AuthService.userModelApi.login}',
            textAlign: TextAlign.left,
          ),
        );

    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
              vertical: 10.0
          ),
          child: Text(
            FlutterI18n.translate(pageNavigatorKey.currentContext, 'CREATE_LOGIN_COMMUNICATE'),
            textAlign: TextAlign.left,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
              vertical: 10.0
          ),
          child: TextFormField(
            cursorColor: Colors.orange,
            initialValue: userModelApi.login,
            maxLength: 25,
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.auto,
              labelText: FlutterI18n.translate(context, 'LOGIN_USER'),
            ),
            validator: (value) {
              return ValidatorsUtil.login(context, value.trim());
            },
            onSaved: (value) {
              userModelApi.login = value.trim();
            },
          ),
        ),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10
          ),
          margin: const EdgeInsets.only(
            bottom: 10
          ),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.grey.shade200,
                width: 1
              )
            )
          ),
        )
      ],
    );

  }

  _emailInput() {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
              vertical: 10.0
          ),
          child: TextFormField(
            cursorColor: Colors.orange,
            initialValue: userModelApi.email,
            decoration: InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.auto,
              labelText: FlutterI18n.translate(context, 'E-mail'),
            ),
            validator: (value) {
              return ValidatorsUtil.email(context, value.trim());
            },
            onSaved: (value) {
              userModelApi.email = value.trim();
            },
          ),
        ),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10
          ),
          margin: const EdgeInsets.only(
            bottom: 10
          ),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.grey.shade200,
                width: 1
              )
            )
          ),
        )
      ],
    );

  }

  Widget _firstNameInput() {

    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10.0
      ),
      child: TextFormField(
        cursorColor: Colors.orange,
        initialValue: userModelApi.firstName,
        decoration: InputDecoration(
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          labelText: FlutterI18n.translate(context, 'First name'),
        ),
        validator: (value) {
          return null;
        },
        onSaved: (value) {
          userModelApi.firstName = value;
        },
      ),
    );

  }

  Widget _lastNameInput() {

    return Padding(
      padding: const EdgeInsets.symmetric(
          vertical: 10.0
      ),
      child: TextFormField(
        cursorColor: Colors.orange,
        initialValue: userModelApi.lastName,
        decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.auto,
          labelText: FlutterI18n.translate(context, 'Surname')
        ),
        validator: (value) {
          return null;
        },
        onSaved: (value) {
          userModelApi.lastName = value;
        },
      ),
    );

  }

  Widget _numberPhoneInput() {

    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10.0
      ),
      child: TextFormField(
        controller: textPhoneController,
        inputFormatters: [maskFormatter],
        cursorColor: Colors.orange,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: '+38 (000) 000-00-00',
          floatingLabelBehavior: FloatingLabelBehavior.auto,
          labelText: FlutterI18n.translate(context, 'Phone number'),
        ),
        validator: (value) {
          return null;
        },
        onSaved: (value) {
          userModelApi.phone = value;
        },
      ),
    );

  }

  Widget _button() {

    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 10.0
      ),
      child: SizedBox(
        width: double.infinity,
        height: 50,
        child: RaisedButton(
          color: Colors.orange,
          padding: const EdgeInsets.all(2.5),
          onPressed: () {

            if (AppService.internetIsConnected) {

              if (_formKey.currentState.validate()) {

                _formKey.currentState.save();

                AuthService.putUserModeApi(userModelApi).then((res) {

                  if (res) {

                    Navigator.pop(context, true);

                    AppService.message(MessageModel(
                        title: 'Profile',
                        message: 'Data is saved',
                        backgroundColor: Colors.green,
                        duration: null,
                        mainButton: null
                    ));

                  } else {

                    AppService.message(MessageModel(
                        title: 'Profile',
                        message: 'Error',
                        backgroundColor: Colors.red,
                        duration: null,
                        mainButton: null
                    ));

                  }

                });

              }

            } else {

              AppService.message(MessageModel(
                  title: 'Profile',
                  message: 'TEXT_DISCONNECT',
                  backgroundColor: Colors.orangeAccent,
                  duration: null,
                  mainButton: null
              ));

            }

          },
          child: Text(
            FlutterI18n.translate(context, 'Save')
          ),
        ),
      ),
    );

  }

}