import 'dart:convert';

import 'package:SaloSale/data.dart';
import 'package:SaloSale/shared/api/app.api.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FeedbackMenuHomePage extends StatefulWidget {

  final String text;

  FeedbackMenuHomePage({Key key, this.text}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FeedbackMenuHomePage();

}

class _FeedbackMenuHomePage extends State<FeedbackMenuHomePage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final myController = TextEditingController();

  bool _sending;

  @override
  initState() {

    _sending = false;

    myController.text = widget.text;
    AppService.analytics.logEvent(name: 'OPEN_FEEDBACK_PAGE');

    super.initState();

  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          FlutterI18n.translate(context, 'Our contacts')
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Tools.launchEmail(Data.supportEmail),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0
                      ),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                              right: 10.0
                            ),
                            child: Icon(
                              FontAwesomeIcons.solidEnvelope,
                              color: Colors.orange,
                            ),
                          ),
                          Text(
                            'feedback@salo-sale.com',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => Tools.launchPhone(Data.supportPhoneNumber),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 20.0,
                        bottom: 10.0
                      ),
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                right: 10.0
                            ),
                            child: Icon(
                              FontAwesomeIcons.phone,
                              color: Colors.orange,
                            ),
                          ),
                          Text(
                            '+38 050 500 60 65',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),

            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    maxLines: 10,
                    cursorColor: Colors.orange,
                    autofocus: false,
                    controller: myController,
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.only(
                        top: 10,
                        left: 10,
                        right: 10,
                        bottom: 10
                      ),
                      hintText: FlutterI18n.translate(context, 'Please enter your suggestion') + '...',
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(
                        top: 25.0,
                      ),
                      child: SizedBox(
                        height: 50.0,
                        width: double.infinity,
                        child: RaisedButton(
                          color: Colors.orange,
                          onPressed: _sendMail,
                          elevation: 5.0,
                          child: _sending ? LoaderComponent(
                            circularColor: Colors.black,
                          ) : Text(
                            FlutterI18n.translate(context, 'Send'),
                            style: TextStyle(
                              fontSize: 20.0,
                            )
                          ),
                        ),
                      )
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

  }


  _sendMail() async {

    setState(() {
      _sending = true;
    });

    if (myController.text.length > 0) {

      var subject = Data.nameOfApp;
      var text = myController.text;
      myController.clear();

      text += '\n\n\n<br><hr><br>';

      text += "<b>Model</b>: ${AppService.deviceInfoModel.model} \n<br> <b>App Version</b>: ${AppService.deviceInfoModel.appVersion} \n<br>";
      text += "\n<br> <b>Language code</b>: ${AppService.deviceInfoModel.languageCode} \n<br>";
      text += "\n<br> <b>Locality</b>: [${AppService.deviceInfoModel.localityModelApi.id}] ${AppService.deviceInfoModel.localityModelApi.name} \n<br>";

      text += "\n<br> <b>UUID Device</b>: ${AppService.deviceInfoModel.uuid} \n<br>";
      text += "\n<br> <b>Platform</b>: ${AppService.deviceInfoModel.platform} \n<br>";
      text += "\n<br> <b>Version info</b>: ${AppService.deviceInfoModel.versionInfo} \n<br>";
      text += "\n<br> <b>HelpIs close</b>: ${AppService.deviceInfoModel.helpIsClose} \n<br>";

      if (AuthService.authModel.isUser) {

        text += "\n<br> <b>User ID</b>: ${AuthService.userModelApi.id} \n<br>";
        text += "\n<br> <b>User E-mail</b>: ${AuthService.userModelApi.email} \n<br>";

      }

      if (widget.text != null && widget.text.length > 0) {
        text += '\n\n\n <hr> <b>ORIGINAL</b> <br> ${widget.text}';
      }

      await AppApi.postFeedback({
        "subject": subject,
        "text": text
      }).then((result) {

        bool success = jsonDecode(result.body) == 1;

        setState(() {
          _sending = false;
        });

        AppService.message(MessageModel(
            title: 'Feedback',
            message: success ? 'Your message has been sent successfully!' : 'Your message has not been sent!',
            backgroundColor: success ? Colors.green : Colors.red,
            duration: null,
            mainButton: null
        ));

      });

    } else {

      setState(() {
        _sending = false;
      });

    }

  }

}