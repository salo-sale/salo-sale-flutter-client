import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/models/api/company.model.api.dart';

import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/ui/components/company-card.component.dart';
import 'package:SaloSale/ui/components/header.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/scroll-event.component.dart';
import 'package:SaloSale/ui/components/search-input.component.dart';
import 'package:SaloSale/ui/components/tags.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FollowsMenuHomePage extends StatefulWidget implements Widget {

  FollowsMenuHomePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FollowsMenuHomePage();

}

class _FollowsMenuHomePage extends State<FollowsMenuHomePage> with SingleTickerProviderStateMixin {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final ScrollController _scrollController = ScrollController();
  final FocusNode myFocusNode = FocusNode();

  final TextEditingController searchInputController = TextEditingController();

  final String filterItem = 'company-follows';
  StreamSubscription scrollToTop;

  @override
  void dispose() {

    scrollToTop?.cancel();
    _scrollController?.dispose();
    searchInputController?.dispose();
    super.dispose();
  }

  @override
  void initState() {

    CompanyApi.putIfNotExistFilterInMap(filterItem);

    CompanyApi.refreshDocumentList(filterItem, myFollow: 1, refreshTagList: true);

    scrollToTop = AppService.scrollToTop.stream.listen((success) {

      if (success && _scrollController.offset > 0) {

        _scrollController.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.easeOut);

      }

    });

    _scrollController.addListener(() {

      if (_scrollController.position.maxScrollExtent <= (_scrollController.offset + 250)) {

        CompanyApi.getDocumentList(filterItem);

      }

    });

    AppService.refreshLayout.stream.listen((refreshLayout) {

      if (refreshLayout) {

        this._refresh();

      }

    });
    AppService.analytics.logEvent(name: 'OPEN_FOLLOWS_PAGE');

    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: GestureDetector(
          child: Text(
            FlutterI18n.translate(context, 'My follows'),
          ),
          onTap: () {
            AppService.scrollToTop.sink.add(true);
          },
        ),
        centerTitle: true,
      ),
      body: RefreshIndicator(
          color: Colors.orange,
          onRefresh: _refresh,
          child: CustomScrollView(
              controller: _scrollController,
              slivers: <Widget>[
                HeaderComponent(
                  action: Container(),
                  title: Container(
                    child: Expanded(
                      child: Text(
                        FlutterI18n.translate(pageNavigatorKey.currentContext, 'These are your subscriptions to stores and services'),
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.grey
                        ),
                      ),
                    ),
                  ),
                ),
                searchInputComponent(
                  myFocusNode,
                  searchInputController,
                      () {
                    myFocusNode.unfocus();
                    if (searchInputController.text.length > 0) {
                      _refresh();
                      searchInputController.clear();
                    }
                  },
                      (term) {
                    AppService.analytics.logEvent(name: 'USE_SEARCH_COMPANY_LIST');
                    CompanyApi.refreshDocumentList(filterItem, search: term, myFollow: 1);
                  },
//                    searchTitle: FlutterI18n.translate(pageNavigatorKey.currentContext, 'Search shops and services in')
                ),
                TagsComponent(
                  tagListName: 'company',
                  onSelectedTagIdList: (list) {
                    CompanyApi.filterMap[filterItem].tagIdList = list;
                    CompanyApi.refreshDocumentList(filterItem, myFollow: 1);
                  },
                ),
                /// Тунель для завантаження пропозиції
                StreamBuilder<bool>(
                    stream: AppService.refreshContent.stream,
                    builder: (context, snapshot) {

                      if (snapshot.hasData && snapshot.data) {
                        Timer(Duration(seconds: 1), () {
                          _refresh();
                        });

                        return SliverToBoxAdapter(
                          child: Container(
                              height: 150.0,
                              child: LoaderComponent(
                                withTimer: false,
                              )
                          ),
                        );
                      } else {
                        return StreamBuilder<List<CompanyModelApi>>(
                            stream: CompanyApi.filterMap[filterItem].documentListChanel.stream,
                            builder: (BuildContext context, AsyncSnapshot<List<CompanyModelApi>> snapshot) {

                              if (snapshot.hasData) {
                                if (snapshot.data.length > 0) {
                                  return SliverList(
                                    delegate: SliverChildBuilderDelegate(
                                          (BuildContext context,
                                          int index) {
                                        CompanyModelApi companyModelApi = snapshot.data[index];

                                        return CompanyCardComponent(companyModelApi: companyModelApi,);
                                      },
                                      childCount: snapshot.data.length,
                                    ),
                                  );

                                } else {
                                  return SliverToBoxAdapter(
                                    child: Container(
                                      height: 100,
                                    ),
                                  );
                                }
                              } else {

                                return SliverToBoxAdapter(
                                  child: Container(
                                      height: 150.0,
                                      child: LoaderComponent(
                                        withTimer: false,
                                      )
                                  ),
                                );
                              }
                            }
                        );
                      }
                    }
                ),
                ScrollEventComponent(
                  stream: CompanyApi.filterMap[filterItem].scrollEvent.stream,
                  iconEmpty: FontAwesomeIcons.bell,
                  textEmpty: 'EMPTY_FOLLOWS_LIST',
                ),
              ]
          )
      ),
    );

  }

  Future<void> _refresh() {

    return CompanyApi.refreshDocumentList(filterItem, myFollow: 1);

  }


}