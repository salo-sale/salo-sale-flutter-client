
import 'package:SaloSale/routing.dart';

import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/ui/components/build-barcode.component.dart';
import 'package:barcode/barcode.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:share/share.dart';

class FriendsMenuHomePage extends StatefulWidget {

  FriendsMenuHomePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FriendsMenuHomePage();

}

class _FriendsMenuHomePage extends State<FriendsMenuHomePage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final TextEditingController invitationCodeController = TextEditingController();

  @override
  void initState() {
    AppService.analytics.logEvent(name: 'OPEN_INVITATION_PAGE');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          FlutterI18n.translate(context, 'Invitations')
        ),
        centerTitle: true,
      ),
      body: RefreshIndicator(
          color: Colors.orange,
          onRefresh: _refresh,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: _list(),
          )
      ),
    );

  }


  Future<void> _refresh() {
    return Future.value();
  }

  _checkEmail() {
    
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(7)),
        color: Colors.orange,
      ),
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(10),
      child: Text(
        FlutterI18n.translate(pageNavigatorKey.currentContext, 'You have not confirmed the e-mail, please confirm and then you can invite friends'),
        style: TextStyle(
          color: Colors.white,
          fontSize: 20
        ),
      ),
    );

  }

  _list() {

    List<Widget> children = [];

    if (AuthService.userModelApi.invitationCodes == null || AuthService.userModelApi.invitationCodes.length == 0) {

      children.add(Container(
        child: Column(
          children: <Widget>[
            Center(
              child: Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'NOT_HAVE_INVITATION_CODE'),
                textAlign: TextAlign.center,
                style: TextStyle(

                ),
              ),
            ),
            RaisedButton(
              color: Colors.orange,
              padding: const EdgeInsets.all(2.5),
              onPressed: () {
                pageNavigatorKey.currentState.pushNamed('feedback');
              },
              child: Container(
                child: Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'Write')
                ),
              ),
            )
          ],
        ),
      ));

    } else {

      if (AuthService.userModelApi.emailIsConfirmed) {

        if (AuthService.userModelApi.invitationUserId == null) {

          children.add(Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(
              bottom: 10
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(7)),
              boxShadow: [
                BoxShadow(
                  color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
                  blurRadius: 10.0,
                  // has the effect of softening the shadow
                  spreadRadius: 0.0,
                  // has the effect of extending the shadow
                  offset: Offset(
                    0.0, // horizontal, move right 10
                    0.0, // vertical, move down 10
                  ),
                )
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    bottom: 10
                  ),
                  child: Text(
                    FlutterI18n.translate(pageNavigatorKey.currentContext, 'MESSAGE_WITHOUT_INVITATION_USER_ID'),
                    textAlign: TextAlign.left,
                    softWrap: true,
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                ),
                Stack(
                    children: <Widget>[
                      TextFormField(
                        cursorColor: Colors.orange,
                        controller: invitationCodeController,
                        decoration: InputDecoration(
                          contentPadding: const EdgeInsets.only(
                              right: 50.0,
                              top: 15.0,
                              left: 15.0,
                              bottom: 15.0
                          ),
                          hintText: FlutterI18n.translate(context, 'Invitation code'),
                        ),
                      ),
                      Positioned(
                        right: 0.0,
                        bottom: 0.0,
                        child: IconButton(
                            icon: Icon(
                              MdiIcons.qrcodeScan,
                              color: Colors.orange,
                            ),
                            onPressed: scan
                        ),
                      )
                    ]
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 10,
                    ),
                    child: FlatButton(
                      child: Text(
                        FlutterI18n.translate(pageNavigatorKey.currentContext, 'Save'),
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 20
                        ),
                      ),
                      padding: const EdgeInsets.symmetric(
                          vertical: 10
                      ),
                      onPressed: () async {
                        AuthService.postInvitation(invitationCodeController.text).then((value) {
                          if (value) {
                            setState(() {

                            });
                          }
                        });
                      },
                    ),
                  ),
                )
              ],
            ),
          ));

        }

        children.addAll([
          GestureDetector(
            onTap: () {

              AppService.showOnTransparent(
                  Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            FlutterI18n.translate(context, 'Show this Qr-code to a friend'),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black
                            ),
                          ),
                        ),
                        SvgPicture.string(
                          buildBarcode(
                            Barcode.qrCode(),
                            AuthService.userModelApi.invitationCodes[0],
                            height: 200,
                          )
                        ),
                      ]
                  )
              );

            },
            child: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(7)),
                boxShadow: [
                  BoxShadow(
                    color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
                    blurRadius: 10.0,
                    // has the effect of softening the shadow
                    spreadRadius: 0.0,
                    // has the effect of extending the shadow
                    offset: Offset(
                      0.0, // horizontal, move right 10
                      0.0, // vertical, move down 10
                    ),
                  )
                ],
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 10
                          ),
                          child: Text(
                            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Is your friend close?'),
                            textAlign: TextAlign.left,
                            softWrap: true,
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                        Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Click and show your qr code'),
                          softWrap: true,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 22.5,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Icon(
                          MdiIcons.qrcodeScan,
                          size: 75,
                          color: Colors.orange,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 10
                          ),
                          child: Text(
                              FlutterI18n.translate(pageNavigatorKey.currentContext, 'Show QR code')
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
                top: 10
            ),
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(7)),
              boxShadow: [
                BoxShadow(
                  color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
                  blurRadius: 10.0,
                  // has the effect of softening the shadow
                  spreadRadius: 0.0,
                  // has the effect of extending the shadow
                  offset: Offset(
                    0.0, // horizontal, move right 10
                    0.0, // vertical, move down 10
                  ),
                )
              ],
            ),
            child: Column(
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(
                        bottom: 10
                    ),
                    padding: const EdgeInsets.only(
                        bottom: 10
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                width: 1,
                                color: Colors.grey.shade300
                            )
                        )
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 10
                          ),
                          child: Text(
                            FlutterI18n.translate(pageNavigatorKey.currentContext, 'Is your friend far away?'),
                            textAlign: TextAlign.left,
                            softWrap: true,
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                        Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Copy the code and send it to a friend'),
                          softWrap: true,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                    width: double.infinity
                ),
                Container(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          padding: const EdgeInsets.only(
                              bottom: 10
                          ),
                          color: Colors.transparent,
                          child: Text(
                            AuthService.userModelApi.invitationCodes != null && AuthService.userModelApi.invitationCodes.length > 0 ? AuthService.userModelApi.invitationCodes[0] : '',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                              color: Colors.orange
                            ),
                          ),
                        ),
                        onDoubleTap: () {

                        },
                        onTap: () {

                          Clipboard.setData(
                            ClipboardData(
                              text: AuthService.userModelApi.invitationCodes[0]
                            )
                          );

                          _scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text(
                                FlutterI18n.translate(pageNavigatorKey.currentContext, 'Copied to Clipboard')
                              ),
                            )
                          );

                        },
                      ),
                      IconButton(
                        icon: Icon(
                          FontAwesomeIcons.paperPlane,
                          size: 25,
                          color: Colors.orange,
                        ),
                        onPressed: () {
                          Share.share(
                            FlutterI18n.translate(pageNavigatorKey.currentContext, 'TITLE_OF_INVITATION_MESSAGE') + '. ' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'BODY_OF_INVITATION_MESSAGE') + ': ' + AuthService.userModelApi.invitationCodes[0] + '\n\nhttps://salo-sale.com/',
//                    subject: FlutterI18n.translate(pageNavigatorKey.currentContext, 'BODY_OF_INVITATION_MESSAGE') + ': ' + AuthService.userModelApi.invitationCodes[0] + '\n https://salo-sale.com/',
                          );
                        },
                      ),
                    ],
                  ),
                )
              ],
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
                top: 10
            ),
            decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(
                        width: 1,
                        color: Colors.grey.shade300
                    )
                )
            ),
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                FlutterI18n.translate(context, 'Get free ikoints by inviting your friends'),
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 20
                ),
              ),
            ),
          )]);

      } else {
        children.add(_checkEmail());
      }

    }

    return ListView(
      children: children,
    );

  }



  Future<void> scan() async {
    BarcodeScanner.scan().then((code) {

      setState(() {

        invitationCodeController.text = code;

      });
    });

  }

}