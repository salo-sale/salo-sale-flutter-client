
import 'dart:async';

import 'package:SaloSale/salo_sale_icons_icons.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HelpMenuHomePage extends StatefulWidget {

  HelpMenuHomePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HelpMenuHomePage();

}

class _HelpMenuHomePage extends State<HelpMenuHomePage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final PageController pageController = PageController(initialPage: 0);

  @override
  void initState() {
    AppService.analytics.logEvent(name: 'OPEN_HELP_PAGE');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          _body(),
          StreamBuilder<bool>(
              stream: AppService.refreshLayout.stream,
              builder: (context, snapshot) {

                if (snapshot.hasData && snapshot.data) {

                  Timer(Duration(milliseconds: 500), () {
                    setState(() {

                    });
                    AppService.refreshLayout.sink.add(false);
                  });

                  return Positioned(
                      left: 0,
                      top: 0,
                      right: 0,
                      bottom: 0,
                      child: Container(
                          color: Color.fromRGBO(0, 0, 0, .5),
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          child: Center(
                              child: LoaderComponent(
                                withTimer: false,
                              )
                          )
                      )
                  );

                }

                return Container();

              }

          ),
//          Positioned(
//            right: 25,
//            top: 25,
//            child: ,
//          )
        ],
      ),
    );

  }

  Widget _body() {

    return PageView(
        controller: pageController,
        children: <Widget>[

          Container(
            color: Colors.blueGrey,
            padding: const EdgeInsets.all(25),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Row(
                  children: <Widget>[
                    Icon(
                      SaloSaleIcons.percentage,
                      color: Colors.white,
                      size: 75,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 20
                        ),
                        child: Text(
                          FlutterI18n.translate(context, 'Offer'),
                          softWrap: true,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 30
                          ),
                        ),
                      ),
                    )
                  ],
                ),
//                Padding(
//                  padding: const EdgeInsets.only(
//                    top: 25,
//                  ),
//                  child: Text(
//                    FlutterI18n.translate(context, 'HELP_PAGE_PAGE_1_SECTION_1'),
//                    style: TextStyle(
//                        color: Colors.white,
//                        fontWeight: FontWeight.w500,
//                        fontSize: 20
//                    ),
//                  ),
//                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'HELP_PAGE_PAGE_1_SECTION_1_1'),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18
                    ),
                  ),
                ),
//                Padding(
//                  padding: const EdgeInsets.only(
//                    top: 25,
//                  ),
//                  child: Text(
//                    FlutterI18n.translate(context, 'HELP_PAGE_PAGE_1_SECTION_2'),
//                    style: TextStyle(
//                        color: Colors.white,
//                        fontWeight: FontWeight.w500,
//                        fontSize: 20
//                    ),
//                  ),
//                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'HELP_PAGE_PAGE_1_SECTION_2_1'),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 50
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () async {
                          await AppService.closeHelpPage();
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 0
                          ),
                          color: Colors.transparent,
                          child: Text(
                            FlutterI18n.translate(context, 'I already know everything'),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          pageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.ease);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 15
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xFF888888),
                                blurRadius: 5.0,
                                // has the effect of softening the shadow
                                spreadRadius: 0.0,
                                // has the effect of extending the shadow
                                offset: Offset(
                                  0.0, // horizontal, move right 10
                                  0.0, // vertical, move down 10
                                ),
                              )
                            ],
                            color: Colors.white,
                          ),
                          child: Text(
                            FlutterI18n.translate(context, 'Next'),
                            style: TextStyle(
                                color: Colors.orange,
                                fontSize: 20,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            color: Colors.brown,
            padding: const EdgeInsets.all(25),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Row(
                  children: <Widget>[
                    Icon(
                      FontAwesomeIcons.globe,
                      color: Colors.white,
                      size: 75,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 15
                      ),
                      child: Text(
                        AppService.languageMap[AppService.deviceInfoModel.languageCode].name,
                        style: TextStyle(
                          fontSize: 35,
                          color: Colors.white,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 50,
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'HELP_PAGE_PAGE_2_SECTION_1'),
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 25
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'HELP_PAGE_PAGE_2_SECTION_1_1'),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'Or here'),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: GestureDetector(
                    onTap: () => Navigator.pushNamed(context, 'change-language'),
                    child: Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.only(
                          top: 10,
                          bottom: 10
                      ),
                      child: Text(
                        FlutterI18n.translate(context, 'Change the language'),
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 20
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 50
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          pageController.previousPage(duration: Duration(milliseconds: 500), curve: Curves.ease);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 0
                          ),
                          color: Colors.transparent,
                          child: Text(
                            FlutterI18n.translate(context, 'Back'),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          pageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.ease);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 15
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xFF888888),
                                blurRadius: 5.0,
                                // has the effect of softening the shadow
                                spreadRadius: 0.0,
                                // has the effect of extending the shadow
                                offset: Offset(
                                  0.0, // horizontal, move right 10
                                  0.0, // vertical, move down 10
                                ),
                              )
                            ],
                            color: Colors.white,
                          ),
                          child: Text(
                            FlutterI18n.translate(context, 'Next'),
                            style: TextStyle(
                                color: Colors.orange,
                                fontSize: 20,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),

          Container(
            color: Colors.brown,
            padding: const EdgeInsets.all(25),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Row(
                  children: <Widget>[
                    Icon(
                      FontAwesomeIcons.mapMarkerAlt,
                      color: Colors.white,
                      size: 75,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 15
                      ),
                      child: Text(
                        AppService.deviceInfoModel?.localityModelApi?.name ?? '',
                        style: TextStyle(
                            fontSize: 35,
                            color: Colors.white,
                            fontWeight: FontWeight.w500
                        ),
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'Wrong localization?'),
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 25
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'HELP_PAGE_PAGE_3_SECTION_1_1'),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: GestureDetector(
                    onTap: () => Navigator.pushNamed(context, 'change-locality'),
                    child: Container(
                      color: Colors.transparent,
                      padding: const EdgeInsets.only(
                          top: 10,
                          bottom: 10
                      ),
                      child: Text(
                        FlutterI18n.translate(context, 'Change location'),
                        style: TextStyle(
                            color: Colors.orange,
                            fontSize: 20
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 50
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          pageController.previousPage(duration: Duration(milliseconds: 500), curve: Curves.ease);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 0
                          ),
                          color: Colors.transparent,
                          child: Text(
                            FlutterI18n.translate(context, 'Back'),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          pageController.nextPage(duration: Duration(milliseconds: 500), curve: Curves.ease);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 15
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xFF888888),
                                blurRadius: 5.0,
                                // has the effect of softening the shadow
                                spreadRadius: 0.0,
                                // has the effect of extending the shadow
                                offset: Offset(
                                  0.0, // horizontal, move right 10
                                  0.0, // vertical, move down 10
                                ),
                              )
                            ],
                            color: Colors.white,
                          ),
                          child: Text(
                            FlutterI18n.translate(context, 'Next'),
                            style: TextStyle(
                                color: Colors.orange,
                                fontSize: 20,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),

          Container(
            color: Colors.blueGrey,
            padding: const EdgeInsets.all(25),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(
                      SaloSaleIcons.ikoint_logo,
                      color: Colors.white,
                      size: 75,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 25,
                        ),
                        child: Text(
                          FlutterI18n.translate(context, 'Ikoint'),
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 25
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'HELP_PAGE_PAGE_4_SECTION_1_1'),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: Text(
                    FlutterI18n.translate(context, 'HELP_PAGE_PAGE_4_SECTION_1_2'),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(
                      top: 50
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          pageController.previousPage(duration: Duration(milliseconds: 500), curve: Curves.ease);
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 0
                          ),
                          color: Colors.transparent,
                          child: Text(
                            FlutterI18n.translate(context, 'Back'),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () async {
                          await AppService.closeHelpPage();
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 15
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(7)),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xFF888888),
                                blurRadius: 5.0,
                                // has the effect of softening the shadow
                                spreadRadius: 0.0,
                                // has the effect of extending the shadow
                                offset: Offset(
                                  0.0, // horizontal, move right 10
                                  0.0, // vertical, move down 10
                                ),
                              )
                            ],
                            color: Colors.white,
                          ),
                          child: Text(
                            FlutterI18n.translate(context, 'Thanks'),
                            style: TextStyle(
                                color: Colors.orange,
                                fontSize: 20,
                                fontWeight: FontWeight.w500
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      );

  }

}