
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/loyalty.api.dart';
import 'package:SaloSale/shared/models/api/loyalty-card.model.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/ui/components/build-barcode.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/pages/menu/loyalty-card/form.loyalty-card.page.dart';
import 'package:barcode/barcode.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoyaltyCardDetailPage extends StatefulWidget {

  final LoyaltyCardModelApi card;

  LoyaltyCardDetailPage({
    Key key, 
    this.card
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoyaltyCardDetailPage();

}

class _LoyaltyCardDetailPage extends State<LoyaltyCardDetailPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final TextEditingController invitationCodeController = TextEditingController();

  LoyaltyCardModelApi card;
  bool makeRefreshOnPop = false;

  @override
  void initState() {
    AppService.checkInternetConnect(refresh: true);
    AppService.analytics.logEvent(name: 'OPEN_DISCOUNT_CARD_DETAIL_PAGE');
    card = widget.card;
    super.initState();
  }

  @override
  Widget build(BuildContext pageContext) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context, makeRefreshOnPop);
          },
          icon: Icon(
            Icons.arrow_back_ios
          ),
        ),
        title: Text(
          card.name
        ),
        centerTitle: true,
      ),
      body: RefreshIndicator(
          color: Colors.orange,
          onRefresh: _refresh,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView(
                children: [
                Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'Show the barcode to the person with the scanner'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    bottom: 10,
                    top: 10,
                  ),
                  width: double.infinity,
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(7)),
                    boxShadow: [
                      BoxShadow(
                        color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
                        blurRadius: 10.0,
                        // has the effect of softening the shadow
                        spreadRadius: 0.0,
                        // has the effect of extending the shadow
                        offset: Offset(
                          0.0, // horizontal, move right 10
                          0.0, // vertical, move down 10
                        ),
                      )
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        FlutterI18n.translate(pageNavigatorKey.currentContext, 'Barcode'),
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                      card.value != null && card.value.length > 0 && Barcode.code128().isValid(card.value) ? SvgPicture.string(
                        buildBarcode(
                          Barcode.code128(),
                          card.value,
                          height: 200,
                          width: 500
                        )
                      ) : Container(),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    bottom: 10,
                  ),
                  width: double.infinity,
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(7)),
                    boxShadow: [
                      BoxShadow(
                        color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
                        blurRadius: 10.0,
                        // has the effect of softening the shadow
                        spreadRadius: 0.0,
                        // has the effect of extending the shadow
                        offset: Offset(
                          0.0, // horizontal, move right 10
                          0.0, // vertical, move down 10
                        ),
                      )
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        FlutterI18n.translate(pageNavigatorKey.currentContext, 'Comment'),
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                      Text(
                        card.comment ?? ''
                      )
                    ],
                  ),
                ),
                _editCard(),
                _deleteCard(pageContext)
              ],
    ),
          )
      ),
    );

  }


  Future<void> _refresh() {
    setState(() {
      
    });
    return Future.value();
  }

  _editCard() {

    return RaisedButton(
      child: Text(
        FlutterI18n.translate(pageNavigatorKey.currentContext, 'Edit'),
        style: TextStyle(
          color: Colors.white,
          fontSize: 20
        ),
      ),
      padding: const EdgeInsets.symmetric(
        vertical: 10
      ),
      color: Colors.orange,
      onPressed: () async {
        card = await Navigator.push(
          context,
          MaterialPageRoute<LoyaltyCardModelApi>(
            builder: (context) => LoyaltyCardFormPage(
              card: card,
              isEdit: true,
            ),
          ),
        ) ?? card;
        makeRefreshOnPop = true;
        _refresh();
        
      },
    );

  }

  _deleteCard(pageContext) {

    return Padding(
      padding: const EdgeInsets.only(
        top: 10,
      ),
      child: RaisedButton(
        child: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Delete'),
          style: TextStyle(
            color: Colors.white,
            fontSize: 20
          ),
        ),
        padding: const EdgeInsets.symmetric(
          vertical: 10
        ),
        color: Colors.red,
        onPressed: AppService.internetIsConnected ? () async {

          showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) {
                return AlertDialog(
                  title: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'Delete')
                  ),
                  content: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, 'You really want to delete the card?')
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(
                        FlutterI18n.translate(pageNavigatorKey.currentContext, 'Cancel'),
                        style: TextStyle(
                            color: Colors.black
                        ),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    FlatButton(
                      onPressed: () async {
                        
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (context) {
                                return AlertDialog(
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      LoaderComponent(
                                        withTimer: false,
                                      ),
                                    ],
                                  ),
                                );
                              }
                          );
                          await LoyaltyApi.deleteDocument(card.id).then((value) {

                            if (value) {

                              Navigator.pop(context);
                              Navigator.pop(context);

                            }

                          });
                          Navigator.pop(pageContext, true);
                      },
                      child: Text(
                        FlutterI18n.translate(pageNavigatorKey.currentContext, 'Yes'),
                        style: TextStyle(
                            color: Colors.red
                        ),
                      ),
                    ),
                  ],
                );
              }
          );
        } : null,
      ),
    );

  }

}