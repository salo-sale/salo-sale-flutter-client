
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/loyalty.api.dart';
import 'package:SaloSale/shared/models/api/loyalty-card.model.api.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/ui/components/build-barcode.component.dart';
import 'package:barcode/barcode.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class LoyaltyCardFormPage extends StatefulWidget {

  final LoyaltyCardModelApi card;
  final bool isEdit;

  LoyaltyCardFormPage({
    Key key, 
    this.card,
    this.isEdit = false
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoyaltyCardFormPage();

}

class _LoyaltyCardFormPage extends State<LoyaltyCardFormPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final TextEditingController invitationCodeController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  LoyaltyCardModelApi _newCard;

  String barcode = '000000000000';

  @override
  void initState() {
    AppService.checkInternetConnect(refresh: true);
    if (widget.card != null) {
      _newCard = widget.card;
      invitationCodeController.text = widget.card.value;
    } else {
      _newCard = LoyaltyCardModelApi(
        id: DateTime.now().millisecondsSinceEpoch,
        status: 2,
        userId: AuthService.userModelApi.id,
        createdAt: DateTime.now().toIso8601String(),
        updatedAt: DateTime.now().toIso8601String()
      );
    }
    AppService.analytics.logEvent(name: 'OPEN_DISCOUNT_CARD_FOR_PAGE');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
            FlutterI18n.translate(context, widget.card == null ? 'Adding a loyalty card' : 'Editing a loyalty card')
        ),
        centerTitle: true,
      ),
      body: RefreshIndicator(
          color: Colors.orange,
          onRefresh: _refresh,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: ListView(
              children: [
                Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      TextFormField(
                        maxLines: 1,
                        maxLength: 45,
                        cursorColor: Colors.orange,
                        initialValue: _newCard.name,
                        autofocus: false,
                        decoration: InputDecoration(
                          labelText: FlutterI18n.translate(context, 'Name'),
                        ),
                        validator: (value) {
                          return value != null && value.length > 0 ? null : FlutterI18n.translate(pageNavigatorKey.currentContext, 'Cannot be empty');
                        },
                        onSaved: (value) => _newCard.name = value.trim(),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 10,
                            bottom: 10
                        ),
                        child: TextFormField(
                          maxLines: null,
                          maxLength: 90,
                          cursorColor: Colors.orange,
                          initialValue: _newCard.comment,
                          autofocus: false,
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.all(10),
                            labelText: FlutterI18n.translate(context, 'Comment')
                          ),
                          onSaved: (value) => _newCard.comment = value.trim(),
                        ),
                      ),
                      invitationCodeController.text != null && invitationCodeController.text.length > 0 ? Container(
                        margin: const EdgeInsets.only(
                          bottom: 10,
                          top: 10,
                        ),
                        width: double.infinity,
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(7)),
                          boxShadow: [
                            BoxShadow(
                              color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
                              blurRadius: 10.0,
                              // has the effect of softening the shadow
                              spreadRadius: 0.0,
                              // has the effect of extending the shadow
                              offset: Offset(
                                0.0, // horizontal, move right 10
                                0.0, // vertical, move down 10
                              ),
                            )
                          ],
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              FlutterI18n.translate(pageNavigatorKey.currentContext, 'Barcode'),
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500
                              ),
                            ),
                            SvgPicture.string(
                                buildBarcode(
                                    Barcode.code128(),
                                    invitationCodeController.text,
                                    height: 200,
                                    width: 500
                                )
                            ),
                          ],
                        ),
                      ) : Container(),
                      RaisedButton(
                          color: Colors.blueAccent,
                          padding: const EdgeInsets.symmetric(
                              vertical: 10
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                FlutterI18n.translate(pageNavigatorKey.currentContext, 'Scan'),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 10
                                ),
                                child: Icon(
                                  MdiIcons.qrcodeScan,
                                  color: Colors.white,
                                ),
                              )
                            ],
                          ),
                          onPressed: () async {

                            bool goodScan = false;

                            try {
                              barcode = await BarcodeScanner.scan();
                              goodScan = true;
                            } on PlatformException catch (e) {
                              if (e.code == BarcodeScanner.CameraAccessDenied) {
                                AppService.message(MessageModel(
                                    title: 'Camera',
                                    message: 'No camera permission!',
                                    backgroundColor: Colors.red,
                                    duration: null,
                                    mainButton: null
                                ));
                              } else {
                                AppService.message(MessageModel(
                                    title: 'Error',
                                    message: 'Unknown error: $e',
                                    backgroundColor: Colors.red,
                                    duration: null,
                                    mainButton: null
                                ));
                              }
                            } on FormatException {
                              goodScan = true;
                            } catch (e) {
                              AppService.message(MessageModel(
                                  title: 'Error',
                                  message: 'Unknown error: $e',
                                  backgroundColor: Colors.red,
                                  duration: null,
                                  mainButton: null
                              ));
                            }

                            if (goodScan) {

                              AppService.message(MessageModel(
                                  title: 'Scanning',
                                  message: 'Successful scan',
                                  backgroundColor: Colors.green,
                                  duration: null,
                                  mainButton: null
                              ));

                            }

                            setState(() {

                              invitationCodeController.text = barcode;

                            });

                          }
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                  ),
                  child: RaisedButton(
                    color: Colors.orange,
                    child: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext, widget.isEdit ? 'Save' : 'Add'),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20
                      ),
                    ),
                    padding: const EdgeInsets.symmetric(
                        vertical: 10
                    ),
                    onPressed: () async {

                      bool success = false;

                      if (_formKey.currentState.validate()) {

                        _formKey.currentState.save();
                        _newCard.value = invitationCodeController.value.text;

                        _newCard.isNewModel = !widget.isEdit;

                        await LoyaltyApi.saveDocument(_newCard).then((value) {

                          if (value != null) {

                            setState(() {

                              success = true;

                              invitationCodeController.clear();
                              Navigator.pop(context, _newCard);

                              AppService.message(MessageModel(
                                  title: 'Loyalty card',
                                  message: 'Data is saved',
                                  backgroundColor: Colors.green,
                                  mainButton: null
                              ));

                            });

                          }

                        });

                      }

                      if (!success) {

                        AppService.message(MessageModel(
                            title: 'Loyalty card',
                            message: 'Error',
                            backgroundColor: Colors.red,
                            mainButton: null
                        ));

                      }

                    },
                  ),
                )
              ],
            ),
          )
      ),
    );

  }


  Future<void> _refresh() {
    setState(() {
      
    });
    return Future.value();
  }


}