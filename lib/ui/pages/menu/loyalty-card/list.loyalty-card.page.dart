
import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/loyalty.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/models/api/loyalty-card.model.api.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/search-input.component.dart';
import 'package:SaloSale/ui/pages/menu/loyalty-card/detail.loyalty-card.page.dart';
import 'package:SaloSale/ui/pages/menu/loyalty-card/form.loyalty-card.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class LoyaltyCardListPage extends StatefulWidget {

  LoyaltyCardListPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoyaltyCardListPage();

}

class _LoyaltyCardListPage extends State<LoyaltyCardListPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final ScrollController _scrollController = ScrollController();
  final TextEditingController searchInputController = TextEditingController();
  final TextEditingController invitationCodeController = TextEditingController();

  final FocusNode myFocusNode = FocusNode();

  bool initialized = false;

  StreamSubscription scrollToTop;
  StreamSubscription refreshLayout;

  @override
  void dispose() {

    scrollToTop?.cancel();
    refreshLayout?.cancel();
    _scrollController?.dispose();
    searchInputController?.dispose();
    super.dispose();
  }

  @override
  void initState() {

    AppService.systemSteps.stream.listen((event) {

      if (event == SystemStepsTypeEnum.INITIALIZED) {

        if (!initialized) {

          initialized = true;

          LoyaltyApi.refreshDocumentList();

          _scrollController.addListener(() {

            if (_scrollController.position.maxScrollExtent <= (_scrollController.offset + 250)) {

              LoyaltyApi.getDocumentList();

            }

          });

          scrollToTop = AppService.scrollToTop.stream.listen((success) {

            if (success && _scrollController.offset > 0) {

              _scrollController.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.easeOut);

            }

          });

          refreshLayout = AppService.refreshLayout.stream.listen((refreshContent) {

            if (refreshContent) {

              LoyaltyApi.refreshDocumentList();

            }

          });

        }

      }

    });

    AppService.analytics.logEvent(name: 'OPEN_DISCOUNT_CARDS_PAGE');

    super.initState();
  }

  Future<void> _refresh({bool checkNotSavedData: true}) {
    return LoyaltyApi.refreshDocumentList(
        search: searchInputController.text,
        checkNotSavedData: checkNotSavedData
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: GestureDetector(
          child: Text(
            FlutterI18n.translate(context, 'Loyalty cards')
          ),
          onTap: () {
            AppService.scrollToTop.sink.add(true);
          },
        ),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          RefreshIndicator(
              color: Colors.orange,
              onRefresh: _refresh,
              child: StreamBuilder(
                stream: LoyaltyApi.filter.documentListChanel.stream,
                builder: (context, AsyncSnapshot<List<LoyaltyCardModelApi>> snapshot) {
                  if (snapshot.hasData) {
                    return _list(snapshot.data);
                  }
                  return LoaderComponent();
                },
              )
          ),
          _primaryButton(),
        ],
      ),
    );

  }

  Widget _primaryButton() {

    return Positioned.fill(
      child: Align(
        alignment: Alignment.bottomCenter,
        child: GestureDetector(
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.symmetric(
              vertical: 15,
              horizontal: 10
            ),
            decoration: BoxDecoration(
              color: Colors.orange,
            ),
            child: Text(
              FlutterI18n.translate(context, 'Add new card'),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 20.0,
                color: Colors.white
              )
            ),
          ),
          onDoubleTap: () {

          },
          onTap: () async {
            LoyaltyCardModelApi result = await Navigator.push(
              context,
              MaterialPageRoute<LoyaltyCardModelApi>(
                builder: (context) => LoyaltyCardFormPage(),
              ),
            );
            if (result != null) {
              _openCard(result, setRefresh: true);
            }
          },
        ),
      ),
    );

  }

  Widget _list(List<LoyaltyCardModelApi> cards) {

    List<Widget> children = [
      Padding(padding: const EdgeInsets.only(top: 10)),
    ];


    if (cards != null && cards.length > 0) {

      List<LoyaltyCardModelApi> listNotSaveLoyaltyCard = cards.where((element) => element.isNotSaved).toList();

      if (listNotSaveLoyaltyCard.length > 0) {

        if (AppService.internetIsConnected) {

          children.insert(0, Container(
            padding: const EdgeInsets.all(10),
            height: 100,
            decoration: BoxDecoration(
                color: Colors.orangeAccent
            ),
            child: Column(
              children: [
                Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'NOT_SAVED_LOYALTY_CARD_COMMUNICATE'),
                  style: TextStyle(
                      color: Colors.brown,
                      fontSize: 20,
                      fontWeight: FontWeight.w500
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    FlatButton(
                      onPressed: () {
                        _refresh(
                            checkNotSavedData: false
                        );
                      },
                      child: Text(
                        FlutterI18n.translate(pageNavigatorKey.currentContext, 'No'),
                      ),
                    ),
                    FlatButton(
                      onPressed: () async {
                        bool success = true;
                        AppService.showLoaderWindow();
                        for (LoyaltyCardModelApi loyaltyCard in listNotSaveLoyaltyCard) {

                          await LoyaltyApi.saveDocument(loyaltyCard).then((document) {
                            if (document == null) {
                              success = false;
                            }
                          });

                        }
                        AppService.closeLoaderWindow();

                        AppService.message(MessageModel(
                            title: 'Loyalty card',
                            message: success ? 'Data is saved' : 'Sorry, there was an error',
                            backgroundColor: success ? Colors.green : Colors.red,
                            mainButton: null
                        ));

                        _refresh();

                      },
                      child: Text(
                        FlutterI18n.translate(pageNavigatorKey.currentContext, 'Yes'),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ));

        } else {

          children.insert(0, Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: Colors.amber
            ),
            child: Column(
              children: [
                Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext, 'Ви маєте не збережені дані, підключіться до сервера щоб зберігти їх'),
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.w500
                  ),
                ),
              ],
            ),
          ));

        }

      }

      children.add(
          Padding(
            padding: const EdgeInsets.only(
                left: 10,
                right: 10
            ),
            child: searchInputComponent(
                myFocusNode,
                searchInputController,
                    () {
                  searchInputController.clear();
                  myFocusNode.unfocus();
                  LoyaltyApi.refreshDocumentList();
                },
                    (value) {
                  AppService.analytics.logEvent(name: 'USE_SEARCH_LOYALTY_CARDS');
                  LoyaltyApi.refreshDocumentList(search: value);
                },
                withSliver: false,
                withPadding: false,
                withCityName: false,
                searchTitle: FlutterI18n.translate(pageNavigatorKey.currentContext, 'Search')
            ),
          )
      );

      cards.forEach((card) {

        children.add(GestureDetector(
          onTap: () =>  _openCard(card),
          onDoubleTap: () {

          },
          child: Container(
            margin: const EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10
            ),
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(7)),
                color: card.color
            ),
            child: Column(
              children: <Widget>[
                Text(
                  card.name,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 25
                  ),
                ),
                Text(
                  card.comment ?? '',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 20
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 10
                  ),
                  child: Text(
                    Tools.dateString(card.createdAt, oneDay: true),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));

      });

    } else {

      children.addAll([
        Padding(
          padding: const EdgeInsets.only(
            left: 10,
            right: 10
          ),
          child: Html(
              data: FlutterI18n.translate(pageNavigatorKey.currentContext, 'EMPTY_LOYALTY_CARDS_TEXT_HTML')
          ),
        ),
      ]);

    }

    children.add(
        SizedBox(
          height: double.parse(((children.length < 6 ? ((6 - children.length) * 125 ) : 0) + 75).toString()),
        )
    );

    return ListView(
      controller: _scrollController,
      children: children,
    );

  }

  _openCard(card, {bool setRefresh: false}) async {

    if (setRefresh) {

      _refresh();

    }

    bool refresh = await Navigator.push(context, MaterialPageRoute<bool>(
        builder: (context) => LoyaltyCardDetailPage(
          card: card,
        ),
      )
    );
    if (refresh != null && refresh) {
      _refresh();
    }

  }

}