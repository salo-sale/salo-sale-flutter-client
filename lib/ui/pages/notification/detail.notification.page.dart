import 'dart:async';

import 'package:SaloSale/shared/api/notification.api.dart';
import 'package:SaloSale/shared/models/api/notification.model.api.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class DetailNotificationPage extends StatefulWidget {

  final String notificationId;

  DetailNotificationPage({
    Key key,
    this.notificationId
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _DetailNotificationPage();

}

class _DetailNotificationPage extends State<DetailNotificationPage> {
  StreamSubscription subscription;

  NotificationModelApi notificationModelApi;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  initState() {

    NotificationApi.getDocument(widget.notificationId).then((value) {

      NotificationApi.putReadNotification(widget.notificationId).then((resp) {

        if (resp) {

          notificationModelApi.read = 1;

        }

      });

    });

    subscription = NotificationApi.filter.documentChanel.stream.listen((_notificationModelApi) => _updateNotificationModelApi(_notificationModelApi));

    super.initState();

  }

  _updateNotificationModelApi(NotificationModelApi _notificationModelApi) {

    if (_notificationModelApi != null) {

      setState(() {
        notificationModelApi = _notificationModelApi;
      });

    }

  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(),
      body: SafeArea(
          child: notificationModelApi == null ? LoaderComponent(refreshFunction: _refresh,) : _content()
      ),
    );

  }

  Future<void> _refresh() {

    return NotificationApi.getDocument(notificationModelApi.id);

  }

  Widget _content() {

    return RefreshIndicator(
      color: Colors.orange,
      onRefresh: _refresh,
      child: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.white
              ),
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    notificationModelApi.title,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 20
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 10
                    ),
                    child: Html(
                      data: notificationModelApi.body,
                      customTextAlign: (node) => TextAlign.center,
                      defaultTextStyle: TextStyle(
                      ),
                      linkStyle: TextStyle(
                        decoration: TextDecoration.underline,
                      ),
                      onLinkTap: Tools.launchUrl,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 10
                    ),
                    child: Text(
                      notificationModelApi.updatedAt,
                      style: TextStyle(
                        color: Colors.grey
                      ),
                    ),
                  ),
                ],
              )
            ),
          ]
      ),
    );

  }

}