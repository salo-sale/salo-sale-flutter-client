
import 'dart:async';

import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/notification.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/models/api/notification.model.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/scroll-event.component.dart';
import 'package:SaloSale/ui/pages/notification/detail.notification.page.dart';
import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class NotificationListPage extends StatefulWidget {

  NotificationListPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NotificationListPage();

}

class _NotificationListPage extends State<NotificationListPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final ScrollController _scrollController = ScrollController();
  StreamSubscription scrollToTop;
  bool initialized = false;

  @override
  void dispose() {

    super.dispose();
    initialized = true;
    scrollToTop?.cancel();
    NotificationApi.getCheckUnread();

  }

  @override
  void initState() {

    AppService.systemSteps.stream.listen((event) {

      if (event == SystemStepsTypeEnum.INITIALIZED) {

        if (!initialized) {
          initialized = true;

          if (AuthService.authModel.isUser) {

            NotificationApi.refreshDocumentList();

            _scrollController.addListener(() {

              if (_scrollController.position.maxScrollExtent <= (_scrollController.offset + 250)) {

                NotificationApi.getDocumentList();

              }

            });

          }

          scrollToTop = AppService.scrollToTop.stream.listen((success) {

            if (success && _scrollController.offset > 0) {

              _scrollController.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.easeOut);

            }

          });

        }

      }

    });

    super.initState();

  }

  Future<void> _refresh() {

    return NotificationApi.refreshDocumentList();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: GestureDetector(
          child: Text(
              FlutterI18n.translate(context, 'Notifications')
          ),
          onTap: () {
            AppService.scrollToTop.sink.add(true);
          },
        ),
        centerTitle: true,
      ),
      body: RefreshIndicator(
          color: Colors.orange,
          onRefresh: _refresh,
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: StreamBuilder(
              stream: NotificationApi.filter.documentListChanel,
              builder: (BuildContext context, AsyncSnapshot<List<NotificationModelApi>> snapshot) {

                if (!snapshot.hasData) {

                  return Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: LoaderComponent(
                      circularMargin: const EdgeInsets.only(
                          top: 40,
                          bottom: 40
                      ),
                      refreshFunction: _refresh,
                    ),
                  );

                } else {

                  List<Widget> children = [];

                  if (snapshot.data.length == 0) {

                    children.add(Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.alternate_email,
                            size: 150.0,
                            color: Color(0xFFDDDDDD),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                bottom: 25
                            ),
                            child: Text(
                              FlutterI18n.translate(context, 'You do not have any messages yet'),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25.0,
                                  color: Color(0xFFAAAAAA)
                              ),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    ));

                  } else {

                    children = snapshot.data.map((notificationModelApi) => _notificationCard(notificationModelApi)).toList();

                  }

                  children.add(_scrollComponent());
                  if (snapshot.data.length == 0 || snapshot.data.length < 6) {
                    children.add(Container(
                      height: MediaQuery.of(context).size.height,
                    ));
                  }

                  return ListView(
                    controller: _scrollController,
                    children: children,
                  );

                }

              },
            ),
          ),
      ),
    );

  }

  Widget _scrollComponent() {
    return ScrollEventComponent(
        useSliver: false,
        stream: NotificationApi.filter.scrollEvent.stream,
        // TODO
        defaultRenderWidget: GestureDetector(
            onTap: () {
              _refresh();
            },
            child: Container(
              child: Text(
                  ''
              ),
            )
        ),
      );
  }

  Widget _notificationCard(NotificationModelApi notificationModelApi) {

    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Badge(
          position: BadgePosition.topRight(
              top: -20,
              right: -5
          ),
          showBadge: notificationModelApi.read == 0,
          badgeContent: Padding(
            padding: const EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 0
            ),
            child: Text(
              '',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 10,
                  fontWeight: FontWeight.w500
              ),
            ),
          ),
          child: GestureDetector(
            onTap: () {
              var page = MaterialPageRoute(
                builder: (context) => DetailNotificationPage(notificationId: notificationModelApi.id.toString()),
              );
              Navigator.push(context, page);
            },
            child: Container(
              margin: const EdgeInsets.only(
                  bottom: 10
              ),
              width: double.infinity,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(7)),
                boxShadow: [
                  BoxShadow(
                    color: Theme.of(pageNavigatorKey.currentContext).primaryColor,
                    blurRadius: 10.0,
                    // has the effect of softening the shadow
                    spreadRadius: 0.0,
                    // has the effect of extending the shadow
                    offset: Offset(
                      0.0, // horizontal, move right 10
                      0.0, // vertical, move down 10
                    ),
                  )
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    notificationModelApi.title,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            top: BorderSide(
                                width: 1,
                                color: Colors.grey.shade200
                            )
                        )
                    ),
                    padding: const EdgeInsets.only(
                        top: 5
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          notificationModelApi.updatedAt,
                          style: TextStyle(
                              color: Colors.grey
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
    );

  }

}