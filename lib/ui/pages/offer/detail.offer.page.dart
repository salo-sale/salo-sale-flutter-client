import 'dart:async';
import 'dart:convert';

import 'package:SaloSale/data.dart';
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/company.api.dart';
import 'package:SaloSale/shared/api/offer.api.dart';
import 'package:SaloSale/shared/models/api/offer.model.api.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/shared/utils/tools.dart';
import 'package:SaloSale/ui/components/build-barcode.component.dart';
import 'package:SaloSale/ui/components/button-like.component.dart';
import 'package:SaloSale/ui/components/button-shared.component.dart';
import 'package:SaloSale/ui/components/container-text.component.dart';
import 'package:SaloSale/ui/components/ikoint.component.dart';
import 'package:SaloSale/ui/components/image.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/menu-item.component.dart';
import 'package:SaloSale/ui/components/notification-icon.component.dart';
import 'package:SaloSale/ui/components/offer-card.component.dart';
import 'package:SaloSale/ui/components/starts.component.dart';
import 'package:SaloSale/ui/pages/auth/main.auth.page.dart';
import 'package:SaloSale/ui/pages/company/detail.company.page.dart';
import 'package:SaloSale/ui/pages/company/map.page.dart';
import 'package:SaloSale/ui/pages/company/point-of-sale.company.page.dart';
import 'package:SaloSale/ui/pages/offer/invoice.offer.page.dart';
import 'package:barcode/barcode.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:rxdart/rxdart.dart';
import 'package:share/share.dart';

class DetailOfferPage extends StatefulWidget {
  final int offerId;
  final bool fromAdvertisingBanners;
  final bool fromOffer;
  final String filterItem;
  DetailOfferPage(
      {Key key,
      this.filterItem,
      this.offerId,
      this.fromAdvertisingBanners = false,
      this.fromOffer = false})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _DetailOfferPage();
}

class _DetailOfferPage extends State<DetailOfferPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController _listViewPointsOfSaleController = ScrollController();
  StreamSubscription subscription;

  OfferModelApi offerModelApi;
  bool likeIsClicked;
  bool initCheckTimer;
  Timer _timer;
  int _startTime;
  DateTime takeDate;
  bool moneyIsTook;
  final String filterCompanyItem = 'offer-detail-company';
  String filterItem = 'offer-detail';
  Map<String, bool> blockIsShow;
  final ScrollController _scrollController = ScrollController();

  final PublishSubject<bool> scrollToTopSinglePage = PublishSubject<bool>();

  @override
  void dispose() {
    _timer?.cancel();
    scrollToTopSinglePage?.close();
    subscription?.cancel();
    initCheckTimer = false;
    _listViewPointsOfSaleController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    if (widget.filterItem != null && widget.filterItem.length > 0) {
      filterItem = widget.filterItem;
    }

    OfferApi.putIfNotExistFilterInMap(filterItem);
    CompanyApi.putIfNotExistFilterInMap(filterCompanyItem);

    initCheckTimer = false;
    moneyIsTook = false;
    likeIsClicked = false;
    _timer?.cancel();

    _startTime = Data.startTimerForTakeIkointForView;

    scrollToTopSinglePage.stream.listen((success) {
      if (success) {
        if (_scrollController.offset > 0) {
          _scrollController.animateTo(0,
              duration: Duration(milliseconds: 500), curve: Curves.easeOut);
        } else {
          Navigator.pop(context);
        }
      }
    });

    OfferApi.getDocument(filterItem, widget.offerId).then((_offerModelApi) {
      if (_offerModelApi != null) {
        AppService.analytics
            .logEvent(name: 'OPEN_OFFER_' + _offerModelApi.id.toString());

        if (_offerModelApi != null) {
          AppService.analytics.logViewItem(
              itemId: widget.offerId.toString(),
              itemName: _offerModelApi.language.title,
              itemCategory: 'offer');
          _updateOfferModelApi(_offerModelApi);
        }

        subscription = OfferApi.filterMap[filterItem].documentChanel.stream
            .listen((_offerModelApi) => _updateOfferModelApi(_offerModelApi));
      } else {
        AppService.showOnTransparent(
          Container(
            child: Text(
              FlutterI18n.translate(pageNavigatorKey.currentContext, 'No data'),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
          ),
        );
      }
    });

    super.initState();
  }

  Future<void> _refresh() {
    setState(() {
      _timer?.cancel();
      moneyIsTook = false;
      _startTime = Data.startTimerForTakeIkointForView;
    });

    return OfferApi.getDocument(filterItem, widget.offerId);
  }

  Future checkTimer() async {
    if (offerModelApi.canTakeIkoint == null) {
      initCheckTimer = true;

      _timer = Timer.periodic(
        const Duration(seconds: 1),
        (Timer timer) => setState(
          () {
            if (_startTime < 1) {
              _timer.cancel();
            } else {
              _startTime = _startTime - 1;
            }
          },
        ),
      );
    } else {}
  }

  _updateOfferModelApi(OfferModelApi _offerModelApi) {
    if (_offerModelApi != null) {
      setState(() {
        switch (_offerModelApi.type) {
          case 1: // Offer
            blockIsShow = {
              'like': true,
              'share': true,
              'takeIkoint': true,
              'buy': true,
              'use': true,
              'validityEvent': false,
            };
            break;
          case 2: // Coupon
            blockIsShow = {
              'like': true,
              'share': true,
              'takeIkoint': true,
              'buy': true,
              'use': true,
              'validityEvent': false,
            };
            break;
          case 3: // Banner
            blockIsShow = {
              'like': true,
              'share': true,
              'takeIkoint': false,
              'buy': false,
              'use': false,
              'validityEvent': false,
            };
            break;
          case 4: // Event
            blockIsShow = {
              'like': true,
              'share': true,
              'takeIkoint': true,
              'buy': false,
              'use': false,
              'validityEvent': true,
            };
            break;
        }
        offerModelApi = _offerModelApi;
//      offerModelApi.tagList = AppService.listOfTags.firstWhere((listOfTags) => listOfTags.name == 'offer').tagList;
//        if (offerModelApi.tagIdList != null && AppService.listsOfTags.length > 0) {
//          offerModelApi.tagList = AppService.listsOfTags.firstWhere((listOfTags) => listOfTags.name == 'offer').tagList.where((tag) => offerModelApi.tagIdList.contains(tag.id.toString())).toList();
//        }
        checkTimer();
      });
    }
  }

  // TODO Comments by offer
  // TODO Download

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        titleSpacing: 0,
        centerTitle: false,
        title: GestureDetector(
          onTap: () => scrollToTopSinglePage.sink.add(true),
          child: Container(
            color: Colors.transparent,
            child: Text(
              FlutterI18n.translate(
                  pageNavigatorKey.currentContext,
                  offerModelApi != null
                      ? offerModelApi.type == 4 ? 'Events' : 'Offers'
                      : 'Loading and three dots'),
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
        actions: <Widget>[
          IkointComponent(
            margin: const EdgeInsets.only(right: 10),
            showLoginIfGuest: true,
          ),
          NotificationIconComponent(
            showLogin: false,
          ),
        ],
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            //disconnectComponent('TEXT_DISCONNECT')
            offerModelApi == null
                ? LoaderComponent(
                    refreshFunction: _refresh,
                  )
                : _offer(),
          ],
        ),
      ),
    );
  }

  _offer() {
    return StreamBuilder<bool>(
        stream: AppService.refreshLayout.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data) {
            Timer(Duration(seconds: 1), () {
              _refresh();
            });
            return LoaderComponent(
              forSliver: false,
              inBox: false,
              circularMargin: const EdgeInsets.only(top: 40, bottom: 40),
              refreshFunction: _refresh,
            );
          } else {
            return RefreshIndicator(
                color: Colors.orange,
                onRefresh: _refresh,
                child: Stack(
                  children: <Widget>[
                    ListView(controller: _scrollController, children: <Widget>[
                      ImageComponent(
                        url: offerModelApi.language.images[0].url.buildUrl,
                        withAspectRation: false,
                        useImageBuilder: false,
                        withHero: false,
                      ),
                      Container(
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: 1, color: Colors.grey.shade200))),
                        child: menuItemComponent(
                          colorBg: Colors.white,
                          context: context,
                          onTap: () => pageNavigatorKey.currentState.pushNamed(
                              'companies/${offerModelApi.company.id}'),
                          useTranslate: false,
                          icon: ImageComponent(
                            tag: 'company_' +
                                offerModelApi.company.id.toString(),
                            url: offerModelApi.company.logoUrl.buildUrl,
                            height: 75,
                            width: 75,
                            margin: const EdgeInsets.only(
                              right: 10,
                              top: 10,
                              bottom: 10,
                            ),
                            withAspectRation: false,
                            withHero: false,
                          ),
                          customTitle: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  offerModelApi.company.name,
                                  textAlign: TextAlign.start,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${offerModelApi.company.score} ', // FlutterI18n.translate(pageNavigatorKey.currentContext, 'Score') +
                                      style: TextStyle(
                                          color: Colors.grey.shade600),
                                    ),
                                    StartsComponent(
                                      value: offerModelApi.company.score,
                                      iconSize: 20.0,
                                    ),
                                    Text(
                                      ' (${offerModelApi.company.numberOfComments})',
                                      style: TextStyle(
                                          color: Colors.grey.shade600),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 4,
                                        child: _showButtonIkointOrTimer(),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: _showLikeAndShare(),
                                      ),
                                    ],
                                  ),
                                  ..._buyUseInfoBlock(),
                                  _commentForBuyButton(),
                                ],
                              ),
                            ),
                            _showValidityEventContainer(),
                            ContainerTextComponent(
                              title: 'Description',
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    offerModelApi.language.title ?? '',
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Html(
                                    data: offerModelApi.language.description ??
                                        '',
                                    linkStyle: const TextStyle(
                                      color: Colors.orange,
                                      decorationColor: Colors.orangeAccent,
                                      decoration: TextDecoration.underline,
                                    ),
                                    onLinkTap: Tools.launchUrl,
                                  )
                                ],
                              ),
                              margin: const EdgeInsets.only(bottom: 10),
                            ),
                            _showAllOffer(),
                            ContainerTextComponent(
                                margin: const EdgeInsets.only(top: 10),
                                show: offerModelApi
                                            .language.descriptionRequirements !=
                                        null &&
                                    offerModelApi.language
                                            .descriptionRequirements.length >
                                        0,
                                title: 'Requirement description',
                                text: offerModelApi
                                    .language.descriptionRequirements),
                            _showLinks(),
                          ],
                        ),
                      ),
                      _openPointOfSaleOnMap(),
                      _commentList(),
                      _menu(),
//                  tagListComponent(offerModelApi.tagList),
                      _similarOfferList(),
                    ]),
                  ],
                ));
          }
        });
  }

  _showButtonIkointOrTimer() {
    if (!blockIsShow['takeIkoint']) {
      return Container();
    }

    Widget afterText = Container();

    bool canTakeIkoint = offerModelApi.canTakeIkoint == null;

    if (AppService.internetIsConnected) {
      if (offerModelApi.canTakeIkoint == null) {
        if (_startTime == 0) {
          if (moneyIsTook) {
            afterText = LoaderComponent(
              refreshFunction: _refresh,
              iconSize: 20,
              fontSize: 16,
              circularMargin: const EdgeInsets.only(left: 10),
            );
          }
        } else {
          afterText = Padding(
            padding: const EdgeInsets.only(left: 10),
            child: CircularPercentIndicator(
              radius: 25.0,
              animation: true,
              animationDuration: _startTime * 1000,
              lineWidth: 2.5,
              percent: 1,
              circularStrokeCap: CircularStrokeCap.round,
              center: Text(
                _startTime.toString(),
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14.0,
                    color: Colors.orange),
              ),
              backgroundColor: Colors.transparent,
              progressColor: Colors.orange,
            ),
          );
        }
      }
    } else {
      canTakeIkoint = false;
    }

    Widget child = Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(7)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color:
                canTakeIkoint ? Colors.orange.shade300 : Colors.grey.shade300,
            offset: Offset(0.0, 0.0),
            blurRadius: 2.5,
          ),
        ],
      ),
      height: 45,
      margin: const EdgeInsets.only(right: 10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: AppService.internetIsConnected
            ? (AuthService.authModel.isGuest
                ? [
                    Text(FlutterI18n.translate(pageNavigatorKey.currentContext,
                        'You must be logged in'))
                  ]
                : [
                    Text(
                      canTakeIkoint
                          ? _startTime == 0
                              ? FlutterI18n.translate(
                                      pageNavigatorKey.currentContext,
                                      'Take yours') +
                                  ' ${offerModelApi.ikointForTake ?? 5} ' +
                                  FlutterI18n.translate(
                                      pageNavigatorKey.currentContext,
                                      'ikoint_2')
                              : moneyIsTook
                                  ? FlutterI18n.translate(
                                      pageNavigatorKey.currentContext,
                                      'In process')
                                  : FlutterI18n.translate(
                                      pageNavigatorKey.currentContext,
                                      'Take your ikoints through')
                          : FlutterI18n.translate(
                                  pageNavigatorKey.currentContext,
                                  'Next time already') +
                              ' ' +
                              Tools.dateString(offerModelApi.canTakeIkoint,
                                  oneDay: true),
                      style: TextStyle(
                          color: canTakeIkoint ? Colors.orange : Colors.grey,
                          fontWeight: FontWeight.w500),
                    ),
                    afterText
                  ])
            : [
                Text(FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'Temporarily unavailable'))
              ],
      ),
    );

    return GestureDetector(
      onTap: () async {
        if (AuthService.authModel.isGuest) {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MainAuthPage(),
              ));
        } else {
          if (offerModelApi.canTakeIkoint == null && _startTime == 0) {
            if (!moneyIsTook) {
              setState(() {
                moneyIsTook = true;

                OfferApi.takeIkoint(filterItem).then((result) {
                  if (result) {
                    setState(() {
                      AppService.message(MessageModel(
                          title: 'Ikoint',
                          withoutTranslate: true,
                          message: FlutterI18n.translate(
                                  pageNavigatorKey.currentContext,
                                  'Congratulations, you just earned') +
                              ' ${offerModelApi.ikointForTake ?? '5'} ik',
                          backgroundColor: Colors.green,
                          duration: null,
                          mainButton: null));
                    });
                  } else {
                    // TODO FlushBar

                  }
                });
              });
            }
          }
        }
      },
      child: child,
    );
  }

  _buttonBuy() {

    if (offerModelApi.canBuy == 0 ||
        !blockIsShow['buy'] ||
        offerModelApi.price == 0 ||
        !AppService.remoteConfig.checkModule('buyOffer')) {
      return Container();
    }

    Widget child = Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(7)),
          color: AuthService.authModel.isUser &&
                  AuthService.userModelApi.emailIsConfirmed &&
                  AppService.internetIsConnected
              ? Colors.orange
              : Colors.grey),
      padding: const EdgeInsets.all(15.0),
      margin: const EdgeInsets.only(top: 10, right: 10),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              FlutterI18n.translate(pageNavigatorKey.currentContext, 'Buy'),
//               + ' ' + offerModelApi.price.toString() + ' ' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'ikoint_2')
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18.0,
                  color: Colors.white),
            ),
          ],
        ),
      ),
    );

    if (AuthService.authModel.isUser &&
        AppService.internetIsConnected &&
        AuthService.userModelApi.emailIsConfirmed) {
      child = GestureDetector(
        onTap: () async {
          await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    InvoiceOfferPage(offerModelApi: offerModelApi),
              ));
          _refresh();
        },
        child: child,
      );
    }

    return Expanded(child: child);
  }

  _buttonUseOffer() {

    if (!blockIsShow['use']) {
      return Container();
    }

    Widget child;
    bool haveIkoints = AuthService.authModel.isGuest
        ? false
        : AuthService.userModelApi.ikoint >= offerModelApi.price;

    if (offerModelApi.codeList.length > 0) {
      child = Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(7)),
            color: AuthService.authModel.isUser &&
                    AuthService.userModelApi.emailIsConfirmed &&
                    haveIkoints
                ? Colors.green
                : Colors.grey),
        padding: const EdgeInsets.all(15.0),
        margin: const EdgeInsets.only(
          top: 10,
        ),
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'Use'),
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 18.0,
                    color: Colors.white),
              ),
            ],
          ),
        ),
      );

      if (AuthService.authModel.isUser &&
          AuthService.userModelApi.emailIsConfirmed &&
          haveIkoints) {
        child = GestureDetector(
          onTap: () {
            AppService.showOnTransparent(
                Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  FlutterI18n.translate(pageNavigatorKey.currentContext,
                      'Show this Qr-code to the person with the scanner'),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black),
                ),
              ),
              SvgPicture.string(buildBarcode(
                Barcode.qrCode(),
                Tools.convertOpposite(Tools.reverseString(utf8.fuse(base64).encode(
                    '{"offer_id":${offerModelApi.id},"user_id":${AuthService.userModelApi.id}}'))),
                height: 200,
              ))
            ]));
          },
          onDoubleTap: () {},
          child: child,
        );
      }
    } else {
      child = Container();
    }

    return Expanded(
      child: child,
    );
  }

  _commentList() {
    if (offerModelApi.commentList.length == 0) {
      return Container();
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding:
              const EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'Comments and ratings'),
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Colors.grey,
                    fontSize: 20),
              ),
              offerModelApi.commentList.length > 0
                  ? GestureDetector(
                      onTap: () {},
                      child: Container(
                        color: Colors.transparent,
                        child: Row(
                          children: <Widget>[
                            Text(
                              FlutterI18n.translate(
                                  pageNavigatorKey.currentContext, 'All_2'),
                              style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15,
                                  color: Colors.orange),
                            ),
                            Icon(
                              FontAwesomeIcons.angleRight,
                              color: Colors.orange,
                              size: 15,
                            )
                          ],
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
        ...offerModelApi.commentList.map((offer) => Container(
              child: Text('Comment'),
            )),
        offerModelApi.commentList.length > 0
            ? Container()
            : Container(
                padding: const EdgeInsets.only(left: 10, bottom: 10),
                child: Text(FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'Not found')),
              )
      ],
    );
  }

  _infoRedeemed() {
    if (offerModelApi.redeemed == null || offerModelApi.redeemed == 0) {
      return Container();
    }
    return Container(
      margin: const EdgeInsets.only(
        bottom: 0,
        top: 10,
      ),
      padding: const EdgeInsets.all(10),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(7)),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.orange.shade300,
            offset: Offset(0.0, 0.0),
            blurRadius: 2.5,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'You have to use') +
                ': ',
            style: TextStyle(color: Colors.orange, fontWeight: FontWeight.w500),
          ),
          Text(
            (offerModelApi?.redeemed?.toString() ?? '') +
                ' ' +
                FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'things'),
            style: TextStyle(color: Colors.orange, fontWeight: FontWeight.w500),
          )
        ],
      ),
    );
  }

  _commentForBuyButton() {
    if (AuthService.authModel.isUser &&
        !AuthService.userModelApi.emailIsConfirmed) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 0, top: 10),
        child: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext,
              'OFFER_DETAIL_NEED_CONFIRM_EMAIL'),
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.w500, color: Colors.red),
        ),
      );
    }

    if (!blockIsShow['buy'] ||
        !AppService.remoteConfig.checkModule('buyOffer')) {
      return Container();
    }

    return Padding(
      padding: const EdgeInsets.only(bottom: 0, top: 10),
      child: Text(
        FlutterI18n.translate(
            pageNavigatorKey.currentContext, 'COMMENT_FOR_BUY_BUTTON'),
        textAlign: TextAlign.left,
        style: TextStyle(color: Colors.grey),
      ),
    );
  }

  _showPriceText() {
    if (!blockIsShow['buy']) {
      return Container();
    }

    if (offerModelApi.price == 0) {
      return Padding(
        padding: const EdgeInsets.only(top: 25),
        child: Center(
          child: Text(
            '* ' +
                FlutterI18n.translate(pageNavigatorKey.currentContext, 'FREE') +
                ' *',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Colors.orange,
                fontWeight: FontWeight.w500,
                fontSize: 20),
          ),
        ),
      );
    }

    return Padding(
      padding: const EdgeInsets.only(top: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'The cost of the offer') +
                ':',
            textAlign: TextAlign.left,
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
          ),
          Text(
            offerModelApi.price.toString() +
                ' ' +
                FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'ikoint_2'),
            textAlign: TextAlign.left,
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
          ),
        ],
      ),
    );
  }

  _showLikeAndShare() {
    List<Widget> children = [];

    if (blockIsShow['like']) {
      children.add(Expanded(
          child: buttonLikeComponent(
              onTap: () {
                if (AppService.internetIsConnected) {
                  setState(() {
                    likeIsClicked = true;
                  });
                  OfferApi.like(filterItem, isLike: !(offerModelApi.liked == 1))
                      .then((value) {
                    setState(() {
                      likeIsClicked = false;
                    });
                  });
                }
              },
              liked: offerModelApi.liked,
              likeIsClicked: likeIsClicked)));
    }

    if (blockIsShow['share']) {
      children.add(Expanded(
          child: buttonSharedComponent(
              onTap: () => Share.share(
                  offerModelApi.language.title +
                      ' \n salo-sale.com/offers/' +
                      offerModelApi.id.toString() +
                      ' \n',
                  subject: offerModelApi.language.description ?? ''))));
    }

    if (children.length == 0) {
      return Container();
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: children,
    );
  }

  _showValidityEventContainer() {
    if (!blockIsShow['validityEvent']) {
      return Container();
    }

    return ContainerTextComponent(
      title: FlutterI18n.translate(pageNavigatorKey.currentContext, 'Validity'),
      margin: const EdgeInsets.only(bottom: 10),
      child: Container(
        height: double.parse((offerModelApi.validityList.length > 4
                ? 200
                : offerModelApi.validityList.length * 50)
            .toString()),
        child: ListView(
          children: offerModelApi.validityList
              .asMap()
              .map((index, validity) {
                return MapEntry(
                  index,
                  Container(
                    height: 50,
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                  left: index != 0 &&
                                          index !=
                                              (offerModelApi
                                                      .validityList.length -
                                                  1)
                                      ? 5
                                      : 0),
                              child: Icon(
                                index == 0
                                    ? FontAwesomeIcons.flag
                                    : index ==
                                            (offerModelApi.validityList.length -
                                                1)
                                        ? FontAwesomeIcons.flagCheckered
                                        : FontAwesomeIcons.dotCircle,
                                size: index != 0 &&
                                        index !=
                                            (offerModelApi.validityList.length -
                                                1)
                                    ? 14
                                    : null,
                                color: Colors.grey,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: index != 0 &&
                                          index !=
                                              (offerModelApi
                                                      .validityList.length -
                                                  1)
                                      ? 15
                                      : 10),
                              child: Text(
                                validity.validityDate,
                                style: TextStyle(
                                    fontWeight: FontWeight.w500, fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                        Text(
                          // TODO added title of event on event list (home page)
                          (validity.openTime ?? '') +
                              ' - ' +
                              (validity.closeTime ?? ''),
                          style: TextStyle(
                              // TODO Whats hapan when events is just start without finish and time!!!
                              color: Colors.grey),
                        ),
                      ],
                    ),
                  ),
                );
              })
              .values
              .toList(),
        ),
      ),
    );
  }

  _menu() {
    return Padding(
      padding: const EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          GestureDetector(
            onTap: () async {
              Tools.launchUrl(
                  AppService.remoteConfig.links['how-to-use-offer']);
            },
            onDoubleTap: () {},
            child: Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(top: 10),
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(7)),
                  border: Border.all(width: 1, color: Colors.transparent)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Icon(
                      FontAwesomeIcons.link,
                      color: Colors.orange,
                    ),
                  ),
                  Text(
                    FlutterI18n.translate(
                        pageNavigatorKey.currentContext, 'How to use offer'),
                    style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.w500,
                        fontSize: 20),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _showLinks() {
    if (offerModelApi.linkList == null || offerModelApi.linkList.length == 0) {
      return Container();
    }

    return ContainerTextComponent(
      title: 'Links',
      child: Container(
        height: (offerModelApi.linkList.length * 50) > 200
            ? 200
            : double.parse((offerModelApi.linkList.length * 50).toString()),
        child: ListView(
          children: offerModelApi.linkList.map((link) {
            return ListTile(
              onTap: () {
                Tools.launchUrl(link.link);
              },
              title: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: Icon(
                      FontAwesomeIcons.link,
                      color: Colors.orange,
                      size: 15,
                    ),
                  ),
                  Text(
                    link.name ??
                        FlutterI18n.translate(
                            pageNavigatorKey.currentContext, 'Review'),
                    textAlign: TextAlign.left,
                  )
                ],
              ),
            );
          }).toList(),
        ),
      ),
      margin: const EdgeInsets.only(top: 10),
    );
  }

  _showAllOffer() {
    return GestureDetector(
        onTap: () {
          if (AppService.internetIsConnected) {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => DetailCompanyPage(
                openOffers: true,
                companyId: offerModelApi.company.id,
              ),
            ));
          } else {
            AppService.message(MessageModel(
                title: 'Registration form',
                message: 'TEXT_DISCONNECT',
                backgroundColor: Colors.orangeAccent,
                duration: null,
                mainButton: null));
          }
        },
        onDoubleTap: () {},
        child: Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(7)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.shade300,
                blurRadius: 10.0,
                // has the effect of softening the shadow
                spreadRadius: 0.0,
                // has the effect of extending the shadow
                offset: Offset(
                  0.0, // horizontal, move right 10
                  0.0, // vertical, move down 10
                ),
              )
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'All offers'),
                textAlign: TextAlign.left,
                style: TextStyle(color: Colors.orange, fontSize: 25),
              ),
              Icon(
                FontAwesomeIcons.angleRight,
                color: Colors.orange,
                size: 30,
              )
            ],
          ),
        ));
  }

  _buyUseInfoBlock() {
    if (offerModelApi.limit == null ||
        (offerModelApi.limit > offerModelApi.amountOfUses)) {
      return [
        _showPriceText(),
        _infoRedeemed(),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _buttonBuy(),
            _buttonUseOffer()
          ],
        ),
      ];
    }

    return [
      Padding(
        padding: const EdgeInsets.only(top: 25),
        child: Center(
          child: Text(
            '* ' +
                FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'The limit has expired') +
                ' *',
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Colors.blue, fontWeight: FontWeight.w500, fontSize: 20),
          ),
        ),
      )
    ];
  }

  _openPointOfSaleOnMap() {
    return ContainerTextComponent(
      title: 'Where to use',
      margin: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          offerModelApi.company.pointOfSaleList.length > 0
              ? Container(
                  height: (offerModelApi.company.pointOfSaleList.length * 75) >
                          250
                      ? 250
                      : double.parse(
                          (offerModelApi.company.pointOfSaleList.length * 75)
                              .toString()),
                  child: ListView(
                    controller: _listViewPointsOfSaleController,
                    children: offerModelApi.company.pointOfSaleList.map((pos) {
                      bool haveWorkHours =
                          pos.workHours != null && pos.workHours.length > 0;
                      bool isOpen = haveWorkHours ? pos.isOpen() : false;

                      return Container(
                        height: 75,
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: 1, color: Colors.grey.shade200))),
                        child: ListTile(
                          title: Text(
                            pos.address ??
                                (FlutterI18n.translate(
                                        pageNavigatorKey.currentContext,
                                        'Point') +
                                    ': ' +
                                    pos.id.toString()),
                            textAlign: TextAlign.left,
                          ),
                          subtitle: Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 5),
                                child: Icon(
                                  FontAwesomeIcons.solidCircle,
                                  size: 10,
                                  color: haveWorkHours
                                      ? isOpen ? Colors.green : Colors.red
                                      : Colors.green,
                                ),
                              ),
                              Text(
                                FlutterI18n.translate(
                                    pageNavigatorKey.currentContext,
                                    haveWorkHours
                                        ? (isOpen ? 'Opened' : 'Closed')
                                        : '24H'),
                                style: TextStyle(
                                    color: haveWorkHours
                                        ? isOpen ? Colors.green : Colors.red
                                        : Colors.green),
                                textAlign: TextAlign.left,
                              ),
                            ],
                          ),
                          leading: Icon(
                            FontAwesomeIcons.mapMarkerAlt,
                            color: Colors.orange,
                            size: 35,
                          ),
                          onTap: () {
                            String uuidFlushbar = AppService.message(
                                MessageModel(
                                    title: 'Map',
                                    message: 'Loading and three dots',
                                    backgroundColor: Colors.black54,
                                    duration: null,
                                    mainButton: null));

                            Navigator.push(
                                context,
                                MaterialPageRoute<String>(
                                  builder: (context) => PointOfSaleCompanyPage(
                                    pos: pos,
                                    isOpen: isOpen,
                                    companyLogoUrl:
                                        offerModelApi.company.logoUrl.buildUrl,
                                    companyId: offerModelApi.company.id,
                                    companyName: offerModelApi.company.name,
                                    companyScore: offerModelApi.company.score,
                                    companyNumberOfComments:
                                        offerModelApi.company.numberOfComments,
                                    haveWorkHours: haveWorkHours,
                                    flushbarUuid: uuidFlushbar,
                                  ),
                                ));
                          },
                        ),
                      );
                    }).toList(),
                  ),
                )
              : Container(
                  child: Text(
                    FlutterI18n.translate(
                            pageNavigatorKey.currentContext, 'At all points') +
                        ': ' +
                        offerModelApi.company.name,
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),
                  ),
                ),
          GestureDetector(
            onTap: () async {
              if (AppService.internetIsConnected) {
//                AppService.showLoaderWindow();
                offerModelApi.company = await CompanyApi.selectDocument(
                    filterCompanyItem, offerModelApi.company,
                    searchInList: true);

                String uuidFlushbar = AppService.message(MessageModel(
                    title: 'Map',
                    message: 'Loading and three dots',
                    backgroundColor: Colors.black54,
                    duration: null,
                    mainButton: null));

                await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MapPage(
                          useSelectedCompany: true,
                          filterCompanyItem: filterCompanyItem,
                          flushbarUuid: uuidFlushbar),
                    ));

//                AppService.closeLoaderWindow();
                setState(() {});
              } else {
                AppService.message(MessageModel(
                    title: 'Registration form',
                    message: 'TEXT_DISCONNECT',
                    backgroundColor: Colors.orangeAccent,
                    duration: null,
                    mainButton: null));
              }
            },
            onDoubleTap: () {},
            child: Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(top: 10),
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(7)),
                  border: Border.all(width: 1, color: Colors.orange)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    FlutterI18n.translate(
                        pageNavigatorKey.currentContext, 'See on map'),
                    style: TextStyle(
                        color: Colors.orange,
                        fontWeight: FontWeight.w500,
                        fontSize: 20),
                  ),
                  Icon(
                    FontAwesomeIcons.mapMarkerAlt,
                    color: Colors.orange,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

//  _comments() {
//
//    // TODO stream builder for get comments by company id
//
//  }

  _similarOfferList() {
    if (offerModelApi.similarOfferList.length == 0) {
      return Container();
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding:
              const EdgeInsets.only(left: 10, top: 10, bottom: 10, right: 10),
          child: Text(
            FlutterI18n.translate(
                pageNavigatorKey.currentContext, 'Similar offers'),
            style: TextStyle(
                fontWeight: FontWeight.w500, color: Colors.grey, fontSize: 20),
          ),
        ),
        ...offerModelApi.similarOfferList.map((offer) => OfferCardComponent(
              offerModelApi: offer,
              fromOffer: true,
            )),
        offerModelApi.similarOfferList.length > 0
            ? Container()
            : Container(
                padding: const EdgeInsets.only(left: 10, bottom: 10),
                child: Text(FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'Not found')),
              )
      ],
    );
  }
}
