import 'package:SaloSale/routing.dart';
import 'package:SaloSale/shared/api/offer.api.dart';
import 'package:SaloSale/shared/models/api/offer.model.api.dart';
import 'package:SaloSale/shared/models/message.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class InvoiceOfferPage extends StatefulWidget {

  final OfferModelApi offerModelApi;

  InvoiceOfferPage({
    Key key,
    this.offerModelApi
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _InvoiceOfferPage();

}

class _InvoiceOfferPage extends State<InvoiceOfferPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    AppService.analytics.logEvent(name: 'OPEN_OFFER_INVOCE');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
            FlutterI18n.translate(context, 'Offer invoice')
        ),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      bottom: 10
                  ),
                  child: Text(
                    FlutterI18n.translate(pageNavigatorKey.currentContext, 'General information'),
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 20
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    bottom: 10
                  ),
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          right: 10
                        ),
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Provider') + ':',
                          overflow: TextOverflow.fade,
                          softWrap: true,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                      Text(
                        widget.offerModelApi?.company?.name ?? '',
                        overflow: TextOverflow.fade,
                        softWrap: true,
                        style: TextStyle(
                            fontSize: 14
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                      bottom: 25
                  ),
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 10
                        ),
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Offer') + ':',
                          overflow: TextOverflow.fade,
                          softWrap: true,
                          style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          widget.offerModelApi?.language?.title ?? '',
                          overflow: TextOverflow.fade,
                          textAlign: TextAlign.right,
                          softWrap: true,
                          style: TextStyle(
                              fontSize: 14
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    bottom: 10
                  ),
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          right: 10
                        ),
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Count') + ':',
                          overflow: TextOverflow.fade,
                          softWrap: true,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                      Text(
                        '1' + ' ' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'things'),
                        overflow: TextOverflow.fade,
                        softWrap: true,
                        style: TextStyle(
                            fontSize: 14
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    bottom: 25
                  ),
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          right: 10
                        ),
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Price') + ':',
                          overflow: TextOverflow.fade,
                          softWrap: true,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                      Text(
                        (widget.offerModelApi?.price.toString() ?? '') + ' ' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'ikoint_2'),
                        overflow: TextOverflow.fade,
                        softWrap: true,
                        style: TextStyle(
                            fontSize: 14
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    bottom: 50
                  ),
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          right: 10
                        ),
                        child: Text(
                          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Amount') + ':',
                          overflow: TextOverflow.fade,
                          softWrap: true,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                      Text(
                        (widget.offerModelApi?.price.toString() ?? '-') + ' ' + FlutterI18n.translate(pageNavigatorKey.currentContext, 'ikoint_2'),
                        overflow: TextOverflow.fade,
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    ],
                  ),
                ),
                _confirmBuy()
              ],
            ),
          )
//          Positioned(
//            bottom: 0,
//            left: 0,
//            right: 0,
//            child: _confirmBuy(),
//          )
        ],
      ),
    );

  }

  _confirmBuy() {

    return GestureDetector(
      onTap: () {
        OfferApi.buyOffer(widget.offerModelApi.id).then((success) {

          if (success) {

            AuthService.initUserApiModel();
            AuthService.saveNewOfferToCache(widget.offerModelApi);

            Navigator.of(context).pop();

            AppService.message(MessageModel(
                title: 'Offer',
                message: 'Congratulations, you have successfully bought the offer, thank you and good mood',
                backgroundColor: Colors.green,
                mainButton: null
            ));

          } else {

            AppService.message(MessageModel(
                title: 'Offer',
                message: 'Sorry, there was an error',
                backgroundColor: Colors.red,
                mainButton: null
            ));

          }

        });
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(7)),
          color: Colors.orange,
        ),
        width: MediaQuery.of(context).size.width,
        height: 50,
        child: Text(
          FlutterI18n.translate(pageNavigatorKey.currentContext, 'Confirm'),
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontSize: 25
          ),
        ),
      ),
    );

  }

}