import 'dart:async';

import 'package:SaloSale/data.dart';
import 'package:SaloSale/routing.dart';
import 'package:SaloSale/salo_sale_icons_icons.dart';

import 'package:SaloSale/shared/api/offer.api.dart';
import 'package:SaloSale/shared/api/recommendation.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/models/api/offer.model.api.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/ui/components/recommended-offers.component.dart';
import 'package:SaloSale/ui/components/header.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/offer-card.component.dart';
import 'package:SaloSale/ui/components/scroll-event.component.dart';
import 'package:SaloSale/ui/components/search-input.component.dart';
import 'package:SaloSale/ui/components/tags.component.dart';
import 'package:SaloSale/ui/pages/menu/feedback.page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ListOfferPage extends StatefulWidget implements Widget {
  ListOfferPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ListOfferPage();
}

class _ListOfferPage extends State<ListOfferPage>
    with SingleTickerProviderStateMixin {
  final TextEditingController searchInputController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final ScrollController _scrollController = ScrollController();
  final FocusNode myFocusNode = FocusNode();
  final String filterItem = 'OfferList';

  StreamSubscription scrollToTop;
  StreamSubscription refreshLayout;

  bool initialized = false;

  @override
  void dispose() {
    initialized = true;
    _scrollController?.dispose();
    searchInputController?.dispose();
    scrollToTop?.cancel();
    refreshLayout?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    OfferApi.putIfNotExistFilterInMap(filterItem);

    AppService.systemSteps.stream.listen((event) {
      if (event == SystemStepsTypeEnum.INITIALIZED) {
        if (!initialized) {
          initialized = true;

          OfferApi.refreshDocumentList(filterItem);

          _scrollController.addListener(() {
            if (_scrollController.position.maxScrollExtent <=
                (_scrollController.offset + 250)) {
              OfferApi.getDocumentList(filterItem);
            }
          });

          scrollToTop = AppService.scrollToTop.stream.listen((success) {
            if (success && _scrollController.offset > 0) {
              _scrollController.animateTo(0,
                  duration: Duration(milliseconds: 500), curve: Curves.easeOut);
            }
          });

          refreshLayout =
              AppService.refreshLayout.stream.listen((refreshContent) {
            if (refreshContent) {
              OfferApi.refreshDocumentList(filterItem, refreshTagList: true);
            }
          });
        }
      }
    });

    if (AuthService.authModel.isShowCompensationBanner) {
      Timer(
          Duration(seconds: 1),
          () => AppService.showOnTransparent(Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    Data.nameOfApp,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Icon(
                      FontAwesomeIcons.gifts,
                      color: Colors.orange,
                      size: 50,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      FlutterI18n.translate(pageNavigatorKey.currentContext,
                          'COMPENSATION_MESSAGE'),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ],
              )));
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Stack(children: <Widget>[
          RefreshIndicator(
              color: Colors.orange,
              onRefresh: _refresh,
              child: CustomScrollView(controller: _scrollController, slivers: <
                  Widget>[
                HeaderComponent(
                  titleOfPage: 'Offers for you',
                ),
                searchInputComponent(
                  myFocusNode,
                  searchInputController,
                  () {
                    searchInputController.clear();
                    myFocusNode.unfocus();
                    _refresh();
                  },
                  (term) {
                    AppService.analytics
                        .logEvent(name: 'USE_SEARCH_OFFER_LIST');
                    OfferApi.refreshDocumentList(filterItem, search: term);
                  },
                ),
                TagsComponent(
                  tagListName: 'offer',
                  onSelectedTagIdList: (list) {
                    OfferApi.refreshDocumentList(filterItem, tagIdList: list);
                    RecommendationApi.refreshDocumentList(tagIdList: list);
                  },
                ),
                RecommendedOffersComponent(),
                _addNewOffer(),

                /// Тунель для завантаження пропозиції
                StreamBuilder<bool>(
                    stream: AppService.refreshContent.stream,
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data) {
                        Timer(Duration(seconds: 1), () {
                          _refresh();
                        });
                      } else {
                        return StreamBuilder<List<OfferModelApi>>(
                            stream: OfferApi.filterMap[filterItem]
                                .documentListChanel.stream,
                            builder: (BuildContext context,
                                AsyncSnapshot<List<OfferModelApi>> snapshot) {
                              if (snapshot.hasData) {
                                if (snapshot.data.length > 0) {
                                  return SliverList(
                                    delegate: SliverChildBuilderDelegate(
                                      (BuildContext context, int index) =>
                                          OfferCardComponent(
                                              offerModelApi:
                                                  snapshot.data[index],
                                              filterItem: filterItem),
                                      childCount: snapshot.data.length,
                                    ),
                                  );
                                }

                                return SliverToBoxAdapter(
                                  child: Container(
                                    padding: const EdgeInsets.all(10),
                                    margin: const EdgeInsets.all(10),
                                    child: Text(FlutterI18n.translate(
                                        pageNavigatorKey.currentContext,
                                        'No data')),
                                  ),
                                );
                              }

                              return _loader();
                            });
                      }

                      return _loader();
                    }),
                ScrollEventComponent(
                  stream: OfferApi.filterMap[filterItem].scrollEvent.stream,
                ),
              ])),
        ]));
  }

  /// Фіункція для оновлення даних
  Future<void> _refresh({bool refreshAdvertisingBanner: true}) {
    if (refreshAdvertisingBanner) {
      AppService.refreshAdvertisingBanners.sink.add(true);
    }
    return OfferApi.refreshDocumentList(filterItem);
  }

  _addNewOffer() {
    if (!AppService.remoteConfig.checkModule('addNewOffer') ||
        !AppService.remoteConfig.modulesControl['addNewOffer'].use) {
      return SliverToBoxAdapter(
        child: Container(),
      );
    }

    return SliverToBoxAdapter(
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => FeedbackMenuHomePage(
                  text: FlutterI18n.translate(pageNavigatorKey.currentContext,
                          'STANDARD_TEXT_NEW_OFFER') +
                      '${AppService.remoteConfig.modulesControl['addNewOffer'].defaultFilterValue['ikoint']} ik' +
                      '\n' +
                      FlutterI18n.translate(pageNavigatorKey.currentContext,
                          'STANDARD_FORM_IN_TEXTAREA'),
                ),
              ));
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(7)),
              border: Border.all(width: 1, color: Colors.orange)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                FlutterI18n.translate(
                    pageNavigatorKey.currentContext, 'BTN_ADD_NEW_OFFER'),
                style: TextStyle(
                    color: Colors.orange,
                    fontWeight: FontWeight.w500,
                    fontSize: 20),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Text(
                      '+ ${AppService.remoteConfig.modulesControl['addNewOffer'].defaultFilterValue['ikoint']}' ??
                          '',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Colors.orange),
                    ),
                  ),
                  Icon(
                    SaloSaleIcons.ikoint_logo,
                    color: Colors.orange,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _loader() {
    return LoaderComponent(
      forSliver: true,
      inBox: true,
      circularMargin: const EdgeInsets.only(top: 40, bottom: 40),
      refreshFunction: _refresh,
    );
  }
}
