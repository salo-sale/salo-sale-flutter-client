import 'dart:async';

import 'package:SaloSale/my_custom_icons_icons.dart';
import 'package:SaloSale/salo_sale_icons_icons.dart';
import 'package:SaloSale/shared/api/notification.api.dart';
import 'package:SaloSale/shared/enums/types/system-steps.type.enum.dart';
import 'package:SaloSale/shared/services/app.service.dart';
import 'package:SaloSale/shared/services/auth.service.dart';
import 'package:SaloSale/ui/components/home-drawer.component.dart';
import 'package:SaloSale/ui/components/loader.component.dart';
import 'package:SaloSale/ui/components/notification-icon.component.dart';
import 'package:SaloSale/ui/pages/company/list.company.page.dart';
import 'package:SaloSale/ui/pages/event/list.event.page.dart';
import 'package:SaloSale/ui/pages/likes/list.page.dart';
import 'package:SaloSale/ui/pages/offer/list.offer.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeScreen extends StatefulWidget {
  final int initPageIndex;
  final String verificationToken;

  const HomeScreen({
    Key key,
    this.initPageIndex = 0,
    this.verificationToken,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Map<String, dynamic>> _children;

  bool firstOpen = true;

  @override
  void initState() {

    _children = List<Map<String, dynamic>>()
      ..addAll([
        {'page': ListOfferPage(), 'icon': Icon(SaloSaleIcons.percentage)},
        {'page': ListEventPage(), 'icon': Icon(MyCustomIcons.event)},
        {
          'page': ListCompanyPage(),
          'icon': Icon(FontAwesomeIcons.solidCompass)
        },
//      {
//        'page': SearchPage(),
//        'icon': Icon(FontAwesomeIcons.search)
//      },
        {'page': LikesPage(), 'icon': Icon(FontAwesomeIcons.solidHeart)},
//      {
//        'page': MenuHomePage(),
//        'icon': ,
//      },
      ]);
    AppService.currentIndexPage = widget.initPageIndex;

    AppService.systemSteps.stream.listen((event) async {
      if (event == SystemStepsTypeEnum.INITIALIZED) {
        if (AppService.needChangeLanguage) {
          await AppService.changeLanguage(
              AppService.deviceInfoModel.languageCode,
              showLoader: false,
              refreshAllLayout: false);
        }

//        print('widget.verificationToken: ${widget.verificationToken}');

        if (widget.verificationToken != null && widget.verificationToken.length > 0) {

          AuthService.postConfirmEmail(widget.verificationToken).then((success) {

//            print("success $success");

            AppService.refreshLayout.sink.add(true);

          });

        } else {

          AppService.refreshLayout.sink.add(true);

        }

      }
    });

    super.initState();
  }

  void onTabTapped(int index) {
    setState(() {
      AppService.currentIndexPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (AuthService.authModel.isUser) {
      NotificationApi.getCheckUnread();
    }

    try {
      return StreamBuilder<bool>(
          stream: AppService.refreshLayout.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data) {
              Timer(Duration(milliseconds: 500), () {
                AppService.refreshLayout.sink.add(false);
                if (AuthService.authModel.isUser) {
                  NotificationApi.getCheckUnread();
                }
                firstOpen = false;
              });

              return Scaffold(
                body: LoaderComponent(
                  withTimer: false,
                ),
              );
            } else {
              if (firstOpen) {
                return Scaffold(
                  body: LoaderComponent(
                    withTimer: false,
                  ),
                );
              }

              return Scaffold(
                key: _scaffoldKey,
                drawer: HomeDrawerComponent(),
                appBar: AppBar(
                  title: IconButton(
                    iconSize: 100,
                    onPressed: () {
                      AppService.scrollToTop.sink.add(true);
                    },
                    icon: Image(
                        image: AssetImage('assets/images/logo-lg-min-2.png')),
                  ),
                  centerTitle: true,
                  actions: <Widget>[
                    NotificationIconComponent(),
                  ],
                ),
                body: SafeArea(
                  bottom: true,
                  child: _children[AppService.currentIndexPage]['page'],
                ),
                bottomNavigationBar: BottomNavigationBar(
                  onTap: onTabTapped,
                  type: BottomNavigationBarType.fixed,
                  currentIndex: AppService.currentIndexPage,
                  showSelectedLabels: true,
                  showUnselectedLabels: true,
                  selectedItemColor: Colors.orange,
                  unselectedItemColor: Color(0xFF787878),
                  items: _children.map((page) {
                    return BottomNavigationBarItem(
                      title: Container(),
                      icon: page['icon'],
                    );
                  }).toList(),
                ),
              );
            }
          });
    } catch (e) {
      print("home 155: $e");
      AppService.systemSteps.add(SystemStepsTypeEnum.REFRESH_APP);
      return Container();
    }
  }
}
